################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../freertos/list.c \
../freertos/queue.c \
../freertos/tasks.c 

OBJS += \
./freertos/list.o \
./freertos/queue.o \
./freertos/tasks.o 

C_DEPS += \
./freertos/list.d \
./freertos/queue.d \
./freertos/tasks.d 


# Each subdirectory must supply rules for building sources it contributes
freertos/%.o: ../freertos/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -D__REDLIB__ -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/freertos/include" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/freertos/portable/gcc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/emwin/GUI" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/emwin/Inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/emwin/Config" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/emwin/System/HW" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/app/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/board/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/mcu/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/fatfs" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/gui_emb/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/panel_touch_FT5216/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/can/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/embnet/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/utilities/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


