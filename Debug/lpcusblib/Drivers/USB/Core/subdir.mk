################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpcusblib/Drivers/USB/Core/ConfigDescriptor.c \
../lpcusblib/Drivers/USB/Core/Host.c \
../lpcusblib/Drivers/USB/Core/HostStandardReq.c \
../lpcusblib/Drivers/USB/Core/Pipe.c \
../lpcusblib/Drivers/USB/Core/PipeStream.c \
../lpcusblib/Drivers/USB/Core/USBController.c \
../lpcusblib/Drivers/USB/Core/USBMemory.c \
../lpcusblib/Drivers/USB/Core/USBTask.c 

OBJS += \
./lpcusblib/Drivers/USB/Core/ConfigDescriptor.o \
./lpcusblib/Drivers/USB/Core/Host.o \
./lpcusblib/Drivers/USB/Core/HostStandardReq.o \
./lpcusblib/Drivers/USB/Core/Pipe.o \
./lpcusblib/Drivers/USB/Core/PipeStream.o \
./lpcusblib/Drivers/USB/Core/USBController.o \
./lpcusblib/Drivers/USB/Core/USBMemory.o \
./lpcusblib/Drivers/USB/Core/USBTask.o 

C_DEPS += \
./lpcusblib/Drivers/USB/Core/ConfigDescriptor.d \
./lpcusblib/Drivers/USB/Core/Host.d \
./lpcusblib/Drivers/USB/Core/HostStandardReq.d \
./lpcusblib/Drivers/USB/Core/Pipe.d \
./lpcusblib/Drivers/USB/Core/PipeStream.d \
./lpcusblib/Drivers/USB/Core/USBController.d \
./lpcusblib/Drivers/USB/Core/USBMemory.d \
./lpcusblib/Drivers/USB/Core/USBTask.d 


# Each subdirectory must supply rules for building sources it contributes
lpcusblib/Drivers/USB/Core/%.o: ../lpcusblib/Drivers/USB/Core/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -DUSE_USB -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\GUI" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\Inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\Config" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\System\HW" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\app\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\board\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\mcu\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\panel_touch_FT5216\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\can\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\embnet\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\utilities\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\freertos\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\mcp9803\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lpcusblib\Drivers\USB" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\fatfs\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc\arch" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc\lwip" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc\ipv4" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\JSON\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


