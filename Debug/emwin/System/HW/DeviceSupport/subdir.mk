################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../emwin/System/HW/DeviceSupport/startup_LPC177x_8x_IAR.s \
../emwin/System/HW/DeviceSupport/startup_LPC177x_8x_Keil.s 

C_SRCS += \
../emwin/System/HW/DeviceSupport/core_cm3.c \
../emwin/System/HW/DeviceSupport/system_LPC177x_8x.c 

OBJS += \
./emwin/System/HW/DeviceSupport/core_cm3.o \
./emwin/System/HW/DeviceSupport/startup_LPC177x_8x_IAR.o \
./emwin/System/HW/DeviceSupport/startup_LPC177x_8x_Keil.o \
./emwin/System/HW/DeviceSupport/system_LPC177x_8x.o 

C_DEPS += \
./emwin/System/HW/DeviceSupport/core_cm3.d \
./emwin/System/HW/DeviceSupport/system_LPC177x_8x.d 


# Each subdirectory must supply rules for building sources it contributes
emwin/System/HW/DeviceSupport/%.o: ../emwin/System/HW/DeviceSupport/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__REDLIB__ -D__USE_CMSIS -D__CODE_RED -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\cmsis\inc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\freertos\include" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\freertos\portable\gcc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\GUI" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\Inc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\Config" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\System\HW" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\app\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

emwin/System/HW/DeviceSupport/%.o: ../emwin/System/HW/DeviceSupport/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: MCU Assembler'
	arm-none-eabi-gcc -c -x assembler-with-cpp -D__REDLIB__ -DDEBUG -D__CODE_RED -g3 -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


