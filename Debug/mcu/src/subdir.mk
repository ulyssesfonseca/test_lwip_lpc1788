################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../mcu/src/adc_17xx_40xx.c \
../mcu/src/can_17xx_40xx.c \
../mcu/src/chip_17xx_40xx.c \
../mcu/src/clock_17xx_40xx.c \
../mcu/src/cmp_17xx_40xx.c \
../mcu/src/crc_17xx_40xx.c \
../mcu/src/dac_17xx_40xx.c \
../mcu/src/eeprom_17xx_40xx.c \
../mcu/src/emc_17xx_40xx.c \
../mcu/src/enet_17xx_40xx.c \
../mcu/src/gpdma_17xx_40xx.c \
../mcu/src/gpio_17xx_40xx.c \
../mcu/src/gpioint_17xx_40xx.c \
../mcu/src/i2c_17xx_40xx.c \
../mcu/src/i2s_17xx_40xx.c \
../mcu/src/iap.c \
../mcu/src/iocon_17xx_40xx.c \
../mcu/src/lcd_17xx_40xx.c \
../mcu/src/pmu_17xx_40xx.c \
../mcu/src/ring_buffer.c \
../mcu/src/ritimer_17xx_40xx.c \
../mcu/src/rtc_17xx_40xx.c \
../mcu/src/sdc_17xx_40xx.c \
../mcu/src/sdmmc_17xx_40xx.c \
../mcu/src/spi_17xx_40xx.c \
../mcu/src/ssp_17xx_40xx.c \
../mcu/src/stopwatch_17xx_40xx.c \
../mcu/src/sysctl_17xx_40xx.c \
../mcu/src/sysinit_17xx_40xx.c \
../mcu/src/timer_17xx_40xx.c \
../mcu/src/uart_17xx_40xx.c \
../mcu/src/wwdt_17xx_40xx.c 

OBJS += \
./mcu/src/adc_17xx_40xx.o \
./mcu/src/can_17xx_40xx.o \
./mcu/src/chip_17xx_40xx.o \
./mcu/src/clock_17xx_40xx.o \
./mcu/src/cmp_17xx_40xx.o \
./mcu/src/crc_17xx_40xx.o \
./mcu/src/dac_17xx_40xx.o \
./mcu/src/eeprom_17xx_40xx.o \
./mcu/src/emc_17xx_40xx.o \
./mcu/src/enet_17xx_40xx.o \
./mcu/src/gpdma_17xx_40xx.o \
./mcu/src/gpio_17xx_40xx.o \
./mcu/src/gpioint_17xx_40xx.o \
./mcu/src/i2c_17xx_40xx.o \
./mcu/src/i2s_17xx_40xx.o \
./mcu/src/iap.o \
./mcu/src/iocon_17xx_40xx.o \
./mcu/src/lcd_17xx_40xx.o \
./mcu/src/pmu_17xx_40xx.o \
./mcu/src/ring_buffer.o \
./mcu/src/ritimer_17xx_40xx.o \
./mcu/src/rtc_17xx_40xx.o \
./mcu/src/sdc_17xx_40xx.o \
./mcu/src/sdmmc_17xx_40xx.o \
./mcu/src/spi_17xx_40xx.o \
./mcu/src/ssp_17xx_40xx.o \
./mcu/src/stopwatch_17xx_40xx.o \
./mcu/src/sysctl_17xx_40xx.o \
./mcu/src/sysinit_17xx_40xx.o \
./mcu/src/timer_17xx_40xx.o \
./mcu/src/uart_17xx_40xx.o \
./mcu/src/wwdt_17xx_40xx.o 

C_DEPS += \
./mcu/src/adc_17xx_40xx.d \
./mcu/src/can_17xx_40xx.d \
./mcu/src/chip_17xx_40xx.d \
./mcu/src/clock_17xx_40xx.d \
./mcu/src/cmp_17xx_40xx.d \
./mcu/src/crc_17xx_40xx.d \
./mcu/src/dac_17xx_40xx.d \
./mcu/src/eeprom_17xx_40xx.d \
./mcu/src/emc_17xx_40xx.d \
./mcu/src/enet_17xx_40xx.d \
./mcu/src/gpdma_17xx_40xx.d \
./mcu/src/gpio_17xx_40xx.d \
./mcu/src/gpioint_17xx_40xx.d \
./mcu/src/i2c_17xx_40xx.d \
./mcu/src/i2s_17xx_40xx.d \
./mcu/src/iap.d \
./mcu/src/iocon_17xx_40xx.d \
./mcu/src/lcd_17xx_40xx.d \
./mcu/src/pmu_17xx_40xx.d \
./mcu/src/ring_buffer.d \
./mcu/src/ritimer_17xx_40xx.d \
./mcu/src/rtc_17xx_40xx.d \
./mcu/src/sdc_17xx_40xx.d \
./mcu/src/sdmmc_17xx_40xx.d \
./mcu/src/spi_17xx_40xx.d \
./mcu/src/ssp_17xx_40xx.d \
./mcu/src/stopwatch_17xx_40xx.d \
./mcu/src/sysctl_17xx_40xx.d \
./mcu/src/sysinit_17xx_40xx.d \
./mcu/src/timer_17xx_40xx.d \
./mcu/src/uart_17xx_40xx.d \
./mcu/src/wwdt_17xx_40xx.d 


# Each subdirectory must supply rules for building sources it contributes
mcu/src/%.o: ../mcu/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -DUSE_USB -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\GUI" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\Inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\Config" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\System\HW" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\app\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\board\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\mcu\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\panel_touch_FT5216\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\can\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\embnet\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\utilities\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\freertos\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\mcp9803\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lpcusblib\Drivers\USB" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\fatfs\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc\arch" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc\lwip" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc\ipv4" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\JSON\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


