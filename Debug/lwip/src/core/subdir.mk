################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip/src/core/def.c \
../lwip/src/core/dhcp.c \
../lwip/src/core/dns.c \
../lwip/src/core/init.c \
../lwip/src/core/mem.c \
../lwip/src/core/memp.c \
../lwip/src/core/netif.c \
../lwip/src/core/pbuf.c \
../lwip/src/core/raw.c \
../lwip/src/core/stats.c \
../lwip/src/core/sys.c \
../lwip/src/core/tcp.c \
../lwip/src/core/tcp_in.c \
../lwip/src/core/tcp_out.c \
../lwip/src/core/timers.c \
../lwip/src/core/udp.c 

OBJS += \
./lwip/src/core/def.o \
./lwip/src/core/dhcp.o \
./lwip/src/core/dns.o \
./lwip/src/core/init.o \
./lwip/src/core/mem.o \
./lwip/src/core/memp.o \
./lwip/src/core/netif.o \
./lwip/src/core/pbuf.o \
./lwip/src/core/raw.o \
./lwip/src/core/stats.o \
./lwip/src/core/sys.o \
./lwip/src/core/tcp.o \
./lwip/src/core/tcp_in.o \
./lwip/src/core/tcp_out.o \
./lwip/src/core/timers.o \
./lwip/src/core/udp.o 

C_DEPS += \
./lwip/src/core/def.d \
./lwip/src/core/dhcp.d \
./lwip/src/core/dns.d \
./lwip/src/core/init.d \
./lwip/src/core/mem.d \
./lwip/src/core/memp.d \
./lwip/src/core/netif.d \
./lwip/src/core/pbuf.d \
./lwip/src/core/raw.d \
./lwip/src/core/stats.d \
./lwip/src/core/sys.d \
./lwip/src/core/tcp.d \
./lwip/src/core/tcp_in.d \
./lwip/src/core/tcp_out.d \
./lwip/src/core/timers.d \
./lwip/src/core/udp.d 


# Each subdirectory must supply rules for building sources it contributes
lwip/src/core/%.o: ../lwip/src/core/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -DUSE_USB -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\GUI" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\Inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\Config" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\emwin\System\HW" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\app\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\board\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\mcu\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\panel_touch_FT5216\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\can\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\embnet\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\utilities\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\freertos\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\mcp9803\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lpcusblib\Drivers\USB" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\fatfs\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc\arch" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc\lwip" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\lwip\inc\ipv4" -I"E:\PROJETOS\CLIENTES\PRATICA\test_lwip_lpc1788\JSON\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


