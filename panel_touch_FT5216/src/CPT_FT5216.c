/***************************************************************************************
	Versao: 001 26/01/2017
	Autor: Ulysses C. Fonseca
	Empresa: Embtech Tecnologia Embarcada SA
	
	Driver de comunica��o com o controlador Panel Touch FT5216
	
	https://www.buydisplay.com/download/ic/FT5206.pdf
	https://www.mouser.com/ds/2/291/NHD-3.5-320240MF-ATXL-CTP-1-1014142.pdf
	http://www.newhavendisplay.com/specs/NHD-5.0-800480TF-ATXL-CTP.pdf
****************************************************************************************/
#include "CPT_FT5216.h"
//#include "I2C_EBT.h"
//#include "I2C.h"
#include "chip.h"
#include "board_pinout.h"

#include "board.h"


volatile CPT_PID_STATE Touch;


volatile uint16_t _LCD_RESOLUTION_WIDTH=700;
volatile uint16_t _LCD_RESOLUTION_HEIGHT=380;

volatile uint16_t _CPT_RESOLUTION_WIDTH=700;
volatile uint16_t _CPT_RESOLUTION_HEIGHT=380;

volatile uint8_t __last_Touch_Pressed=0;
volatile uint8_t __Touch_Release=0;
volatile uint8_t __Touched=0;

volatile uint8_t __values_default = 0;


extern PIN_COM_TypeDef TOUCH_RESET;
extern PIN_COM_TypeDef TOUCH_WAKE;
extern PIN_COM_TypeDef TOUCH_EINT;

#define TOUCH_EINT_READ  	Chip_GPIO_GetPinState(LPC_GPIO,TOUCH_EINT.PORT,TOUCH_EINT.PIN)?0:1
#define TOUCH_EINT_SET   	Chip_GPIO_SetPinState(LPC_GPIO,TOUCH_EINT.PORT,TOUCH_EINT.PIN, 1);
#define TOUCH_EINT_CLR   	Chip_GPIO_SetPinState(LPC_GPIO,TOUCH_EINT.PORT,TOUCH_EINT.PIN, 0);




static void __WriteCPT(uint8_t addr, uint8_t data){
	Chip_I2C_MasterSend(I2C0, addr>>1, (uint8_t*)&data, 1);
}

static void __WriteCPT_reg(uint8_t addr, uint8_t data, uint8_t value){
	char buf[2];
	buf[0] = data;
	buf[1] = value;
	Chip_I2C_MasterSend(I2C0, addr>>1, (uint8_t*)&buf, 2);
}

static void __ReadCPT(uint8_t addr, uint8_t *data, uint8_t cont){
	Chip_I2C_MasterRead(I2C0, _TC_BASE_ADDRESS>>1, data, cont);
}


static void CPT_FT5216_SetDefaultTouchRegister(void){
#define __XY_FLAG 0x00
#define __X -1
#define __Y -1
#define __TD_STATUS 0x00
#define __GESTURE_ID 0x00
	
	Touch.X1_Flag = __XY_FLAG;
	Touch.X2_Flag = __XY_FLAG;
	Touch.X3_Flag = __XY_FLAG;
	Touch.X4_Flag = __XY_FLAG;
	Touch.X5_Flag = __XY_FLAG;
	
	Touch.Y1_Flag = __XY_FLAG;
	Touch.Y2_Flag = __XY_FLAG;
	Touch.Y3_Flag = __XY_FLAG;
	Touch.Y4_Flag = __XY_FLAG;
	Touch.Y5_Flag = __XY_FLAG;
	
	Touch.X1 = __X;
	Touch.X2 = __X;
	Touch.X3 = __X;
	Touch.X4 = __X;
	Touch.X5 = __X;
	
	Touch.Y1 = __Y;
	Touch.Y2 = __Y;
	Touch.Y3 = __Y;
	Touch.Y4 = __Y;
	Touch.Y5 = __Y;
	
	Touch.TD_STATUS = __TD_STATUS;
	
	Touch.GEST_ID = __GESTURE_ID;
}

//------------------------------------------------------------------------------------
//					REGISTERS GETTERS
//------------------------------------------------------------------------------------
/*******************************************************************
	Verifica qual o modo de opera��o do chip
	000 - Normal Operating Mode
	001 - System Information Mode (Reserved)
	100 - Test Mode - read raw data (Reserved)
*******************************************************************/
void CPT_FT5216_Read_DEVICE_MODE(void){
	uint8_t __tmp;
	//I2C_Read(_TC_BASE_ADDRESS|0x01,_TC_ADDR_DEVICE_MODE,&__tmp,1);
//	ebt_i2c_start();
//	ebt_i2c_write(_TC_BASE_ADDRESS);
//	ebt_i2c_write(_TC_ADDR_DEVICE_MODE);
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	ebt_i2c_write(_TC_BASE_ADDRESS|0x01);
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_DEVICE_MODE);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	Touch.DEVICE_MODE = (__tmp&0x70)>>4;
}
/*******************************************************************
	Verifica se houve gesto no toque
	0x00 - Sem gesto
	0x10 - Move UP
	0x14 - Move Left
	0x18 - Move Down
	0x1C - Move Right
	0x48 - Zoom In
	0x49 - Zoom Out
*******************************************************************/
void CPT_FT5216_Read_GEST_ID(void){
	uint8_t __tmp[2];
	//I2C_Read(_TC_BASE_ADDRESS|0x01,_TC_ADDR_GEST_ID,&__tmp,1);
//	ebt_i2c_start();
//	ebt_i2c_write(_TC_BASE_ADDRESS);
//	ebt_i2c_write(_TC_ADDR_GEST_ID);
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	ebt_i2c_write(_TC_BASE_ADDRESS|0x01);
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_GEST_ID);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, __tmp, 1);
	Touch.GEST_ID = __tmp[0];

	DEBUGOUT("Gest %d\n",Touch.GEST_ID);
}
/*******************************************************************
	Verifica a quantidade de pontos de toques detectados.
	1-5 toques
*******************************************************************/
void CPT_FT5216_Read_TD_STATUS(void){
	uint8_t __tmp[2];
	//I2C_Read(_TC_BASE_ADDRESS|0x01,_TC_ADDR_TD_STATUS,&__tmp,1);
	
//	ebt_i2c_start();
//	ebt_i2c_write(_TC_BASE_ADDRESS);
//	ebt_i2c_write(_TC_ADDR_TD_STATUS);
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	ebt_i2c_write(_TC_BASE_ADDRESS|0x01);
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_TD_STATUS);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, __tmp, 1);
	Touch.TD_STATUS = __tmp[0]&0x0F;
}
void CPT_FT5216_Read_XY1(void){
	uint8_t __xy[4];
	//I2C_Read(_TC_BASE_ADDRESS|0x01,_TC_ADDR_TOUCH1_XH,(uint8_t *)&__xy,sizeof(__xy));
	
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_TOUCH1_XH)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__xy[0] = ebt_i2c_read(I2C_ACK);
//	__xy[1] = ebt_i2c_read(I2C_ACK);
//	__xy[2] = ebt_i2c_read(I2C_ACK);
//	__xy[3] = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_TOUCH1_XH);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, __xy, 4);
	
	Touch.X1_Flag = (__xy[0]&0xC0)>>6;
	Touch.Y1_Flag = (__xy[2]&0xC0)>>6;
	Touch.X1 = (((__xy[0]&0x0F)<<8|__xy[1])*_LCD_RESOLUTION_WIDTH)/(_CPT_RESOLUTION_WIDTH);
	Touch.Y1 = (((__xy[2]&0x0F)<<8|__xy[3])*_LCD_RESOLUTION_HEIGHT)/(_CPT_RESOLUTION_HEIGHT);
}
void CPT_FT5216_Read_XY2(void){
	uint8_t __xy[4];
	//I2C_Read(_TC_BASE_ADDRESS|0x01,_TC_ADDR_TOUCH2_XH,(uint8_t *)&__xy,sizeof(__xy));
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_TOUCH2_XH)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__xy[0] = ebt_i2c_read(I2C_ACK);
//	__xy[1] = ebt_i2c_read(I2C_ACK);
//	__xy[2] = ebt_i2c_read(I2C_ACK);
//	__xy[3] = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_TOUCH2_XH);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, __xy, 4);
	
	Touch.X2_Flag = (__xy[0]&0xC0)>>6;
	Touch.Y2_Flag = (__xy[2]&0xC0)>>6;
	Touch.X2 = (((__xy[0]&0x0F)<<8|__xy[1])*_LCD_RESOLUTION_WIDTH)/(_CPT_RESOLUTION_WIDTH);
	Touch.Y2 = (((__xy[2]&0x0F)<<8|__xy[3])*_LCD_RESOLUTION_HEIGHT)/(_CPT_RESOLUTION_HEIGHT);
}
void CPT_FT5216_Read_XY3(void){
	uint8_t __xy[4];
	//I2C_Read(_TC_BASE_ADDRESS|0x01,_TC_ADDR_TOUCH3_XH,(uint8_t *)&__xy,sizeof(__xy));
//		ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_TOUCH3_XH)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__xy[0] = ebt_i2c_read(I2C_ACK);
//	__xy[1] = ebt_i2c_read(I2C_ACK);
//	__xy[2] = ebt_i2c_read(I2C_ACK);
//	__xy[3] = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_TOUCH3_XH);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, __xy, 4);

	Touch.X3_Flag = (__xy[0]&0xC0)>>6;
	Touch.Y3_Flag = (__xy[2]&0xC0)>>6;
	Touch.X3 = (((__xy[0]&0x0F)<<8|__xy[1])*_LCD_RESOLUTION_WIDTH)/(_CPT_RESOLUTION_WIDTH);
	Touch.Y3 = (((__xy[2]&0x0F)<<8|__xy[3])*_LCD_RESOLUTION_HEIGHT)/(_CPT_RESOLUTION_HEIGHT);
}
void CPT_FT5216_Read_XY4(void){
	uint8_t __xy[4];
	//I2C_Read(_TC_BASE_ADDRESS|0x01,_TC_ADDR_TOUCH4_XH,(uint8_t *)&__xy,sizeof(__xy));
//		ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_TOUCH4_XH)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__xy[0] = ebt_i2c_read(I2C_ACK);
//	__xy[1] = ebt_i2c_read(I2C_ACK);
//	__xy[2] = ebt_i2c_read(I2C_ACK);
//	__xy[3] = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_TOUCH4_XH);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, __xy, 4);
	
	Touch.X4_Flag = (__xy[0]&0xC0)>>6;
	Touch.Y4_Flag = (__xy[2]&0xC0)>>6;
	Touch.X4 = (((__xy[0]&0x0F)<<8|__xy[1])*_LCD_RESOLUTION_WIDTH)/(_CPT_RESOLUTION_WIDTH);
	Touch.Y4 = (((__xy[2]&0x0F)<<8|__xy[3])*_LCD_RESOLUTION_HEIGHT)/(_CPT_RESOLUTION_HEIGHT);
}
void CPT_FT5216_Read_XY5(void){
	uint8_t __xy[4];
	//I2C_Read(_TC_BASE_ADDRESS|0x01,_TC_ADDR_TOUCH5_XH,(uint8_t *)&__xy,sizeof(__xy));
//		ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_TOUCH5_XH)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__xy[0] = ebt_i2c_read(I2C_ACK);
//	__xy[1] = ebt_i2c_read(I2C_ACK);
//	__xy[2] = ebt_i2c_read(I2C_ACK);
//	__xy[3] = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_TOUCH5_XH);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, __xy, 4);
	
	Touch.X5_Flag = (__xy[0]&0xC0)>>6;
	Touch.Y5_Flag = (__xy[2]&0xC0)>>6;
	Touch.X5 = (((__xy[0]&0x0F)<<8|__xy[1])*_LCD_RESOLUTION_WIDTH)/(_CPT_RESOLUTION_WIDTH);
	Touch.Y5 = (((__xy[2]&0x0F)<<8|__xy[3])*_LCD_RESOLUTION_HEIGHT)/(_CPT_RESOLUTION_HEIGHT);
}


void CPT_FT5216_Read_ID_G_THGROUP(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_THGROUP)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THGROUP);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_THGROUP = __tmp;
}
void CPT_FT5216_Read_ID_G_THPEAK(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_THPEAK)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THPEAK);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_THPEAK = __tmp;
}
void CPT_FT5216_Read_ID_G_THCAL(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_THCAL)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THCAL);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_THCAL = __tmp;
}
void CPT_FT5216_Read_ID_G_THWATER(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_THWATER)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THWATER);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_THWATER = __tmp;
}
void CPT_FT5216_Read_ID_G_THDIFF(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_THDIFF)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();

	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THDIFF);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_THDIFF = __tmp;
}
void CPT_FT5216_Read_ID_G_CTRL(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_CTRL)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_CTRL);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_CTRL = __tmp;
}
void CPT_FT5216_Read_ID_G_TIME_ENTER_MONITOR(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_TIME_ENTER_MONITOR)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_TIME_ENTER_MONITOR);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_TIME_ENTER_MONITOR = __tmp;
}
void CPT_FT5216_Read_ID_G_PERIOD_ACTIVE(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_PERIOD_ACTIVE)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_PERIOD_ACTIVE);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_PERIOD_ACTIVE = __tmp;
}
void CPT_FT5216_Read_ID_G_PERIOD_MONITOR(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_PERIOD_MONITOR)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_PERIOD_MONITOR);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_PERIOD_MONITOR = __tmp;
}
void CPT_FT5216_Read_ID_G_AUTO_CLB_MODE(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_AUTO_CLB_MODE)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_AUTO_CLB_MODE);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_AUTO_CLB_MODE = __tmp;
}

void CPT_FT5216_Read_ID_G_LIB_VERSION(void){
	uint8_t __tmp[2];
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_LIB_VERSION_H)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp[0] = ebt_i2c_read(I2C_ACK);
//	__tmp[1] = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_LIB_VERSION_H);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, __tmp, 2);
	
	Touch.ID_G_LIB_VERSION = (__tmp[0]<<8)|__tmp[1];
}
void CPT_FT5216_Read_ID_G_VENDOR_ID(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_CIPHER)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_CIPHER);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.VENDOR_ID = __tmp;
}
void CPT_FT5216_Read_ID_G_MODE(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_MODE)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_MODE);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_MODE = __tmp;
}
void CPT_FT5216_Read_ID_G_PMODE(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_PMODE)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_PMODE);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_PMODE = __tmp;
}
void CPT_FT5216_Read_ID_G_FIRMID(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_FIRMID)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_FIRMID);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_FIRMID = __tmp;
}
void CPT_FT5216_Read_ID_G_STATE(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_STATE)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_STATE);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_STATE = __tmp;
}
void CPT_FT5216_Read_ID_G_FT5201ID(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_FT5201ID)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_FT5201ID);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_FT5201ID = __tmp;
}
void CPT_FT5216_Read_ID_G_ERR(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_ERR)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_ERR);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_ERR = __tmp;
}
void CPT_FT5216_Read_ID_G_CLB(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_CLB)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_CLB);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_CLB = __tmp;
}
void CPT_FT5216_Read_ID_G_B_AREA_TH(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_B_AREA_TH)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_B_AREA_TH);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_B_AREA_TH = __tmp;
}



void CPT_FT5216_ReadIDGPeriodMonitor(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_PERIOD_MONITOR)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_PERIOD_MONITOR);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_PERIOD_MONITOR = __tmp;
}
void CPT_FT5216_ReadIDGCLB(void){
	uint8_t __tmp;
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS)!=I2C_ACK){return;}
//	if(ebt_i2c_write(_TC_ADDR_ID_G_CLB)!=I2C_ACK){return;}
//	ebt_i2c_stop();
//
//	ebt_i2c_start();
//	if(ebt_i2c_write(_TC_BASE_ADDRESS|0x01)!=I2C_ACK){return;}
//	__tmp = ebt_i2c_read(I2C_ACK);
//	ebt_i2c_read(I2C_NAK);
//	ebt_i2c_stop();
	
	__WriteCPT(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_CLB);
	__ReadCPT(_TC_BASE_ADDRESS|0x01, &__tmp, 1);
	
	Touch.ID_G_CLB = __tmp;
}

//------------------------------------------------------------------------------------
//					REGISTER SETTERS
//------------------------------------------------------------------------------------
/*
*/
void CPT_FT5216_Set_DeviceMode(uint8_t value){
	uint8_t __default = 000;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_DEVICE_MODE,value);
}
void CPT_FT5216_Set_ID_G_THGROUP(uint8_t value){
	uint8_t __default = 280/4;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THGROUP,value);
}
void CPT_FT5216_Set_ID_G_THPEAK(uint8_t value){
	uint8_t __default = 60;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THPEAK,value);
}
void CPT_FT5216_Set_ID_G_THCAL(uint8_t value){
	uint8_t __default = 16;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THCAL,value);
}
void CPT_FT5216_Set_ID_G_THWATER(uint8_t value){
	uint8_t __default = 60;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THWATER,value);
}
void CPT_FT5216_Set_ID_G_THTEMP(uint8_t value){
	uint8_t __default = 10;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THTEMP,value);
}
void CPT_FT5216_Set_ID_G_THDIFF(uint8_t value){
	uint8_t __default = 20;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_THDIFF,value);
}
void CPT_FT5216_Set_ID_G_CTRL(uint8_t value){
	uint8_t __default = 0;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_CTRL,value);
}
void CPT_FT5216_Set_ID_G_TIME_ENTER_MONITOR(uint8_t value){
	uint8_t __default = 2;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_TIME_ENTER_MONITOR,value);
}
void CPT_FT5216_Set_ID_G_PERIOD_ACTIVE(uint8_t value){
	uint8_t __default = 12;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_PERIOD_ACTIVE,value);
}
void CPT_FT5216_Set_ID_G_PERIOD_MONITOR(uint8_t value){
	uint8_t __default = 40;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_PERIOD_MONITOR,value);
}
void CPT_FT5216_Set_ID_G_AUTO_CLB_MODE(uint8_t value){
	uint8_t __default = 00;
	if(__values_default){value = __default;}
	__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_AUTO_CLB_MODE,value);
}
void CPT_FT5216_Set_ID_G_CLB(uint8_t value){
	if(!__values_default){
		__WriteCPT_reg(_TC_BASE_ADDRESS,_TC_ADDR_ID_G_CLB,value);
	}
}




//------------------------------------------------------------------------------------
//					SUPPORT GETTERS
//------------------------------------------------------------------------------------
void CPT_FT5216_ReadTouch(void){


	if(TOUCH_EINT_READ){
//		DEBUGOUT("TOUCH_EINT_READ \n");
//		Touch.Pressed=1;
		__Touch_Release=0;
		CPT_FT5216_Read_GEST_ID();
		CPT_FT5216_Read_TD_STATUS();
	
		if(Touch.TD_STATUS>=1){
			Touch.Pressed=1;
			CPT_FT5216_Read_XY1();
		}
		if(Touch.TD_STATUS>=2){
			CPT_FT5216_Read_XY2();
		}
		if(Touch.TD_STATUS>=3){
			CPT_FT5216_Read_XY3();
		}
		if(Touch.TD_STATUS>=4){
			CPT_FT5216_Read_XY4();
		}
		if(Touch.TD_STATUS>=5){
			CPT_FT5216_Read_XY5();
		}
	}
	else{
//		__Touch_Release++;
//		if(__Touch_Release>3){
			Touch.Pressed=0;
			CPT_FT5216_SetDefaultTouchRegister();
//		}
	}
	
}
uint16_t CPT_FT526_GetCalibY(void){
	return _LCD_RESOLUTION_HEIGHT;
}
uint16_t CPT_FT526_GetCalibX(void){
	return _LCD_RESOLUTION_WIDTH;
}
//------------------------------------------------------------------------------------
//					SUPPORT SETTERS
//------------------------------------------------------------------------------------
/*******************************************************************
	Habilita modo de gravar valores padr�es das fun��es de Set
	@param 
		uint8_t value -
							1 - setDefault
							0 - noDefault
*******************************************************************/
void CPT_FT5216_Config_Defaults(uint8_t value){
	__values_default = value;	
}
void CPT_FT5216_SetCalib(uint16_t x, uint16_t y){
	_CPT_RESOLUTION_WIDTH 	= x;
	_CPT_RESOLUTION_HEIGHT	= y;
}
void CPT_FT5216_SetPointCalib(uint16_t x, uint16_t y){
	_LCD_RESOLUTION_WIDTH 	= x;
	_LCD_RESOLUTION_HEIGHT	= y;
	
	CPT_FT5216_SetCalib(x,y);
}
//------------------------------------------------------------------------------------
//					INIT FUNCTIONS
//------------------------------------------------------------------------------------
void CPT_FT5216_Init(void){
	
	Chip_IOCON_PinMuxSet(LPC_IOCON,TOUCH_RESET.PORT,TOUCH_RESET.PIN,TOUCH_RESET.FUNC);
	Chip_IOCON_PinMuxSet(LPC_IOCON,TOUCH_WAKE.PORT,TOUCH_WAKE.PIN,TOUCH_WAKE.FUNC);
	Chip_IOCON_PinMuxSet(LPC_IOCON,TOUCH_EINT.PORT,TOUCH_EINT.PIN,TOUCH_EINT.FUNC);

	Chip_GPIO_SetPinDIRInput(LPC_GPIO, TOUCH_EINT.PORT,TOUCH_EINT.PIN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, TOUCH_RESET.PORT,TOUCH_RESET.PIN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, TOUCH_WAKE.PORT,TOUCH_WAKE.PIN);

	Chip_GPIO_SetPinState(LPC_GPIO, TOUCH_RESET.PORT,TOUCH_RESET.PIN, 1);
	Chip_GPIO_SetPinState(LPC_GPIO, TOUCH_WAKE.PORT,TOUCH_WAKE.PIN, 0);
//	TOUCH_RESET_IOCON = 0x00; TOUCH_RESET_PORT->DIR |= (1<<TOUCH_RESET_BIT); TOUCH_RESET_SET;
//	TOUCH_WAKE_IOCON = 0x80|0x10;  TOUCH_WAKE_PORT->DIR |= (1<<TOUCH_WAKE_BIT); TOUCH_WAKE_CLR;
//	TOUCH_EINT_IOCON = 0x80|0x10;  TOUCH_EINT_PORT->DIR &= ~(1<<TOUCH_EINT_BIT);
	
	
	DEBUGOUT("CPT_FT5216_Init\n");

	CPT_FT5216_SetDefaultTouchRegister();

	__WriteCPT_reg(_TC_BASE_ADDRESS, _TC_ADDR_ID_G_MODE, 0x01);

	CPT_FT5216_Read_DEVICE_MODE();
//	ebt_i2c_init();


}
