/*
	INSTRU��ES DE USO DA BIBLIOTECA DE BOT�O
	
	1 - inserir a fun��o '_check_botao_press();' na task touch
	2 - inserir a fun��o '_clean_botao();' toda vez que desejar limpar os bot�es criados
	3 - fazer as defini��es dos bot�es que quiser criar, utilizando a struct 'botao'
	4 - logo em seguida chamar a fun��o '_check_update();' para que os bot�es sejam desenhados
	5 - na fun��o de update da tela inserir a verifica��o seguinte, para atualiza��o do bot�o:
		if(UpdateBotao){_check_update();}

*/

#include <stdio.h> 
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include "GUI.h"
#include "WM.h"

#define GUI_EMB_VERSION 	 1.0.0

#define TOTAL_BOTOES 	 40

#define CHANGE					0
#define NO_CHANGE				1

//Alinhamento do Texto
#define LEFT          	0
#define RIGHT         	1
#define CENTER        	2
#define TOP			      	3
#define BOTTOM	      	4

//Esquema de Cores do bot�o: Normal ou Reverso
#define NORMAL        	0
#define REV           	1

//Estilos do bot�o
#define IMAGEM        	0
#define IMAGEM_TEXT			1
#define BUTTON        	2
#define ICO_TEXT				3
#define AREA_TOUCH			4

//Define de utiliza��o de mem�ria RAM para armazenamento de imgagens
#define USE_RAM        	1
#define USE_FATFS_CHAN  1


#define countof(Array) (sizeof(Array) / sizeof(Array[0]))

extern GUI_PID_STATE touch;

volatile uint16_t time_opcao=0;
volatile uint16_t erros_sd=0;
volatile uint16_t last_erro_sd=0;


volatile uint8_t ScrLoad;
volatile uint8_t UpdateScr=0;
volatile uint8_t UpdateBotao=0;

volatile uint8_t _bt_ExibeHome=0;
volatile uint8_t _bt_VOLTAR=0;
volatile uint8_t _f_modo_prog=0;
volatile uint8_t _bt_VOLTAR_prog=0;
volatile uint8_t STATUS=0;
volatile uint8_t Next_STATUS;
volatile uint8_t NextNext_STATUS;
volatile uint8_t lock_touch_num=30;
volatile uint8_t lock_botao_press=0;
volatile uint8_t touch_pressed=0;
volatile uint8_t tmp_bot=0;
volatile uint8_t lock_touch=0;
volatile uint8_t lock_beep=0;
volatile uint8_t option=0;
volatile uint8_t ID_IDIOMA=0;
volatile uint8_t bot=0;
volatile char tmpNomeRecAtual[30];


volatile uint8_t tmp_var_8;
volatile uint8_t tmp_var_8_2;
volatile uint16_t tmp_var_16;
volatile uint32_t tmp_var_32;

#if USE_RAM
	volatile uint8_t _tmp_ram[0x3E800]  __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram0[0x3E800] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram1[0x3E800] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram2[0x3E800] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram3[0x3E800] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram4[0x3E800] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram5[0x3E800] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram6[0x3E800] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram7[0x3E800] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram8[0x3E800] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram9[0x3E800] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram10[0xC350] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram11[0xC350] __attribute__ ((section (".TEMP_APP"))); //RAM temp
	volatile uint8_t _tmp_ram12[0xC350] __attribute__ ((section (".TEMP_APP"))); //RAM temp

	volatile uint8_t _ram_draw=0;
#endif


enum{
	RELEASE,
	PRESSED_DOWN,
	PRESSED_UP,
	TOGGLE,
	LAST,
	NEXT,
};
typedef struct{
	char title[30];
	char title2[30];

															//DEFINI��ES PARA STATUS DE PRESSIONADO E SOLTO PARA IMAGENS
	uint8_t image_norm[6][23];
	uint8_t image_press[6][23];

															//PONTEIROS PARA ITERA��O COM VARIAVEIS DA APLICA��O
	uint8_t * var_8;						
	uint8_t set_var_8;
	uint8_t * var_8_2;
	uint8_t set_var_8_2;

	uint16_t * var_16;
	uint16_t set_var_16;

	uint32_t * var_32;
	uint32_t set_var_32;

	uint8_t style;
	uint8_t alignH;
	uint8_t alignV;
	uint8_t mode;								//PINTA O BOT�O COM A COR DEFINIDA OU INVERTE (NORMAL,REV)
	uint8_t round;							//RAIO DO BOTAO

	uint8_t selec;							//MARCA QUAL BOT�O EST� PRESSIONADO
	uint8_t * option;						

	uint8_t toggle;
	uint8_t state;							//DEFINE QUAL O ESTADO ATUAL DO BOTAO
	uint8_t last_state;					//DEFINE QUAL O ESTADO ANTERIOR DO BOTAO
	uint8_t state_change;				//DEFINE SE HOUVE ALTERA��O NO ESTADO DO BOT�O
	uint8_t change_window;
	
	uint8_t * pagina;
	uint8_t paginacao;
	
	uint8_t disable;
	uint16_t timecont;					//TEMPO EM SEG. PARA HABILITAR A SEGUNDA OPCAO

	int x;											//POSI��O X0 DO BOTAO
	int dx;											//DELTA DO X0 DO BOTAO
	int y;											//POSI��O Y0 DO BOTAO
	int height;									//ALTURA DO BOT�O
	int width;									//LARGURA DO BOT�O
	
	uint16_t Next_Status;				//TELA QUE DEVER� IR PARA QUANDO O BOT�O FOR PRESSIONADO
	uint16_t Second_Status;			//SEGUNDA OP��O DE TELA QUE � DEFINIDA PELO timecount
	
															//ESTILOS DE CORES DE FUNDO, FONTE, CONTORNO
	uint32_t color_norm;
	uint32_t color_font_norm;
	uint32_t color_cont_norm;
	
	uint32_t color_press;
	uint32_t color_font_press;
	uint32_t color_cont_press;
	
	const GUI_FONT GUI_UNI_PTR * pNovaFont;		//FONTE UTILIZADA NO TEXT
	const GUI_FONT GUI_UNI_PTR * pNovaFont2;	//FONTE UTILIZADA NO TEXT2
	
	void (*function)();						//FUN��O QUE SER� CHAMADA QUANDO O BOT�O FOR CLICADO
} Botao;

volatile Botao botao[TOTAL_BOTOES];

void _null(void){}
	
//---------------------------------------------------------------------------
// _DrawButton
//---------------------------------------------------------------------------
void _DrawButton(int x, int y, int w, int h, char * Titulo,U32 color, U32 color_font, U32 color_cont, const GUI_FONT GUI_UNI_PTR * pNovaFont, U8 AlignH, U8 AlignV, U8 Mode, U8 Raio) {
	
  GUI_RECT Area; 
	unsigned char Align=0;
	
	if (Mode==NORMAL) {GUI_SetColor(color);} else {GUI_SetColor(color_font);}

	GUI_FillRoundedRect(x,y,x+w,y+h,Raio);		
	GUI_SetColor(color_cont);
	GUI_DrawRoundedRect(x,y,x+w,y+h,Raio);	

	Area.x0 = x;
	Area.x1 = x + w;
	Area.y0 = y;
	Area.y1 = y + h;
	
	GUI_SetFont(pNovaFont);
		

	if (Mode==NORMAL) {GUI_SetColor(color_font);} else {GUI_SetColor(color);}
	GUI_SetTextMode(GUI_TM_TRANS);
	
	if (AlignH==CENTER){
		Align|=GUI_TA_HCENTER;
	} else if (AlignH==LEFT){
		Align|=GUI_TA_LEFT;
		Area.x0 = Area.x0+5;
		Area.x1 = Area.x1-10;
	} else if (AlignH==RIGHT){
		Align|=GUI_TA_RIGHT;	
		Area.x0 = Area.x0+10;
		Area.x1 = Area.x1-5;
	}
	
	if (AlignV==CENTER){
		Align|=GUI_TA_VCENTER;
	} else if (AlignV==TOP){
		Align|=GUI_TA_TOP;
		Area.y0 = Area.y0+2;
		Area.y1 = Area.y1-4;
	} else if (AlignV==BOTTOM){
		Align|=GUI_TA_BOTTOM;
		Area.y0 = Area.y0+4;
		Area.y1 = Area.y1-2;		
	}
	GUI_DispStringInRectWrap(Titulo,&Area,Align,GUI_WRAPMODE_WORD);	
}	

/********************************************************* UTILIZAR APENAS **************************************************************/
//---------------------------------------------------------------------------------
// MensagemDebug
//----------------------------------------------------------------------------------
void MensagemDebug(int posx1, int posy1, int posx2, int posy2, char* texto) {

	#if USE_RAM
		GUI_MEMDEV_Handle hMem;	
		
		hMem = GUI_MEMDEV_Create(posx1,posy1,posx2,posy2);
		GUI_MEMDEV_Select(hMem);	
	#endif
	
	GUI_SetColor(GUI_BLACK);
	GUI_FillRect(posx1,posy1,posx2,posy2);
	
	GUI_SetColor(GUI_WHITE);
	GUI_SetTextMode(GUI_TM_TRANS);
	GUI_SetFont(&GUI_Font10_1); 
	GUI_DispStringAt(texto,posx1+10,posy1);

	#if USE_RAM
		GUI_MEMDEV_Select(0);
		GUI_MEMDEV_CopyToLCD(hMem);
		GUI_MEMDEV_Delete(hMem);	
	#endif
}

//---------------------------------------------------------------------------
// _DrawButtonTransparente
//---------------------------------------------------------------------------
void _DrawButtonTransparente(int x, int y, int w, int h, const char * Titulo, U32 color_font, const GUI_FONT GUI_UNI_PTR * pNovaFont,U8 AlignH, U8 AlignV) {
	
	unsigned char Align=0;
  GUI_RECT Area; 
	
	Area.x0 = x;
	Area.x1 = x + w;
	Area.y0 = y;
	Area.y1 = y + h;
	
	GUI_SetFont(pNovaFont);	

	GUI_SetColor(color_font);
	GUI_SetTextMode(GUI_TM_TRANS);
	
	if (AlignH==CENTER){
		Align|=GUI_TA_HCENTER;
	} else if (AlignH==LEFT){
		Align|=GUI_TA_LEFT;
	} else if (AlignH==RIGHT){
		Align|=GUI_TA_RIGHT;	
	}
	
	if (AlignV==CENTER){
		Align|=GUI_TA_VCENTER;
	} else if (AlignV==TOP){
		Align|=GUI_TA_TOP;
	} else if (AlignV==BOTTOM){
		Align|=GUI_TA_BOTTOM;	
	}
	GUI_DispStringInRectWrap(Titulo,&Area,Align,GUI_WRAPMODE_WORD);	
}


void _DrawText(int pX, int pY, char *pText, U32 pColor, const GUI_FONT *pFont){

	GUI_SetFont(pFont);
	GUI_SetColor(pColor);
	GUI_SetTextMode(GUI_TM_TRANS); // GUI_TEXTMODE_REV
	GUI_DispStringAt(pText, pX, pY);

}


void _DrawButtonTransparenteContorno(int x, int y, int w, int h, int r, int e, U32 border_color){

	GUI_SetColor(border_color);
	GUI_SetPenSize(e);
	GUI_AA_DrawRoundedRect(x,y,w + x,h + y,r);

}

volatile uint32_t size_last_file = 0;
int CarregaArquivoRAM(char *buf, char *filename) {
#if USE_RAM
#if USE_FATFS_CHAN
	FIL file;
	long int sf=0,cnt;

	if(!f_open(&file,filename,FA_OPEN_EXISTING | FA_READ)){
		sf = f_size(&file);
		size_last_file = sf;
		f_read(&file,(void *)buf, sf, (UINT*)&cnt);
		f_sync(&file);
		f_close(&file);
	}
	else{
		erros_sd++;
		return 1;
	}


#else
	FILE *f;
	FINFO info;
	long int sf=0;

	info.fileID = 0;

	f = NULL;
	if (ffind (filename, &info) == 0) {
		sf = info.size;
		if (f = fopen (filename,"rb")) {
			fread (buf, sf,1, f);
			fflush(f);
			fclose(f);
		}
		else{
			erros_sd++;
			return 1;
		}
	}
	else{return 1;}
#endif
#endif
	return 0;
}



/*************************************************************************************
	@autor Ulysses C. Fonseca
	@descricao
		Realiza o carregamento da imagem e a impressao, fun��o otimizada para realizar o 
		escalonamento de endere�os na mem�ria ram.
	@param
		char * filename - endere�o de mem�ria da imagem no cartao
		int x - posicao em x no display
		int y - posicao em y no display
*************************************************************************************/
void Draw_BMP(const unsigned char filename[][20], int x, int y){
#if USE_RAM
	U8 idioma2=0;
	unsigned char icon[25], icon2[25];
	int w=atoi((const char*)filename[1]);
	int h=atoi((const char*)filename[2]);
	/*memset(&_tmp_ram,0,sizeof(_tmp_ram));*/
	snprintf((char*)icon2,25,(char*)filename[0],ID_IDIOMA);
	snprintf((char*)icon,25,"SYSTEM\\%s",icon2);
	if(ID_IDIOMA!=1){idioma2=3;}
	switch(_ram_draw){//filename[idioma]
	case 0: memset((void*)&_tmp_ram,0,sizeof(_tmp_ram));   if(CarregaArquivoRAM((char *)&_tmp_ram, (char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram,x,y);} else{_DrawButton(x,y,h,w,(char *)filename[idioma2],0xCCCCCC,0x4D4D4D,0xCCCCCC,&GUI_Font16_1, CENTER,CENTER, NORMAL, 0);} break;
	case 1: memset((void*)&_tmp_ram1,0,sizeof(_tmp_ram1)); if(CarregaArquivoRAM((char *)&_tmp_ram1,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram1,x,y);}else{_DrawButton(x,y,h,w,(char *)filename[idioma2],0xCCCCCC,0x4D4D4D,0xCCCCCC,&GUI_Font16_1, CENTER,CENTER, NORMAL, 0);} break;
	case 2: memset((void*)&_tmp_ram2,0,sizeof(_tmp_ram2)); if(CarregaArquivoRAM((char *)&_tmp_ram2,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram2,x,y);}else{_DrawButton(x,y,h,w,(char *)filename[idioma2],0xCCCCCC,0x4D4D4D,0xCCCCCC,&GUI_Font16_1, CENTER,CENTER, NORMAL, 0);} break;
	case 3: memset((void*)&_tmp_ram3,0,sizeof(_tmp_ram3)); if(CarregaArquivoRAM((char *)&_tmp_ram3,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram3,x,y);}else{_DrawButton(x,y,h,w,(char *)filename[idioma2],0xCCCCCC,0x4D4D4D,0xCCCCCC,&GUI_Font16_1, CENTER,CENTER, NORMAL, 0);} break;
	case 4: memset((void*)&_tmp_ram4,0,sizeof(_tmp_ram4)); if(CarregaArquivoRAM((char *)&_tmp_ram4,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram4,x,y);}else{_DrawButton(x,y,h,w,(char *)filename[idioma2],0xCCCCCC,0x4D4D4D,0xCCCCCC,&GUI_Font16_1, CENTER,CENTER, NORMAL, 0);} break;
	case 5: memset((void*)&_tmp_ram5,0,sizeof(_tmp_ram5)); if(CarregaArquivoRAM((char *)&_tmp_ram5,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram5,x,y);}else{_DrawButton(x,y,h,w,(char *)filename[idioma2],0xCCCCCC,0x4D4D4D,0xCCCCCC,&GUI_Font16_1, CENTER,CENTER, NORMAL, 0);} break;
	case 6: memset((void*)&_tmp_ram6,0,sizeof(_tmp_ram6)); if(CarregaArquivoRAM((char *)&_tmp_ram6,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram6,x,y);}else{_DrawButton(x,y,h,w,(char *)filename[idioma2],0xCCCCCC,0x4D4D4D,0xCCCCCC,&GUI_Font16_1, CENTER,CENTER, NORMAL, 0);} break;
	case 7: memset((void*)&_tmp_ram7,0,sizeof(_tmp_ram7)); if(CarregaArquivoRAM((char *)&_tmp_ram7,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram7,x,y);}else{_DrawButton(x,y,h,w,(char *)filename[idioma2],0xCCCCCC,0x4D4D4D,0xCCCCCC,&GUI_Font16_1, CENTER,CENTER, NORMAL, 0);} break;
	case 8: memset((void*)&_tmp_ram8,0,sizeof(_tmp_ram8)); if(CarregaArquivoRAM((char *)&_tmp_ram8,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram8,x,y);}else{_DrawButton(x,y,h,w,(char *)filename[idioma2],0xCCCCCC,0x4D4D4D,0xCCCCCC,&GUI_Font16_1, CENTER,CENTER, NORMAL, 0);} break;
	case 9: memset((void*)&_tmp_ram9,0,sizeof(_tmp_ram9)); if(CarregaArquivoRAM((char *)&_tmp_ram9,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram9,x,y);}else{_DrawButton(x,y,h,w,(char *)filename[idioma2],0xCCCCCC,0x4D4D4D,0xCCCCCC,&GUI_Font16_1, CENTER,CENTER, NORMAL, 0);} break;
	}
	_ram_draw++;
	if(_ram_draw>9){_ram_draw=0;}
	GUI_SetColor(0x726EAF);
	GUI_SetColor(GUI_WHITE);
	GUI_SetColor(0x2D167E);
#endif
}
void Draw_BMP2(const unsigned char *filename, int x, int y){
#if USE_RAM
	unsigned char icon[25];
	snprintf((char*)icon,25,"%s",filename);
	switch(_ram_draw){//filename[idioma]
	case 0: memset((void*)&_tmp_ram,0,sizeof(_tmp_ram));   if(CarregaArquivoRAM((char *)&_tmp_ram, (char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram,x,y);} break;
	case 1: memset((void*)&_tmp_ram1,0,sizeof(_tmp_ram1)); if(CarregaArquivoRAM((char *)&_tmp_ram1,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram1,x,y);} break;
	case 2: memset((void*)&_tmp_ram2,0,sizeof(_tmp_ram2)); if(CarregaArquivoRAM((char *)&_tmp_ram2,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram2,x,y);} break;
	case 3: memset((void*)&_tmp_ram3,0,sizeof(_tmp_ram3)); if(CarregaArquivoRAM((char *)&_tmp_ram3,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram3,x,y);} break;
	case 4: memset((void*)&_tmp_ram4,0,sizeof(_tmp_ram4)); if(CarregaArquivoRAM((char *)&_tmp_ram4,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram4,x,y);} break;
	case 5: memset((void*)&_tmp_ram5,0,sizeof(_tmp_ram5)); if(CarregaArquivoRAM((char *)&_tmp_ram5,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram5,x,y);} break;
	case 6: memset((void*)&_tmp_ram6,0,sizeof(_tmp_ram6)); if(CarregaArquivoRAM((char *)&_tmp_ram6,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram6,x,y);} break;
	case 7: memset((void*)&_tmp_ram7,0,sizeof(_tmp_ram7)); if(CarregaArquivoRAM((char *)&_tmp_ram7,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram7,x,y);} break;
	case 8: memset((void*)&_tmp_ram8,0,sizeof(_tmp_ram8)); if(CarregaArquivoRAM((char *)&_tmp_ram8,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram8,x,y);} break;
	case 9: memset((void*)&_tmp_ram9,0,sizeof(_tmp_ram9)); if(CarregaArquivoRAM((char *)&_tmp_ram9,(char *)icon)==0) {GUI_BMP_Draw((void*)&_tmp_ram9,x,y);} break;

	}
	_ram_draw++;
	if(_ram_draw>9){_ram_draw=0;}
	GUI_SetColor(0x726EAF);
	GUI_SetColor(GUI_WHITE);
	GUI_SetColor(0x2D167E);
#endif
}
//---------------------------------------------------------------------------
// FUNCOES DE DESENHO DO BOTAO
//---------------------------------------------------------------------------
/*****************************************************************************************************************************************
	@autor Ulysses C. Fonseca
	
	@descricao
		Realiza a limpeza do vetor 'botao' que cont�m todos os bot�es.
		Deve ser utilizada toda vez que uma nova tela � criada.
*****************************************************************************************************************************************/
void _clean_botao(void){
	register U8 i;
	for(i=0; i<TOTAL_BOTOES; i++){
		memset((void*)&botao[i],0,sizeof(botao[i]));
	}
	for(i=0; i<TOTAL_BOTOES; i++){
		botao[i].change_window=PRESSED_DOWN;
		botao[i].style=BUTTON;
		botao[i].option=(U8*)&tmp_bot;
		botao[i].alignH=CENTER;
		botao[i].alignV=CENTER;
		botao[i].color_font_press=GUI_WHITE;
		botao[i].color_font_norm=GUI_WHITE;
		botao[i].mode=NORMAL;
		botao[i].set_var_8=0xFF;
		botao[i].set_var_8_2=0xFF;
		botao[i].set_var_16=0xFFFF;
		botao[i].set_var_32=0xFFFFFFFF;
		botao[i].pNovaFont=&GUI_Font24_1;
		botao[i].Second_Status=0xFFFF;
		botao[i].function=_null;
	}
	lock_touch_num=30;
}
/*****************************************************************************************************************************************
	@autor Ulysses C. Fonseca
	
	@descricao
		Varre o vetor que cont�m os bot�es 'botao' e verifica se algum foi pressionado, 
		caso detecte o toque em algum ira atribuir a variavel UpdateBotao=1 para que possa ser feita
		a atualiza��o do bot�o e realizar os eventos necessarios.
*****************************************************************************************************************************************/
void _check_botao_press(void){
	register int i=0;
	for(i=0;i<=bot;i++){
		if(((touch.x>botao[i].x) && (touch.x<(botao[i].x+botao[i].width)) && (touch.y>botao[i].y) && (touch.y<(botao[i].y+botao[i].height))) && !botao[i].disable){	//verifica se h� toque na area do botao
			lock_beep=1;
			if(botao[i].paginacao!=TOGGLE){
				lock_touch=1;
			}
						
			switch(botao[i].state){
				case RELEASE:
				case PRESSED_DOWN:{
					botao[i].state=PRESSED_DOWN;
					if(botao[i].state_change==CHANGE){
						snprintf((char *)tmpNomeRecAtual,30,"%s",botao[i].title);//*remover daqui
						/*if(botao[i].selec){
							for(j=0;j<TOTAL_BOTOES;j++){
								if(j!=i){botao[j].selec=0;}
							}
						}*/
					}
				}break;				
			}
		}
		else{
			switch(botao[i].state){
				case RELEASE:{
				}break;
				case PRESSED_DOWN:{
					botao[i].state=PRESSED_UP;
				}break;
				case PRESSED_UP:{
					botao[i].state=RELEASE;
				}break;
			}
		}
		
		if(botao[i].last_state!=botao[i].state){
			botao[i].last_state=botao[i].state;
			//while(UpdateBotao){vTaskDelay(1);} //comentado pois está sendo utilizado o press e o update na mesma task
			UpdateBotao=1;
			botao[i].state_change = CHANGE;
		}
	}
}
/*****************************************************************************************************************************************
*****************************************************************************************************************************************/
void _check_update(void){
	register int i=0;
	for(i=0;i<=bot;i++){
		if(botao[i].state_change==CHANGE){
			switch(botao[i].state){
				case RELEASE:{
					switch(botao[i].style){
						case IMAGEM:{
							Draw_BMP((const unsigned char(*)[20])botao[i].image_norm, botao[i].x, botao[i].y);
						}break;
						case IMAGEM_TEXT:{
							Draw_BMP((const unsigned char(*)[20])botao[i].image_norm, botao[i].x+botao[i].dx, botao[i].y);
							_DrawButtonTransparente(botao[i].x, botao[i].y, botao[i].width, botao[i].height-2,(char *)botao[i].title,botao[i].color_font_norm,botao[i].pNovaFont,botao[i].alignH, BOTTOM);
						}break;
						case BUTTON:{
							_DrawButton(botao[i].x, botao[i].y, botao[i].width, botao[i].height,(char *)botao[i].title,botao[i].color_norm, botao[i].color_font_norm, botao[i].color_cont_norm, botao[i].pNovaFont, botao[i].alignH, botao[i].alignV, botao[i].mode, botao[i].round);
						}break;
						case ICO_TEXT:{
							_DrawButton(botao[i].x, botao[i].y, botao[i].width, botao[i].height,(char *)botao[i].title,botao[i].color_norm, botao[i].color_font_norm, botao[i].color_cont_norm, botao[i].pNovaFont, botao[i].alignH, botao[i].alignV, botao[i].mode, botao[i].round);
							_DrawButtonTransparente(botao[i].x, botao[i].y, botao[i].width, botao[i].height-2,(char *)botao[i].title2,botao[i].color_font_norm,botao[i].pNovaFont2,botao[i].alignH, BOTTOM);
						}break;
						case AREA_TOUCH:{
							//APENAS AREA DE TOQUE N�O EXISTE IMAGEM
						}break;
						default:{
							_DrawButton(botao[i].x, botao[i].y, botao[i].width, botao[i].height,(char *)botao[i].title,botao[i].color_norm, botao[i].color_font_norm, botao[i].color_cont_norm, botao[i].pNovaFont, botao[i].alignH, botao[i].alignV, botao[i].mode, botao[i].round);
						}break;
					}
				}break;
				case PRESSED_UP:
				case PRESSED_DOWN:{
					switch(botao[i].style){
						case IMAGEM:{
							Draw_BMP((const unsigned char(*)[20])botao[i].image_press, botao[i].x, botao[i].y);
						}break;
						case IMAGEM_TEXT:{
							Draw_BMP((const unsigned char(*)[20])botao[i].image_press, botao[i].x+botao[i].dx, botao[i].y);
							_DrawButtonTransparente(botao[i].x, botao[i].y, botao[i].width, botao[i].height-2,(char *)botao[i].title,botao[i].color_font_press,botao[i].pNovaFont,botao[i].alignH, BOTTOM);
						}break;
						case BUTTON:{
							_DrawButton(botao[i].x, botao[i].y, botao[i].width, botao[i].height,(char *)botao[i].title,botao[i].color_press, botao[i].color_font_press, botao[i].color_cont_press, botao[i].pNovaFont, botao[i].alignH, botao[i].alignV, botao[i].mode, botao[i].round);
						}break;
						case ICO_TEXT:{
							_DrawButton(botao[i].x, botao[i].y, botao[i].width, botao[i].height,(char *)botao[i].title,botao[i].color_press, botao[i].color_font_press, botao[i].color_cont_press, botao[i].pNovaFont, botao[i].alignH, botao[i].alignV, botao[i].mode, botao[i].round);
							_DrawButtonTransparente(botao[i].x, botao[i].y, botao[i].width, botao[i].height-2,(char *)botao[i].title2,botao[i].color_font_press,botao[i].pNovaFont2,botao[i].alignH, BOTTOM);
						}break;
						case AREA_TOUCH:{
							//APENAS AREA DE TOQUE N�O EXISTE IMAGEM
						}break;
						default:{
							_DrawButton(botao[i].x, botao[i].y, botao[i].width, botao[i].height,(char *)botao[i].title,botao[i].color_press, botao[i].color_font_press, botao[i].color_cont_press, botao[i].pNovaFont, botao[i].alignH, botao[i].alignV, botao[i].mode, botao[i].round);
						}break;
					}
				}break;
			}
			
			switch(botao[i].state){
				case RELEASE:{
				}break;
				case PRESSED_DOWN:{
						switch(botao[i].paginacao){
							case 0:	//BOTAO DE NAVEGA��O NORMAL
								STATUS=botao[i].Next_Status; 
								ScrLoad=1;
							break;
							case TOGGLE:{
								UpdateScr=1;
							}break;
						}
						if(botao[i].set_var_8<0xFF)*botao[i].var_8=botao[i].set_var_8;
						if(botao[i].set_var_8_2<0xFF)*botao[i].var_8_2=botao[i].set_var_8_2;
						if(botao[i].set_var_16<0xFFFF)*botao[i].var_16=botao[i].set_var_16;
						if(botao[i].set_var_32<0xFFFFFFFF)*botao[i].var_32=botao[i].set_var_32;
						(*botao[i].function)();
						
				}break;
				case PRESSED_UP:{
				}break;
			}
			botao[i].state_change = NO_CHANGE;
		}
	}
}
