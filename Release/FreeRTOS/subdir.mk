################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../freertos/list.c \
../freertos/queue.c \
../freertos/tasks.c 

OBJS += \
./freertos/list.o \
./freertos/queue.o \
./freertos/tasks.o 

C_DEPS += \
./freertos/list.d \
./freertos/queue.d \
./freertos/tasks.d 


# Each subdirectory must supply rules for building sources it contributes
freertos/%.o: ../freertos/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DNDEBUG -D__REDLIB__ -D__USE_CMSIS -D__CODE_RED -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject\cmsis\inc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject\freertos\include" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject\freertos\portable\gcc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject\emwin\GUI" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject\emwin\Inc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject\emwin\Config" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject\emwin\System\HW" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject\app\inc" -Os -Os -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


