################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../emwin/Config/Calibrate.c \
../emwin/Config/GUIConf.c \
../emwin/Config/GUI_X.c \
../emwin/Config/LCDConf.c \
../emwin/Config/SIMConf.c 

OBJS += \
./emwin/Config/Calibrate.o \
./emwin/Config/GUIConf.o \
./emwin/Config/GUI_X.o \
./emwin/Config/LCDConf.o \
./emwin/Config/SIMConf.o 

C_DEPS += \
./emwin/Config/Calibrate.d \
./emwin/Config/GUIConf.d \
./emwin/Config/GUI_X.d \
./emwin/Config/LCDConf.d \
./emwin/Config/SIMConf.d 


# Each subdirectory must supply rules for building sources it contributes
emwin/Config/%.o: ../emwin/Config/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -D__USE_CMSIS -D__CODE_RED -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\cmsis\inc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\freertos\include" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\freertos\portable\gcc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\emwin\GUI" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\emwin\Inc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\emwin\Config" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\emwin\System\HW" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\app\inc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\board\inc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\mcu\inc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\usb\inc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\fatfs" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\gui_emb\inc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\panel_touch_FT5216\inc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\i2c_emb\inc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\can\inc" -I"E:\compvm\EMBTECH_SVN\PRATICA\IHM_5_POL\SOF\embnet\inc" -Os -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


