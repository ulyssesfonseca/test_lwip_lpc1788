

#ifndef __SD_CONTROL__
#define __SD_CONTROL__

#include "board.h"

void waitMs(uint32_t time);
uint32_t waitEvIRQDriven(void);
void DMA_IRQHandler (void);
void SDIO_IRQHandler(void);
void Init_SDMMC(void);
void DeInit_SDMMC(void);

#endif
