#include "CAN_LPC1788.h"


CAN_msg       CAN_TxMsg;                      /* CAN message for sending */
CAN_msg       CAN_RxMsg;                      /* CAN message for receiving */                                

unsigned int  CAN_TxRdy = 0;              /* CAN HW ready to transmit a message */
unsigned int  CAN_RxRdy = 0;              /* CAN HW received a message */

LPC_CAN_T *LPC_CAN = LPC_CAN1;			//CAN CHANNEL USED
uint8_t CAN_CTRL_NO = 0;					//0-CAN1 1-CAN2

extern uint32_t _DeviceID; 
extern uint32_t _NetID;

extern uint32_t FRAME_SET;
extern uint32_t FRAME_GET;
extern uint32_t FRAME_REPLY;
extern uint32_t FRAME_DISCOVERY;
extern uint32_t FRAME_LIVE;
extern uint32_t FRAME_GETERROR;
extern uint32_t FRAME_REPLYERROR;
extern uint32_t FRAME_ATTRIBERROR;
extern uint32_t FRAME_SETDEVICE;
extern uint32_t FRAME_TRIGGER;


void CAN_SetChannel(uint8_t pCh){
	switch(pCh){
		case 1:{
			LPC_CAN = LPC_CAN1;
			CAN_CTRL_NO = 0;
		}break;
		case 2:{
			LPC_CAN = LPC_CAN2;
			CAN_CTRL_NO = 1;
		}break;

	}
}

/*----------------------------------------------------------------------------
  setup CAN interface.  CAN controller (1..2)
 *----------------------------------------------------------------------------*/
void CAN_setup ()  {
	Chip_CAN_Init(LPC_CAN, LPC_CANAF, LPC_CANAF_RAM);
	Chip_CAN_SetBitRate(LPC_CAN, 250000);
	Chip_CAN_EnableInt(LPC_CAN, CAN_IER_BITMASK);
}

/*----------------------------------------------------------------------------
  leave initialisation mode.  CAN controller (1..2)
 *----------------------------------------------------------------------------*/
void CAN_start ()  {
	Chip_CAN_ConfigFullCANInt(LPC_CANAF, ENABLE);
	Chip_CAN_SetAFMode(LPC_CANAF, CAN_AF_FULL_MODE);
	NVIC_EnableIRQ(CAN_IRQn);
}

/*----------------------------------------------------------------------------
  check if transmit mailbox is empty
 *----------------------------------------------------------------------------*/
void CAN_waitReady ()  {
	while ((Chip_CAN_GetStatus(LPC_CAN) & (1<<2)) == 0);              /* Transmitter ready for transmission */
	CAN_TxRdy = 1;
}

/*----------------------------------------------------------------------------
  wite a message to CAN peripheral and transmit it.  CAN controller (1..2)
 *----------------------------------------------------------------------------*/
void CAN_wrMsg (CAN_msg *msg)  {
	uint8_t i = 0;
	CAN_BUFFER_ID_T   WrBuf;
	CAN_MSG_T WrCAN;

	WrCAN.ID = ((msg->format&EXTENDED_FORMAT) ? CAN_EXTEND_ID_USAGE: 0) | msg->id;
	WrCAN.DLC = msg->len;
	WrCAN.Type = ((msg->type&REMOTE_FRAME) ? CAN_REMOTE_MSG : 0);
	for(i=0; i<WrCAN.DLC; i++){
		WrCAN.Data[i] = msg->data[i];
	}

	WrBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN);
	Chip_CAN_Send(LPC_CAN, WrBuf, &WrCAN);

	while ((Chip_CAN_GetStatus(LPC_CAN) & CAN_SR_TCS(WrBuf)) == 0) {}
	//msg sended
}

/*----------------------------------------------------------------------------
  read a message from CAN peripheral and release it.  CAN controller (1..2)
 *----------------------------------------------------------------------------*/
void CAN_rdMsg (CAN_msg *msg)  {
	uint8_t i = 0;
	CAN_MSG_T RdCAN;

	Chip_CAN_Receive(LPC_CAN, &RdCAN);

	msg->format = (RdCAN.ID & CAN_EXTEND_ID_USAGE) ? 1 : 0;
	msg->type = (RdCAN.Type & CAN_REMOTE_MSG) ? 1 : 0;
	msg->id = RdCAN.ID & (~CAN_EXTEND_ID_USAGE);
	msg->len = RdCAN.DLC;
	for (i = 0; i < RdCAN.DLC; i++){
		msg->data[i] = RdCAN.Data[i];
	}
}

/*----------------------------------------------------------------------------
  setup acceptance filter.  CAN controller (1..2)
 *----------------------------------------------------------------------------*/
void CAN_wrFilter (uint32_t id, uint8_t format)  {

	CAN_EXT_ID_ENTRY_T ExtEntry = {CAN_CTRL_NO, ((1 << 11) | 0x00)};
	CAN_STD_ID_ENTRY_T StdEntry = {CAN_CTRL_NO, 0, 0x00};

	if(format==EXTENDED_FORMAT){
		ExtEntry.ID_29 = /*(1 << 11) |*/ id;
		Chip_CAN_InsertEXTEntry(LPC_CANAF, LPC_CANAF_RAM, &ExtEntry);
	}
	else{
		StdEntry.ID_11 = 0x20;
		Chip_CAN_InsertSTDEntry(LPC_CANAF, LPC_CANAF_RAM, &StdEntry);
	}
}

/*----------------------------------------------------------------------------
  CAN interrupt handler
 *----------------------------------------------------------------------------*/
void CAN_IRQHandler (void)  {
	volatile uint32_t icr;
//	volatile uint16_t tmpAttribute;
//	uint32_t tmpData;

	/* check CAN controller 1 */
	icr = Chip_CAN_GetIntStatus(LPC_CAN);                           /* clear interrupts */

	if (icr & (1 << 0)) {                          	/* CAN Controller #1 meassage is received */
		CAN_rdMsg (&CAN_RxMsg);                		/*  read the message */
		CAN_RxRdy = 1;                            	/*  set receive flag */
	}

	if (CAN_RxRdy) {
		__EmbNet_ReadFrames(CAN_RxMsg);
//		_EmbNET_Sniffer(CAN_RxMsg);
//		//Pacotes broadcast
//		if (CAN_RxMsg.id == FRAME_DISCOVERY) { EmbNET_SendLive();  } 	//recebe um pacote discovery
//
//		if (CAN_RxMsg.id == FRAME_LIVE) {  														//----Recebe um pacote tipo live-----------
//
//			tmpAttribute = (uint16_t)CAN_RxMsg.data[2];
//			tmpAttribute = tmpAttribute  << 8;
//			tmpAttribute |= (uint16_t)CAN_RxMsg.data[3];
//
//			tmpData = (uint32_t)CAN_RxMsg.data[7];
//			tmpData |= ((uint32_t)CAN_RxMsg.data[6]) << 8;
//			tmpData |= ((uint32_t)CAN_RxMsg.data[5]) << 16;
//			tmpData |= ((uint32_t)CAN_RxMsg.data[4]) << 24;
//
//			_EmbNET_CallBk_Live(CAN_RxMsg.data[0], CAN_RxMsg.data[1], tmpAttribute, tmpData);
//		}
//
//		if((CAN_RxMsg.id&0x000F0000) == FRAME_SETDEVICE){
//			tmpAttribute = (uint16_t)CAN_RxMsg.data[2];
//			tmpAttribute = tmpAttribute  << 8;
//			tmpAttribute |= (uint16_t)CAN_RxMsg.data[3];
//
//			tmpData = (uint32_t)CAN_RxMsg.data[7];
//			tmpData |= ((uint32_t)CAN_RxMsg.data[6]) << 8;
//			tmpData |= ((uint32_t)CAN_RxMsg.data[5]) << 16;
//			tmpData |= ((uint32_t)CAN_RxMsg.data[4]) << 24;
//			_EmbNET_CallBk_FrameSetDevice((uint8_t)(CAN_RxMsg.id&0x000000FF),(uint8_t)((CAN_RxMsg.id & 0x0000FF00)>>8),CAN_RxMsg.data[0], CAN_RxMsg.data[1], tmpAttribute, tmpData);
//		}
//
//		//Pacotes destinados ao device local
//		if (((CAN_RxMsg.id & 0x000000FF) == (_DeviceID)) && ((CAN_RxMsg.id & 0x0000FF00) == (_NetID)))  {
//
//
//			if 			((CAN_RxMsg.id & 0x000F0000) == FRAME_SET) { 						//recebe um pacote tipo SET
//				tmpAttribute = (uint16_t)CAN_RxMsg.data[2];
//				tmpAttribute = tmpAttribute  << 8;
//				tmpAttribute |= (uint16_t)CAN_RxMsg.data[3];
//
//				tmpData = (uint32_t)CAN_RxMsg.data[7];
//				tmpData |= ((uint32_t)CAN_RxMsg.data[6]) << 8;
//				tmpData |= ((uint32_t)CAN_RxMsg.data[5]) << 16;
//				tmpData |= ((uint32_t)CAN_RxMsg.data[4]) << 24;
//
//				_EmbNET_CallBk_FrameSet( CAN_RxMsg.data[0], CAN_RxMsg.data[1], tmpAttribute, tmpData);
//			}
//			else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_GET) { 				//Recebe um pacote tipo GET
//
//				tmpAttribute = (uint16_t)CAN_RxMsg.data[2];
//				tmpAttribute = tmpAttribute  << 8;
//				tmpAttribute |= (uint16_t)CAN_RxMsg.data[3];
//
//				_EmbNET_CallBk_FrameGet(CAN_RxMsg.data[0],CAN_RxMsg.data[1],tmpAttribute,tmpData);
//
//			}
//			else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_REPLY) { 			//recebe um pacote tipo Reply
//
//				tmpAttribute = (uint16_t)CAN_RxMsg.data[2];
//				tmpAttribute = tmpAttribute  << 8;
//				tmpAttribute |= (uint16_t)CAN_RxMsg.data[3];
//
//				tmpData = (uint32_t)CAN_RxMsg.data[7];
//				tmpData |= ((uint32_t)CAN_RxMsg.data[6]) << 8;
//				tmpData |= ((uint32_t)CAN_RxMsg.data[5]) << 16;
//				tmpData |= ((uint32_t)CAN_RxMsg.data[4]) << 24;
//
//				_EmbNET_CallBk_FrameReply( CAN_RxMsg.data[0], CAN_RxMsg.data[1], tmpAttribute, tmpData);
//
//
//			}
//			else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_TRIGGER) {
//
//				tmpAttribute = (uint16_t)CAN_RxMsg.data[2];
//				tmpAttribute = tmpAttribute  << 8;
//				tmpAttribute |= (uint16_t)CAN_RxMsg.data[3];
//
//				tmpData = (uint32_t)CAN_RxMsg.data[7];
//				tmpData |= ((uint32_t)CAN_RxMsg.data[6]) << 8;
//				tmpData |= ((uint32_t)CAN_RxMsg.data[5]) << 16;
//				tmpData |= ((uint32_t)CAN_RxMsg.data[4]) << 24;
//
//				_EmbNET_CallBk_FrameTrigger( CAN_RxMsg.data[0], CAN_RxMsg.data[1], tmpAttribute, tmpData);
//
//			}
//			else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_ATTRIBERROR) {
//
//				tmpAttribute = (uint16_t)CAN_RxMsg.data[2];
//				tmpAttribute = tmpAttribute  << 8;
//				tmpAttribute |= (uint16_t)CAN_RxMsg.data[3];
//
//
//				_EmbNET_CallBk_FrameAttribError(CAN_RxMsg.data[0], CAN_RxMsg.data[1], tmpAttribute);
//
//			}
//			else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_GETERROR) {
//
//				tmpAttribute = (uint16_t)CAN_RxMsg.data[2];
//				tmpAttribute = tmpAttribute  << 8;
//				tmpAttribute |= (uint16_t)CAN_RxMsg.data[3];
//
//
//				_EmbNET_CallBk_FrameGetError(CAN_RxMsg.data[0], CAN_RxMsg.data[1], tmpAttribute);
//
//			}
//			else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_REPLYERROR) {
//
//				tmpAttribute =  (uint16_t)CAN_RxMsg.data[2];
//				tmpAttribute =  tmpAttribute  << 8;
//				tmpAttribute |= (uint16_t)CAN_RxMsg.data[3];
//
//				tmpData =  (uint32_t)CAN_RxMsg.data[7];
//				tmpData |= ((uint32_t)CAN_RxMsg.data[6]) << 8;
//				tmpData |= ((uint32_t)CAN_RxMsg.data[5]) << 16;
//				tmpData |= ((uint32_t)CAN_RxMsg.data[4]) << 24;
//
//				_EmbNET_CallBk_FrameReplyError(CAN_RxMsg.data[0], CAN_RxMsg.data[1], tmpAttribute, tmpData);
//
//			}
//
//		}
	}

	CAN_RxRdy=0;

	if (icr & (1 << 1)) {                          /* CAN Controller #1 meassage is transmitted */
		CAN_TxRdy = 1;
	}
}

/*----------------------------------------------------------------------------
  CAN Counter Error
 *----------------------------------------------------------------------------*/
uint32_t CAN_Error(void){

	uint8_t eRX = (uint8_t)(Chip_CAN_GetGlobalStatus(LPC_CAN)&0x00FF0000)>>16;
	uint8_t eTX = (uint8_t)(Chip_CAN_GetGlobalStatus(LPC_CAN)&0xFF000000)>>24;

	return (uint32_t)(eRX<<8|eTX);
}



