
#ifndef _CAN_H_
#define _CAN_H_
//#include "LPC177x_8x.H"
#include <stdint.h>                   /* Include standard types               */
#include "chip.h"

#define CAN_VERSION 103

#define STANDARD_FORMAT  0
#define EXTENDED_FORMAT  1

#define DATA_FRAME       0
#define REMOTE_FRAME     1

typedef struct  {
  unsigned int   id;                    /* 29 bit identifier */
  unsigned char  data[8];               /* Data field */
  unsigned char  len;                   /* Length of data field in bytes */
  unsigned char  format;                /* 0 - STANDARD, 1- EXTENDED IDENTIFIER */
  unsigned char  type;                  /* 0 - DATA FRAME, 1 - REMOTE FRAME */
} CAN_msg;

extern CAN_msg       CAN_TxMsg;      /* CAN messge for sending */
extern CAN_msg       CAN_RxMsg;      /* CAN message for receiving */                                
extern unsigned int  CAN_TxRdy;      /* CAN HW ready to transmit a message */
extern unsigned int  CAN_RxRdy;      /* CAN HW received a message */

/* Functions defined in module CAN.c */
void CAN_setup         (void);
void CAN_start         (void);
void CAN_waitReady     (void);
void CAN_wrMsg         (CAN_msg *msg);
void CAN_rdMsg         (CAN_msg *msg);
void CAN_wrFilter      (uint32_t id, uint8_t filter_type);
uint32_t CAN_Error		 (void);


extern void _EmbNET_Sniffer(CAN_msg);
extern void EmbNET_SendLive(void);

extern void _EmbNET_CallBk_Live( uint8_t FDeviceID, uint8_t FNetID, uint16_t Class, uint32_t Serial );
extern void _EmbNET_CallBk_FrameSet( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data );
extern void _EmbNET_CallBk_FrameGet( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data);
extern void _EmbNET_CallBk_FrameReply( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data );
extern void _EmbNET_CallBk_FrameTrigger( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data );
extern void _EmbNET_CallBk_FrameSetDevice(uint8_t ToDeviceID, uint8_t ToNetID, uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data);
extern void _EmbNET_CallBk_FrameAttribError(uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID);
extern void _EmbNET_CallBk_FrameGetError(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID);
extern void _EmbNET_CallBk_FrameReplyError(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue);

#endif
