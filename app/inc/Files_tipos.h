/**********************************************************************
 * $Id$		Files_tipos.h			2017-05-02
 * @file	Files_tipos.h
 * @brief	Tipos de arquivos utilizados no projeto
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 *
 *
 **********************************************************************/
/*********************************************************************
 * 	ERRATA
 *
 *
 *
 **********************************************************************/


#ifndef __FILES_TIPOS_H__
#define __FILES_TIPOS_H__


#define MAX_PASSOS_REC  8

#define version_Rec	0x0002

/*tipo da receita para não ser lida em equipamentos diferentes
 *
 * 0x0001 - NACIONAL OU EXPORT
 * 0x0002 - NACIONAL E EXPORT UNIFICADOS
 *
 */
#define type_Bravo 0x0002


typedef struct{
		uint16_t    version;                //VERSAO DA ESTRUTURA
		uint16_t    type;                   //TIPO DA ESTRUTURA PARA NAO SER LIDA EM EQUIP. DIFERENTE
		uint8_t     path[50];
		uint8_t     titulo[30];             //TITULO DA CATEGORIA
		uint8_t     ico[30];                //ICONE UTILIZADO NA CATEGORIA
		uint8_t     ativo;                  //SE O GRUPO ESTA ATIVO
		uint8_t     favorito;               //SE O GRUPO ESTA ATIVO
		uint8_t     delete; 	            //FLAG QUE DEFINE QUE DEVERA SER DELETADO ESTE ITEM
		uint32_t	FLAGS;					//0-EXPORT 0x02-ICONE_PRIMEIRA_LETRA (era uint32_t, virou 16 pela TEMPERATURA_PRE)
		int32_t		TEMPERATURA_PRE;		//TEMPERATURA DE PRE DO GRUPO
		uint32_t	FUTURO[10];				//SE PRECISAR DE NOVAS VARIAVEIS REMOVER DAQUI MANTENDO O TAMANHO DO STRUCT
}s_Lista;


//SALVAR O NOME DA RECEITA COM O NUMERO DE POSICIONAMENTO
//ESTRUTURA 0x0002
typedef struct{
		uint16_t    version;                //VERSAO DA ESTRUTURA
		uint16_t    type;                   //TIPO DA ESTRUTURA PARA NAO SER LIDA EM EQUIP. DIFERENTE
		uint8_t     titulo[30];             //TITULO DA RECEITA
		uint8_t     categoria[30];          //NOME DA CATEGORIA ATUAL
		uint8_t     ico[50];                //ICONE DA RECEITA ATUAL
		uint8_t     favorito;               //MARCA A RECEITA COMO FAVORITA
		uint8_t     total_passos;           //TOTAL DE PASSOS DA RECEITA
		uint8_t     dourar;                 //TEMPO PARA DOURAR 0-DESABILITADO
		uint8_t     aquecer;	            //TEMPO PARA COZINHAR 0-DESABILITADO
		uint8_t     dourar_aquecer;         //TEMPO PARA DOURAR/COZINHAR 0-DESABILITADO
} s_RecCabecalho;   //NO MAXIMO 256 BYTES
typedef struct{
		uint8_t     passo_ativo;            //SE O PASSO ESTA ATIVO
		uint8_t     instrucao[100];         //INSTRUCAO DO PASSO
		uint8_t     vel_motor;              //PERCENTUAL VELOCIDADE DO MOTOR
		uint8_t     percent_microondas;     //PERCENTUAL DE MICROONDAS
		uint8_t     percent_teto_lastro;    //PERCENTUAL DE TEMPERATURA DE TETO LASTRO
		int32_t     temperatura;            //VALOR DE TEMPERATURA °C*18
		uint32_t    tempo;                  //TEMPO DO PASSO
} s_RecPasso;
typedef struct{
		s_RecCabecalho cabecalho;
		s_RecPasso passos[MAX_PASSOS_REC];
} s_RecBravo;

typedef struct{
		char 		text[30];
		uint8_t 	hora;
		uint8_t 	min;
		uint8_t 	dia;
		uint8_t 	mes;
		uint16_t 	ano;
		uint64_t 	cont;
} s_Registros;


#endif
