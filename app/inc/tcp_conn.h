#ifndef TCP_APP_H
#define TCP_APP_H

#include "lwip/init.h"
#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/memp.h"
#include "lwip/tcpip.h"
#include "lwip/ip_addr.h"
#include "lwip/netif.h"
#include "lwip/timers.h"
#include "netif/etharp.h"

#if LWIP_DHCP
#include "lwip/dhcp.h"
#endif

#include <string.h>
#include "board.h"
#include "chip.h"
#include "lpc_phy.h"
#include "arch/lpc17xx_40xx_emac.h"
#include "arch/lpc_arch.h"
#include "arch/sys_arch.h"
#include "lpc_phy.h"/* For the PHY monitor support */

#include "lwip/tcp.h"
#include "lwip/dns.h"

//	ETHERNET
//struct tcp_pcb *testpcb;
//err_t error;

void tcpip_init_done_signal(void *arg);
void tcp_close_con(struct tcp_pcb *pcb);
//uint32_t tcp_send_packet();
uint32_t tcp_send_packet(void *arg, struct tcp_pcb *pcb, err_t err);
err_t tcpRecvCallback(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
err_t tcpSendCallback(void *arg, struct tcp_pcb *tpcb, u16_t len);
void tcpErrorHandler(void *arg, err_t err);
err_t connectCallback(void *arg, struct tcp_pcb *tpcb, err_t err);
err_t tcp_setup(const char* _server, const uint16_t pPort);

extern volatile char frame[1*1024*1024];
extern volatile char response[1*1024*1024];
extern void response_SERVER();
extern void error_SERVER(int8_t pEr);
extern void cbk_TCP_SEND(uint16_t pLen);

#endif /* TCP_APP_H */
