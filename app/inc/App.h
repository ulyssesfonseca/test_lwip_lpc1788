/*
 * App.h
 *
 *  Created on: 26 de jan de 2017
 *      Author: Ulysses
 */

#ifndef INC_APP_H_
#define INC_APP_H_


#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "GUI.h"
#include "DIALOG.h"
#include "GRAPH.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "ff.h"

#include "MCP9803.h"
#include "GUI_EMB.h"
#include "CPT_FT5216.h"

#include "EMBNET.h"

#include "Textos.h"


#include "Files.h"

#include "chip.h"
#include "board_types.h"
#include "rtc.h"

#include "diskio.h"
#include "usbMassStorageHost.h"

#include "tcp_conn.h"
#include "JSON.h"


#ifdef DEBUG
#	define  Debug_Log(...)       //UART_Printf(__VA_ARGS__)
#else
#	define  Debug_Log(...)
#endif


//#define WATCHDOG

//#define PLAY_FDA	//habilita play pelo fechamento da porta após finalizar receita

//#define SIMULA_OPERACAO
//#define SIMULA_TEMPERATURA
//#define VINCULO_INVERSOR

#ifdef SIMULA_OPERACAO
	volatile uint8_t _f_simula_temp=0;
	volatile uint8_t _f_simula_desenho_porta=0;
	volatile uint8_t _f_simula_botao_porta=0;
	volatile uint8_t _f_tmp=0;
#endif

extern volatile CPT_PID_STATE Touch;

#define VERSAO_BETA
#define EXIBE_BANNER

const uint8_t VERSION[] = {1,0,4,' '};

uint8_t VERSION_FIRM_POWER[4]={0,0,0,0};  //VERSAO FIRMWARE
uint16_t CLASS_ID_POWER_READ = 0x0000;


extern PIN_COM_TypeDef LED_MOD[2];
extern PIN_COM_TypeDef LCD[21];

#define MODE_8_BIT EEPROM_RWSIZE_8BITS
#define EEPROM_Write(a,b,c,d,e) Chip_EEPROM_Write(LPC_EEPROM, a,b,c,d,e)
#define EEPROM_Read(a,b,c,d,e) Chip_EEPROM_Read(LPC_EEPROM, a,b,c,d,e)


//===================================================================
//	 EMBNET
//===================================================================
#define DELAY_CAN_WRITE		5
#define CLASS_ID 0x1000
volatile uint8_t Type = EMBNET_MASTER;
volatile uint8_t Net = 0x0A;
volatile uint8_t IDMod = 0x01;
volatile uint8_t _f_EmbNet_OnLine = 0;


//DEFINES E VARIAVEIS PARA CONTROLE DA POWER
#define CLASS_ID_POWER_EXPRESS 0x0010//0x0003//0x0010
#define CLASS_ID_POWER_FULL 0x0011
#define CLASS_ID_POWER_EXPRESS_EXPORT 0x0012
#define MASK_MOD_POWER 0x01
#define MOD_POWER_RESET_TIME_OUT 5	//EM SEG
#define MOD_POWER_DETECT_OFFLINE 5	//EM SEG
volatile uint8_t IDMod_Power = 0x0A;
volatile uint8_t _c_Mod_Power_offLine = 10;	//SE NAO DETECTAR A POWER EM 5SEG GERA ERRO
volatile uint8_t _f_Mod_Power_offLine = 1;
volatile uint8_t _c_Mod_Power_reset_time_out = 0;
volatile uint8_t _f_MOD_connect = 0x00; //0x01-power
volatile uint8_t _f_MOD_config = 0x00; //0x01-power
volatile uint8_t _c_MOD_config = 0; //0x01-power
volatile uint8_t _f_Embnet_off   = 0;	//EM 1 DESLIGA A EMBNET
volatile uint8_t _c_count_erro_CAN = 0;


volatile uint16_t _v_revisao=0;

volatile union {
	uint32_t buffer;
	struct{
		uint32_t b1:1;
		uint32_t b2:1;
		uint32_t S_PORTA:1;
		uint32_t CORRENTE:1;
		uint32_t b5:1;
		uint32_t b6:1;
		uint32_t b7:1;
		uint32_t b8:1;
		uint32_t b9:1;
		uint32_t b10:1;
		uint32_t b11:1;
		uint32_t b12:1;
		uint32_t b13:1;
		uint32_t b14:1;
		uint32_t b15:1;
		uint32_t b16:1;
	};
}STATUS_POWER;
volatile uint32_t _b_STATUS_POWER = 0x00000000;

volatile uint8_t _c_CONT=0;
volatile uint8_t _c_SEG=0;
volatile uint8_t __c_min=0;
volatile uint8_t __c_hora=0;
volatile uint8_t _c_gets_power __attribute__ ((section (".TEMP_APP")));

struct{	//	POWER TEMPS
		int32_t TEMP_BOARD;
		int32_t TEMP_1;		//CAMARA
		int32_t TEMP_2;		//RI
		int32_t TEMP_3;
		int32_t TEMP_4;
		int32_t TEMP_5;
		int32_t TEMP_6;
}POWER;// __attribute__ ((section (".TEMP_APP")));

struct{
	uint8_t TEMP_BOARD;
	uint8_t TEMP_1;		//CAMARA
	uint8_t TEMP_2;		//RI
	uint8_t TEMP_3;
	uint8_t TEMP_4;
	uint8_t TEMP_5;
	uint8_t TEMP_6;
}COUNT_ERRO_SENSOR;

#define TIME_MAX_ERRO_PAINEL	600
volatile uint16_t _c_time_erro_Painel=TIME_MAX_ERRO_PAINEL;

#define TIME_MAX_ERRO_SENSOR_TEMP	60
volatile uint16_t _c_time_erro_CAM=6;//TIME_MAX_ERRO_SENSOR_TEMP;
volatile uint16_t _c_time_erro_IR=6;//TIME_MAX_ERRO_SENSOR_TEMP;
volatile uint8_t  _c_time_erro_init=10;

//FLAGS DE LEITURA PARA GET
volatile union {	// EMBNET POWER CICLO UPDATE
		uint16_t buffer;
		struct{
				uint16_t STATUS:1;			//0x01
				uint16_t TEMPERATURAS:1;	//0x02
				uint16_t DIG_IN:1;			//0x04
				uint16_t DIG_OUT:1;			//0x08
				uint16_t COUNTER:1;			//0x10
				uint16_t RESET_POWER:1;		//0x20
				uint16_t bit6:1;
				uint16_t bit7:1;
				uint16_t bit8:1;
				uint16_t bit9:1;
				uint16_t bit10:1;
				uint16_t bit11:1;
				uint16_t bit12:1;
				uint16_t bit13:1;
				uint16_t bit14:1;
				uint16_t bit15:1;
		}bits;
}EMBNET_POWER;// __attribute__ ((section (".TEMP_APP")));

volatile uint16_t _LAST_POWER_DIG_IN = 0xffff;
//ACIONAMENTOS
volatile union {	//	POWER DIG_IN
		uint16_t buffer;
		struct{
				uint16_t b0:1;
				uint16_t PORTA:3;
				uint16_t b4:1;
				uint16_t b5:1;
				uint16_t b6:1;
				uint16_t b7:1;
				uint16_t b8:1;
				uint16_t b9:1;
				uint16_t b10:1;
				uint16_t b11:1;
				uint16_t b12:1;
				uint16_t b13:1;
				uint16_t b14:1;
				uint16_t b15:1;
		}bits;
}POWER_DIG_IN __attribute__ ((section (".TEMP_APP")));
volatile union {	//	POWER DIG_OUT
		uint16_t buffer;
		struct{
				uint16_t RELE_MAG:1;
				uint16_t RELE_CURTO:1;
				uint16_t AQ_CAM:1;
				uint16_t AQ_RI:1;
				uint16_t VENTILADOR:1;
				uint16_t BUZZER:1;
				uint16_t GERAL:1;
				uint16_t TENSAO:1;
				uint16_t b8:1;
				uint16_t b9:1;
				uint16_t b10:1;
				uint16_t b11:1;
				uint16_t b12:1;
				uint16_t b13:1;
				uint16_t b14:1;
				uint16_t b15:1;
		}bits;
}POWER_DIG_OUT __attribute__ ((section (".TEMP_APP")));
volatile uint16_t POWER_DIG_OUT_buffer __attribute__ ((section (".TEMP_APP")));
volatile union {	//	POWER MODBUS
		uint32_t buffer;
		struct{
				uint32_t b0:1;
				uint32_t LIGA_MOTOR:1;		//0-STOP 1-RUN
				uint32_t SENTIDO_MOTOR:1; 	//0-CW 1-CCW
				uint32_t VELOCIDADE:7;		//VELOCIDADE DO MOTOR EM %
				uint32_t b10:1;
				uint32_t b11:1;
				uint32_t b12:1;
				uint32_t b13:1;
				uint32_t b14:1;
				uint32_t b15:1;
		}bits;
}POWER_MODBUS __attribute__ ((section (".TEMP_APP")));
volatile union {	//	POWER CONFIG
		uint32_t buffer;
		struct{
				uint32_t CTRL_DOOR:1;
				uint32_t CTRL_ON_OFF:1;
				uint32_t CTRL_OUTS_A_INV:1;
				uint32_t STATUS:1;
				uint32_t FDA:1;
				uint32_t b5:1;
				uint32_t b6:1;
				uint32_t b7:1;
				uint32_t b8:1;
				uint32_t b9:1;
				uint32_t INV_HAB:1; 		//1-HABILITA INVERSOR 0-DESABILITA
				uint32_t INV_MODELO:2;		//00-HONEYWELL 01-WEG 10-RESERVADO 11-RESERVADO
				uint32_t b13:1;
				uint32_t b14:1;
				uint32_t b15:1;
		}bits;
}POWER_CONFIG __attribute__ ((section (".TEMP_APP")));
volatile union {	//	POWER ERROS
		uint32_t buffer;
		struct{
				uint32_t INVERSOR:1;
				uint32_t MAG:1;
				uint32_t b2:1;
				uint32_t b3:1;
				uint32_t b4:1;
				uint32_t b5:1;
				uint32_t b6:1;
				uint32_t b7:1;
				uint32_t b8:1;
				uint32_t b9:1;
				uint32_t b10:1;
				uint32_t b11:1;
				uint32_t b12:1;
				uint32_t b13:1;
				uint32_t b14:1;
				uint32_t b15:1;
		}bits;
}POWER_ERROS __attribute__ ((section (".TEMP_APP")));
volatile union {	//	TRIGGERS
		uint32_t buffer;
		struct{
				uint32_t FUNC:3;
				uint32_t VETOR:4;
				uint32_t VAZIO:9;
				uint32_t ATTRIB:16;
		}bits;
}TRIGGERS __attribute__ ((section (".TEMP_APP")));

volatile union{
		uint8_t buffer;
		struct{
				uint8_t BUZZER:1;
				uint8_t MOTOR:1;
				uint8_t VENTILADOR:1;
				uint8_t RES_LASTRO:1;
				uint8_t RES_TETO:1;
				uint8_t MICROONDAS:1;
				uint8_t CONTATOR:1;
		};
}TESTE_POWER __attribute__ ((section (".TEMP_APP")));


volatile union{
		uint32_t buffer;
		struct{
			uint32_t PERIODO:24;
			uint32_t DUTY:8;
			uint32_t CANAL:8;
		};
}PWM_POWER __attribute__ ((section (".TEMP_APP")));

#define MAX_TIME_TESTE_CARGA	20
volatile uint8_t _c_time_teste_carga=0;
volatile uint8_t _f_teste_carga=0;
volatile uint8_t _f_teste_carga_ON=0;
volatile uint8_t _f_teste_Vinculo=0;

//VINCULOS POWER
#define VINC_IN1	1
#define VINC_IN2	2
#define VINC_IN3	3
#define VINC_IN4	4
#define VINC_IN5	5
#define VINC_IN6	6
#define VINC_NVL1	7
#define VINC_NVL2	8
#define VINC_OUT1	9
#define VINC_OUT2	10
#define VINC_OUT3	11
#define VINC_OUT4	12
#define VINC_OUT5	13
#define VINC_OUT6	14
#define VINC_OUT7	15
#define VINC_OUT8	16
#define VINC_OUT9	17
#define VINC_OUT10	18
#define VINC_OUT11	19
#define VINC_OUT12	20
#define VINC_INV	21
volatile union{	//	VINCULOS
		uint32_t buffer;
		struct{
				uint32_t FUNC:3;
				uint32_t VETOR:4;
				uint32_t VALOR:9;
				uint32_t DEST:8;
				uint32_t ORIG:8;
		}bits;
}VINCULOS;

typedef struct{	//STRUCT SET_POINT
		uint8_t VELOCIDADE_MOTOR;
		uint8_t RI;
		uint16_t MICROONDAS;
		int32_t TEMPERATURA;
		uint32_t TEMPO;
}_s_SET_POINT;

typedef struct{
	int32_t TEMPERATURA_PRE;
	int32_t LAST_TEMPERATURA_PRE;
	uint8_t AQUECE;
	uint8_t PREAQUECEU;
	uint8_t PRE_ATIVO;
	uint8_t BYPASS;
}_s_PRE_AQ;
#define HISTERESE_TEMP_CAM 1
#define HISTERESE_TEMP_RI  1
#define MAX_TEMPO_SETPOINT 5*60
volatile _s_SET_POINT SET_POINT;
volatile _s_SET_POINT LAST_SET_POINT;
volatile _s_SET_POINT EDT_SET;
volatile _s_SET_POINT LAST_EDT_SET;
volatile _s_PRE_AQ SET_PRE;

#define INV_MOD_HONEYWELL	0x00
#define INV_MOD_WEG			0x01

volatile uint8_t  tmpMaisExe=0;
volatile uint8_t  tmpStatusAlerta=0;
volatile uint8_t  _f_save_passo_MT=0;
volatile uint8_t  _c_AtrasoPlay=0;
volatile uint8_t  _f_PreAquecimento=1;
volatile uint8_t  _f_TimePreAque=0;
volatile uint32_t _c_TimePreAque=0;
volatile uint32_t _c_TimeResfriamento=0;
volatile uint32_t _bf_Last_MODBUS=0x00000000;
volatile uint32_t _bf_Last_CONFIG=0x00000000;
volatile uint32_t _c_TEMPO_CORRENTE_PASSO=0;
volatile uint32_t _c_TEMPO_CORRENTE=0;
volatile uint32_t _c_Last_TEMPO_CORRENTE=0;




//===================================================================
//	 ETHERNET E IOT
//===================================================================
static struct netif lpc_netif;
volatile uint8_t _f_CABLE_DISCONNECTED = 1;
volatile uint8_t _f_ETH_SEND = 0;
volatile uint8_t _c_ETH_TIMER = 0;

volatile uint16_t _c_ETH_Tempo_resposta = 0;
volatile uint16_t _c_ETH_Tempo_medio_resposta = 0;

struct{
	uint8_t _c_time_out_response;
	uint8_t _c_wait_send;
	uint8_t IOT_FRAME;
	uint8_t IOT_FRAME_SENDED;

	uint16_t _c_time_get_status;


	struct{
		uint8_t  _c_max_grupos;
		uint8_t  _c_index_grupo;

		uint16_t _c_max_receitas;
		uint16_t _c_index_receita;

		uint16_t _c_received;

		union{
			uint8_t buf_downlad;
			struct{
				uint8_t save:1;
				uint8_t delete:1;
				uint8_t replace:1;
				uint8_t load:1;
				uint8_t passos:1;
			}flags;
		};
	} download;

	struct{
		uint16_t _c_total_grupos;
		uint16_t _c_total_receitas;

		uint16_t _c_index_grupo;
		uint16_t _c_index_receita;
		uint16_t _c_index_receitas;
		uint16_t _c_aux_grupo;
		uint16_t _c_aux_receita;
	} upload;


	union{
		uint8_t buf_backup;
		struct{
			uint8_t save_sd:1;
			uint8_t send_sd:1;
			uint8_t del_register_sd:1;

		}backup;
	};

	union{
		uint32_t buf_flags;
		struct {
			uint32_t cable_disconnected:1;

			uint32_t need_upload:1;
			uint32_t info_upload:1;
			uint32_t end_upload_grp:1;
			uint32_t end_upload_rec:1;
			uint32_t end_upload_conf:1;
			uint32_t end_upload:1;

			uint32_t need_download:1;
			uint32_t info_download:1;
			uint32_t end_download_grp:1;
			uint32_t end_download_rec:1;
			uint32_t end_download_conf:1;
			uint32_t end_download:1;

			uint32_t send_frame:1;
			uint32_t send_success:1;

			uint32_t next_grupo:1;

			uint32_t lock:1;
		} flags;
	};

	union{
		uint8_t buf_load;
		struct{
			uint8_t grupos:1;
			uint8_t receitas:1;
			uint8_t totais:1;
			uint8_t done:1;
			uint8_t end:1;
		} load;
	};

	uint8_t _c_timer_task;
	union {
		uint8_t buf_task;
		struct{
			uint8_t run:1;
			uint8_t running:1;
			uint8_t start:1;

		}task;
	};

}IOT_MANAGER;

volatile uint16_t IOT_TIME_ERROR = 0;
volatile uint16_t IOT_SENDED = 0;
volatile uint8_t IOT_ERROR = 0;
volatile uint8_t IOT_CHANGE_IP_CONFIG = 0;
volatile uint8_t IOT_TIMER_CHANGE_IP = 20;
volatile uint16_t IOT_WAIT_CLOSE_ALL_CONNECTIONS = 0;
enum{
	SIOT_CABLE_DISCONECTED,
	SIOT_WAIT_IP,
	SIOT_ERRO_SERVER,
	SIOT_ERRO_DNS,
	SIOT_CONECTION_FAILED,
	SIOT_ALL_OK
};
volatile uint8_t IOT_LINE_PASS = 0;

volatile char server_url[100]		__attribute__ ((section (".TEMP_APP")));
volatile uint16_t server_port = 80;
volatile char frame[1*1024*1024]		__attribute__ ((section (".TEMP_APP")));
volatile char json_body[1*1024*1024] __attribute__ ((section (".TEMP_APP")));
volatile char sd_backup[1*1024*1024] __attribute__ ((section (".TEMP_APP")));
volatile char response[1*1024*1024]	__attribute__ ((section (".TEMP_APP")));

volatile s_Lista IOT_GRUPOS[4] __attribute__ ((section (".TEMP_APP")));		//LISTA TEMPORARIA
volatile s_Lista IOT_LISTA;			//LISTA TEMPORARIA
volatile s_RecBravo IOT_RECEITA;	//RECEITA EM EXECUCAO OU EM EDICAO

char json_variable[20];
char json_value[40];

char IP_used[20];
char GATWAY_used[20];
char MASK_used[20];
char DNS_used[20];



enum{
	IOT_IDDLE,

	IOT_AUTHENTICATE,
	IOT_RESET,

	IOT_GET_STATUS,

	IOT_UPLOAD_INFO,
	IOT_UPLOAD_GRUPO,
	IOT_UPLOAD_RECEITA,
	IOT_UPLOAD_CONFIG,
	IOT_UPLOAD_FIM,

	IOT_DOWNLOAD_INFO,
	IOT_DOWNLOAD_GRUPO,
	IOT_DOWNLOAD_RECEITA,
	IOT_DOWNLOAD_CONFIG,
	IOT_DOWNLOAD_FIM,

	IOT_TAGO
};

typedef struct{
	struct{
		char TOKEN[37];
		char PINCODE[5];
		uint32_t VERSAO_SERVER;
		uint32_t VERSAO_LOCAL;
	} server;
	union{
		uint8_t buf;
		struct{
			uint8_t AUTENTICATE:1;
			uint8_t RESET:1;
			uint8_t	Updated:1;
		};
	};
} t_IOT_CONF;

t_IOT_CONF IOT_Config;

//===================================================================
//	 OPERAÇÃO
//===================================================================
#define ON  1
#define OFF 0
/*	0x00 - OPERCAO NORMAL
 *  0x01 - PORTA ABERTA
 * 	0x02 - OPERACAO PAUSADA
 * 	0x04 - FIM OPERACAO
 * 	0x08 - ERRO SENSOR
 * 	0x10 - ERRO NET
 * 	0x20 -
 * 	0x40 - NAO EXECUTAR NADA
 * 	0x80 - FORA DA OPERACAO
 */
volatile uint8_t CTRL_OPERACAO=0x00;
volatile uint8_t LAST_CTRL_OPERACAO=0x80;		//ATUALIZADA SEMPRE NO FIM DA CTRL_SYS
volatile uint8_t LAST_CTRL_OPERACAO_tmp=0x80;	//USADO NA TELA APENAS PARA ATUALIZACAO DE ICONES
volatile uint8_t _c_conexao=0;					//NUMERO DE CONEXOES COM O MODULO
volatile uint8_t _f_Motor=0x00;
volatile uint8_t _c_Motor_Reversao=0;
volatile uint8_t _f_Motor_Sentido=0;
volatile uint8_t _f_RstMsgExecucao=0;
volatile uint8_t _f_AguardaCorrente_Mag=0;
volatile uint8_t _c_AguardaCorrente_Mag=0;

union{
		uint16_t total;
		struct{
			uint16_t Erro:1;		//0-Alerta 1-Erro
			uint16_t CAM:1;		//0-sem erro 1-erro
			uint16_t IR:1;		//0-sem erro 1-erro
			uint16_t Painel:2;	//0-sem erro 1-erro 2-alerta
			uint16_t INV:2;		//0-sem erro 1-erro 2-alerta
			uint16_t CAN:1;		//0-sem erro 1-erro
			uint16_t MAG:1;		//0-sem erro 1-erro
		};
}ERROS;
volatile uint8_t _f_FALHA_SENSORES=0;			//0-CAMARA 1-IR
volatile uint16_t Last_Erro_Total=0;
volatile uint8_t _f_SalvaErro=0;				//FLAG QUE VERIFICA SE HÁ ERRO/ALERTA PARA SER SALVO
volatile uint8_t _c_SalvaErro=0;				//CONTADOR PARA SALVAR OS ERROS/ALERTAS
//volatile uint8_t _f_VelocidadeMotor=0x00;

#define INV_FREQ_MOTOR 		0x000003F8 		//1 a 8-Porcentagem da frequencia do motor que será enviado ao inversor via Modbus
#define INV_SENTIDO_MOTOR 	0x00000004
#define INV_LIGA_MOTOR 		0x00000002
#define INV_SET_HABILITA	0x00000400		//0-Habilita o uso da porta MODBUS com inversor   0-Desabilitado   1-Habilitado
#define INV_MODELO_INVERSOR	0x00001800		//00- Inversor Honeywell	01- Inversor WEG  10- Reservado	11- Reservado

volatile uint32_t INVERSOR=0x00;



volatile uint8_t _c_Ciclo_Mag=0;
#define MAX_CICLO_MAG 300
const volatile uint8_t POTENCIA_MAG[11] = {30,24,22,20,17,13,10,8,6,2,0};
//#define MAX_CICLO_MAG 100
//const volatile uint8_t POTENCIA_MAG[11] = {10,9,8,7,6,5,4,3,2,1,0};

#define MAX_CICLO_AQ 100
volatile uint8_t _c_Ciclo_Aquec=0;

#define MAX_GRUPOS		16
#define MAX_RECEITAS	64

volatile uint8_t  _f_Grupo_ID_Sel 	= 0;
volatile uint8_t  _f_Botao_click	= 0;
volatile uint8_t  _f_Info_Passo 	= 0;	//FLAG QUE INDICA QUE HA INFORMACAO NO PASSO
volatile uint8_t  _f_Erro_Exe	 	= 0;	//FLAG QUE INDICA QUE HA ERRO NA EXECUCAO
volatile uint8_t  _f_Update_Itm		= 0;	//FLAG UTILIZADA PARA NAO ATUALIZAR ITENS JÁ DESENHADOS PARA OTIMIZAR PROCESSAMENTO (ZERADO TODA VEZ ScrLoad)
volatile uint8_t  _f_Rec_Carregada	= 0;	//1-INDICA QUE OS PARAMETROS DA RECEITA JA FORAM CARREGADOS PARA EXECUTAR
volatile uint8_t  _f_Botoes_Fim_Exe	= 0;	//INDICA QUAL BOTAO FOI PRESSIONADO 1-AQUECER/DOURAR 2-DOURAR 3-AQUECER 4-REPETIR
volatile uint8_t  _f_tick_error		= 0;
volatile uint8_t  _f_Ordenar 		= 0;
volatile uint8_t  CTRL_UPDATE		= 0;
volatile uint8_t  LAST_CTRL_UPDATE	= 0;
volatile uint8_t  _f_Progress_USB	= 0;
volatile uint8_t  _f_WatchDog		= 0;	//INDICA QUE A PLACA FOI REINICIADA POR WATCHDOG
volatile uint8_t  _f_WatchDogFeed	= 0x00;	//0x01-GUI 0x02-CTRLSYS 0x04-USB 0x08-LED
volatile uint8_t  _c_TimeOFFPower   = 0;	//TEMPO PARA A POWER DESLIGAR E REINICIAR APOS ATUALIZACAO
volatile uint8_t  _c_TimeTempPainel = 0;	//TEMPO PARA LEITURA TEMP PAINEL
volatile uint8_t  _c_TimeMenuInicial= 60;	//TEMPO NO MENU INICIAL PARA IR PARA PRE-AQUECIMENTO
volatile uint8_t  _f_Temperatura_Pre= 1;	//1-Usa grupo de pré, 0-usa temperatura setada

volatile uint16_t _v_TempPainel		= 0;
volatile uint16_t _f_info_total_bytes=0;
volatile uint16_t _f_Rec_ID_Sel 	= 0;
volatile uint16_t _f_Ico_ID_Sel 	= 0;
volatile uint16_t _f_Passo_Rec_Sel 	= 0;
volatile uint16_t _f_Tmp_ID_Sel 	= 0;
volatile uint16_t _f_Max_Progress	= 0;
volatile uint16_t _f_Last_Progress	= 0;
volatile uint16_t _f_Progress		= 0;
volatile uint16_t _c_Filtro_Seg		= 0;	//CONTADOR DE FILTRO QUANDO ATINGE 1MIN SALVA NO CONTADOR
volatile uint16_t _c_Filtro_Min		= 0;	//CONTADOR DE FILTRO QUANDO ATINGE 10MIN SALVA NA EEPROM



//===================================================================
//	 TELAS
//===================================================================
enum{// DEFINES DO BUZZER
	BUZ_DESLIGADO,
	BUZ_PULSANTE,
	BUZ_LIGADO,
};
enum{//	DEFINES DE TELAS
	ST_DESLIGADO,
	ST_ERRO,
	ST_MENU_PRINCIPAL,
	ST_EXECUCAO_PRINCIPAL,
	ST_HOME,
	ST_RECEITAS,
	ST_FAVORITOS,						//TELA COM AS RECEITAS DE FAVORITOS
	ST_PRE_AQUECIMENTO,
	ST_PRE_AQUEC_GRP,
	ST_SEL_PRE_AQUECIMENTO,
	ST_GRUPOS,
	ST_DESENVOLVIMENTO,
	ST_EXE_LANCHES,
	ST_EXE_BREAKFAST,
	ST_EXE_FINALIZACAO,
	ST_CONFIGURACOES,
	ST_CONF_EMBTECH,					//TELA DE PARAMETROS DA EMBTECH
	ST_CONFIGURACOES_FABRICANTE,
	ST_TESTAR_PASSO_CONFIRMA,			//TELA DE CONFIRMACAO QUE IRA EXECUTAR APENAS O PASSO SELECIONADO
	ST_TESTAR_PASSO,					//TELA DE EXECUCAO PERSONALIZADA PARA TESTAR PASSO
	ST_TESTAR_CARGAS,
	ST_TESTE_INVERSOR,
	ST_LER_ENTRADAS,
	ST_ALTERAR_SENHA,
	ST_NOVA_SENHA_USER,
	ST_NOVA_SENHA_FAB,
	ST_CONFIRMA_SENHA_USER_USER,
	ST_CONFIRMA_SENHA_USER,
	ST_CONFIRMA_SENHA_FAB,
	ST_TEMP_MAX_MIN,
	ST_CONTADORES,
	ST_CONT_FALHAS,
	ST_CONT_RECEITAS,
	ST_CONT_FILTROS,
	ST_CONT_LIMPEZAS,
	ST_DADOS_DO_FORNO,
	ST_CONTROLE_MOTOR,
	ST_VEL_PRE_AQUE,
	ST_CONF_PRE_AQUEC,
	ST_CONF_DATA_HORA,
	ST_FORMATO_DATA,
	ST_CONF_INTERNET,
	ST_CONF_ALT_IDIOMA,
	ST_CONF_TEMP_ATUAL,
	ST_CONF_CELSIUS_FAHREN,
	ST_CONF_EDITAR_ACESSOS,
	ST_CONF_INFO_FABRICANTE,
	ST_CONF_VOLUME,
	ST_CONF_SAC,
	ST_CONF_RESTAURAR_FABRICA,
	ST_CONF_RESTAURAR_FABRICA_PROCESS,	//PROCESSAMENTO DA RESTAURACAO
	ST_USB_REMOVIDO,
	ST_USB_MENU,						//OPCOES DE IMPORTAR EXPORTAR E ATUALIZAR SISTEMA
	ST_USB_UPDATE_FIRMWARE_INSERIR,		//AGUARDA A INSERCAO DO USB PARA INICIAR O PROCESSO DE ATUALIZACAO
	ST_USB_IMPORTAR_INSERIR,			//AGUARDA A INSERCAO DO USB PARA INICIAR O PROCESSO
	ST_USB_EXPORTAR_INSERIR,			//AGUARDA A INSERCAO DO USB PARA INICIAR O PROCESSO
	ST_USB_IMPORTAR_MENU,				//MENU DE IMPORTACAO
	ST_USB_EXPORTAR_MENU,				//MENU DE EXPORTACAO
	ST_USB_IMPORTAR_TUDO,				//IMPORTA RECEITAS, GRUPOS E CONFIG APAGANDO O QUE ESTA NO CARTAO
	ST_USB_EXPORTAR_TUDO,				//EXPORTA RECEITAS, GRUPOS E CONFIG APAGANDO O QUE ESTA NO CARTAO
	ST_USB_IMPORTAR_RECEITAS,			//IMPORTA RECEITAS E GRUPOS APAGANDO O QUE ESTA NO CARTAO
	ST_USB_EXPORTAR_RECEITAS,			//EXPORTA RECEITAS E GRUPOS APAGANDO O QUE ESTA NO USB
	ST_USB_IMPORTAR_GRUPOS,				//IMPORTA RECEITAS E GRUPOS APAGANDO O QUE ESTA NO CARTAO
	ST_USB_EXPORTAR_GRUPOS,				//EXPORTA RECEITAS E GRUPOS APAGANDO O QUE ESTA NO CARTAO
	ST_USB_IMPORTAR_CONF_USER,			//IMPORTA AS CONFIGURACOES DO USUARIO SALVAS NO USB
	ST_USB_EXPORTAR_CONF_USER,			//EXPORTA AS CONFIGURACOES DO USUARIO SALVAS NO USB
	ST_USB_IMPORTAR_FALHA,				//FALHA DURANTE O PROCESSO DE IMPORTACAO
	ST_USB_EXPORTAR_FALHA,				//FALHA DURANTE O PROCESSO DE EXPORTACAO
	ST_USB_UPDATE_FIRMWARE,				//STATUS ONDE OCORRE A ATUALIZACAO DO FIRMWARE DO EQUIPAMENTO
	ST_USB_UPDATE_FIRMWARE_ERRO,		//TELA DE ERRO NA ATUALIZACAO DO FIRMWARE EXIBE O CODIGO DE ERRO
	ST_CONF_ADD_SEL_GRUPO,				//TELA QUE EXIBE OS GRUPOS E O BOTAO DE CRIAR NOVO GRUPO
	ST_CONF_ADD_SEL_RECEITA,			//TELA PARA ADICIONAR E SELECIONAR RECEITA
	ST_CONF_EDT_REC_SEL_GRUPO,			//TELA QUE EXIBE OS GRUPOS PARA EDICAO DE RECEITA
	ST_CONF_MENU_EDT_GRUPO,				//TELA COM AS OPCOES DE EDITAR O GRUPO
	ST_CONF_MENU_EDT_RECEITA,			//MENU PARA EDITAR RECEITA
	ST_CONF_EDT_AQ_DOU_RECEITA,			//TELA EDITAR AQUECER DOURAR MAIS RECEITA
	ST_CONF_ADD_NOME_GRUPO,				//TELA PARA ADICIONAR NOME AO GRUPO
	ST_CONF_ADD_NOME_RECEITA,			//TELA PARA ADICIONAR NOME A RECEITA
	ST_CONF_EDT_NOME_GRUPO,				//TELA COM O TECLADO PARA EDITAR O NOME DO GRUPO
	ST_CONF_EDT_NOME_RECEITA,			//TELA COM O TECLADO PARA EDITAR O NOME DO GRUPO
	ST_CONF_NOME_EXISTENTE,
	ST_CONF_DEL_OP_GRUPO,				//TELA COM OPCOES DE APAGAR RECEITAS DO GRUPO (SELECIONAR OU APAGAR TODAS)
	ST_CONF_DEL_CONFIRMA_GRUPO_RECS,	//TELA PARA DE CONFIRMACAO APAGAR RECEITAS DO GRUPO
	ST_CONF_DEL_CONFIRMA_RECEITA,		//TELA PARA DE CONFIRMACAO APAGAR RECEITA
	ST_CONF_DEL_CONFIRMA_SEL_RECEITAS,	//TELA PARA DE CONFIRMACAO APAGAR RECEITAS SELECIONADAS
	ST_CONF_DEL_CONFIRMA_GRUPO,			//TELA PARA DE CONFIRMACAO APAGAR GRUPO
	ST_CONF_DEL_SEL_RECEITAS,			//TELA COM AS RECEITAS PARA SELECAO DE DELETE
	ST_CONF_DELETANDO_GRUPO,			//TELA PARA APAGAR GRUPO
	ST_CONF_DELETANDO_RECEITA,			//TELA PARA APAGAR RECEITA
	ST_CONF_DELETANDO_REC_SEL_GRUPO,	//TELA PARA APAGAR SELECAO RECEITAS DO GRUPO
	ST_CONF_DELETANDO_REC_GRUPO,		//TELA PARA APAGAR TODAS RECEITAS DO GRUPO
	ST_CONF_ORDENAR_OP_GRUPO,			//TELA COM OPCOES DE ORDENAR GRUPO
	ST_CONF_ORDENAR_OP_RECEITA,			//TELA COM OPCOES DE ORDENAR RECEITA
	ST_CONF_ORDENAR_POS_GRUPO,			//TELA PARA SELECIONAR A NOVA POSICAO DO GRUPO
	ST_CONF_ORDENAR_POS_RECEITA,		//TELA PARA SELECIONAR A NOVA POSICAO DA RECEITA
	ST_CONF_SEL_IMG_GRUPO,				//TELA PARA SELECIONAR IMAGEM DO GRUPO
	ST_CONF_SEL_IMG_RECEITA,			//TELA PARA SELECIONAR IMAGEM DA RECEITA
	ST_CONF_PASSOS_RECEITA,				//TELA COM OS PASSOS DA RECEITA
	ST_CONF_EDT_PASSO_RECEITA,			//TELA DE EDITAR PASSO DA RECEITA
	ST_CONF_ADD_INFO_PASSO,				//TELA ADICIONAR INFORMACAO AO PASSO DA RECEITA
	ST_CONF_EDT_PARAM_PASSO_RECEITA,	//TELA COM OS PARAMETROS PARA O PASSO DA RECEITA
	ST_CONF_EDT_TEMPERATURA_GRUPO,		//EXIBE AS DUAS TEMPERATURAS POSSIVEIS PARA O GRUPO
	ST_CONF_SAIR_PROGRAMACAO,
	ST_CONF_EDT_TENSAO,
	ST_CONFIGURACOES_RECEITAS,
	ST_EXE_FINALIZACAO_BREAKFAST,
	ST_MODO_MANUAL_AJ,
	ST_MODO_MANUAL_OPER,
	PP_TECLADO_NUM,
	ST_HIGIENIZACAO,
	ST_USB_IMPORTAR_TENSAO,
	ST_USB_EXPORTAR_TENSAO,
	ST_USB_IMPORTAR_TENSAO_INSERIR,
	ST_USB_EXPORTAR_TENSAO_INSERIR,
	ST_USB_IMPORTAR_TENSAO_FALHA,
	ST_USB_EXPORTAR_TENSAO_FALHA,
};

//====================================
//	DEFINES DE POP UP
//====================================
#define PP_TEMPERATURA 		1
#define PP_TEMPO 			2
#define PP_TURBINA 			3
#define PP_MAGNETRON		4
#define PP_TEMP_LASTRO  	5
#define PP_CONFIG	 		6
#define PP_AQUECER			7
#define PP_DOURAR			8
#define PP_AQUECER_DOURAR	9
#define PP_TEMP_TETO		10
#define PP_AJUSTE_DATA		11
#define PP_AJUSTE_HORAS		12
#define PP_VEL_MOTOR		13
#define PP_NUM_SERIE		14
#define PP_DATA_FAB			15
#define PP_IP				16
#define PP_MASCARA			17
#define PP_GATEWAY			18
#define PP_DNS				19
#define PP_TESTAR_CARGAS    20
#define PP_TEMP_PRE_1		21
#define PP_TEMP_PRE_2		22
#define PP_TEMPO_PRE		23
#define PP_BUTTON_1			24
#define PP_BUTTON_2			25


//====================================
//	DEFINES DE CORES DA APLICACAO
//====================================
#define GUI_PADRAO_BLUE   0x00E9C82D


#define _COR_LARANJA   			0x003486F5
#define _COR_LARANJA_ESCURO		0x001038C4
#define _COR_LARANJA_FOSCO		0x003686F5

#define _COR_CINZA_ESCURO		0x009A9896
#define _COR_VERMELHO			0x003732ED
#define _COR_CINZA_CLARO		0x00D5D3D2		//R:210 G:211 B:213
#define _COR_BRANCO				0x00FEFEFE

#define _COR_AZUL   			0x00E9C82D
#define _COR_AZUL_CLARO			0X00F4D26E
#define _COR_AZUL_ESCURO		0x00D97D48

#define _COR_VERDE_ESCURO		0x0052B84E
#define _COR_VERDE_FOSCO		0x0054A314
#define _COR_VERDE_FORTE		0x0054BE56
#define _COR_VERDE_MUSGO		0x009BA381
#define _COR_VERDE_LIMAO		0x0022D3AC

#define _COR_AZUL_FORTE			0X00E9A70D   	//R:13 G:167 B:233
#define _COR_AZUL_CLARO			0X00F4D26E
#define _COR_AZUL_ESCURO		0x00D97D48		//R:72 G:125 B:217
#define _COR_AZUL_FOSCO			0X00D98F74
#define _COR_AZUL_BEM_CLARO		0X00D4C800
#define _COR_AZUL   			0x00E9C82D   	//R:45 G:200 B:233

#define _COR_ROXO				0X00E39A7E
#define _COR_ROXO_CLARO			0X00CC95A6

#define _COR_MARRON				0X0094A9AB

#define _COR_ROSA				0X0092ACF8

#define _COR_AMARELO			0x0012F2FF
#define _COR_AMARELO_ESCURO		0x003AA7D4
#define _COR_AMARELO_FOSCO		0X007CDFFF

#define _COR_DE_PELE			0X006CA0C6
#define _COR_VINHO				0X006A647B
#define _COR_PRETO				0X00000000

#define _COR_R168_G207_B69		(69<<16)|(207<<8)|(168)
#define _COR_R92_G198_B208		(208<<16)|(198<<8)|(92)
#define _COR_R72_G136_B123		(123<<16)|(136<<8)|(72)


#define _COR_R19_G145_B225		(225<<16)|(145<<8)|(19)		//R:19 G:145 B:225
#define _COR_R77_G77_B77		(77<<16)|(77<<8)|(77)		//R:77 G:77 B:77
#define _COR_R157_G211_B175		(175<<16)|(211<<8)|(157)	//R:157 G:211 B:175
#define _COR_R200_G197_B226		(226<<16)|(197<<8)|(200)	//R:200 G:197 B:226
#define _COR_R145_G216_B247		(247<<16)|(216<<8)|(145)	//R:145 G:216 B:247
#define _COR_R248_G237_B132		(132<<16)|(237<<8)|(248) 	//R:248 G:237 B:132
#define _COR_R150_G183_B207		(207<<16)|(183<<8)|(150) 	//R:150 G:183 B:207
#define _COR_R139_G209_B223		(223<<16)|(209<<8)|(139) 	//R:139 G:209 B:223
#define _COR_R184_G225_B209		(209<<16)|(225<<8)|(184) 	//R:184 G:225 B:209
#define _COR_R226_G177_B110		(110<<16)|(177<<8)|(226)    //R:226 G:177 B:110
#define _COR_R180_G194_B207		(207<<16)|(194<<8)|(180)    //R:180 G:194 B:207
#define _COR_R143_G207_B242		(242<<16)|(207<<8)|(143)    //R:143 G:207 B:242
#define _COR_R110_G210_B244		(244<<16)|(210<<8)|(110)
#define _COR_R157_G211_B175		(175<<16)|(211<<8)|(157)
#define _COR_R168_G207_B69		(69<<16)|(207<<8)|(168)
#define _COR_R253_G228_B0		(0<<16)|(228<<8)|(253)
#define _COR_R114_G201_B107		(107<<16)|(201<<8)|(114)
#define _COR_R212_G167_B58		(58<<16)|(167<<8)|(212)
#define _COR_R245_G134_B52		(52<<16)|(134<<8)|(245)
#define _COR_R255_G242_B18		(18<<16)|(242<<8)|(255)
#define _COR_R250_G236_B90		(90<<16)|(236<<8)|(250)
#define _COR_R77_G77_B77		(77<<16)|(77<<8)|(77)
#define _COR_R210_G211_B213		(213<<16)|(211<<8)|(210)
#define _COR_R0_G168_B89		(89<<16)|(168<<8)|(0)
#define _COR_R167_G196_B210		(210<<16)|(196<<8)|(167)
#define _COR_R237_G50_B55		(55<<16)|(50<<8)|(237)
#define _COR_R198_G214_B162		(162<<16)|(214<<8)|(198)
#define _COR_R198_G214_B162		(162<<16)|(214<<8)|(198)
#define _COR_R196_G217_B231		(231<<16)|(217<<8)|(196)
#define _COR_R219_G196_B181		(181<<16)|(196<<8)|(219)
#define _COR_R238_G204_B224		(224<<16)|(204<<8)|(238)
#define _COR_R0_G173_B166		(166<<16)|(173<<8)|(0)
#define _COR_R237_G46_B56		(56<<16)|(46<<8)|(237)
#define _COR_R0_G175_B239		(239<<16)|(175<<8)|(0)
#define _COR_R114_G115_B118     (118<<16)|(115<<8)|(114)
#define _COR_R161_G63_B0     	(0<<16)|(63<<8)|(161)
#define _COR_R86_G190_B84     	(86<<16)|(190<<8)|(84)



volatile GUI_MEMDEV_Handle hMem2 			__attribute__ ((section (".TEMP_APP")));

//====================================
//	FONTES ESPECIFICAS DA APLICACAO
//====================================
//static GUI_FONT SIF_FONT_ICON_99x99 		__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_EXO2_REGULAR_24 	__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_EXO2_REGULAR_30 	__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_EXO2_REGULAR_36 	__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_EXO2_REGULAR_42 	__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_EXO2_REGULAR_48 	__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_EXO2_REGULAR_55 	__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_EXO2_REGULAR_65 	__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_EXO2_REGULAR_70 	__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_EXO2_BOLD_31 		__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_DIVERSOS_60 		__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_DIVERSOS_44 		__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_DIVERSOS_27 		__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_ICON_160x99 		__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_ICON_68x68 		__attribute__ ((section (".TEMP_APP")));
//static GUI_FONT SIF_FONT_ICON_180x125 		__attribute__ ((section (".TEMP_APP")));


static GUI_FONT SIF_FONT_EXO2_REGULAR_24;
static GUI_FONT SIF_FONT_EXO2_REGULAR_30;
static GUI_FONT SIF_FONT_EXO2_REGULAR_36;
static GUI_FONT SIF_FONT_EXO2_REGULAR_42;
static GUI_FONT SIF_FONT_EXO2_REGULAR_48;
static GUI_FONT SIF_FONT_EXO2_REGULAR_55;
static GUI_FONT SIF_FONT_EXO2_REGULAR_65;
static GUI_FONT SIF_FONT_EXO2_REGULAR_70;
static GUI_FONT SIF_FONT_EXO2_BOLD_31;

static GUI_FONT SIF_FONT_ICON_99x99;
static GUI_FONT SIF_FONT_ICON_160x99;
static GUI_FONT SIF_FONT_ICON_68x68;
static GUI_FONT SIF_FONT_ICON_180x125;

#define FONTE_PADRAO GUI_Font20_1


char buf_SIF_FONT_EXO2_REGULAR_24[1024*64] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_EXO2_REGULAR_30[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_EXO2_REGULAR_36[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_EXO2_REGULAR_42[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_EXO2_REGULAR_48[1024*256] __attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_EXO2_REGULAR_55[1024*256] __attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_EXO2_REGULAR_65[1024*256] __attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_EXO2_REGULAR_70[1024*512] __attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_EXO2_BOLD_31[1024*128] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp

char buf_SIF_FONT_ICON_99x99[1024*512] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_ICON_160x99[1024*512] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_ICON_68x68[1024*32] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp
char buf_SIF_FONT_ICON_180x125[1024*256] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp


//volatile char buf_SIF_FONT[4*1024*512] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp

//volatile char buf_SIF_FONT_ICON_99x99[1024*512] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_EXO2_REGULAR_24[1024*64] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_EXO2_REGULAR_30[1024*128] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_EXO2_REGULAR_36[1024*128] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_EXO2_REGULAR_42[1024*128] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_EXO2_REGULAR_48[1024*256] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_EXO2_REGULAR_55[1024*256] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_EXO2_REGULAR_65[1024*256] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_EXO2_REGULAR_70[1024*512] 	__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_EXO2_BOLD_31[1024*128] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_DIVERSOS_60[1024*32] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_DIVERSOS_44[1024*32] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_DIVERSOS_27[1024*16] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_ICON_160x99[1024*512] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_ICON_68x68[1024*32] 			__attribute__ ((section (".TEMP_APP"))); //RAM temp
//volatile char buf_SIF_FONT_ICON_180x125[1024*256] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp
//
//
//volatile char buf_SIF_FONT[4*1024*512] 		__attribute__ ((section (".TEMP_APP"))); //RAM temp

//====================================
//	ICONES DA FONTE
//====================================

//FONT ICO 99X99
#define ICO_HIGIENIZACAO 		"\xE1\x80\x80"
#define ICO_PRE_AQUECIMENTO 	"\xE1\x80\x81"
#define ICO_CONFIGURACOES 		"\xE1\x80\x82"
#define ICO_DESLIGAR 			"\xE1\x80\x83"

#define ICO_MODO_TESTE 			"\xE1\x80\x84"
#define ICO_FAVORITOS 			"\xE1\x80\x85"		//0x1005
#define ICO_SEM_FAVORITO		"\xE1\x80\xB7"		//0x1037
#define ICO_ALERTA 				"\xE1\x80\x86"
#define ICO_ALERTA_FUNDO		"\xE1\x80\x87"

#define ICO_PLAY 				"\xE1\x80\x88"
#define ICO_PAUSE 				"\xE1\x80\x89"
#define ICO_STOP 				"\xE1\x80\x8A"
#define ICO_OPCOES 				"\xE1\x80\x8B"

#define ICO_AQUECER_MAIS_METADE "\xE1\x80\x8C"
#define ICO_DOURAR_MAIS_METADE 	"\xE1\x80\x8D"
#define ICO_DOURAR_MAIS 		"\xE1\x80\x8E"
#define ICO_AQUECER_MAIS 		"\xE1\x80\x8F"
#define ICO_REPETIR_REC 		"\xE1\x80\x90"

#define ICO_ORDENAR_INICIO		"\xE1\x80\x92"
#define ICO_ORDENAR_FIM			"\xE1\x80\x93"
#define ICO_ORDENAR_ESCOLHER	"\xE1\x80\x94"

#define ICO_EDITA_GRUPO 		"\xE1\x80\x95"
#define ICO_EDITA_RECEITA 		"\xE1\x80\x96"
#define ICO_USB 				"\xE1\x80\x97"
#define ICO_DATA_HORA 			"\xE1\x80\x99"
#define ICO_CELSIUS_FAHREN 		"\xE1\x80\x9A"

#define ICO_IDIOMA 				"\xE1\x80\x98"

#define ICO_EDITA_NOME 			"\xE1\x80\xAD"
#define ICO_EDITA_IMAGEM 		"\xE1\x80\xAE"
#define ICO_EDITAR_RECEITAS 	"\xE1\x80\x96"
#define ICO_APAGAR_RECEITAS 	"\xE1\x80\xAF"


#define ICO_APAGAR_GRUPOS 		"\xE1\x80\xB0"		//0x1030
#define ICO_ORDENAR 			"\xE1\x80\xB1"
#define ICO_OCULTAR 			"\xE1\x80\xB2"
#define ICO_EXIBIR 				"\xE1\x80\xB3"
#define ICO_SELECIONAR			"\xE1\x80\xB4"		//0x1034
#define ICO_CONTINUAR			"\xE1\x80\xB5"		//0x1035
#define ICO_VOLTAR				"\xE1\x80\xB6"		//0x1036


#define ICO_ALTERA_SENHA 		"\xE1\x80\x9B"
#define ICO_EDITA_ACESSOS 		"\xE1\x80\x9C"
#define ICO_INFOS_FABRICANTE 	"\xE1\x80\x9D"
#define ICO_VOLUME 				"\xE1\x80\x9E"

#define ICO_MANUAL 				"\xE1\x80\x9F"
#define ICO_PROGRAM_ON_OFF 		"\xE1\x80\xA0"
#define ICO_INTERNET 			"\xE1\x80\xA1"
#define ICO_SAC 				"\xE1\x80\xA2"
#define ICO_RESTAURAR_CONFS 	"\xE1\x80\xA3"

#define ICO_TESTAR_CARGAS 		"\xE1\x80\xA4"
#define ICO_HISTORICO_FALHAS	"\xE1\x80\xA5"
#define ICO_LER_ENTRADAS 		"\xE1\x80\xA6"

#define ICO_TEMP_MAX_MIN 		"\xE1\x80\xA7"
#define ICO_CONTADORES 			"\xE1\x80\xA8"
#define ICO_DADOS_FORNO 		"\xE1\x80\xA9"
#define ICO_CONTROLE_MOTOR 		"\xE1\x80\xAA"

#define ICO_CONFIG_VELO 		"\xE1\x80\xAB"
#define ICO_VEL_PRE_AQUEC 		"\xE1\x80\xAC"

#define _ICO_BACKSPACE 			"\xE1\x80\x8F"
#define _ICO_OK		 			"\xE1\x80\x89"		//0x1009
#define _ICO_ESPACO 			"\xE1\x80\x8A"		//0x100A

#define ICO_CIRC_EXTERNO 		"\xE1\x80\x8D"
#define ICO_CIRC_INTERNO 		"\xE1\x80\x8E"

#define ICO_SALVAR_PASSO 		"\xE1\x80\x91"

#define ICO_BUZZER 				"\xE1\x80\xBC"
#define ICO_VENT_MET 			"\xE1\x80\xBD"
#define ICO_STIRRER_MET 		"\xE1\x80\xBE"
#define ICO_TESTE_MICROONDAS 	"\xE1\x80\x8F"
#define ICO_CONTATOR_MET 		"\xE1\x81\x80"
#define ICO_RELE_MET 			"\xE1\x81\x81"
#define ICO_RESI_SUPERIOR 		"\xE1\x81\x82"
#define ICO_RESI_INF 			"\xE1\x81\x83"

#define ICO_ON 					"\xE1\x81\x84"
#define ICO_OFF 				"\xE1\x81\x85"

#define ICO_99X99_1038			"\xE1\x80\xB8"
#define ICO_99X99_1039			"\xE1\x80\xB9"
#define ICO_99X99_103B			"\xE1\x80\xBB"
#define ICO_99X99_103A			"\xE1\x80\xBA"
#define ICO_99X99_1008			"\xE1\x80\x88"
#define ICO_99X99_1011			"\xE1\x80\x91"
#define ICO_99X99_1061			"\xE1\x81\xA1"
#define ICO_99X99_1062			"\xE1\x81\xA2"
#define ICO_USUARIO				"\xE1\x81\x86"
#define ICO_FABRICANTE			"\xE1\x81\x87"
#define ICO_99X99_101E			"\xE1\x80\x9E"

#define ICO_CONT_FALHAS			"\xE1\x80\xA5"
#define ICO_CONT_RECEITAS		"\xE1\x81\x88"
#define ICO_CONT_FILTROS		"\xE1\x81\x89"
#define ICO_CONT_LIMPEZAS		"\xE1\x81\x8A"

#define ICO_68X68_1005			"\xE1\x80\x85"
#define ICO_68X68_1018			"\xE1\x80\x98"
#define ICO_68X68_1015			"\xE1\x80\x95"
#define ICO_68X68_101F			"\xE1\x80\x9F"

#define ICO_BRAVO				"\xE1\x80\x94"
#define ICO_CHEFF				"\xE1\x80\x95"

#define ICO_WEG					"\xE1\x81\x8B"
#define ICO_HONEYWELL			"\xE1\x81\x8C"

#define ICO_INGLES				"\xE1\x81\x8D"
#define ICO_ESPANHOL			"\xE1\x81\x8E"
#define ICO_FRANCES				"\xE1\x81\x8F"
#define ICO_PORTUGUES			"\xE1\x81\x90"

#define ICO_180X125_100B		"\xE1\x80\x8B"
#define ICO_CELSIUS				"\xE1\x81\x9B"
#define ICO_FAHRENHEIT			"\xE1\x81\x9C"

#define ICO_VOL_BAIXO			"\xE1\x81\x9D"
#define ICO_VOL_MEDIO			"\xE1\x81\x9E"
#define ICO_VOL_ALTO			"\xE1\x81\x9F"


#define ICO_99X99_1060			"\xE1\x81\xA0"
#define ICO_99X99_1063			"\xE1\x81\xA3"
#define ICO_99X99_1064			"\xE1\x81\xA4"
#define ICO_99X99_1065			"\xE1\x81\xA5"
#define ICO_99X99_1066			"\xE1\x81\xA6"
#define ICO_99X99_1067			"\xE1\x81\xA7"
#define ICO_99X99_1068			"\xE1\x81\xA8"
#define ICO_99X99_1069			"\xE1\x81\xA9"
#define ICO_99X99_106A			"\xE1\x81\xAA"
#define ICO_99X99_106B			"\xE1\x81\xAB"
#define ICO_99X99_106C			"\xE1\x81\xAC"
#define ICO_99X99_106D			"\xE1\x81\xAD"
#define ICO_99X99_106E			"\xE1\x81\xAE"
#define ICO_99X99_106F			"\xE1\x81\xAF"
#define ICO_99X99_1007			"\xE1\x80\x87"


//FONT DIVERSOS 60
#define ICO_HOME 				"\xE1\x80\x80"
#define ICO_GRUPOS 				"\xE1\x80\x81"
#define ICO_SETA_FRENTE 		"\xE1\x80\x82"
#define ICO_SETA_ATRAS 			"\xE1\x80\x83"
#define ICO_SETA_VOLTAR			"\xE1\x80\x97"
#define ICO_TERMOMETRO 			"\xE1\x80\x86"

#define ICO_ALERTA_FRENTE		"\xE1\x80\x84"

#define ICO_TERMOMETRO_PEQ 		"\xE1\x80\x85"
#define ICO_TERMOMETRO_RETO 	"\xE1\x80\x87"
#define ICO_INFORMACOES 		"\xE1\x80\x88"

#define ICO_RELOGIO 			"\xE1\x80\x8C"

#define ICO_TURBINA 			"\xE1\x80\x89"
#define ICO_MICROONDAS 			"\xE1\x80\x8A"
#define ICO_LASTRO 				"\xE1\x80\x8B"

#define ICO_DATA				"\xE1\x81\x91"
#define ICO_HORA				"\xE1\x81\x92"
#define ICO_METADE_DATA			"\xE1\x81\x93"
#define ICO_METADE_HORA			"\xE1\x81\x94"

#define ICO_DATA_EUR			"\xE1\x81\x95"
#define ICO_DATA_USA			"\xE1\x81\x96"
#define ICO_DATA_ISO			"\xE1\x81\x97"

#define ICO_HORA_24				"\xE1\x81\x98"
#define ICO_HORA_12				"\xE1\x81\x99"

#define ICO_OK					"\xE1\x81\x9A"
#define ICO_RESFRIAR			"\xE1\x80\x99"

#define ICO_68x68_1020			"\xE1\x80\xA0"



//FONT ICO 160X99
#define ICO_BREAKFAST 			"\xE1\x80\x80"		//0x1000
#define ICO_SUBS 				"\xE1\x80\x81"		//0x1001
#define ICO_PIZZA 				"\xE1\x80\x82"		//0x1002

#define ICO_BAKERY 				"\xE1\x80\x83"		//0x1003
#define ICO_REFEICAO 			"\xE1\x80\x84"		//0x1004
#define ICO_TEX_MEX 			"\xE1\x80\x85"		//0x1005
#define ICO_ASAS 				"\xE1\x80\x86"		//0x1006
#define ICO_GRATINADOS 			"\xE1\x80\x87"		//0x1007

#define ICO_MAIS	 			"\xE1\x80\x88"		//0x1008

#define ICO_LANCHE_01 			"\xE2\x84\x80"
#define ICO_LANCHE_02 			"\xE2\x84\x81"
#define ICO_LANCHE_03 			"\xE2\x84\x82"
#define ICO_LANCHE_04 			"\xE2\x84\x83"
#define ICO_LANCHE_05 			"\xE2\x84\x84"
#define ICO_LANCHE_06 			"\xE2\x84\x85"
#define ICO_LANCHE_07 			"\xE2\x84\x86"
#define ICO_LANCHE_08 			"\xE2\x84\x87"


#define ICO_BREAKFAST_01 		"\xE2\x80\x80"
#define ICO_BREAKFAST_02 		"\xE2\x80\x81"
#define ICO_BREAKFAST_03 		"\xE2\x80\x82"
#define ICO_BREAKFAST_04 		"\xE2\x80\x83"
#define ICO_BREAKFAST_05 		"\xE2\x80\x84"
#define ICO_BREAKFAST_06 		"\xE2\x80\x85"
#define ICO_BREAKFAST_07 		"\xE2\x80\x86"
#define ICO_BREAKFAST_08 		"\xE2\x80\x87"

#define ICO_PASSO_ATIVO			"\xE1\x80\x8B"
#define ICO_PASSO_DESATIVADO	"\xE1\x80\x93"


#define ICO_ZERO				"\xE1\x84\xA3"

//FONT REC 180x125
#define ICO_SUBS_REC 			"\xE1\x80\x82"

#define ICO_MODO_TESTE_REC 		"\xE1\x80\x88"




#define MAX_CHAR_TIT_GRUPO			22


volatile uint8_t _f_ScrLoad=0;
volatile uint8_t _f_STATUS=0;
volatile uint8_t _f_PAGE=0;
volatile uint8_t _f_MAX_PAGE=0;
volatile uint8_t _f_STATUS_BUZZER=0;
volatile uint16_t _f_buzzer_erro_alerta=0;
volatile uint8_t LAST_STATUS=0;
volatile uint8_t _f_LastSTATUS=0;
volatile uint8_t _f_beep=0;
volatile uint8_t _c_time_beep_int=0;
volatile uint8_t _c_timeOutBuzzerExt=0;
volatile uint8_t _c_timeOutTouch=0;
volatile uint8_t _c_TecladoPos=0;
volatile uint8_t _c_TecladoMax=0;
volatile uint8_t _f_op_Touch=0;
volatile uint8_t _c_Change_Scr=0;
//volatile uint8_t _c_TimeOut_Buzzer=0;


volatile char 	_tmp_char[512];
volatile char 	_s_TecladoCaracter=0;
volatile char   _s_TecladoDigitado[30];

volatile char TemperaturaConvertida[10];
volatile char DiaDaSemana[15];

volatile const char SenhaFabricante[6] = {'4','5','9','3','8','1'};
volatile const char SenhaUser[6] = {'4','5','6','7','8','9'};
volatile const char SenhaTemp[6] = {'4','5','6','7','8','9'};
volatile const char SenhaEmbtech[6] = {'0','0','0','5','9','7'};
volatile const char SenhaPontos[6] = {'2','0','0','0','0','0'};
volatile const char SenhaTensaoExport[6] = {'2','0','0','0','0','1'};
volatile const char SenhaTensaoImport[6] = {'2','0','0','0','0','2'};
volatile const char SenhaBypassPre[6] = {'3','0','0','0','0','0'};
volatile const char SenhaBypassPre2[6] = {'3','0','0','0','0','1'};


volatile const char UN_TEMP[3][4] = {"ºC","ºF","K"};


#define MAX_TIME_USB_CONNECT	10				//TEMPO EM SEGUNDOS

volatile uint8_t _f_SD_Connet=0;
volatile uint8_t _f_USB_file_calib=0;			//ARQUIVO PARA RECALIBRAR A PLACA
volatile uint8_t _f_USB_Connet=0;
volatile uint8_t _f_USB_Process=0;
volatile uint8_t _f_USB_Test=0;					//STATUS DO TESTE DO USB 0-FORA DO TESTE 1-AGUARDANDO PENDRIVE 2-GRAVANDO 3-LENDO 4-DELETANDO 5-OK 6-ERRO
volatile uint8_t _c_USB_timeout_connect=0;
volatile uint8_t _f_IMG_Back=0;

volatile uint32_t timerCntms=0;

#define FATOR_TEMP		18
#define FATOR_TEMP_F	10

#define TEC_ALPHA	0
#define TEC_NUM		1

char __text[100];

volatile uint8_t _f_last_PP_teclado=0;//utilizado para configurar qual funcao é o teclado
volatile uint8_t _f_PP_teclado=0;//utilizado para configurar qual funcao é o teclado
volatile uint8_t _f_Last_PP_teclado=0;
volatile uint8_t _f_Clear_Text = 0;

volatile uint8_t _tmp_ram_BACK[256*1024] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE BACKGROUND

volatile uint8_t _tmp_ram_B0001[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0001
volatile uint8_t _tmp_ram_B0002[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0002
volatile uint8_t _tmp_ram_B0003[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0003
volatile uint8_t _tmp_ram_B0004[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0004
volatile uint8_t _tmp_ram_B0005[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0005
volatile uint8_t _tmp_ram_B0006[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0006
volatile uint8_t _tmp_ram_B0007[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0007
volatile uint8_t _tmp_ram_B0008[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0008
volatile uint8_t _tmp_ram_B0009[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0008
volatile uint8_t _tmp_ram_B0010[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0008
volatile uint8_t _tmp_ram_B0011[1024*128] __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE _tmp_ram_B0008
//volatile uint8_t *_tmp_ram_BACK __attribute__ ((section (".TEMP_APP"))); //RAM temp DA IMAGEM DE BACKGROUND
volatile char buffer[0x7D000] __attribute__ ((section (".TEMP_APP")));

//	FATFS
FATFS *fs_sd;
FATFS *fs_usb;


//===================================================================
//	 CONFIGURACOES
//===================================================================
#define ADDRESS_IOT_CONFIG 24
#define ADDRESS_CONFIG 6
#define ADDRESS_EPROM  0
#define ADDRESS_RESET  2
volatile uint8_t _f_Reset_IHM = 0;
#define TEMPERATURA_MIN	30*FATOR_TEMP
typedef struct{	//	IHM CONFIG
		uint8_t 	Format;
		uint8_t		Version;				//VERSAO DA ESTRUTURA SALVA
		uint8_t 	Calib;
		uint8_t 	Demonstracao;
		uint16_t 	CALIB_SCREEN_X;
		uint16_t 	CALIB_SCREEN_Y;
		int32_t  	TEMPERATURA_PRE;
		uint32_t 	TEMPO_PRE;				//tempo de estabilizacao temperatura pre em seg
		uint8_t		VELOCIDADE_PRE;			//VELOCIDADE DO MOTOR PARA PRE-AQUECIMENTO
		uint64_t 	Serial;					//NUMERO DE SERIE DA PLACA
		uint8_t		Escala_Temperatura;		//UNIDADE DE MEDIDA PARA EXIBICAO DE TEMPERATURA 0-CELSIUS 1-FARENHEIT 2-KELVIN
		int32_t		TEMPERATURA_MAX;		//MAXIMA TEMPERATURA QUE O FORNO PODERA ATINGIR
		char		SENHA_USER[6];			//SENHA DO USUARIO
		char		SENHA_FAB[6];			//SENHA DO FABRICANTE
		int32_t  	TEMPERATURA_PRE_LASTRO;
		uint8_t 	IDIOMA;					//0-PORTUGUES 1-INGLES 2-ESPANHOL 3-FRANCES
		uint8_t 	CONTROLE_MOTOR;
		uint8_t		Formato_data;			//0 - ICO_DATA_EUR 1- ICO_DATA_USA 2 - ICO_DATA_ISO
		uint8_t		Formato_hora;			//0x00-24 horas 0x01-12 horas	0x10- 1-AM 0-PM
		char 		DATA_FABRICACAO[8];
		char		NUM_SERIE[20];
		uint16_t	VOLUME_BUZZER;
		uint32_t  	_c_WatchDog;
		uint8_t		ACESSOS;				//0x01-FAVORITOS 0x02-MODOTESTE 0x04-AQUECER/DOURAR 0x08-REPETIR
		uint8_t     MODELO;					//0x00-BRAVO 0x01-CHEF
		char 		IP[13];
		char 		GATWAY[13];
		char 		MASCARA[13];
		char 		DNS[13];
		uint32_t	ERROS[20];				//0-NET 1-PAINEL 2-SENSORES TEMP 3-INVERSOR
		uint32_t	CONTADORES[20];			//0-CONTADOR RECEITA
		uint32_t    TimePainel[4];			//0-65°C 1-75°C 2-85°C
		uint16_t	MaxTempPainel;			//Maxima temperatura registrada no painel
		int32_t  	TEMPERATURA_PRE_2;		//TEMPERATURA DE PRE DO GRUPO 2
		uint8_t		TENSAO_ENTRADA;			//0-208 1-220 2-230 3-240 //0-240V 1-208V
		uint8_t		SENSOR_CORRENTE;		//0-DESATIVADO 1-ATIVADO
		uint8_t		_f_FDA;
		uint8_t 	_f_Pontos_Toque;		//1-habilita exibir toques na tela 0-desabilita
		uint8_t 	_f_DHCP;
}s_Config;

struct{
		uint8_t  week;
		uint8_t  seg;
		uint8_t  min;
		uint8_t  hora;
		uint8_t  dia;
		uint8_t  mes;
		uint16_t  ano;
}DATA_HORA;

volatile s_Config Config;// __attribute__ ((section (".TEMP_APP"), zero_init));
volatile s_Config Last_Config;
volatile uint8_t _c_save_eeprom=0;//Salva a Config
volatile uint8_t _c_save_Serial=0;

volatile uint8_t _c_save_IOT_config = 0;



struct{						//SERIAL_PLACA
	char Flag_Update_Firmware;			//Flag para renomear as pastas após a atualização do firmware
	uint32_t SN_LOTE;  //Serial Number: LOTE da IHM
	uint32_t SN_SERIE; //Serial Number: SÉRIE da IHM
	uint32_t CONT_UPDATE_FIRMWARE;		//CONTADOR DE TOTAL DE ATUALIZACOES
}Eeprom __attribute__ ((section (".TEMP_APP")));

uint8_t _c_send_Analise = 0;
uint32_t Analise_CAN = 0x00001000;
uint32_t Analise_CAN_read = 0;
uint8_t _c_send_Analise_Registros = 0;
uint32_t Analise_Registros = 0x00001000;
uint32_t Analise_Registros_read = 0;
struct{
	uint32_t _c_Erro_Inversor;
	uint32_t _c_Erro_INV_frame;
	uint32_t _c_Erro_INV_timeOut;
	uint32_t _c_Erro_INV_alarme;
	uint32_t _c_Erro_INV_falha;

	uint32_t _c_Erro_WatchDog;

	uint32_t _c_Erro_MAG;

	int32_t  Max_TempBoard;		//máxima temperatura na placa
	uint32_t _c_Time_65_board;
	uint32_t _c_Time_75_board;
	uint32_t _c_Time_85_board;

	uint32_t _c_ErroSensor[10];
	uint32_t _c_FalhaSensor[10];

	uint32_t _CAN_BusOFF;
} sAnalise_Registros;

struct{
	uint32_t CAN_STAT;
	uint32_t CAN_ERROR;
} sAnalise_CAN;

//===================================================================
//	 PWM
//===================================================================
volatile uint8_t _f_send_pwm = 0;
char _csv_Value[5];



//===================================================================
//	 PROTOTIPOS DE FUNCOES
//===================================================================
void _load_passo_receita(void);
char Delete_Receitas_SD(void);
char Delete_Receitas_USB(void);
char week_day(uint8_t dia, uint8_t mes, uint16_t ano);
void _check_pre_grupo(void);

#endif /* INC_APP_H_ */


