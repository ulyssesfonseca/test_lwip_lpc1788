/**********************************************************************
 * $Id$		Files_tipos.h			2017-05-02
 * @file	Files_tipos.h
 * @brief	Tipos de arquivos utilizados no projeto
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 *
 *
 **********************************************************************/
/*********************************************************************
 * 	ERRATA
 *
 *
 *
 **********************************************************************/


#ifndef __FILES_TIPOS_OLD_H__
#define __FILES_TIPOS_OLD_H__


// VERSAO ANTIGA DO NACIONAL E EXPORT 0x0001
typedef struct{
    uint8_t     path[50];
    uint8_t     titulo[30];             //TITULO DA CATEGORIA
    uint8_t     ico[30];                //ICONE UTILIZADO NA CATEGORIA
    uint8_t     ativo;                  //SE O GRUPO ESTA ATIVO
    uint8_t     favorito;               //SE O GRUPO ESTA ATIVO
    uint8_t     delete; 	            //FLAG QUE DEFINE QUE DEVERA SER DELETADO ESTE ITEM
    uint32_t	FLAGS;					//0-EXPORT 0x02-ICONE_PRIMEIRA_LETRA (era uint32_t, virou 16 pela TEMPERATURA_PRE)
    int32_t		TEMPERATURA_PRE;		//TEMPERATURA DE PRE DO GRUPO
    uint32_t	FUTURO[10];				//SE PRECISAR DE NOVAS VARIAVEIS REMOVER DAQUI MANTENDO O TAMANHO DO STRUCT
}s_Lista_EXPORT_0x0001;

typedef struct{
    uint8_t     path[50];
    uint8_t     titulo[30];             //TITULO DA CATEGORIA
    uint8_t     ico[30];                //ICONE UTILIZADO NA CATEGORIA
    uint8_t     ativo;                  //SE O GRUPO ESTA ATIVO
    uint8_t     favorito;               //SE O GRUPO ESTA ATIVO
    uint8_t     delete; 	            //FLAG QUE DEFINE QUE DEVERA SER DELETADO ESTE ITEM
    uint32_t	FLAGS;					//0-EXPORT 0x02-ICONE_PRIMEIRA_LETRA
}s_Lista_NACIONAL_0x0001;


//SALVAR O NOME DA RECEITA COM O NUMERO DE POSICIONAMENTO
//ESTRUTURA 0x0001
typedef struct{
	uint16_t    version;                //VERSAO DA ESTRUTURA
	uint16_t    type;                   //TIPO DA ESTRUTURA PARA NAO SER LIDA EM EQUIP. DIFERENTE
	uint8_t     titulo[30];             //TITULO DA RECEITA
	uint8_t     categoria[30];          //NOME DA CATEGORIA ATUAL
	uint8_t     ico[50];                //ICONE DA RECEITA ATUAL
	uint8_t     favorito;               //MARCA A RECEITA COMO FAVORITA
	uint8_t     total_passos;           //TOTAL DE PASSOS DA RECEITA
	uint8_t     dourar;                 //TEMPO PARA DOURAR 0-DESABILITADO
	uint8_t     aquecer;	            //TEMPO PARA COZINHAR 0-DESABILITADO
	uint8_t     dourar_aquecer;         //TEMPO PARA DOURAR/COZINHAR 0-DESABILITADO
} s_RecCabecalho_0x0001;   //NO MAXIMO 256 BYTES
typedef struct{
	uint8_t     passo_ativo;            //SE O PASSO ESTA ATIVO
	uint8_t     instrucao[100];         //INSTRUCAO DO PASSO
	uint8_t     vel_motor;              //PERCENTUAL VELOCIDADE DO MOTOR
	uint8_t     percent_microondas;     //PERCENTUAL DE MICROONDAS
	uint8_t     percent_teto_lastro;    //PERCENTUAL DE TEMPERATURA DE TETO LASTRO
	int32_t     temperatura;            //VALOR DE TEMPERATURA °C*18
	uint32_t    tempo;                  //TEMPO DO PASSO
} s_RecPasso_0x0001;
typedef struct{
	s_RecCabecalho_0x0001 cabecalho;
	s_RecPasso_0x0001 passos[MAX_PASSOS_REC];
} s_RecBravo_0x0001;


typedef struct{	//	IHM CONFIG
		uint8_t 	Format;
		uint8_t		Version;				//VERSAO DA ESTRUTURA SALVA
		uint8_t 	Calib;
		uint8_t 	Demonstracao;
		uint16_t 	CALIB_SCREEN_X;
		uint16_t 	CALIB_SCREEN_Y;
		int32_t  	TEMPERATURA_PRE;
		uint32_t 	TEMPO_PRE;				//tempo de estabilizacao temperatura pre em seg
		uint8_t		VELOCIDADE_PRE;			//VELOCIDADE DO MOTOR PARA PRE-AQUECIMENTO
		uint64_t 	Serial;					//NUMERO DE SERIE DA PLACA
		uint8_t		Escala_Temperatura;		//UNIDADE DE MEDIDA PARA EXIBICAO DE TEMPERATURA 0-CELSIUS 1-FARENHEIT 2-KELVIN
		int32_t		TEMPERATURA_MAX;		//MAXIMA TEMPERATURA QUE O FORNO PODERA ATINGIR
		char		SENHA_USER[6];			//SENHA DO USUARIO
		char		SENHA_FAB[6];			//SENHA DO FABRICANTE
		int32_t  	TEMPERATURA_PRE_LASTRO;
		uint8_t 	IDIOMA;					//0-PORTUGUES 1-INGLES 2-ESPANHOL 3-FRANCES
		uint8_t 	CONTROLE_MOTOR;
		uint8_t		Formato_data;			//0 - ICO_DATA_EUR 1- ICO_DATA_USA 2 - ICO_DATA_ISO
		uint8_t		Formato_hora;			//0- 24 horas 1- 12 horas
		char 		DATA_FABRICACAO[8];
		char		NUM_SERIE[20];
		uint16_t	VOLUME_BUZZER;
		uint32_t  	_c_WatchDog;
		uint8_t		ACESSOS;				//0x01-FAVORITOS 0x02-MODOTESTE 0x04-AQUECER/DOURAR 0x08-REPETIR
		uint8_t     MODELO;					//0x00-BRAVO 0x01-CHEF
		char 		IP[13];
		char 		GATWAY[13];
		char 		MASCARA[13];
		char 		DNS[13];
		uint32_t	ERROS[20];				//0-NET 1-PAINEL 2-SENSORES TEMP 3-INVERSOR
		uint32_t	CONTADORES[20];			//0-CONTADOR RECEITA
		uint32_t    TimePainel[4];			//0-65°C 1-75°C 2-85°C
		uint16_t	MaxTempPainel;			//Maxima temperatura registrada no painel
}s_Config_NACIONAL;


#endif
