/*
 * @brief LWIP FreeRTOS HTTP Webserver example
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2014
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#include "tcp_conn.h"


volatile char tmp_buff[1*1024*1024]		__attribute__ ((section (".TEMP_APP")));
char eth_buff[1400];
uint16_t eth_len = 0;
uint8_t wait_send = 0;
uint8_t error_send = 0;

uint8_t tcp_free = 1;


void load_buff_send(char *pBuff, uint32_t pLen, uint32_t pStart){
	uint16_t i = 0;
	memset(&eth_buff, 0x00, sizeof(eth_buff));
	eth_len = 0;

	for(i=0; i<1400; i++){
		eth_buff[i] = 0x00;
		if((eth_len+pStart) < pLen){
			eth_len += 1;
			eth_buff[i] = pBuff[i+pStart];
		}
		else{
			return ;
		}
	}

}

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/* Callback for TCPIP thread to indicate TCPIP init is done */
void tcpip_init_done_signal(void *arg)
{
	/* Tell main thread TCP/IP init is done */
	*(s32_t *) arg = 1;
}

void tcp_close_con(struct tcp_pcb *pcb){

	tcp_err(pcb, NULL);
	tcp_arg(pcb, NULL);
	tcp_recv(pcb, NULL);
	tcp_sent(pcb, NULL);
	tcp_close(pcb);
	tcp_free = 1;
}

void tcp_end(struct tcp_pcb *pcb, u8_t *state){
	tcp_err(pcb, NULL);
	tcp_recv(pcb, NULL);
	tcp_sent(pcb, NULL);
	tcp_poll(pcb, NULL, 0);
	mem_free(state);
	//mem_free(pcb);	//teste
}

uint32_t tcp_send_packet(void *arg, struct tcp_pcb *pcb, err_t err){
	static uint16_t count = 0;
	uint32_t len = 0;
	err_t erro;

	count++;

	len = strlen(frame);
	if(tcp_sndbuf(pcb) < len){
		len = tcp_sndbuf(pcb);
		DEBUGOUT("Frame exced maximum lenght\r\n");
	}

	//	pbuf_free(testpcb);

	//	do{
	erro = tcp_write(pcb, frame, len, 0);

	if (erro) {
		DEBUGOUT("ERROR: Code: %d (tcp_send_packet :: tcp_write)\n", erro);
		error_SERVER(erro);
		return 1;
	}

	erro = 0;

	return 0;
}

err_t tcpSendPoll(void *arg, struct tcp_pcb *pcb){
	u8_t *state = (u8_t *)arg;
	uint32_t len = 0;

	DEBUGOUT("tcpSendPoll\n");

	if((*state & 1) == 0){
		len = strlen(frame);
		DEBUGOUT("Sending %d\n%s\n",len,frame);
		if(tcp_write(pcb, frame, len, 0) == ERR_OK)
			*state |= 1;
	}

	if(*state == 3){
		DEBUGOUT("Closing...\n");
		if(tcp_close(pcb) == ERR_OK)
			tcp_end(pcb, state);
	}

	return ERR_OK;
}

err_t tcpRecvCallback(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err){
	uint16_t i = 0, j=0;
	uint8_t save=0;
	u8_t *state = (u8_t *)arg;
	uint16_t len = 0;
	//char string[100];

	DEBUGOUT("Data recieved.\n");
    if (p == NULL) {
    	if(*state == 255){
    		DEBUGOUT("The remote host closed the connection.\n");
    		DEBUGOUT("Now I'm closing the connection.\n");
    		tcp_end(tpcb, state);
    		error_SERVER(ERR_ABRT);
    		error_send = 1;
    	}
    	else{
    		*state |= 2;
    	}
    } else {
    	len = p->tot_len+1;
    	DEBUGOUT("Number of pbufs %d size %d\n", pbuf_clen(p),len);
    	if(len > sizeof(tmp_buff)){
    		snprintf((char*)&tmp_buff,sizeof(tmp_buff),p->payload);
    	}
    	else{
    		snprintf((char*)&tmp_buff,len,p->payload);
    	}

    	for(i=0; i<strlen(tmp_buff); i++){
    		if(tmp_buff[i]=='{'){save=1;}
    		if(save && j<sizeof(response)){response[j] = tmp_buff[i]; j++;}
    		//if(tmp_buff[i]=='}'){save=0;}
    	}
    	response[j]=0x00;
    	//DEBUGOUT("Contents of pbuf %s\n", p->payload);
    	response_SERVER();	//EMIT SIGNAL
    	//DEBUGOUT("Contents of pbuf %s\n", p->payload);

    	pbuf_free(p);
    	tcp_recved(tpcb, len);
//    	tcp_close_con(testpcb);
    }

    return ERR_OK;
}

err_t tcpSendCallback(void *arg, struct tcp_pcb *tpcb, u16_t len){

	DEBUGOUT("Data tcpSendCallback.\n");
	DEBUGOUT("Send %d\n",len);
	wait_send = 0;
	cbk_TCP_SEND(len);

    return 0;
}

void tcpErrorHandler(void *arg, err_t err){
	DEBUGOUT("Data tcpErrorHandler.\n");

    DEBUGOUT("The remote host closed the connection.\n");
    DEBUGOUT("Now I'm closing the connection.\n");
    //tcp_close_con(testpcb);

    mem_free(arg);

    error_SERVER(err);
    error_send = 1;

}
/* connection established callback, err is unused and only return 0 */
err_t connectCallback(void *arg, struct tcp_pcb *tpcb, err_t err){
	DEBUGOUT("Connection Established.\n");
	DEBUGOUT("Now sending a packet\n");
    tcp_send_packet(arg, tpcb, err);

    return ERR_OK;
}

err_t tcp_setup(const char* _server, const uint16_t pPort){
	//    uint32_t data = 0xdeadbeef;
	static int8_t passo=ERR_CONN;
	u8_t *state;
	/* create an ip */
	struct tcp_pcb *pcb;
	static struct ip_addr ip;
	err_t err;
	//    static char tmp_buff[16];
	uint16_t mPort = pPort;

	//    struct netconn *conn1 = NULL;
	//    int rc1, rc2;


	if (passo!=ERR_OK){
		passo = dns_gethostbyname(_server, &ip, NULL, NULL);
	}


	switch(passo){

	case ERR_OK:
		// FORCA SERVIDOR
//		IP4_ADDR(&ip, 192, 168, 222, 192);
//		mPort = 8000;


		if((state = mem_malloc(1)) == NULL)
			return ERR_MEM;
		*state = 1;

		if((pcb = tcp_new()) == NULL){
			mem_free(state);
			return ERR_MEM;
		}

		tcp_arg(pcb, state);
		tcp_err(pcb, tcpErrorHandler);
		tcp_recv(pcb, tcpRecvCallback);
		tcp_sent(pcb, tcpSendCallback);
		tcp_poll(pcb, tcpSendPoll, 10);
		tcp_setprio(pcb, TCP_PRIO_NORMAL);
		/* now connect */
		err = tcp_connect(pcb, &ip, mPort, connectCallback);
		if(err != ERR_OK){
			mem_free(state);
			tcp_abort(pcb);
		}

		return err;

		break;

	case ERR_INPROGRESS:
		// need to ask, will return data via callback
		DEBUGOUT("DNS GET NAME HOST IN PROGRESS\n");
		break;


	default:
		// bad arguments in function call
		DEBUGOUT("DNS GET NAME HOST ERROR\n");
		break;

	}

	return passo;

}


/**
 * @}
 */
