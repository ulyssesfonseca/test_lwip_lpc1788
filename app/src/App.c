/*
 * App.c
 *
 *  Created on: 26 de jan de 2017
 *      Author: Ulysses
 */


/*	CONVENSAO DO CODIGO
 *
 *	//! - INDICA QUE É UM TESTE E NÃO DEVE PERMANECER
 *	//!!- INDICA QUE HÁ UMA LOGICA NÃO IMPLEMENTADA
 *
 */

//https://community.nxp.com/thread/423722
//https://www.utwente.nl/en/eemcs/dacs/assignments/completed/bachelor/reports/B-assignment_vanderPloeg.pdf
//https://android.googlesource.com/kernel/lk/+/1d0df6996457273367e6d9d9d08bf6adb0fc9b65/lib/lwip/doc/rawapi.txt
//http://lwip.100.n7.nabble.com/bug-20237-pbuf-memory-corruption-tt14505.html
//https://lists.gnu.org/archive/html/lwip-users/2015-02/msg00060.html

#include "App.h"

#include "board.h"


static void task_MainGUI( void *pvParameters );
static void task_CTRLSys( void *pvParameters );
static void task_USB( void *pvParameters );
static void task_Led01( void *pvParameters );
static void task_IOT(void *pvParameters);
static void task_Ms( void *pvParameters );

xTaskHandle xtask_IOT = NULL;

volatile uint16_t _c_fps_gui=0;
volatile uint16_t _c_fps_CtrlSys=0;
volatile uint16_t _c_fps_Led=0;
volatile uint16_t _c_fps_USB=0;

volatile uint16_t cont_GUI=0;
volatile uint16_t cont_LED01=0;
volatile uint16_t cont_CTRLSys=0;
volatile uint16_t cont_USB=0;
volatile uint16_t cont_Touch=0;
volatile uint8_t _imprimeBarra=0;
volatile uint8_t _erro_anterior=0;
volatile uint8_t _aguarda_confirma=0;
volatile uint8_t _last_bot=0;
volatile uint8_t _last_grupos=0;
volatile uint8_t _last_ST = 0;

volatile uint16_t _c_timeOutStatus = 0;
volatile uint16_t _c_lasttimeOutStatus = 0;

volatile uint8_t _TMP_VALOR = 0;

volatile uint32_t size_Tux_png = 0;

ip_addr_t ipaddr, netmask, gw, dns;


static __attribute__ ((used,section(".TEMP_APP"))) uint8_t heap_sram_lower[50*1024];
static HeapRegion_t xHeapRegions[] =
{
    {
        &heap_sram_lower[0], sizeof(heap_sram_lower)
    },
    {
        NULL, 0 // << Terminates the array.
    }
};




void GPIO_Set(uint8_t pin, uint8_t state){
	if(!state){
		LPC_GPIO2->CLR |= (1<<pin);
	}
	else{
		LPC_GPIO2->SET |= (1<<pin);
	}
}

void Buz_Set(uint8_t pState){

}

uint64_t sd_free_space_bytes(void){
	DWORD fre_clust;
	uint8_t res=0;


	/* Get volume information and free clusters of drive 1 */
	res = f_getfree("0:", &fre_clust, &fs_sd);
	if (res){return 0;}

	return (uint64_t)((fs_sd->free_clst) * (fs_sd->csize));

}


volatile uint8_t print_log_5sec = 0;
//----------------------------------------------------------------------------
//	RELOGIO E RTC 1SEG
//----------------------------------------------------------------------------

void set_rtctime(void){
	RTC rtc;

	rtc.sec		= 0;
	rtc.min		= DATA_HORA.min;
	rtc.hour	= DATA_HORA.hora;
	rtc.mday	= DATA_HORA.dia;
	rtc.month	= DATA_HORA.mes;
	rtc.year	= DATA_HORA.ano;

	rtc_settime(&rtc);
}
void get_rtctime(void){
	RTC rtc;
	rtc_gettime(&rtc);

	DATA_HORA.seg = rtc.sec;
	DATA_HORA.min = rtc.min;
	DATA_HORA.hora= rtc.hour;
	DATA_HORA.dia = rtc.mday;
	DATA_HORA.mes = rtc.month;
	DATA_HORA.ano = rtc.year;
	DATA_HORA.week= week_day(DATA_HORA.dia, DATA_HORA.mes, DATA_HORA.ano);
}
/*
*Biblioteca para validacao de datas
*
*Retorna 0 caso a data seja correta
*e 1 caso a data esteja incorreta
*
*Angelito M. Goulart
*Setembro/2009
*
*/
char valida_data(uint8_t dia, uint8_t mes, uint16_t ano){
	if ((dia >= 1 && dia <= 31) && (mes >= 1 && mes <= 12) && (ano >= 1900 && ano <= 2100)) //verifica se os numeros sao validos
	{
		if ((dia == 29 && mes == 2) && ((ano % 4) == 0)) //verifica se o ano e bissexto
		{
			return 0;
		}
		if (dia <= 28 && mes == 2) //verifica o mes de feveireiro
		{
			return 0;
		}
		if ((dia <= 30) && (mes == 4 || mes == 6 || mes == 9 || mes == 11)) //verifica os meses de 30 dias
		{
			return 0;
		}
		if ((dia <=31) && (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes ==8 || mes == 10 || mes == 12)) //verifica os meses de 31 dias
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		return 1;
	}
}
char week_day(uint8_t dia, uint8_t mes, uint16_t ano){
	int adjustment, mm, yy;

	adjustment = (14 - mes) / 12;
	mm = mes + 12 * adjustment - 2;
	yy = ano - adjustment;
	return (dia + (13 * mm - 1) / 5 +
		yy + yy / 4 - yy / 100 + yy / 400) % 7;
}
char _retornaDia(uint8_t week){
	switch(week){
		case 0:{
			snprintf((char*)DiaDaSemana, sizeof(DiaDaSemana),TXT_DIA_0[ID_IDIOMA]);
		}break;

		case 1:{
			snprintf((char*)DiaDaSemana, sizeof(DiaDaSemana),TXT_DIA_1[ID_IDIOMA]);
		}break;

		case 2:{
			snprintf((char*)DiaDaSemana, sizeof(DiaDaSemana),TXT_DIA_2[ID_IDIOMA]);
		}break;

		case 3:{
			snprintf((char*)DiaDaSemana, sizeof(DiaDaSemana),TXT_DIA_3[ID_IDIOMA]);
		}break;

		case 4:{
			snprintf((char*)DiaDaSemana, sizeof(DiaDaSemana),TXT_DIA_4[ID_IDIOMA]);
		}break;

		case 5:{
			snprintf((char*)DiaDaSemana, sizeof(DiaDaSemana),TXT_DIA_5[ID_IDIOMA]);
		}break;

		case 6:{
			snprintf((char*)DiaDaSemana, sizeof(DiaDaSemana),TXT_DIA_6[ID_IDIOMA]);
		}break;
	}

	return 1;
}
/********************************************************************
 * Valida o formato da hora
 *
 *	@param
 *		uint8_t hora - hora
 *		uint8_t min	 - minuto
 *		uint8_t sec	 - segundo
 *
 * 	@return
 * 		0 - sucess
 * 		1 - error
 *
 ********************************************************************/
char valida_hora(uint8_t hora, uint8_t min, uint8_t sec){

	if(hora>23){return 1;}
	if(min>59){return 1;}
	if(sec>59){return 1;}

	return 0;
}
void RTC_IRQHandler(void){

	if (Chip_RTC_GetIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE)){

		_c_fps_gui=cont_GUI;
		_c_fps_CtrlSys = cont_CTRLSys;
		_c_fps_Led = cont_LED01;
		_c_fps_USB = cont_USB;
		cont_USB=0;
		cont_LED01=0;
		cont_GUI=0;
		cont_CTRLSys=0;

		if(print_log_5sec == 0){
			DEBUGOUT("%d %d %d %d %d %d",IOT_MANAGER._c_time_get_status, IOT_TIME_ERROR, IOT_SENDED, IOT_ERROR, IOT_MANAGER.IOT_FRAME, IOT_LINE_PASS);
			print_log_5sec = 5;
		}
		else{
			print_log_5sec --;
		}

		if(IOT_TIMER_CHANGE_IP > 0 ){ IOT_TIMER_CHANGE_IP--; if(IOT_TIMER_CHANGE_IP==0){IOT_CHANGE_IP_CONFIG=1;}}
		if(IOT_TIME_ERROR < 65000){IOT_TIME_ERROR++;}
		if(IOT_WAIT_CLOSE_ALL_CONNECTIONS>0){IOT_WAIT_CLOSE_ALL_CONNECTIONS--;}

		if(IOT_MANAGER._c_time_get_status > 0){IOT_MANAGER._c_time_get_status --;}
		if(IOT_MANAGER._c_timer_task > 0){IOT_MANAGER._c_timer_task--;}
		if(_c_save_IOT_config>1){_c_save_IOT_config--;}
		if(IOT_MANAGER._c_wait_send > 0){
			IOT_MANAGER._c_wait_send--;
		}
		if(IOT_MANAGER._c_time_out_response > 0){IOT_MANAGER._c_time_out_response--; if(IOT_MANAGER._c_time_out_response==0){IOT_MANAGER.backup.save_sd=1;}}
		if(IOT_MANAGER._c_time_out_response>0 && IOT_MANAGER._c_time_out_response%5){
			DEBUGOUT("%d %d\n",IOT_MANAGER._c_time_out_response,IOT_TIMER_CHANGE_IP);
		}

		if(_c_ETH_TIMER>0){_c_ETH_TIMER--; }
//DEBUGOUT("IOT_MANAGER._c_timer_task %d %d %d %d %d\r\n",IOT_MANAGER._c_timer_task,
//		IOT_MANAGER.task.running,
//		IOT_MANAGER.task.run,
//		IOT_MANAGER.task.start,
//		timerCntms);
		if(_c_send_Analise>0){_c_send_Analise--;}
		if(_c_send_Analise_Registros>0){_c_send_Analise_Registros--;}
		//=======================================================================================
		// EMBNET
		//=======================================================================================
		if(_f_MOD_connect&MASK_MOD_POWER){
			if(_c_Mod_Power_reset_time_out){_c_Mod_Power_reset_time_out--;}

//			//DETECCAO QUE O MODULO CAIU PARA OFFLINE
//			if(_c_Mod_Power_offLine && _f_Mod_Power_offLine){_c_Mod_Power_offLine--;}
//			if(_f_Mod_Power_offLine && !_c_Mod_Power_offLine){_f_MOD_connect &= ~MASK_MOD_POWER; CTRL_OPERACAO |= 0x10;}

			_c_gets_power<<=1;
			if(_c_gets_power>0x40){_c_gets_power = 0x01;}
			EMBNET_POWER.buffer = _c_gets_power;

			EMBNET_POWER.bits.STATUS = 1;
		}

		if(_c_MOD_config>0){_c_MOD_config--;}

		//DETECCAO QUE O MODULO CAIU PARA OFFLINE
		if(_c_Mod_Power_offLine && _f_Mod_Power_offLine){_c_Mod_Power_offLine--;}
#ifndef SIMULA_OPERACAO
		if(_f_Mod_Power_offLine && !_c_Mod_Power_offLine){_f_MOD_connect &= ~MASK_MOD_POWER; if(!(CTRL_OPERACAO&0x10)){_c_count_erro_CAN++;} CTRL_OPERACAO |= 0x10; _c_conexao=0;}
#else
		if(_f_Mod_Power_offLine && !_c_Mod_Power_offLine){_f_MOD_connect &= ~MASK_MOD_POWER; CTRL_OPERACAO |= 0x10; _c_conexao=0;}
//		_c_conexao=0x07;
		_f_simula_temp=1;
		if(POWER_DIG_OUT.bits.AQ_CAM){POWER.TEMP_1+=FATOR_TEMP*5;}else{POWER.TEMP_1-=FATOR_TEMP;}
		if(POWER_DIG_OUT.bits.AQ_RI){POWER.TEMP_2+=FATOR_TEMP*5;}else{POWER.TEMP_2-=FATOR_TEMP;}
		if(_f_simula_botao_porta>0){_f_simula_botao_porta--;}
#endif

		//=======================================================================================
		// OPERACAO
		//=======================================================================================
		if(!CTRL_OPERACAO || Config.Demonstracao == 1 || _f_STATUS == ST_HIGIENIZACAO){
			if(_f_Motor){
				_c_Motor_Reversao++;
			}

			if(_c_AtrasoPlay<3){_c_AtrasoPlay++;}			//ATRASO DE 3 SEGUNDOS PARA INICIAR O PROCESSO
			else if(SET_POINT.TEMPO > _c_TEMPO_CORRENTE /*&& ((!_f_AguardaCorrente_Mag && !_c_AguardaCorrente_Mag && SET_POINT.MICROONDAS>0) || SET_POINT.MICROONDAS==0 || Config.SENSOR_CORRENTE==0)*/){
				_c_TEMPO_CORRENTE++;
				if(_c_TEMPO_CORRENTE_PASSO){_c_TEMPO_CORRENTE_PASSO--;}
				UpdateScr=1;
			}

			if(_aguarda_confirma == 0 && _f_STATUS == ST_HIGIENIZACAO){
				_c_TEMPO_CORRENTE=0;
			}

		}

		if(_c_timeOutBuzzerExt){
			_c_timeOutBuzzerExt--;
			if(!_c_timeOutBuzzerExt){
				_f_STATUS_BUZZER = BUZ_DESLIGADO;
			}
		}

		if(!(CTRL_OPERACAO&0x01)){
			_c_Filtro_Seg++;
			if(_c_Filtro_Seg>60){
				_c_Filtro_Seg = 0;
				Config.CONTADORES[1]++;
				_c_Filtro_Min++;
				if(_c_Filtro_Min>10){_c_save_eeprom=3; _c_Filtro_Min=0;}
			}
		}

		//=======================================================================================
		// LOGICA DE TELAS
		//=======================================================================================

		if(_f_TimePreAque && _c_TimePreAque!=0){_c_TimePreAque--;}
		if(STATUS==ST_PRE_AQUECIMENTO || STATUS==ST_MENU_PRINCIPAL || STATUS==ST_GRUPOS || STATUS==ST_HIGIENIZACAO || STATUS==ST_PRE_AQUEC_GRP){UpdateScr=1;}

		if(_f_STATUS==ST_EXE_LANCHES || _f_STATUS==ST_MODO_MANUAL_OPER || _f_STATUS==ST_TESTAR_PASSO){
			if(_c_CONT > 0){//CONTADOR QUE ENQUANTO MAIOR QUE 0 EXIBE A MENSAGEM DA TEMPERATURA NA EXECUCAO
				_c_CONT --;
				UpdateScr=1;
			}
		}

		if(_f_STATUS==ST_LER_ENTRADAS){UpdateScr=1;}

		if(_f_STATUS==ST_TESTAR_CARGAS){
			if(_f_teste_carga_ON>0){_f_teste_carga_ON--;}
			if(_c_time_teste_carga<(MAX_TIME_TESTE_CARGA+5)){_c_time_teste_carga++;}
		}

#ifndef PLAY_FDA
		if(!_c_Change_Scr){
			if(_f_STATUS==ST_EXE_FINALIZACAO){_c_Change_Scr=10; _c_timeOutBuzzerExt=30;}
		}
		else{
			_c_Change_Scr--;
			if(!_c_Change_Scr){
				if(_f_STATUS==ST_EXE_FINALIZACAO){STATUS=ST_HOME; _f_PAGE=0;}
			}
		}
#endif

		if(_c_time_erro_Painel){_c_time_erro_Painel--;}//CONTADOR DISPARADO DEPOIS DA TEMPERATURA DO PAINEL PASSAR DE 80°C

		if(_c_time_erro_CAM){_c_time_erro_CAM--;}	//TIME OUT ERRO SENSOR CAMARA
		if(_c_time_erro_IR){_c_time_erro_IR--;}		//TIME OUT ERRO SENSOR INFERIOR
		if(_c_time_erro_init){_c_time_erro_init--;} //TIME OUT ERRO INICIALIZACAO PLACA

		if(_f_AguardaCorrente_Mag==0){ if(_c_AguardaCorrente_Mag>0){_c_AguardaCorrente_Mag--;}}else{_c_AguardaCorrente_Mag=5;}

		if(_c_TimeOFFPower){_c_TimeOFFPower--;}

		if(_c_TimeMenuInicial>0){_c_TimeMenuInicial--; if(!_c_TimeMenuInicial && _f_STATUS==ST_SEL_PRE_AQUECIMENTO){STATUS=ST_PRE_AQUECIMENTO;}}
		if(_f_STATUS!=ST_SEL_PRE_AQUECIMENTO && _f_STATUS!=ST_PRE_AQUECIMENTO && _f_STATUS!=ST_MENU_PRINCIPAL){_c_TimeMenuInicial=0; _f_USB_file_calib=1;}

		//=======================================================================================
		// TIME OUT USB
		//=======================================================================================
//		if(!Host_UsbConnect()){_f_USB_Connet=0;} //!

//		if(_f_USB_Connet && !Host_UsbConnect()){
//			if(_c_USB_timeout_connect>MAX_TIME_USB_CONNECT){
//				f_mount(0, "1:", 1);
//				STATUS = ST_USB_REMOVIDO;
//				vTaskDelete(xHandle_USB);
//
//				xTaskCreate(task_USB, (signed char *) "t_USB", configMINIMAL_STACK_SIZE*20, NULL, 4, &xHandle_USB);
//
//
//				_f_USB_Connet = 0;
//				_f_USB_Process = 0;
//			}
//			_c_USB_timeout_connect++;
//		}
//		else{
//			_c_USB_timeout_connect=0;
//		}

		//=======================================================================================
		// DEMONSTRAÇÃO
		//=======================================================================================
		if(Config.Demonstracao==1){
			if(SET_POINT.TEMPERATURA>POWER.TEMP_1){
				POWER.TEMP_1 +=18;
				POWER.TEMP_2 +=18;
				if(POWER.TEMP_1 > 536){
					POWER.TEMP_1 = 536;
					POWER.TEMP_2 = 536;
				}
			}
		}


		//=======================================================================================
		// BUSCA NO RTC O VALOR REAL
		//=======================================================================================
		get_rtctime();


		//=======================================================================================
		// CONTADORES DE TEMPO
		//=======================================================================================
		_c_SEG++;
		if(_c_SEG>59){_c_SEG=0;__c_min++;}
		if(__c_min>59){__c_min=0;__c_hora++;}
		if(_c_save_eeprom>1){_c_save_eeprom--;}
		if(_c_SalvaErro>0){_c_SalvaErro--;}

		if(_c_TimeTempPainel){_c_TimeTempPainel--;}

		Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE);
	}
}


volatile uint8_t _c_live = 0;


/*	ETHERNET
 */
void insert_json_body_TAGO(char* pVar, char* pValue){
	json_openContainer();
	json_insertStr("variable", pVar);
	json_insertStr("value", pValue);
	json_closeContainer();
}
void make_frame_IOT(void){
	uint8_t i = 0;

	if(IOT_MANAGER._c_time_out_response > 0 || IOT_MANAGER.flags.send_frame==1 || IOT_MANAGER.flags.lock == 1){return ;}
	IOT_MANAGER.flags.lock = 1;
	IOT_LINE_PASS = 0;
	/*IOT_Config.AUTENTICATE = 1;
	IOT_Config.RESET = 0;
	snprintf(&IOT_Config.server.TOKEN,sizeof(IOT_Config.server.TOKEN),"21MTU8BL-2UZJ-PLYT-Y9FR-ZKJMZUTNFCEC");*/
	//snprintf(&Config.NUM_SERIE,sizeof(Config.NUM_SERIE),"9575-1");

	if 		(IOT_Config.RESET == 1){
		IOT_MANAGER.IOT_FRAME = IOT_RESET; IOT_LINE_PASS=1;
	}
	else if	(IOT_Config.AUTENTICATE == 0 || IOT_Config.server.TOKEN[0] == 0x00){
		IOT_MANAGER.IOT_FRAME = IOT_AUTHENTICATE; IOT_LINE_PASS=2;
	}
	else if (IOT_MANAGER.flags.need_upload == 1){	//ALGO PARA UPLOAD
		if (IOT_MANAGER.flags.end_upload_grp != 1){
			IOT_MANAGER.IOT_FRAME = IOT_UPLOAD_GRUPO; IOT_LINE_PASS=3;
		}
		else if (IOT_MANAGER.flags.end_upload_rec != 1){
			IOT_MANAGER.IOT_FRAME = IOT_UPLOAD_RECEITA; IOT_LINE_PASS=4;
		}
//		else if (IOT_MANAGER.flags.end_upload_conf != 1){
//			IOT_MANAGER.IOT_FRAME = IOT_UPLOAD_CONFIG;
//		}
		else if(IOT_MANAGER.flags.end_upload != 1){
			IOT_MANAGER.IOT_FRAME = IOT_UPLOAD_FIM; IOT_LINE_PASS=5;
		}
	}
	else if (IOT_MANAGER.flags.info_upload==1){
		IOT_MANAGER.IOT_FRAME = IOT_UPLOAD_INFO; IOT_LINE_PASS=6;
	}
	else if(IOT_MANAGER.flags.need_download == 1){	//ALGO PARA DOWNLOAD
		if		(IOT_MANAGER.download.flags.save == 1){
			IOT_MANAGER.IOT_FRAME = IOT_IDDLE; IOT_LINE_PASS=7;
		}
		else if	(IOT_MANAGER.flags.info_download == 1){
			IOT_MANAGER.IOT_FRAME = IOT_DOWNLOAD_INFO; IOT_LINE_PASS=8;
		}
		else if (IOT_MANAGER.flags.end_download_grp != 1){
			IOT_MANAGER.IOT_FRAME = IOT_DOWNLOAD_GRUPO; IOT_LINE_PASS=9;
		}
		else if (IOT_MANAGER.flags.end_download_rec != 1){
			IOT_MANAGER.IOT_FRAME = IOT_DOWNLOAD_RECEITA; IOT_LINE_PASS=10;
		}
//		else if (IOT_MANAGER.flags.end_download_conf != 1){
//			IOT_MANAGER.IOT_FRAME = IOT_DOWNLOAD_CONFIG;
//		}
		else if (IOT_MANAGER.flags.end_download != 1){
			IOT_MANAGER.IOT_FRAME = IOT_DOWNLOAD_FIM; IOT_LINE_PASS=11;
		}
	}
	else{
		if(IOT_MANAGER._c_time_get_status == 0){
			IOT_MANAGER.IOT_FRAME = IOT_GET_STATUS; IOT_LINE_PASS=12;
		}
		else{
			IOT_MANAGER.IOT_FRAME = IOT_IDDLE;
		}
	}

	IOT_MANAGER.IOT_FRAME = IOT_TAGO;

	IOT_MANAGER.flags.send_frame = 1;
	IOT_MANAGER.flags.send_success = 0;

	switch(IOT_MANAGER.IOT_FRAME){
	case IOT_AUTHENTICATE:{
		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		json_clear();
		json_openContainer();
		json_insertStr("dvcpd", "E76XJBP5-48UL-EB1T-MA6Z-BA3DAPMVGTCQ");
		json_insertStr("numse", Config.NUM_SERIE);
		json_closeContainer();

		snprintf((char*)frame, sizeof(frame),
				"POST /v2/controller/adm/auth_fr.php HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"content-length: %d\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);
	}break;

	case IOT_RESET:{
		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		json_clear();
		json_openContainer();
		json_insertStr("dvcpd", "E76XJBP5-48UL-EB1T-MA6Z-BA3DAPMVGTCQ");
		json_insertStr("numse", Config.NUM_SERIE);
		json_closeContainer();

		snprintf((char*)frame, sizeof(frame),
				"POST /v2/controller/adm/rem_frn.php HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"content-length: %d\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);
	}break;

	case IOT_GET_STATUS:{
		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		json_clear();
		json_openContainer();
		json_insertStr("dvctk", IOT_Config.server.TOKEN);
		json_closeContainer();

		snprintf((char*)frame, sizeof(frame),
				"POST /v2/controller/adm/stats_fr.php HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"Keep-Alive: timeout=10\r\n"
				"content-length: %d\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);
	}break;

	case IOT_TAGO:{
		snprintf((char*)server_url,sizeof(server_url),"api.tago.io");
		server_port = 80;

		json_clear();
		json_openMatrix();
		for(i=0; i<4; i++){
			insert_json_body_TAGO("cam","10");
			insert_json_body_TAGO("ir","10");
			insert_json_body_TAGO("board","10");
			insert_json_body_TAGO("setp","10");
			insert_json_body_TAGO("erro","10");
			insert_json_body_TAGO("porta","10");
			insert_json_body_TAGO("tensao","10");
			insert_json_body_TAGO("operacao","10");
			insert_json_body_TAGO("grupo","10");
			insert_json_body_TAGO("receita","10");
		}
		json_closeMatrix();


		snprintf(frame, sizeof(frame),
				"POST /data HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"content-length: %d\r\n"
				"device-token: c7f71928-9510-4cda-9c69-29a0e35d44b9\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);

	}break;

	case IOT_UPLOAD_INFO:{

		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		IOT_MANAGER.flags.send_frame = 0;

		if(IOT_MANAGER.load.done == 1 && IOT_MANAGER.load.totais == 1){
			json_clear();
			json_openContainer();
			json_insertStr("dvctk", IOT_Config.server.TOKEN);
			json_insertInt("qtdgr", IOT_MANAGER.upload._c_total_grupos);
			json_insertInt("qtdrc", IOT_MANAGER.upload._c_total_receitas);
			json_closeContainer();

			snprintf(frame, sizeof(frame),
					"POST /v2/controller/adm/inf_upl.php HTTP/1.1\r\n"
					"Host: %s\r\n"
					"content-type: application/json\r\n"
					"Keep-Alive: timeout=10\r\n"
					"content-length: %d\r\n\r\n"
					"%s",server_url,strlen(json_body),json_body);

			IOT_MANAGER.flags.send_frame = 1;
			IOT_Config.Updated = 0;
		}
		else{
			IOT_MANAGER.buf_load = 0;
			IOT_MANAGER.load.totais = 1;
		}

	}break;

	case IOT_UPLOAD_GRUPO:{

		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		IOT_MANAGER.flags.send_frame = 0;

		if(IOT_MANAGER.load.done == 1 && IOT_MANAGER.load.grupos == 1){

			json_closeMatrix();
			json_closeContainer();


			snprintf(frame, sizeof(frame),
					"POST /v2/controller/adm/upl_grp.php HTTP/1.1\r\n"
					"Host: %s\r\n"
					"content-type: application/json\r\n"
					"Keep-Alive: timeout=10\r\n"
					"content-length: %d\r\n\r\n"
					"%s",server_url,strlen((const char*)json_body),json_body);

			IOT_MANAGER.flags.send_frame = 1;
//			IOT_MANAGER.load.done = 0;
		}
		else if (IOT_MANAGER.load.grupos != 1){
			json_clear();
			json_openContainer();
			json_insertStr("dvctk", IOT_Config.server.TOKEN);
			json_openMatrixStr("grupo");

			IOT_MANAGER.load.grupos = 1;
		}

	}break;

	case IOT_UPLOAD_RECEITA:{

		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		IOT_MANAGER.flags.send_frame = 0;

		if(IOT_MANAGER.load.done == 1 && IOT_MANAGER.load.receitas == 1){

			json_closeMatrix();
			json_closeContainer();


			snprintf(frame, sizeof(frame),
					"POST /v2/controller/adm/upl_rec.php HTTP/1.1\r\n"
					"Host: %s\r\n"
					"content-type: application/json\r\n"
					"Keep-Alive: timeout=10\r\n"
					"content-length: %d\r\n\r\n"
					"%s",server_url,strlen(json_body),json_body);

			IOT_MANAGER.flags.send_frame = 1;
			//IOT_MANAGER.load.done = 0;
		}
		else if (IOT_MANAGER.load.receitas != 1){
			json_clear();
			json_openContainer();
			json_insertStr("dvctk", IOT_Config.server.TOKEN);
			json_openMatrixStr("recei");

			IOT_MANAGER.load.receitas = 1;
		}

	}break;

	case IOT_UPLOAD_CONFIG:{
		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		json_clear();
		json_openContainer();
		json_insertStr("dvctk", IOT_Config.server.TOKEN);
		json_insertInt("dvctk", Config.Escala_Temperatura);
		json_insertInt("dvctk", Config.IDIOMA);
		json_insertInt("dvctk", Config.TEMPO_PRE);
		json_insertInt("dvctk", Config.TEMPERATURA_PRE);
		json_insertInt("dvctk", Config.TEMPERATURA_PRE_2);
		json_insertInt("dvctk", Config.VELOCIDADE_PRE);
		json_closeContainer();

		snprintf(frame, sizeof(frame),
				"POST /v2/controller/adm/upl_fim.php HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"Keep-Alive: timeout=10\r\n"
				"content-length: %d\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);
	}break;

	case IOT_UPLOAD_FIM:{

		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		json_clear();
		json_openContainer();
		json_insertStr("dvctk", IOT_Config.server.TOKEN);
		json_closeContainer();

		snprintf(frame, sizeof(frame),
				"POST /v2/controller/adm/upl_fim.php HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"Keep-Alive: timeout=10\r\n"
				"content-length: %d\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);

	}break;

	case IOT_DOWNLOAD_INFO:{

		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		json_clear();
		json_open();
		json_insertStr("dvctk", IOT_Config.server.TOKEN);
		json_close();

		snprintf(frame, sizeof(frame),
				"POST /v2/controller/adm/inf_dwn.php HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"Keep-Alive: timeout=10\r\n"
				"content-length: %d\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);

	}break;

	case IOT_DOWNLOAD_GRUPO:{

		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		json_clear();
		json_open();
		json_insertStr("dvctk", IOT_Config.server.TOKEN);
		json_insertInt("inici", IOT_MANAGER.download._c_index_grupo);
		json_insertInt("qtdgr", 4);
		json_close();

		snprintf(frame, sizeof(frame),
				"POST /v2/controller/adm/dwn_grp.php HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"Keep-Alive: timeout=10\r\n"
				"content-length: %d\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);

	}break;

	case IOT_DOWNLOAD_RECEITA:{
		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		json_clear();
		json_open();
		json_insertStr("dvctk", IOT_Config.server.TOKEN);
		json_insertInt("idgrp", IOT_MANAGER.download._c_index_grupo+1);
		json_insertInt("inici", IOT_MANAGER.download._c_index_receita);
		json_insertInt("qtrec", 1);
		json_close();

		snprintf(frame, sizeof(frame),
				"POST /v2/controller/adm/dwn_rec.php HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"Keep-Alive: timeout=10\r\n"
				"content-length: %d\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);

	}break;

	case IOT_DOWNLOAD_CONFIG:{
		snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
		server_port = 80;

		json_clear();
		json_open();
		json_insertStr("dvctk", IOT_Config.server.TOKEN);
		json_close();

		snprintf(frame, sizeof(frame),
				"POST /v2/controller/adm/dwn_rec.php HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"Keep-Alive: timeout=10\r\n"
				"content-length: %d\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);

	}break;

	case IOT_DOWNLOAD_FIM:{
			snprintf((char*)server_url,sizeof(server_url),"praticaiok.com");
			server_port = 80;

			json_clear();
			json_open();
			json_insertStr("dvctk", IOT_Config.server.TOKEN);
			json_close();

			snprintf(frame, sizeof(frame),
					"POST /v2/controller/adm/dwn_fim.php HTTP/1.1\r\n"
					"Host: %s\r\n"
					"content-type: application/json\r\n"
					"Keep-Alive: timeout=10\r\n"
					"content-length: %d\r\n\r\n"
					"%s",server_url,strlen(json_body),json_body);

		}break;

	default:
		IOT_MANAGER.flags.send_frame = 0;
	}

	IOT_MANAGER.flags.lock = 0;
	//DEBUG
//	if(IOT_MANAGER.flags.send_frame == 1){
//		DEBUGOUT("SEND\r\n");
//		DEBUGOUT(frame);
//		DEBUGOUT("\r\n\r\n");
//	}

}

void load_iot_parameters(void){
	char tmp[30];
	uint8_t i = 0, j = 0;
	uint32_t total = 0;

	if(IOT_MANAGER._c_time_out_response > 0 || IOT_MANAGER.load.done == 1 || IOT_MANAGER.flags.send_frame == 1 || IOT_MANAGER.flags.lock == 1){return ;}
	IOT_MANAGER.flags.lock = 1;
	IOT_MANAGER.upload._c_aux_grupo = 0;
	IOT_MANAGER.upload._c_aux_receita = 0;

	if		(IOT_MANAGER.load.grupos == 1){
		total = readTotalId("RECUSER/ROSTER.DAT");

		for(i=0; i<4; i++){
			if(IOT_MANAGER.upload._c_index_grupo >= total){IOT_MANAGER.load.end = 1; break;}

			readItemList("RECUSER/ROSTER.DAT", (s_Lista*)&IOT_LISTA, IOT_MANAGER.upload._c_index_grupo);

			json_openContainer();
			json_insertStr("nomgr", IOT_LISTA.titulo);
			snprintf(tmp, sizeof(tmp), "%x%x%x",IOT_LISTA.ico[0],IOT_LISTA.ico[1],IOT_LISTA.ico[2]);
			json_insertStr("fotgr", tmp);
			json_insertInt("tepgr", IOT_LISTA.TEMPERATURA_PRE);
			json_insertInt("ordgr", IOT_MANAGER.upload._c_index_grupo);
			json_insertInt("tipgr", IOT_LISTA.type);
			json_insertStr("idgfr", IOT_LISTA.path);
			json_closeContainer();

			IOT_MANAGER.upload._c_index_grupo++;
			IOT_MANAGER.upload._c_aux_grupo++;
		}

		IOT_MANAGER.load.done = 1;
	}
	else if		(IOT_MANAGER.load.receitas == 1){

//		IOT_MANAGER._c_index_grupo = 0;
//		IOT_MANAGER._c_index_receita = 0;

		readItemList("RECUSER/ROSTER.DAT", (s_Lista*)&IOT_LISTA, IOT_MANAGER.upload._c_index_grupo);
		snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", IOT_LISTA.path);

		total = readTotalId((char*)_tmp_char);

		if(IOT_MANAGER.flags.next_grupo == 1){
			IOT_MANAGER.upload._c_index_receita = 0;
			IOT_MANAGER.flags.next_grupo = 0;
		}


		for(i=IOT_MANAGER.upload._c_index_grupo; i<IOT_MANAGER.upload._c_total_grupos; i++){
			readItemList("RECUSER/ROSTER.DAT", (s_Lista*)&IOT_LISTA, IOT_MANAGER.upload._c_index_grupo);
			snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", IOT_LISTA.path);

			total = readTotalId((char*)_tmp_char);

			if(IOT_MANAGER.upload._c_index_receita >= total){
				IOT_MANAGER.upload._c_aux_grupo = IOT_MANAGER.upload._c_index_grupo++;
				IOT_MANAGER.upload._c_index_receita = 0;
			}
			else{
				break;
			}
		}

		if(IOT_MANAGER.upload._c_index_grupo >= IOT_MANAGER.upload._c_total_grupos && IOT_MANAGER.upload._c_index_receitas >= IOT_MANAGER.upload._c_total_receitas){
			IOT_MANAGER.load.end = 1;
			IOT_MANAGER.flags.end_upload_rec = 1;
		}
		else{

			for(i=0; i<1; i++){

				readItemList((char*)_tmp_char, (s_Lista*)&IOT_LISTA, IOT_MANAGER.upload._c_index_receita);

				if(!(loadRec((char*)IOT_LISTA.path, (s_RecBravo*)&IOT_RECEITA))){

					json_openContainer();
					json_insertStr("nomrc", (char*)IOT_RECEITA.cabecalho.titulo);
					snprintf(tmp, sizeof(tmp), "%x%x%x",IOT_LISTA.ico[0],IOT_LISTA.ico[1],IOT_LISTA.ico[2]);
					json_insertStr("fotrc", tmp);
					json_insertInt("ordrc", IOT_MANAGER.upload._c_index_receita);
					json_insertInt("favrc", IOT_RECEITA.cabecalho.favorito);
					json_insertInt("aqcrc", IOT_RECEITA.cabecalho.aquecer);
					json_insertInt("dorrc", IOT_RECEITA.cabecalho.dourar);
					json_insertInt("aqdrc", IOT_RECEITA.cabecalho.dourar_aquecer);
					json_insertStr("idgrp", (char*)IOT_LISTA.path);
					json_openMatrixStr("passo");
					for(j=0; j<8; j++){
						json_openContainer();
						json_insertInt("pospas", j+1);
						json_insertInt("velpas", IOT_RECEITA.passos[j].vel_motor);
						json_insertInt("micpas", IOT_RECEITA.passos[j].percent_microondas);
						json_insertInt("porpas", IOT_RECEITA.passos[j].percent_teto_lastro);
						json_insertInt("teppas", IOT_RECEITA.passos[j].temperatura);
						json_insertInt("tempas", IOT_RECEITA.passos[j].tempo);
						json_insertInt("atvpas", IOT_RECEITA.passos[j].passo_ativo);
						json_insertStr("infpas", (char*)IOT_RECEITA.passos[j].instrucao);
						json_closeContainer();
					}
					json_closeMatrix();
					json_closeContainer();

					IOT_MANAGER.upload._c_index_receita++;
					IOT_MANAGER.upload._c_index_receitas++;
					IOT_MANAGER.upload._c_aux_receita++;
				}
			}
		}

		IOT_MANAGER.load.done = 1;
	}
	else if (IOT_MANAGER.load.totais == 1){
		IOT_MANAGER.upload._c_total_grupos = readTotalId("RECUSER/ROSTER.DAT");

		IOT_MANAGER.upload._c_total_receitas = 0;

		for(i=0; i<IOT_MANAGER.upload._c_total_grupos; i++){
			readItemList("RECUSER/ROSTER.DAT", (s_Lista*)&IOT_LISTA, i);
			snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", IOT_LISTA.path);

			IOT_MANAGER.upload._c_total_receitas += readTotalId(_tmp_char);
		}

		IOT_MANAGER.load.done = 1;
	}

	IOT_MANAGER.flags.lock = 0;
}

void save_iot_grupos_receitas(void){
	uint8_t i = 0;

	if(IOT_MANAGER.download.flags.save != 1){ return ;}

	switch(IOT_MANAGER.IOT_FRAME_SENDED){
	case IOT_DOWNLOAD_INFO:{
		if(createFolder("RECUSER_IOT") == 0){
			IOT_MANAGER.download.flags.save = 0;
		}
		else{
			deleteFolderRecursive("RECUSER_IOT");
			//deleteFolder("RECUSER_IOT");
		}
	}break;

	case IOT_DOWNLOAD_GRUPO:{

		for(i=0; i<IOT_MANAGER.download._c_received; ){
			if(addItemList("RECUSER_IOT/ROSTER.DAT", IOT_GRUPOS[i]) == 0){
				snprintf(_tmp_char,sizeof(_tmp_char),"RECUSER_IOT/GRP%d",IOT_MANAGER.download._c_index_grupo);
				if(createFolder(_tmp_char) == 0){
					IOT_MANAGER.download.flags.save = 0;
				}
				i++;
				IOT_MANAGER.download._c_index_grupo++;
			}
		}

		IOT_MANAGER.download.flags.save = 0;
		IOT_MANAGER.download._c_received = 0;

		if(IOT_MANAGER.flags.end_download_grp == 1){
			IOT_MANAGER.download._c_index_grupo = 0;
			IOT_MANAGER.download._c_index_receita = 0;
		}


	}break;

	case IOT_DOWNLOAD_RECEITA:{

		snprintf(_tmp_char,sizeof(_tmp_char),"RECUSER_IOT/GRP%d/ROSTER.DAT",IOT_MANAGER.download._c_index_grupo);
		addItemList(_tmp_char, IOT_LISTA);
		snprintf(_tmp_char,sizeof(_tmp_char),"RECUSER_IOT/GRP%d/REC%d.DAT",IOT_MANAGER.download._c_index_grupo,IOT_MANAGER.download._c_index_receita);
		saveRec(_tmp_char, (s_RecBravo*)&IOT_RECEITA);

		IOT_MANAGER.download.flags.save = 0;

		IOT_MANAGER.download._c_index_receita++;
		IOT_MANAGER.download._c_received = 0;

	}break;

	case IOT_DOWNLOAD_FIM:{
		deleteFolderRecursive("RECUSER_old");
		f_rename("RECUSER", "RECUSER_old");
		if(f_rename("RECUSER_IOT", "RECUSER") == 0){
			IOT_MANAGER.flags.end_download = 1;
			IOT_MANAGER.flags.need_download = 0;
			IOT_MANAGER.download.flags.save = 0;
		}
	}break;

	}
}

void error_SERVER(int8_t pEr){
	DEBUGOUT("\r\nerror Server %d\r\n",pEr);
	IOT_MANAGER._c_time_out_response = 0;
	memset(&frame,0x00,sizeof(frame));
	IOT_MANAGER.task.run = 0;
	if(pEr == -10){
//		lwip_init();	//dá hardfaul em sys_timeouts_init()

//		/* Add netif interface for lpc17xx_8x */
//		memset(&lpc_netif, 0, sizeof(lpc_netif));
//		if (!netif_add(&lpc_netif, &ipaddr, &netmask, &gw, NULL, lpc_enetif_init,
//				tcpip_input)) {
//			DEBUGSTR("Net interface failed to initialize\r\n");
//			while(1);
//		}
//		netif_set_default(&lpc_netif);
//		netif_set_up(&lpc_netif);
//
//		if(Config._f_DHCP == 1){
//			dhcp_start(&lpc_netif);
//		}
//		lpc_phy_init(true, msDelay);
//		while(1);
	}
}

void response_SERVER(void){
	DEBUGOUT("'%s'\n",response);

	IOT_MANAGER._c_wait_send = 5;
	IOT_ERROR = SIOT_ALL_OK;

	_c_ETH_Tempo_medio_resposta += _c_ETH_Tempo_resposta;
	_c_ETH_Tempo_medio_resposta /= 2;

	IOT_TIME_ERROR = 0;

	if (strstr((char*)response, "retorno\":\"true")!=NULL){
		json_parse(response, 0);
		IOT_MANAGER.flags.send_success = 1;
	}
	else {
		switch(IOT_MANAGER.IOT_FRAME_SENDED){
		case IOT_AUTHENTICATE:{
			if (strstr((char*)response, "duplicado")!=NULL){
				IOT_Config.RESET = 1;
			}
		}break;
		case IOT_RESET:{
			IOT_Config.RESET = 0;
			IOT_Config.AUTENTICATE = 0;
		}break;
		}
		IOT_ERROR = SIOT_ERRO_SERVER;
	}

	memset(&frame,0x00,sizeof(frame));
	IOT_MANAGER._c_time_out_response = 0;
}

void json_call_field(char* pVar, char* pValue, int pIndex, char pSub){
	char tmp[10];
	static uint8_t i = 0;
	DEBUGOUT("%d %d %s %s\n",pSub, pIndex, pVar, pValue);

	switch(IOT_MANAGER.IOT_FRAME_SENDED){

	case IOT_AUTHENTICATE:{
		if		(pSub == 0){
			switch(pIndex){
			case 2:{if(strstr(pVar,"pincd")!=NULL) snprintf(IOT_Config.server.PINCODE,sizeof(IOT_Config.server.PINCODE),pValue);}break;
			case 3:{if(strstr(pVar,"dvctk")!=NULL) snprintf(IOT_Config.server.TOKEN,sizeof(IOT_Config.server.TOKEN),pValue);}break;
			}
		}
		else if (pSub == 2){
			IOT_Config.AUTENTICATE = 1;
			_c_save_IOT_config = 3;
		}
	}break;

	case IOT_RESET:{
		memset(&IOT_Config.server.PINCODE, 0x00, sizeof(IOT_Config.server.PINCODE));
		memset(&IOT_Config.server.TOKEN, 0x00, sizeof(IOT_Config.server.TOKEN));
		IOT_Config.RESET = 0;
		IOT_Config.AUTENTICATE = 0;
		_c_save_IOT_config = 3;
	}break;

	case IOT_GET_STATUS:{
		if		(pSub == 0){
			/*switch(pIndex){
			case 2:{if(strstr(pVar,"versao")!=NULL) IOT_Config.server.VERSAO_SERVER = atoi(pValue);}break;
			case 3:{if(strstr(pVar,"dados_dwn")!=NULL){if(strstr(pVar,"true")!=NULL){IOT_MANAGER.flags.need_download=1; IOT_MANAGER.flags.info_download=1;}}}break;
			}*/
			if		(strstr(pVar,"versao")!=NULL){ IOT_Config.server.VERSAO_SERVER = atoi(pValue);}
			else if	(strstr(pVar,"dados_dwn")!=NULL){if(strstr(pValue,"true")!=NULL){IOT_MANAGER.flags.need_download=1; IOT_MANAGER.flags.info_download=1;}}
		}
		else if (pSub == 2){
			DEBUGOUT("versao '%d'\n",IOT_Config.server.VERSAO_SERVER);
			//IOT_Config.server.VERSAO_SERVER = 0;
			//IOT_MANAGER.flags.need_download=1; IOT_MANAGER.flags.info_download=1;

			if	(IOT_MANAGER.flags.need_download){DEBUGOUT("Start Download\n");}
			if	((IOT_Config.server.VERSAO_SERVER == 0) || (IOT_Config.server.VERSAO_SERVER < IOT_Config.server.VERSAO_LOCAL)){
				DEBUGOUT("Start Upload\n");
				IOT_MANAGER.flags.info_upload = 1;
			}
			else if (IOT_Config.server.VERSAO_SERVER > (IOT_Config.server.VERSAO_LOCAL+1)){
				/*	Se o server for maior que o client, pode significar uma troca de IHM,
				 * 	entao é necessário que o client fique com a mesma versao do server
				 * 	para que não acumule uploads.
				 */
				IOT_Config.server.VERSAO_LOCAL = IOT_Config.server.VERSAO_SERVER;
				IOT_MANAGER.flags.info_upload = 1;
			}

			if(IOT_MANAGER.flags.info_upload == 0 && IOT_MANAGER.flags.info_download == 0){
				IOT_MANAGER._c_wait_send = 60;
			}

			IOT_MANAGER._c_time_get_status = 1*60; //consulta status a cada 30min
		}
	}break;

	case IOT_UPLOAD_INFO:{
		IOT_MANAGER.flags.info_upload = 0;
		IOT_MANAGER.flags.need_upload = 1;
		IOT_MANAGER.flags.end_upload_grp = 0;
		IOT_MANAGER.flags.end_upload_rec = 0;
		IOT_MANAGER.flags.end_upload_conf = 0;
		IOT_MANAGER.flags.end_upload = 0;
		IOT_MANAGER.upload._c_index_grupo = 0;
		IOT_MANAGER.upload._c_index_receita = 0;

		IOT_MANAGER.IOT_FRAME = IOT_UPLOAD_GRUPO;
	}break;

	case IOT_UPLOAD_GRUPO:{

		if(IOT_MANAGER.load.end == 1){
			IOT_MANAGER.flags.end_upload_grp = 1;
			IOT_MANAGER.upload._c_index_grupo = 0;
			IOT_MANAGER.upload._c_index_receita = 0;
		}
	}break;

	case IOT_UPLOAD_RECEITA:{
		if(IOT_MANAGER.load.end == 1){
			IOT_MANAGER.flags.end_upload_rec = 1;
		}
	}break;

	case IOT_UPLOAD_FIM:{
		IOT_MANAGER.flags.end_upload = 1;
		IOT_MANAGER.flags.need_upload = 0;
	}break;

	case IOT_DOWNLOAD_INFO:{
		if		(pSub == 0){
			switch(pIndex){
			case 3:{if(strstr(pVar,"qtdgr")!=NULL) IOT_MANAGER.download._c_max_grupos = atoi(pValue);}break;
			case 4:{if(strstr(pVar,"qtdrc ")!=NULL) IOT_MANAGER.download._c_max_receitas = atoi(pValue);}break;
			}
		}
		else if (pSub == 2){
			IOT_MANAGER.flags.info_download = 0;
			IOT_MANAGER.flags.end_download_grp = 0;
			IOT_MANAGER.flags.end_download_rec = 0;
			IOT_MANAGER.flags.end_download_conf = 0;
			IOT_MANAGER.flags.end_download = 0;
			IOT_MANAGER.download._c_index_grupo = 0;
			IOT_MANAGER.download._c_index_receita = 0;
			IOT_MANAGER.download.flags.save = 1;
		}
	}break;

	case IOT_DOWNLOAD_GRUPO:{
		if 		(pSub == 3){	//OPEN MATRIX
			if(strstr(pVar,"grupo")!=NULL){
				i = 0;
				IOT_MANAGER.download.flags.load = 1;
				IOT_MANAGER.download._c_received = 0;
				memset(&IOT_GRUPOS,0x00,sizeof(IOT_GRUPOS));
			}
		}
		else if (pSub == 4){	//CLOSE MATRIX
			IOT_MANAGER.download.flags.save = 1;
			IOT_MANAGER.download.flags.load = 0;
			if(IOT_MANAGER.download._c_received < 4){		//SE RETORNAR MENOS QUE O QUE FOI SOLICITADO, SIGNIFICA QUE CHEGOU AO FIM
				IOT_MANAGER.flags.end_download_grp = 1;
				IOT_MANAGER.download._c_index_grupo = 0;
				IOT_MANAGER.download._c_index_receita = 0;
			}
		}
		else if (pSub == 0){	//READING VARIABLES
			if(IOT_MANAGER.download.flags.load){
				if		(strstr(pVar,"nomgr")!=NULL){
					snprintf(IOT_GRUPOS[i].titulo,sizeof(IOT_GRUPOS[i].titulo),pValue);
					snprintf(IOT_GRUPOS[i].path,sizeof(IOT_GRUPOS[i].path),"RECUSER/GRP%d",IOT_MANAGER.download._c_index_grupo+IOT_MANAGER.download._c_received);
				}
				else if	(strstr(pVar,"fotgr")!=NULL){
					snprintf(tmp,sizeof(tmp),"%c%c",pValue[0],pValue[1]);
					IOT_GRUPOS[i].ico[0] = (char)strtol(tmp, NULL, 16);

					snprintf(tmp,sizeof(tmp),"%c%c",pValue[2],pValue[3]);
					IOT_GRUPOS[i].ico[1] = (char)strtol(tmp, NULL, 16);

					snprintf(tmp,sizeof(tmp),"%c%c",pValue[4],pValue[5]);
					IOT_GRUPOS[i].ico[2] = (char)strtol(tmp, NULL, 16);

					//em caso de icone conforme a primeira letra
					if(IOT_GRUPOS[i].ico[0]==0xE1 && IOT_GRUPOS[i].ico[1]==0x84 && (IOT_GRUPOS[i].ico[2]>=0x80 && IOT_GRUPOS[i].ico[2]<=0xBB)){
						IOT_GRUPOS[i].FLAGS |= 0x02;
					}
				}
				else if (strstr(pVar,"tepgr")!=NULL) IOT_GRUPOS[i].TEMPERATURA_PRE = atoi(pValue);
				else if (strstr(pVar,"tipgr")!=NULL) IOT_GRUPOS[i].type = atoi(pValue);

			}
		}
		else if (pSub == 2){	//END CONTAINER
			if(IOT_MANAGER.download.flags.load){
				i ++;
				IOT_MANAGER.download._c_received ++;
//				IOT_MANAGER.download._c_index_grupo++;
			}
		}

	}break;

	case IOT_DOWNLOAD_RECEITA:{
		if 		(pSub == 3){	//OPEN MATRIX
			if		(strstr(pVar,"receitas")!=NULL){
				i = 0;
				IOT_MANAGER.download.flags.load = 1;
				IOT_MANAGER.download.flags.passos = 0;
				IOT_MANAGER.download._c_received = 0;
				memset(&IOT_LISTA,0x00,sizeof(IOT_LISTA));
				memset(&IOT_RECEITA,0x00,sizeof(IOT_RECEITA));
			}
			else if (strstr(pVar,"passos")!=NULL){
				IOT_MANAGER.download.flags.passos = 1;
			}
		}
		else if (pSub == 4){	//CLOSE MATRIX
			if(IOT_MANAGER.download._c_received < 1){	//SE FOR MENOR QUE O SOLICITADO SIGNIFICA QUE CHEGOU AO FIM
				if(IOT_MANAGER.download._c_index_grupo >= IOT_MANAGER.download._c_max_grupos){	//CHEGOU AO FIM DOS GRUPOS
					IOT_MANAGER.flags.end_download_rec = 1;
				}
				else{
					IOT_MANAGER.download._c_index_grupo++;
					IOT_MANAGER.download._c_index_receita = 0;
				}
			}
			if(IOT_MANAGER.download.flags.passos == 1){
				IOT_MANAGER.download.flags.passos = 0;
			}
			else{
				IOT_MANAGER.download.flags.load = 0;
			}
		}
		else if (pSub == 0){	//READING VARIABLES
			if		(IOT_MANAGER.download.flags.passos == 1){
				if 		(strstr(pVar,"velpas")!=NULL) IOT_RECEITA.passos[i].vel_motor = atoi(pValue);
				else if (strstr(pVar,"micpas")!=NULL) IOT_RECEITA.passos[i].percent_microondas = atoi(pValue);
				else if (strstr(pVar,"porpas")!=NULL) IOT_RECEITA.passos[i].percent_teto_lastro = atoi(pValue);
				else if (strstr(pVar,"teppas")!=NULL) IOT_RECEITA.passos[i].temperatura = atoi(pValue);
				//else if (strstr(pVar,"tempas")!=NULL) IOT_RECEITA.passos[i].tempo = atoi(pValue);
				else if (strstr(pVar,"atvpas")!=NULL) IOT_RECEITA.passos[i].passo_ativo = atoi(pValue);
				else if (strstr(pVar,"infpas")!=NULL){
					if(strlen(pValue) == 0){
						IOT_RECEITA.passos[i].instrucao[0] = 0x00;
					}
					else{
						snprintf(IOT_RECEITA.passos[i].instrucao, sizeof(IOT_RECEITA.passos[i].instrucao), pValue);
					}
				}
			}
			else if	(IOT_MANAGER.download.flags.load){
				if 		(strstr(pVar,"nomrc")!=NULL){
					snprintf(IOT_LISTA.titulo, sizeof(IOT_LISTA.titulo), pValue);
					snprintf(IOT_LISTA.path,sizeof(IOT_LISTA.path),"RECUSER/GRP%d/REC%d.DAT",IOT_MANAGER.download._c_index_grupo,IOT_MANAGER.download._c_index_receita+IOT_MANAGER.download._c_received);
				}
				else if (strstr(pVar,"favrc")!=NULL) IOT_LISTA.favorito = atoi(pValue);
				else if (strstr(pVar,"aqcrc")!=NULL) IOT_RECEITA.cabecalho.aquecer = atoi(pValue);
				else if (strstr(pVar,"dorrc")!=NULL) IOT_RECEITA.cabecalho.dourar = atoi(pValue);
				else if (strstr(pVar,"aqdrc")!=NULL) IOT_RECEITA.cabecalho.dourar_aquecer = atoi(pValue);
				else if	(strstr(pVar,"fotgr")!=NULL){
					snprintf(tmp,sizeof(tmp),"%c%c",pValue[0],pValue[1]);
					IOT_LISTA.ico[0] = (char)strtol(tmp, NULL, 16);

					snprintf(tmp,sizeof(tmp),"%c%c",pValue[2],pValue[3]);
					IOT_LISTA.ico[1] = (char)strtol(tmp, NULL, 16);

					snprintf(tmp,sizeof(tmp),"%c%c",pValue[4],pValue[5]);
					IOT_LISTA.ico[2] = (char)strtol(tmp, NULL, 16);

					//em caso de icone conforme a primeira letra
					if(IOT_LISTA.ico[0]==0xE1 && IOT_LISTA.ico[1]==0x84 && (IOT_LISTA.ico[2]>=0x80 && IOT_LISTA.ico[2]<=0xBB)){
						IOT_LISTA.FLAGS |= 0x02;
					}
				}
			}
		}
		else if (pSub == 2){	//END CONTAINER
			if (IOT_MANAGER.download.flags.passos == 1){
				i ++;
				if(i>=8){
					IOT_MANAGER.download._c_received ++;
				}
			}
			if(IOT_MANAGER.download._c_received > 0 && IOT_MANAGER.download.flags.load == 0){
				IOT_MANAGER.download.flags.save = 1;
			}
		}
	}break;

	case IOT_DOWNLOAD_FIM:{
		IOT_MANAGER.download.flags.save = 1;
	}break;

	}
}

void cbk_TCP_SEND(uint16_t pLen){
	DEBUGOUT("SEND %d\r\n",pLen);
	DEBUGOUT(frame);
	DEBUGOUT("\r\n\r\n");
	IOT_SENDED += 1;
}

void update_IOT_Version(void){
	if(IOT_Config.Updated == 0){
		IOT_Config.Updated = 1;
		IOT_Config.server.VERSAO_LOCAL+=1;
		_c_save_IOT_config = 3;
	}
}








//----------------------------------------------------------------------------
//	EMBNET
//----------------------------------------------------------------------------
void EmbNET_CallBk_Live(uint8_t FDeviceID, uint8_t FNetID, uint16_t Class, uint32_t Serial){
	//if((FDeviceID==IDMod_Power)&&(FNetID==Net)&&!(_f_MOD_connect&MASK_MOD_POWER)&&
	//		(Class==CLASS_ID_POWER_EXPRESS || Class==CLASS_ID_POWER_EXPRESS_EXPORT || Class==CLASS_ID_POWER_FULL)
	//		&& !_f_Embnet_off){
//	if(!_f_Embnet_off){
	if(!_f_Embnet_off){
		EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Init_Slave,0);
		_c_Mod_Power_reset_time_out = MOD_POWER_RESET_TIME_OUT-1;
		_f_MOD_connect |= MASK_MOD_POWER;
		_f_MOD_config |= MASK_MOD_POWER;
		CLASS_ID_POWER_READ = Class;
		_bf_Last_CONFIG=0xFFFFFFFF;
		_bf_Last_MODBUS=0xFFFFFFFF;
		CTRL_OPERACAO &= ~0x10;
		_c_live++;
	}
}
void EmbNET_CallBk_FrameSetDevice(uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data){
	switch(AttributeID){
	case Attrib_Serial:{
		Eeprom.SN_LOTE = 0x00000000; Eeprom.SN_SERIE = 0x00000000;
		Eeprom.SN_LOTE  = (Data&0xFFFF0000)>>16;
		Eeprom.SN_SERIE = (Data&0x0000FFFF);
		EmbNET_SetDevice(IDMod,Net,Type,(uint16_t)CLASS_ID,(uint16_t)(Eeprom.SN_LOTE),(uint16_t)(Eeprom.SN_SERIE));
		_c_save_Serial = 1;
	}break;
	default:{
		EmbNET_SendFrame_AttribError(FDeviceID,FNetID,AttributeID);
	}break;
	}
}
void EmbNET_CallBk_FrameReply( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data){

	if(_f_MOD_connect&MASK_MOD_POWER && FNetID==Net && FDeviceID==IDMod_Power){
		switch(AttributeID){
			case Attrib_Get_Status:{
				_c_lasttimeOutStatus = _c_timeOutStatus;
				_c_timeOutStatus = 0;
				_f_Mod_Power_offLine = 0;
				_c_Mod_Power_offLine = 0;
				_b_STATUS_POWER = (uint32_t)Data;
				STATUS_POWER.buffer = _b_STATUS_POWER;
				if(STATUS_POWER.CORRENTE){_f_AguardaCorrente_Mag=0;}
			}break;
			case Attrib_Get_Temp_Board:{
//				if((((int32_t)Data)>-100 && ((int32_t)Data)<100)){
					POWER.TEMP_BOARD = ((int32_t)Data)*FATOR_TEMP;
//					COUNT_ERRO_SENSOR.TEMP_BOARD = 0;
//				}
//				else if (COUNT_ERRO_SENSOR.TEMP_BOARD>3){
//					POWER.TEMP_BOARD = 998*FATOR_TEMP;
//				}
//				else{COUNT_ERRO_SENSOR.TEMP_BOARD++;}
			}break;
			case Attrib_Get_Temperatura_1:{
#ifndef SIMULA_OPERACAO
#ifndef SIMULA_TEMPERATURA
//				if((((int32_t)Data)>-500 && ((int32_t)Data)<500)){
					POWER.TEMP_1 = ((int32_t)Data)*FATOR_TEMP;
//					COUNT_ERRO_SENSOR.TEMP_1 = 0;
//				}
//				else if (COUNT_ERRO_SENSOR.TEMP_1>3){
//					POWER.TEMP_1 = 998*FATOR_TEMP;
//				}
//				else{COUNT_ERRO_SENSOR.TEMP_1++;}
#else
				if(POWER_DIG_OUT.bits.AQ_CAM || POWER_DIG_OUT.bits.AQ_RI){ POWER.TEMP_1 += 15*FATOR_TEMP;} else{POWER.TEMP_1-=1*FATOR_TEMP;}
#endif
#endif
				_c_conexao|=0x01;
			}break;
			case Attrib_Get_Temperatura_2:{
#ifndef SIMULA_OPERACAO
#ifndef SIMULA_TEMPERATURA
//				if((((int32_t)Data)>-500 && ((int32_t)Data)<500)){
					POWER.TEMP_2 = ((int32_t)Data)*FATOR_TEMP;
//					COUNT_ERRO_SENSOR.TEMP_2 = 0;
//				}
//				else if (COUNT_ERRO_SENSOR.TEMP_2>3){
//					POWER.TEMP_2 = 998*FATOR_TEMP;
//				}
//				else{COUNT_ERRO_SENSOR.TEMP_2++;}
#else
				if(POWER_DIG_OUT.bits.AQ_CAM || POWER_DIG_OUT.bits.AQ_RI){ POWER.TEMP_2 += 15*FATOR_TEMP;} else{POWER.TEMP_2-=1*FATOR_TEMP;}
#endif
#endif
				_c_conexao|=0x02;
			}break;
			case Attrib_Get_Temperatura_3:{	POWER.TEMP_3 = (int32_t)Data*FATOR_TEMP; }break;
			case Attrib_Get_Temperatura_4:{	POWER.TEMP_4 = (int32_t)Data*FATOR_TEMP; }break;
			case Attrib_Get_Temperatura_5:{	POWER.TEMP_5 = (int32_t)Data*FATOR_TEMP; }break;
			case Attrib_Get_Temperatura_6:{	POWER.TEMP_6 = (int32_t)Data*FATOR_TEMP; }break;
			case Attrib_Get_In:{POWER_DIG_IN.buffer = (uint16_t)Data&0x0000FFFF; _c_conexao|=0x04;}break;
			case Attrib_Get_Erros:{POWER_ERROS.buffer = (uint32_t)Data&0xFFFFFFFF;}break;
			case Attrib_Get_Firmware:{
				VERSION_FIRM_POWER[0] = (Data&0x000000FF);
				VERSION_FIRM_POWER[1] = (Data&0x0000FF00)>>8;
				VERSION_FIRM_POWER[2] = (Data&0x00FF0000)>>16;
				VERSION_FIRM_POWER[3] = (Data&0xFF000000)>>24;
			}break;
			case Attrib_Get_ClassId:{
				CLASS_ID_POWER_READ = (uint16_t)Data&0x0000FFFF;
			}break;
			case Attrib_Analise_CAN:{
				switch(Analise_CAN){
				case 0x00001000:{sAnalise_CAN.CAN_STAT = Data;}break;
				case 0x00001001:{sAnalise_CAN.CAN_ERROR = Data;}break;
				}
				Analise_CAN_read = Analise_CAN;
				UpdateScr = 1;
			}break;
			case Attrib_Analise_Registros:{
				Analise_Registros_read=Analise_Registros;
				switch(Analise_Registros){
				case 0x00001000:{ sAnalise_Registros._c_Time_65_board = Data; Analise_Registros++;}break;
				case 0x00001001:{ sAnalise_Registros._c_Time_75_board = Data; Analise_Registros++;}break;
				case 0x00001002:{ sAnalise_Registros._c_Time_85_board = Data; Analise_Registros=0x00001008;}break;
				case 0x00001003:{ sAnalise_Registros._c_Erro_INV_alarme = Data; Analise_Registros++;}break;
				case 0x00001004:{ sAnalise_Registros._c_Erro_INV_falha = Data; Analise_Registros++;}break;
				case 0x00001005:{ sAnalise_Registros._c_Erro_INV_frame = Data; Analise_Registros++;}break;
				case 0x00001006:{ sAnalise_Registros._c_Erro_INV_timeOut = Data; Analise_Registros=0x0000100A;}break;
				case 0x00001007:{ sAnalise_Registros._c_Erro_WatchDog = Data; Analise_Registros=0x0000100B;}break;
				case 0x00001008:{ sAnalise_Registros.Max_TempBoard = Data; Analise_Registros=0x00001016;}break;
				case 0x00001009:{ sAnalise_Registros._c_Erro_Inversor = Data; Analise_Registros=0x00001003;}break;
				case 0x0000100A:{ sAnalise_Registros._c_Erro_MAG = Data; Analise_Registros=0x00001000;}break;
				case 0x0000100B:{ sAnalise_Registros._CAN_BusOFF = Data; Analise_Registros=0x00001009;}break;
				case 0x0000100C:{ sAnalise_Registros._c_ErroSensor[0] = Data; Analise_Registros++;}break;
				case 0x0000100D:{ sAnalise_Registros._c_ErroSensor[1] = Data; Analise_Registros++;}break;
				case 0x0000100E:{ sAnalise_Registros._c_ErroSensor[2] = Data; Analise_Registros=0x00001007;}break;
				case 0x0000100F:{ sAnalise_Registros._c_ErroSensor[3] = Data;}break;
				case 0x00001010:{ sAnalise_Registros._c_ErroSensor[4] = Data;}break;
				case 0x00001011:{ sAnalise_Registros._c_ErroSensor[5] = Data;}break;
				case 0x00001012:{ sAnalise_Registros._c_ErroSensor[6] = Data;}break;
				case 0x00001013:{ sAnalise_Registros._c_ErroSensor[7] = Data;}break;
				case 0x00001014:{ sAnalise_Registros._c_ErroSensor[8] = Data;}break;
				case 0x00001015:{ sAnalise_Registros._c_ErroSensor[9] = Data;}break;
				case 0x00001016:{ sAnalise_Registros._c_FalhaSensor[0] = Data; Analise_Registros++;}break;
				case 0x00001017:{ sAnalise_Registros._c_FalhaSensor[1] = Data; Analise_Registros++;}break;
				case 0x00001018:{ sAnalise_Registros._c_FalhaSensor[2] = Data; Analise_Registros=0x0000100C;}break;
				case 0x00001019:{ sAnalise_Registros._c_FalhaSensor[3] = Data;}break;
				case 0x0000101A:{ sAnalise_Registros._c_FalhaSensor[4] = Data;}break;
				case 0x0000101B:{ sAnalise_Registros._c_FalhaSensor[5] = Data;}break;
				case 0x0000101C:{ sAnalise_Registros._c_FalhaSensor[6] = Data;}break;
				case 0x0000101D:{ sAnalise_Registros._c_FalhaSensor[7] = Data;}break;
				case 0x0000101E:{ sAnalise_Registros._c_FalhaSensor[8] = Data;}break;
				case 0x0000101F:{ sAnalise_Registros._c_FalhaSensor[9] = Data;}break;
				}

				UpdateScr = 1;
			}break;
		}
	}
}
void EmbNET_CallBk_FrameTrigger( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data ) {
	if(_f_MOD_connect&MASK_MOD_POWER && FNetID==Net && FDeviceID==IDMod_Power){
		switch(AttributeID){
			case Attrib_Get_In:{POWER_DIG_IN.buffer = (uint16_t)Data; if(STATUS==ST_LER_ENTRADAS){UpdateScr=1;}}break;
			case Attrib_Get_Erros:{POWER_ERROS.buffer = (uint32_t)Data&0xFFFFFFFF;}break;
			case Attrib_Get_Status:{
				_f_Mod_Power_offLine = 0;
				_c_Mod_Power_offLine = 0;
				_b_STATUS_POWER = (uint32_t)Data;
				STATUS_POWER.buffer = _b_STATUS_POWER;

				if(STATUS_POWER.CORRENTE){_f_AguardaCorrente_Mag=0;}
			}break;
		}
	}
}
void EmbNET_CallBk_FrameGet (uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t * DataReply, uint32_t DataValue, uint8_t *ValidReply){
	uint32_t firmware=0x00000000;

		*ValidReply = 1;
		switch(AttributeID){
			case Attrib_Get_Temp_Board:{
				*DataReply = (uint32_t)_v_TempPainel/FATOR_TEMP;
			}break;
			case Attrib_Get_Firmware:{
				firmware = 0;
				firmware |= VERSION[0];
				firmware |= VERSION[1]<<8;
				firmware |= VERSION[2]<<16;
				firmware |= VERSION[3]<<24;
				*DataReply=(uint32_t)firmware;
			}break;
			default:{
				*ValidReply = 0;//RETORNA UM ATTRIBERROR
			}break;
		}

}

void Tratativas_Embnet_CTRLSys(void){

	//======================================================================
	//	TRATATIVAS EMBNET
	//======================================================================
	if(_f_MOD_connect&MASK_MOD_POWER && !_f_Embnet_off){

		//======================================================================
		//	SETs DO MODULO
		//======================================================================
		//CONFIGURACOES APOS CONEXAO COM O MODULO
		if(_f_MOD_config&MASK_MOD_POWER /*|| (_c_MOD_config==0 && !(_f_MOD_config&MASK_MOD_POWER))*/){
			EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Time_Out_Slave,MOD_POWER_RESET_TIME_OUT*10);
			vTaskDelay(DELAY_CAN_WRITE);

			//QUANDO HOUVER ALTERACAO NAS ENTRADAS GERAR TRIGGER
			TRIGGERS.bits.FUNC = 0x04; TRIGGERS.bits.VETOR = 0; TRIGGERS.bits.ATTRIB = Attrib_Get_In;
			EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Triggers,TRIGGERS.buffer);
			vTaskDelay(DELAY_CAN_WRITE);

			//QUANDO HOUVER ALTERACAO NOS ERROS GERAR TRIGGER
			TRIGGERS.bits.FUNC = 0x04; TRIGGERS.bits.VETOR = 1; TRIGGERS.bits.ATTRIB = Attrib_Get_Erros;
			EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Triggers,TRIGGERS.buffer);
			vTaskDelay(DELAY_CAN_WRITE);

			//QUANDO HOUVER ALTERACAO NOS STATUS GERAR TRIGGER
			TRIGGERS.bits.FUNC = 0x04; TRIGGERS.bits.VETOR = 2; TRIGGERS.bits.ATTRIB = Attrib_Get_Status;
			EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Triggers,TRIGGERS.buffer);
			vTaskDelay(DELAY_CAN_WRITE);

			if(STATUS!=ST_TESTAR_CARGAS){
				//ERRO DE INVERSOR COM SAIDA CONTATOR GERAL
				VINCULOS.bits.FUNC = 0x04|0x02; VINCULOS.bits.VETOR = 0; VINCULOS.bits.VALOR = 0;
				VINCULOS.bits.DEST = VINC_OUT7; VINCULOS.bits.ORIG = VINC_INV;
				EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Vinculos,VINCULOS.buffer);
				vTaskDelay(DELAY_CAN_WRITE);
			}

			//ERRO TERMOSTATO COM SAIDA CONTATOR GERAL
//			VINCULOS.bits.FUNC = 0x04; VINCULOS.bits.VETOR = 1; VINCULOS.bits.VALOR = 0;
//			VINCULOS.bits.DEST = VINC_OUT7; VINCULOS.bits.ORIG = VINC_IN5;
//			EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Vinculos,VINCULOS.buffer);
//			vTaskDelay(1);

			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_Firmware,0x00000000);
			vTaskDelay(DELAY_CAN_WRITE);

			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_ClassId,0x00000000);
			vTaskDelay(DELAY_CAN_WRITE);

			_f_MOD_config &= ~MASK_MOD_POWER;
			_c_MOD_config = 3;
		}
		//RESET TIME OUT DO MODULO
		if(!_c_Mod_Power_reset_time_out || EMBNET_POWER.bits.RESET_POWER){
			EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Reset_TimeOut,0);
			_c_Mod_Power_reset_time_out = MOD_POWER_RESET_TIME_OUT-2;
			EMBNET_POWER.bits.RESET_POWER = 0;
			vTaskDelay(DELAY_CAN_WRITE);

			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_Firmware,0x00000000);
			vTaskDelay(DELAY_CAN_WRITE);
		}

		//CONFIGURACOES DO MODULO
		if(_bf_Last_CONFIG!=POWER_CONFIG.buffer){
			POWER_CONFIG.bits.CTRL_DOOR = ON;//HABILITA CTRL DOOR
			POWER_CONFIG.bits.CTRL_ON_OFF = ON;
#ifdef VINCULO_INVERSOR
			POWER_CONFIG.bits.CTRL_OUTS_A_INV = OFF;
#else
			POWER_CONFIG.bits.CTRL_OUTS_A_INV = ON;
#endif
			POWER_CONFIG.bits.FDA = Config._f_FDA;
			EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Configuracao,POWER_CONFIG.buffer);
			_bf_Last_CONFIG=POWER_CONFIG.buffer;
			vTaskDelay(DELAY_CAN_WRITE);
		}

		//MODBUS DO MODULO
		if(_bf_Last_MODBUS!=POWER_MODBUS.buffer){
			EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Modbus,POWER_MODBUS.buffer);
			_bf_Last_MODBUS=POWER_MODBUS.buffer;
			vTaskDelay(DELAY_CAN_WRITE);
		}

		//SETA O VALOR DAS SAIDAS DIGITAIS DA POWER
		if(POWER_DIG_OUT_buffer!=POWER_DIG_OUT.buffer || EMBNET_POWER.bits.DIG_OUT){
			EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Out,(uint32_t)POWER_DIG_OUT.buffer);
			POWER_DIG_OUT_buffer=POWER_DIG_OUT.buffer;
			EMBNET_POWER.bits.DIG_OUT = 0;
			vTaskDelay(DELAY_CAN_WRITE);
		}

		if(_f_send_pwm){
			EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_PWM,(uint32_t)PWM_POWER.buffer);
			_f_send_pwm = 0;
			vTaskDelay(DELAY_CAN_WRITE);
		}


		//======================================================================
		//	GETs DO MODULO
		//======================================================================
		if(EMBNET_POWER.bits.STATUS){
			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_Status,0);
			if(_f_Mod_Power_offLine!=1){
				_c_Mod_Power_offLine = MOD_POWER_DETECT_OFFLINE;
				_f_Mod_Power_offLine = 1;
			}
			EMBNET_POWER.bits.STATUS = 0;
			vTaskDelay(DELAY_CAN_WRITE);
			_f_send_pwm = 1;
		}
		if(EMBNET_POWER.bits.TEMPERATURAS){
			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_Temp_Board,0);
			vTaskDelay(DELAY_CAN_WRITE);
			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_Temperatura_1,0);
			vTaskDelay(DELAY_CAN_WRITE);
			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_Temperatura_2,0);
			vTaskDelay(DELAY_CAN_WRITE);
//			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_Temperatura_3,0);
//			vTaskDelay(1);
//			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_Temperatura_4,0);
//			vTaskDelay(1);
//			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_Temperatura_5,0);
//			vTaskDelay(1);
//			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_Temperatura_6,0);
//			vTaskDelay(1);
			EMBNET_POWER.bits.TEMPERATURAS = 0;
		}
		if(EMBNET_POWER.bits.DIG_IN){
			EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Get_In,0);
			vTaskDelay(DELAY_CAN_WRITE);
			EMBNET_POWER.bits.DIG_IN = 0;
		}

		if(_f_STATUS==ST_CONF_EMBTECH){
			if(_c_send_Analise==0){
				switch(Analise_CAN){
				case 0x00001000: Analise_CAN=0x00001001; break;
				case 0x00001001: Analise_CAN=0x00001000; break;
				}
				EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Analise_CAN,Analise_CAN);
				vTaskDelay(1);
				_c_send_Analise = 2;
			}
			if(_c_send_Analise_Registros==0){
				//				if(Analise_Registros<0x0000100E){Analise_Registros++;}
				//				else if(Analise_Registros<=0x0000100E){Analise_Registros=0x00001016;}
				//				else if(Analise_Registros<0x00001018){Analise_Registros++;}
				//				else{Analise_Registros=0x00001000;}

				EmbNET_SendFrame_Get(IDMod_Power,Net,Attrib_Analise_Registros,Analise_Registros);
				vTaskDelay(1);

				_c_send_Analise_Registros = 1;
			}
		}
	}

}

void VariaveisInit(void){
	//memset((void*)&EMBNET_POWER,0x00,sizeof(EMBNET_POWER));
	memset((char*)&POWER,0xFF,sizeof(POWER));
	memset((char*)&SET_POINT,0x00,sizeof(SET_POINT));


	EMBNET_POWER.buffer = 0x01;
	POWER_MODBUS.buffer = 0x00000000;
	POWER_CONFIG.buffer = 0x00000000;
	POWER_DIG_OUT.buffer= 0x00000000;
	POWER_ERROS.buffer  = 0x00000000;
	POWER_DIG_IN.buffer = 0xFFFF;

	ERROS.total = 0x00;

	_bf_Last_CONFIG		= 0xFFFFFFFF;
	_bf_Last_MODBUS		= 0xFFFFFFFF;
	_c_gets_power		= 0x01;


	//!INICIAR OS ENDERECOS DE MEMORIA EXTERNA COM UM VALOR PADRAO NESTE PONTO
//	memset(&SIF_FONT_ICON_99x99, 		0x00, sizeof(GUI_FONT));
//	memset(&SIF_FONT_EXO2_REGULAR_24, 	0x00, sizeof(SIF_FONT_EXO2_REGULAR_24));
//	memset(&SIF_FONT_EXO2_REGULAR_30, 	0x00, sizeof(SIF_FONT_EXO2_REGULAR_30));
//	memset(&SIF_FONT_EXO2_REGULAR_36, 	0x00, sizeof(SIF_FONT_EXO2_REGULAR_36));
//	memset(&SIF_FONT_EXO2_REGULAR_42, 	0x00, sizeof(SIF_FONT_EXO2_REGULAR_42));
//	memset(&SIF_FONT_EXO2_REGULAR_48, 	0x00, sizeof(SIF_FONT_EXO2_REGULAR_48));
//	memset(&SIF_FONT_EXO2_REGULAR_55, 	0x00, sizeof(SIF_FONT_EXO2_REGULAR_55));
//	memset(&SIF_FONT_EXO2_REGULAR_65, 	0x00, sizeof(SIF_FONT_EXO2_REGULAR_65));
//	memset(&SIF_FONT_EXO2_REGULAR_70, 	0x00, sizeof(SIF_FONT_EXO2_REGULAR_70));
//	memset(&SIF_FONT_EXO2_BOLD_31, 		0x00, sizeof(SIF_FONT_EXO2_BOLD_31));
//	memset(&SIF_FONT_DIVERSOS_60, 		0x00, sizeof(SIF_FONT_DIVERSOS_60));
//	memset(&SIF_FONT_DIVERSOS_44, 		0x00, sizeof(SIF_FONT_DIVERSOS_44));
//	memset(&SIF_FONT_DIVERSOS_27, 		0x00, sizeof(SIF_FONT_DIVERSOS_27));
//	memset(&SIF_FONT_ICON_68x68,		0x00, sizeof(SIF_FONT_ICON_68x68));
//	memset(&SIF_FONT_ICON_180x125,		0x00, sizeof(SIF_FONT_ICON_180x125));


	memset((char*)buf_SIF_FONT_EXO2_REGULAR_24, 	0x00, sizeof(buf_SIF_FONT_EXO2_REGULAR_24));
	memset((char*)buf_SIF_FONT_EXO2_REGULAR_30, 	0x00, sizeof(buf_SIF_FONT_EXO2_REGULAR_30));
	memset((char*)buf_SIF_FONT_EXO2_REGULAR_36, 	0x00, sizeof(buf_SIF_FONT_EXO2_REGULAR_36));
	memset((char*)buf_SIF_FONT_EXO2_REGULAR_42, 	0x00, sizeof(buf_SIF_FONT_EXO2_REGULAR_42));
	memset((char*)buf_SIF_FONT_EXO2_REGULAR_48, 	0x00, sizeof(buf_SIF_FONT_EXO2_REGULAR_48));
	memset((char*)buf_SIF_FONT_EXO2_REGULAR_55, 	0x00, sizeof(buf_SIF_FONT_EXO2_REGULAR_55));
	memset((char*)buf_SIF_FONT_EXO2_REGULAR_65, 	0x00, sizeof(buf_SIF_FONT_EXO2_REGULAR_65));
	memset((char*)buf_SIF_FONT_EXO2_REGULAR_70, 	0x00, sizeof(buf_SIF_FONT_EXO2_REGULAR_70));
	memset((char*)buf_SIF_FONT_EXO2_BOLD_31, 		0x00, sizeof(buf_SIF_FONT_EXO2_BOLD_31));

	memset((char*)buf_SIF_FONT_ICON_99x99, 			0x00, sizeof(buf_SIF_FONT_ICON_99x99));
	memset((char*)buf_SIF_FONT_ICON_160x99, 		0x00, sizeof(buf_SIF_FONT_ICON_160x99));
	memset((char*)buf_SIF_FONT_ICON_68x68,			0x00, sizeof(buf_SIF_FONT_ICON_68x68));
	memset((char*)buf_SIF_FONT_ICON_180x125,		0x00, sizeof(buf_SIF_FONT_ICON_180x125));
	memset((char*)_tmp_ram_BACK, 					0x00, sizeof(_tmp_ram_BACK));



	CTRL_OPERACAO = 0x00;
	_f_STATUS_BUZZER = BUZ_DESLIGADO;

	POWER_DIG_OUT_buffer = 0xFFFF;

	//VALOR SETADO INICIALMENTE
	SET_POINT.TEMPERATURA = 280*FATOR_TEMP;
	SET_POINT.TEMPO = 5;
	SET_POINT.VELOCIDADE_MOTOR = 0;
	SET_POINT.MICROONDAS = 0;
	SET_POINT.RI = 0;


//	Config.Escala_Temperatura = 0;				//! REMOVER DEPOIS DE CRIAR A TELA DE ALTERAR ESCALA DE TEMPERATURA

	Config.TEMPERATURA_MAX = 280*FATOR_TEMP;

	POWER_CONFIG.bits.INV_HAB = 1;
	POWER_CONFIG.bits.INV_MODELO = INV_MOD_WEG; //FORCAR SEMPRE O MODELO WEG
	POWER_CONFIG.bits.STATUS = ON;


	ID_IDIOMA = Config.IDIOMA;

	Config.Demonstracao = 0; 					//NUNCA ENTRAR EM MODO DE DEMONSTRACAO (NAO TEM O MENU)
	//Config.TEMPO_PRE = 5; 						//! REMOVER DEPOIS QUE CRIAR A TELA DE AJUSTAR A TEMPERATURA DE PRE
	//Config.TEMPERATURA_PRE = 280*FATOR_TEMP;			//! REMOVER DEPOIS QUE CRIAR A TELA DE AJUSTAR A TEMPERATURA DE PRE

	if(_f_WatchDog){
		Config._c_WatchDog+=1;
	}


	memcpy((void*)SenhaUser, (void*)Config.SENHA_USER, sizeof(SenhaUser));
	memcpy((void*)SenhaFabricante, (void*)Config.SENHA_FAB, sizeof(SenhaFabricante));

//#ifdef SIMULA_TEMPERATURA
//	POWER.TEMP_1 = 200*FATOR_TEMP;
//	POWER.TEMP_2 = 200*FATOR_TEMP;
//#endif

#ifdef SIMULA_OPERACAO
		POWER.TEMP_1 = 200*FATOR_TEMP;
		POWER.TEMP_2 = 200*FATOR_TEMP;
#endif

}


int concatInt( int last, int first ){
	int i;
	int fNeg = 0; // Flag for negative

	if( last < 0 || first < 0 ){
		// Set flag for negative value
		fNeg = 1;

		// If a value is negative, make sure we have positive (you can separate these)
		last = abs( last );
		first = abs( first );
	}

	if( first ){
		// A loop to get number of decimal places to move 'last' (last is leftmost)
		for( i = 1; first / i; i *= 10 );
	}
	else{
		// 'first' is 0, left shift 1
		i = 10;
	}

	// Add negative flag back if 'fNeg' is set
	return fNeg ? -( last * i + first ) : last * i + first;
}

/*************************************************************************************
	S32 Convert_Temp(S32 temp, U8 tipo)
	@autor Ulysses C. Fonseca

	@descricao
		Realiza a conversão de temperaturas de Celsius para Farenheit ou Kelvin

	@param
		_ S32 temp - temperatura a ser convertida
		- U8 tipo - para qual unidade deve ser convertida 0-Celsius 1-Farenheit 2-Kelvin
	@return
		- S32 retorna o valor da temperatura convertida
		_ U8 TemperaturaConvertida[30];				 	//armazena o valor de temperatura convertida ja com a unidade de medida

 *************************************************************************************/
int32_t convert_temp(int32_t temp){
	if(temp>(500*FATOR_TEMP) || temp<(-500*FATOR_TEMP)){
		snprintf((char*)TemperaturaConvertida, sizeof(TemperaturaConvertida),"%s",TXT_ERRO[ID_IDIOMA]);
	}
	else{
		switch(Config.Escala_Temperatura){
			case 0: {	//Celsius
				temp /= FATOR_TEMP;
				snprintf((char*)TemperaturaConvertida, sizeof(TemperaturaConvertida),"%dºC",temp);
			}break;
			case 1: { // Farenheint
				temp= (temp/FATOR_TEMP_F)+32;
				snprintf((char*)TemperaturaConvertida, sizeof(TemperaturaConvertida),"%dºF",temp);
			}break;
			case 2: { // Kelvin
				temp = (temp/FATOR_TEMP)+273;
				snprintf((char*)TemperaturaConvertida, sizeof(TemperaturaConvertida),"%dK",temp);
			}break;
			default: break;
		}
	}
	return temp;
}

uint8_t erro_font=0;
/*********************************************************************
 *	Funcao responsavel por carregar fontes no formato SIF do cartao
 *	na memoria RAM
 *
 *	@param
 *		const char *filename - endereco com o nome do arquivo a ser utilizado
 *		GUI_FONT *pFont - ponteiro da fonte a ser carregada
 *
 *	@return
 *		0 - sucess
 *		1 - nao foi possivel carregar o arquivo
 *		2 - arquivo não existente
 *
 *
 *********************************************************************/
char load_font_SIF(const char *filename, GUI_FONT *pFont, void *pBuffer){
	FIL file;
	unsigned int sf=0, cnt=0;


	if(!f_open(&file,filename,FA_OPEN_EXISTING | FA_READ)){
		sf = f_size(&file);
		f_read(&file,pBuffer,sf, &cnt);
		f_sync(&file);
		f_close(&file);

		if(sf != cnt){erro_font=1; return 1;}

		GUI_SIF_CreateFont(pBuffer, pFont, GUI_SIF_TYPE_PROP_AA2_EXT);

		return 0;
	}
	erro_font=1;
	return 2;
}


void carrega_receita_execucao(void){
	uint8_t i=0;

	_f_Passo_Rec_Sel = 0;
	_load_passo_receita();

	_c_Last_TEMPO_CORRENTE = 0xFF;
	_c_TEMPO_CORRENTE = 0;

	SET_POINT.TEMPO = 0;
	for(i=0; i<8; i++){
		if(RecBravo.passos[i].passo_ativo==1){
			SET_POINT.TEMPO += RecBravo.passos[i].tempo;//CARREGA O TEMPO TOTAL DA RECEITA
		}
	}

	_f_Passo_Rec_Sel = 0;
	_c_TEMPO_CORRENTE_PASSO = RecBravo.passos[_f_Passo_Rec_Sel].tempo;
	if(RecBravo.passos[_f_Passo_Rec_Sel].instrucao[0]!=0x00){
		_f_Info_Passo = 1;
	}
}
void carrega_ultimo_passo(void){
	uint8_t i=0;
	for(i=0; i<8; i++){
		if(RecBravo.passos[i].passo_ativo==1){
			SET_POINT.MICROONDAS 		= RecBravo.passos[i].percent_microondas;
			switch(RecBravo.passos[i].temperatura){
				case 0: SET_POINT.TEMPERATURA = Config.TEMPERATURA_PRE;break;
				case 1: SET_POINT.TEMPERATURA = Config.TEMPERATURA_PRE_2;break;
				default:SET_POINT.TEMPERATURA = RecBravo.passos[i].temperatura; break;
			}
			SET_POINT.TEMPO 			= RecBravo.passos[i].tempo;
			SET_POINT.VELOCIDADE_MOTOR 	= RecBravo.passos[i].vel_motor;
			SET_POINT.RI 				= RecBravo.passos[i].percent_teto_lastro;
		}
	}
}
void carrega_proximo_passo(void){

	if((_f_STATUS==ST_EXE_LANCHES || _f_STATUS==ST_TESTAR_PASSO) && _f_Rec_Carregada){
		if(_c_TEMPO_CORRENTE_PASSO==0 && !(CTRL_OPERACAO&0x04) && _f_Passo_Rec_Sel<7){//PASSO DE 0 A 7 (8 PASSOS)
			if(RecBravo.passos[_f_Passo_Rec_Sel].instrucao[0]!=0x00){
				_f_Info_Passo = 1;
			}
			if(!_f_Info_Passo){
				_f_Passo_Rec_Sel++;
				if(RecBravo.passos[_f_Passo_Rec_Sel].passo_ativo==1){
					_c_TEMPO_CORRENTE_PASSO 	= RecBravo.passos[_f_Passo_Rec_Sel].tempo;
					SET_POINT.MICROONDAS 		= RecBravo.passos[_f_Passo_Rec_Sel].percent_microondas;
					switch(RecBravo.passos[_f_Passo_Rec_Sel].temperatura){
						case 0: SET_POINT.TEMPERATURA = Config.TEMPERATURA_PRE;break;
						case 1: SET_POINT.TEMPERATURA = Config.TEMPERATURA_PRE_2;break;
						default:SET_POINT.TEMPERATURA = RecBravo.passos[_f_Passo_Rec_Sel].temperatura; break;
					}
					SET_POINT.VELOCIDADE_MOTOR 	= RecBravo.passos[_f_Passo_Rec_Sel].vel_motor;
					SET_POINT.RI 				= RecBravo.passos[_f_Passo_Rec_Sel].percent_teto_lastro;

					//				if(RecBravo.passos[_f_Passo_Rec_Sel].instrucao[0]!=0x00){
					//					_f_Info_Passo = 1;
					//				}
				}
			}

			//if(SET_POINT.MICROONDAS>0){_f_AguardaCorrente_Mag=1;}
		}
	}

}




//============================================================
// CONTROLES DE OPERACOES
//============================================================
void _ctrl_mag(void){
	if(!_f_PreAquecimento){
		if(!SET_POINT.MICROONDAS){
			POWER_DIG_OUT.bits.RELE_MAG = OFF;
		}
		else{
			if(_c_Ciclo_Mag<=(MAX_CICLO_MAG-(POTENCIA_MAG[SET_POINT.MICROONDAS/10]*10))){
				POWER_DIG_OUT.bits.RELE_MAG = ON;
			}
			else{
				POWER_DIG_OUT.bits.RELE_MAG = OFF;
			}
		}
	}
	else{
		POWER_DIG_OUT.bits.RELE_MAG = OFF;
	}
}
void _ctrl_aquecimento(void){
	int32_t _f_SET_POINT=0;
	uint8_t _f_SET_POINT_RI=0;
	uint8_t _f_SET_POINT_MICROONDAS=0;

	if(!_f_PreAquecimento){
		_f_SET_POINT = SET_POINT.TEMPERATURA;
		_f_SET_POINT_RI = SET_POINT.RI;
		_f_SET_POINT_MICROONDAS = SET_POINT.MICROONDAS;
	}
	else{
		SET_POINT.TEMPERATURA = SET_PRE.TEMPERATURA_PRE;
		_f_SET_POINT = SET_PRE.TEMPERATURA_PRE;//SET_POINT.TEMPERATURA;//Config.TEMPERATURA_PRE;
		_f_SET_POINT_RI = 100;
		_f_SET_POINT_MICROONDAS = 0;
	}


	//MICRO-ONDEAS LIGADA FICA CICLANDO AS DUAS RESISTENCIAS
	if(_c_Ciclo_Aquec<(((MAX_CICLO_AQ*(100-_f_SET_POINT_RI)/100))-1)){
		POWER_DIG_OUT.bits.AQ_RI = OFF;
		if(_f_SET_POINT_MICROONDAS){
			if(POWER.TEMP_1 < (_f_SET_POINT-HISTERESE_TEMP_CAM)){ 	POWER_DIG_OUT.bits.AQ_CAM = ON; }
			else {													POWER_DIG_OUT.bits.AQ_CAM = OFF;}
		}
	}
	else if(_c_Ciclo_Aquec<(((MAX_CICLO_AQ*(100-_f_SET_POINT_RI)/100)))){
		POWER_DIG_OUT.bits.AQ_CAM = OFF;
		POWER_DIG_OUT.bits.AQ_RI = OFF;
	}
	else if(_c_Ciclo_Aquec<(MAX_CICLO_AQ-1)){
		POWER_DIG_OUT.bits.AQ_CAM = OFF;
		if(POWER.TEMP_2 < (_f_SET_POINT-HISTERESE_TEMP_RI)){ 	POWER_DIG_OUT.bits.AQ_RI = ON; }
		else {													POWER_DIG_OUT.bits.AQ_RI = OFF;}
	}
	else{
		POWER_DIG_OUT.bits.AQ_CAM = OFF;
		POWER_DIG_OUT.bits.AQ_RI = OFF;
	}

	//SE MICRO-ONDAS DESLIGADA LIGA CAMARA DIRETO
	if(!_f_SET_POINT_MICROONDAS){
		if(POWER.TEMP_1 < (_f_SET_POINT-HISTERESE_TEMP_CAM)){ 	POWER_DIG_OUT.bits.AQ_CAM = ON; }
		else {													POWER_DIG_OUT.bits.AQ_CAM = OFF;}
	}

//	if(SET_POINT.VELOCIDADE_MOTOR==0 && _f_STATUS==ST_TESTAR_PASSO && _f_STATUS==ST_MODO_MANUAL_OPER && _f_STATUS==ST_EXE_LANCHES){
//		POWER_DIG_OUT.bits.AQ_RI = OFF;
//		POWER_DIG_OUT.bits.AQ_CAM = OFF;
//	}
}
void _ctrl_tempo_sonda(void){
	if(SET_POINT.TEMPO <= _c_TEMPO_CORRENTE){
		if(!(CTRL_OPERACAO&0x04) && _f_Rec_Carregada){
			CTRL_OPERACAO |= 0x04;
			UpdateScr = 1;

			//FIM DA EXECUCAO DA RECEITA POR TEMPO
			if(CTRL_OPERACAO&0x04){
				if(_f_STATUS == ST_TESTAR_PASSO){
					STATUS = ST_CONF_PASSOS_RECEITA;//ST_CONF_EDT_PARAM_PASSO_RECEITA;	//TERMINA O TESTE VOLTA PARA A TELA DE EDT PARAM DO PASSO
				}
				else{
					STATUS = ST_EXE_FINALIZACAO;
				}
			}
		}
	}
}

volatile uint8_t chg_bk = 0;

void Draw_Background(void){

//	if(chg_bk){
//		if(_f_IMG_Back){
//			GUI_BMP_Draw((void*)_tmp_ram_BACK,0,0);
//			GUI_SetColor(0x726EAF);	GUI_SetColor(_COR_BRANCO); GUI_SetColor(0x2D167E);
//		}
//		else{
//			_DrawButton(0,0,800,480,"",0x41631f,0x41631f,0x41631f,GUI_DEFAULT_FONT,CENTER,CENTER,NORMAL,0);
//		}
//	}
//	else{
		_DrawButton(0,0,800,480,"",GUI_BLACK,GUI_BLACK,GUI_BLACK,GUI_DEFAULT_FONT,CENTER,CENTER,NORMAL,0);
		//GUI_PNG_Draw((void*)_tmp_ram_B0008, size_Tux_png, 0, 0);
//	}
//
//	chg_bk = !chg_bk;
}


void _load_receita_favorito(void){
	readItemList("RECUSER/ROSTER.DAT", (s_Lista*)&Grupo_Sel, _f_Grupo_ID_Sel);
	_load_passo_receita();

	_check_pre_grupo();
}
void _load_passo_receita(void){
	uint8_t __pas = 0;
	char erro = 0;

	snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", Grupo_Sel.path);
	readItemList((char*)_tmp_char, (s_Lista*)&Rec_Sel, _f_Rec_ID_Sel);
//	memcpy((void*)&Rec_Sel, (void*)&Lista, sizeof(Lista));					//ATUALIZA A LISTA DA RECEITA SELECIONADA

	if(_f_STATUS==ST_RECEITAS || _f_STATUS==ST_FAVORITOS){
		switch(RecBravo.passos[_f_Passo_Rec_Sel].temperatura){
//			case 0: EDT_SET.TEMPERATURA = Config.TEMPERATURA_PRE;  _f_Temperatura_Pre = 0;break;	//TEMP PRE 1 NO PASSO DA RECEITA
//			case 1: EDT_SET.TEMPERATURA = Config.TEMPERATURA_PRE_2;_f_Temperatura_Pre = 1; break;	//TEMP PRE 2 NO PASSO DA RECEITA
			case 0:
			case 1:
				switch(Grupo_Sel.TEMPERATURA_PRE){
					case 0: EDT_SET.TEMPERATURA = Config.TEMPERATURA_PRE;  _f_Temperatura_Pre = 0; break;
					case 1: EDT_SET.TEMPERATURA = Config.TEMPERATURA_PRE_2; _f_Temperatura_Pre = 1; break;
				}
				break;
			default:{
				_f_Temperatura_Pre = 2;
				EDT_SET.TEMPERATURA 	= RecBravo.passos[_f_Passo_Rec_Sel].temperatura;
			}break;
		}
		/*if(Grupo_Sel.TEMPERATURA_PRE==0){SET_PRE.TEMPERATURA_PRE = Config.TEMPERATURA_PRE;}
		else{SET_PRE.TEMPERATURA_PRE = Config.TEMPERATURA_PRE_2;}*/
	}


	if(!_f_save_passo_MT){
//		_f_Temperatura_Pre = Rec_Sel.TEMPERATURA_PRE;
		memset((void*)&RecBravo, 0x00, sizeof(RecBravo));
		memset((void*)&EDT_SET, 0x00, sizeof(EDT_SET));
		if(!(erro = loadRec((char*)Rec_Sel.path, (s_RecBravo*)&RecBravo))){	//CARREGA OS PARAMETROS DA RECEITA SELECIONADA

			for(__pas=0; __pas<8; __pas++){
				if(RecBravo.passos[__pas].passo_ativo!=1){ //CASO O PASSO NAO EXISTA PARA PEGAR UM VALOR VALIDO DE PARAMETROS
					memset((void*)&RecBravo.passos[__pas], 0x00, sizeof(RecBravo.passos[__pas]));
					switch(RecBravo.passos[_f_Passo_Rec_Sel].temperatura){
						case 0:
						case 1:
							switch(Grupo_Sel.TEMPERATURA_PRE){
								case 0: EDT_SET.TEMPERATURA = Config.TEMPERATURA_PRE;  _f_Temperatura_Pre = 0; break;
								case 1: EDT_SET.TEMPERATURA = Config.TEMPERATURA_PRE_2; _f_Temperatura_Pre = 1; break;
							}
							break;
						default:{
							_f_Temperatura_Pre = 2;
							EDT_SET.TEMPERATURA 	= RecBravo.passos[_f_Passo_Rec_Sel].temperatura;
						}break;
					}
				}
			}

			EDT_SET.MICROONDAS 		= RecBravo.passos[_f_Passo_Rec_Sel].percent_microondas;

			if(RecBravo.passos[_f_Passo_Rec_Sel].passo_ativo==1){
				switch(RecBravo.passos[_f_Passo_Rec_Sel].temperatura){
				case 0:
				case 1:
					switch(Grupo_Sel.TEMPERATURA_PRE){
						case 0: EDT_SET.TEMPERATURA = Config.TEMPERATURA_PRE;  _f_Temperatura_Pre = 0; break;
						case 1: EDT_SET.TEMPERATURA = Config.TEMPERATURA_PRE_2; _f_Temperatura_Pre = 1; break;
					}
					break;
					default:{
						_f_Temperatura_Pre = 2;
						EDT_SET.TEMPERATURA 	= RecBravo.passos[_f_Passo_Rec_Sel].temperatura;
					}break;
				}
			}

			EDT_SET.TEMPO 			= RecBravo.passos[_f_Passo_Rec_Sel].tempo;
			EDT_SET.VELOCIDADE_MOTOR= RecBravo.passos[_f_Passo_Rec_Sel].vel_motor;
			EDT_SET.RI 				= RecBravo.passos[_f_Passo_Rec_Sel].percent_teto_lastro;

		}
		else{
			memset((void*)&RecBravo, 0x00, sizeof(RecBravo));
			switch(erro){
				case 4:{ snprintf((char*)RecBravo.cabecalho.titulo, sizeof(RecBravo.cabecalho.titulo), "ERRO REC VERSION");}break;
				case 3:{ snprintf((char*)RecBravo.cabecalho.titulo, sizeof(RecBravo.cabecalho.titulo), "ERRO REC MOD");}break;
				default:{snprintf((char*)RecBravo.cabecalho.titulo, sizeof(RecBravo.cabecalho.titulo), "ERRO REC");}break;
			}
			EDT_SET.TEMPERATURA		= 0;
			EDT_SET.MICROONDAS 		= 0;
			EDT_SET.TEMPERATURA 	= 0;
			EDT_SET.VELOCIDADE_MOTOR= 0;
			EDT_SET.RI 				= 0;
		}

		if(!_f_PreAquecimento){
			SET_POINT.MICROONDAS = EDT_SET.MICROONDAS;
			SET_POINT.RI = EDT_SET.RI;
			SET_POINT.TEMPERATURA = EDT_SET.TEMPERATURA;
			SET_POINT.TEMPO = EDT_SET.TEMPO;
			SET_POINT.VELOCIDADE_MOTOR = EDT_SET.VELOCIDADE_MOTOR;
		}

	}
	else{
		Rec_Sel.TEMPERATURA_PRE = _f_Temperatura_Pre;
	}

	_TMP_VALOR = erro;
	strip((char*)Rec_Sel.titulo); //REMOVE O \n DO NOME PARA EXIBIR NO TOPO *NAO PODE SER SALVO ASSIM

}
void _load_grupo_sel(void){

	if(_f_STATUS != ST_CONF_ORDENAR_POS_GRUPO){
		if(_f_STATUS==ST_HOME){
			readItemList("RECUSER/ROSTER_PRE.DAT", (s_Lista*)&Grupo_Sel, _f_Grupo_ID_Sel);
		}
		else{
			readItemList("RECUSER/ROSTER.DAT", (s_Lista*)&Grupo_Sel, _f_Grupo_ID_Sel);
		}
		strip((char*)Grupo_Sel.titulo);	//REMOVE O \n DO TEXTO PARA EXIBIT NO TITULO

		_check_pre_grupo();
	}
}
void _load_grupos_pre(void){
	char file_pre[40];
	char file[40];
	uint32_t __total_Grp=0;
	uint8_t  __pos_Grp=0;
	uint8_t  __pre_atual = 0;

	snprintf(file, sizeof(file), "RECUSER/ROSTER.DAT");
	snprintf(file_pre, sizeof(file_pre), "RECUSER/ROSTER_PRE.DAT");


	deleteFile(file_pre);

	if(SET_PRE.TEMPERATURA_PRE==Config.TEMPERATURA_PRE){__pre_atual=0;}else{__pre_atual=1;}


	__total_Grp = readTotalId(file);				//TOTAL DE GRUPOS

	if(__total_Grp>0){	_f_MAX_PAGE = (__total_Grp-1)/8;}
	else{				_f_MAX_PAGE = 0;}

	//BOTÕES
	for(__pos_Grp=0; __pos_Grp<__total_Grp; __pos_Grp++){

		if(readItemList((char*)file, (s_Lista*)&Lista, __pos_Grp)){break;}

		if(Lista.TEMPERATURA_PRE==__pre_atual){
			addItemList(file_pre, Lista);
		}

	}
}
void _load_edt_aquec_dourar(void){
	if(_f_PP_teclado==PP_AQUECER){
		_c_TecladoPos=0;
		_c_TecladoMax=2;
		//snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"%02ds",RecBravo.cabecalho.aquecer);
		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"  s");
		hMem2=GUI_MEMDEV_Create(128,90,260,42);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButtonTransparente(128,90,260,42,(char*)_s_TecladoDigitado,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,CENTER);
		_DrawButtonTransparente(128,90,260,42,"__  ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,BOTTOM);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}
	if(_f_PP_teclado==PP_DOURAR){
		_c_TecladoPos=0;
		_c_TecladoMax=2;
		//snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"%02ds",RecBravo.cabecalho.dourar);
		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"  s");
		hMem2=GUI_MEMDEV_Create(128,206,260,42);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButtonTransparente(128,206,260,42,(char*)_s_TecladoDigitado,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,CENTER);
		_DrawButtonTransparente(128,206,260,42,"__  ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,BOTTOM);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}
	if(_f_PP_teclado==PP_AQUECER_DOURAR){
		_c_TecladoPos=0;
		_c_TecladoMax=2;
		//snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"%02ds",RecBravo.cabecalho.dourar_aquecer);
		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"  s");
		hMem2=GUI_MEMDEV_Create(128,322,260,42);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButtonTransparente(128,322,260,42,(char*)_s_TecladoDigitado,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,CENTER);
		_DrawButtonTransparente(128,322,260,42,"__  ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,BOTTOM);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}

}
void _load_edt_pp_temp(void){
	if(_f_PP_teclado==PP_TEMP_TETO){
		_c_TecladoPos=0;
		_c_TecladoMax=3;
//		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"%03dºC",(Config.TEMPERATURA_PRE/FATOR_TEMP));
		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"   %s",UN_TEMP[Config.Escala_Temperatura]);
		hMem2=GUI_MEMDEV_Create(15,50,160,99);	GUI_MEMDEV_Select(hMem2);

		_DrawButton(15,50,160,99,(char*)_s_TecladoDigitado,_COR_CINZA_CLARO, _COR_PRETO,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);
		_DrawButtonTransparente(15,50,160,99," ___",_COR_PRETO,&SIF_FONT_EXO2_REGULAR_65,LEFT,BOTTOM);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}
	if(_f_PP_teclado==PP_TEMP_LASTRO){
		_c_TecladoPos=0;
		_c_TecladoMax=3;
//		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"%03dºC",(Config.TEMPERATURA_PRE_LASTRO)/FATOR_TEMP);
		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"   %s",UN_TEMP[Config.Escala_Temperatura]);
		hMem2=GUI_MEMDEV_Create(15,168,160,99);	GUI_MEMDEV_Select(hMem2);

		_DrawButton(15,168,160,99,(char*)_s_TecladoDigitado,_COR_CINZA_CLARO, _COR_PRETO,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);
		_DrawButtonTransparente(15,168,160,99," ___",_COR_PRETO,&SIF_FONT_EXO2_REGULAR_65,LEFT,BOTTOM);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}
	if(_f_PP_teclado==PP_TEMPO){
		_c_TecladoPos=0;
		_c_TecladoMax=4;


		//		snprintf(text,sizeof(text),"%02d:%02d",(SET_POINT.TEMPO/60),(SET_POINT.TEMPO%60));
		hMem2=GUI_MEMDEV_Create(15,282,160,99);	GUI_MEMDEV_Select(hMem2);
//		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"%02d%02d",(Config.TEMPO_PRE/60),(Config.TEMPO_PRE%60));
		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"    ");
		_DrawButton(15,282,160,99,(char*)_s_TecladoDigitado,_COR_CINZA_CLARO, _COR_PRETO,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);
		_DrawButtonTransparente(15,282,160,99,"__ __",_COR_PRETO,&SIF_FONT_EXO2_REGULAR_65,CENTER,BOTTOM);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}

}
void _load_edt_data_hora(void){
	char __text[15];
	if(_f_PP_teclado==PP_AJUSTE_DATA){
		if(_f_last_PP_teclado!=_f_PP_teclado){snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"        "); _c_TecladoPos=0;_c_TecladoMax=8;}

		if(_f_last_PP_teclado==_f_PP_teclado){
			Config.Formato_data++;
			if(Config.Formato_data>2){Config.Formato_data=0;}
			UpdateScr = 1;
		}

		hMem2=GUI_MEMDEV_Create(152,56,220,55);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		switch(Config.Formato_data){
		case 0:
		case 1:{
			snprintf(__text,sizeof(__text),"%c%c/%c%c/%c%c%c%c", _s_TecladoDigitado[0],_s_TecladoDigitado[1], _s_TecladoDigitado[2],_s_TecladoDigitado[3],_s_TecladoDigitado[4],_s_TecladoDigitado[5], _s_TecladoDigitado[6],_s_TecladoDigitado[7]);
			_DrawButtonTransparente(152,56,220,55,"__  __  ____ ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_48,CENTER,BOTTOM);
		}break;
		case 2:{
			snprintf(__text,sizeof(__text),"%c%c%c%c/%c%c/%c%c", _s_TecladoDigitado[0],_s_TecladoDigitado[1], _s_TecladoDigitado[2],_s_TecladoDigitado[3],_s_TecladoDigitado[4],_s_TecladoDigitado[5], _s_TecladoDigitado[6],_s_TecladoDigitado[7]);
			_DrawButtonTransparente(152,56,220,55,"____  __  __ ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_48,CENTER,BOTTOM);
		}break;
		}

		_DrawButtonTransparente(152,56,220,55,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_48,CENTER,CENTER);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}
	if(_f_PP_teclado==PP_AJUSTE_HORAS){
		hMem2=GUI_MEMDEV_Create(152,167,220,55);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();

		if(_f_last_PP_teclado!=_f_PP_teclado){snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"    "); _c_TecladoPos=0;_c_TecladoMax=4;}

		if(_f_last_PP_teclado==_f_PP_teclado){
			if(Config.Formato_hora&0x01){
				if(Config.Formato_hora&0x10){Config.Formato_hora= 0x01;}
				else{Config.Formato_hora=0x00;}
			}
			else{
				Config.Formato_hora = 0x01 | 0x10;
			}
			UpdateScr = 1;
		}
		if(Config.Formato_hora&0x01){
			snprintf(__text,sizeof(__text),"  %c%c:%c%c%s",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],_s_TecladoDigitado[3],Config.Formato_hora&0x10?"am":"pm");
			_DrawButtonTransparente(152,167,220,55,"__ __       ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_55,CENTER,BOTTOM);
		}
		else{
			snprintf(__text,sizeof(__text),"     %c%c:%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
			_DrawButtonTransparente(152,167,220,55,"__ __ ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_55,CENTER,BOTTOM);
		}

		_DrawButtonTransparente(152,167,220,55,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_55,LEFT,CENTER);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

	}
	_f_last_PP_teclado=_f_PP_teclado;
}
void _load_edt_formato_hora(void){
	switch(_f_Botao_click){
	case 1:{
		if(Config.Formato_hora&0x01){Config.Formato_hora&=~0x01;}else{Config.Formato_hora|=0x01;}
		UpdateScr=1;
	}break;
	case 2:{
		if(Config.Formato_hora&0x10){Config.Formato_hora&=~0x10;}else{Config.Formato_hora|=0x10;}
		UpdateScr=1;
	}break;
	}

	get_rtctime();
	if(Config.Formato_hora&0x01){
		if(Config.Formato_hora&0x10 && DATA_HORA.hora>12){DATA_HORA.hora-=12;}
		else if(DATA_HORA.hora<12){DATA_HORA.hora+=12;}
	}


	if(valida_data(DATA_HORA.dia, DATA_HORA.mes, DATA_HORA.ano)){get_rtctime();}
	else{set_rtctime();}


	_f_Botao_click = 0;
}
void _load_padrao_fabrica(void){
	Config.TEMPERATURA_PRE = (350-32) * FATOR_TEMP_F;
	Config.TEMPERATURA_PRE_LASTRO = 275 * FATOR_TEMP;
	Config.TEMPERATURA_PRE_2 = (500-32)*FATOR_TEMP_F;
	Config.TEMPO_PRE = 5 * 60;
	Config.ACESSOS = 0xFF;

	Config.SENHA_USER[0] = '4';
	Config.SENHA_USER[1] = '5';
	Config.SENHA_USER[2] = '6';
	Config.SENHA_USER[3] = '7';
	Config.SENHA_USER[4] = '8';
	Config.SENHA_USER[5] = '9';
	memcpy((void*)SenhaUser, (void*)Config.SENHA_USER, sizeof(SenhaUser));

	_c_save_eeprom = 1;
	Delete_Receitas_SD();

	update_IOT_Version();

	STATUS=ST_CONFIGURACOES;
}
void _load_edt_dados_forno(void){

	if(_f_PP_teclado==PP_NUM_SERIE){
		_c_TecladoPos=0;
		_c_TecladoMax=6;

		//snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"%c%c%c%c%c%c", Config.NUM_SERIE[0], Config.NUM_SERIE[1], Config.NUM_SERIE[2], Config.NUM_SERIE[3], Config.NUM_SERIE[4]);
		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"      ");
//		_DrawButtonTransparente(202,104,174,36,"__  ___",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_24,CENTER,TOP);

	}

	else if(_f_PP_teclado==PP_DATA_FAB){
		_c_TecladoPos=0;
		_c_TecladoMax=8;

		//snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"%c%c%c%c%c%c%c%c", Config.DATA_FABRICACAO[0],Config.DATA_FABRICACAO[1], Config.DATA_FABRICACAO[2], Config.DATA_FABRICACAO[3], Config.DATA_FABRICACAO[4], Config.DATA_FABRICACAO[5],Config.DATA_FABRICACAO[6],Config.DATA_FABRICACAO[7]);
		snprintf((char*)_s_TecladoDigitado,sizeof(_s_TecladoDigitado),"        ");
		//_DrawButtonTransparente(202,230,174,36,"__ __ ____",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_24,CENTER,TOP);
	}


}
void load_param_teclado(void){
	memset((char*)&_s_TecladoDigitado,0x00,sizeof(_s_TecladoDigitado));
	switch(_f_STATUS){
		case ST_CONF_INTERNET:{
			if		(_f_PP_teclado==PP_IP){
				_c_TecladoMax=12;
//				snprintf((char*)_s_TecladoDigitado,sizeof(Config.IP),"%s", Config.IP);
				snprintf((char*)_s_TecladoDigitado,sizeof(Config.IP),"            ");
				_c_TecladoPos=0;//strlen((char*)_s_TecladoDigitado);
			}
			else if (_f_PP_teclado==PP_MASCARA){
				_c_TecladoMax=12;
				//snprintf((char*)_s_TecladoDigitado,sizeof(Config.MASCARA),"%s", Config.MASCARA);
				snprintf((char*)_s_TecladoDigitado,sizeof(Config.MASCARA),"            ");
				_c_TecladoPos=0;//strlen((char*)_s_TecladoDigitado);
			}
			else if (_f_PP_teclado==PP_GATEWAY){
				_c_TecladoMax=12;
				//snprintf((char*)_s_TecladoDigitado,sizeof(Config.GATWAY),"%s", Config.GATWAY);
				snprintf((char*)_s_TecladoDigitado,sizeof(Config.GATWAY),"            ");
				_c_TecladoPos=0;//strlen((char*)_s_TecladoDigitado);
			}
			else if (_f_PP_teclado==PP_DNS){
				_c_TecladoMax=12;
				//snprintf((char*)_s_TecladoDigitado,sizeof(Config.DNS),"%s", Config.DNS);
				snprintf((char*)_s_TecladoDigitado,sizeof(Config.DNS),"            ");
				_c_TecladoPos=0;//strlen((char*)_s_TecladoDigitado);
			}
		}break;
	}
}
void _check_pre_grupo(void){
	int32_t __menor_temp = 0;
	if(_f_STATUS==ST_HOME || _f_STATUS==ST_FAVORITOS){
		if((Grupo_Sel.TEMPERATURA_PRE==0 && SET_PRE.TEMPERATURA_PRE!=Config.TEMPERATURA_PRE && Config.TEMPERATURA_PRE!=Config.TEMPERATURA_PRE_2) ||
				(Grupo_Sel.TEMPERATURA_PRE==1 && SET_PRE.TEMPERATURA_PRE!=Config.TEMPERATURA_PRE_2 && Config.TEMPERATURA_PRE!=Config.TEMPERATURA_PRE_2)){
			SET_PRE.LAST_TEMPERATURA_PRE = SET_PRE.TEMPERATURA_PRE;
			if(Grupo_Sel.TEMPERATURA_PRE==0){SET_PRE.TEMPERATURA_PRE = Config.TEMPERATURA_PRE;}else{SET_PRE.TEMPERATURA_PRE = Config.TEMPERATURA_PRE_2;}
			SET_POINT.TEMPERATURA = SET_PRE.TEMPERATURA_PRE;
			if(SET_PRE.TEMPERATURA_PRE>=SET_PRE.LAST_TEMPERATURA_PRE){SET_PRE.AQUECE=1;}else{SET_PRE.AQUECE=0;}
			if(Config.TEMPERATURA_PRE<Config.TEMPERATURA_PRE_2){__menor_temp=Config.TEMPERATURA_PRE;} else{__menor_temp=Config.TEMPERATURA_PRE_2;}

			if(SET_PRE.BYPASS==0){
				if((POWER.TEMP_1<__menor_temp) || (POWER.TEMP_2<__menor_temp)){
					STATUS = ST_PRE_AQUECIMENTO;
					SET_PRE.PREAQUECEU = 0;
				}
				else{
					STATUS = ST_PRE_AQUEC_GRP;
				}
			}
		}
	}
}
uint8_t _check_name_grupo(void){
	uint8_t __c_grp = 0;
	uint8_t __t_grp = 0;
	__t_grp = readTotalId("RECUSER/ROSTER.DAT");
	for(__c_grp=0;__c_grp<__t_grp; __c_grp++){
		if(_f_Grupo_ID_Sel!=__c_grp){
			readItemList("RECUSER/ROSTER.DAT", (s_Lista*)&Lista, __c_grp);
			if(memcmp((const void*)&Grupo_Sel.titulo, (const void*)&Lista.titulo, sizeof(Lista.titulo))==0){return 1;}
		}
	}
	return 0;
}
uint8_t _check_name_receitas(void){
	uint32_t __c_rec = 0;
	uint32_t __t_rec = 0;
	char path[50];
	snprintf(path,sizeof(path),"%s/ROSTER.DAT",Grupo_Sel.path);
	__t_rec = readTotalId(path);
	for(__c_rec=0;__c_rec<__t_rec; __c_rec++){
		if(_f_Rec_ID_Sel!=__c_rec){
			readItemList(path, (s_Lista*)&Lista, __c_rec);
			if(memcmp((const void*)&Rec_Sel.titulo, (const void*)&Lista.titulo, sizeof(Lista.titulo))==0){return 1;}
		}
	}
	return 0;
}
void upd_SetPoint(void){
	SET_POINT.MICROONDAS = EDT_SET.MICROONDAS;
	SET_POINT.RI = EDT_SET.RI;
	SET_POINT.TEMPERATURA = EDT_SET.TEMPERATURA;
	SET_POINT.TEMPO = EDT_SET.TEMPO;
	SET_POINT.VELOCIDADE_MOTOR = EDT_SET.VELOCIDADE_MOTOR;
}



void verifica_contadores(void){

	createFolder("CNT");

	if(checkFile("CNT/LIMPEZAS.DAT")){createFile("CNT/LIMPEZAS.DAT");}
	if(checkFile("CNT/FALHAS.DAT")){createFile("CNT/FALHAS.DAT");}


}


void _config_edt_acessos(void){
	if(Config.ACESSOS&_f_Botao_click){
		Config.ACESSOS &= ~_f_Botao_click;
	}
	else{
		Config.ACESSOS |= _f_Botao_click;
	}

	_c_save_eeprom = 3;
}


void _save_rec_sel(void){
	snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", Grupo_Sel.path);
	uptItemList((char*)_tmp_char, Rec_Sel, _f_Rec_ID_Sel);

	saveRec((char*)Rec_Sel.path, (s_RecBravo*)&RecBravo);
	_load_passo_receita();
	update_IOT_Version();
}
void _save_passo_ed_rec_sel(void){

	if(_f_STATUS == ST_CONF_EDT_PASSO_RECEITA){	//SALVAR ESTES PARAMETROS APENAS NA EDICAO DOS PARAM DO PASSO
		RecBravo.passos[_f_Passo_Rec_Sel].passo_ativo 			= 1;
		switch(_f_Temperatura_Pre){
			case 0:
			case 1:  RecBravo.passos[_f_Passo_Rec_Sel].temperatura = _f_Temperatura_Pre; break;
			default: RecBravo.passos[_f_Passo_Rec_Sel].temperatura = EDT_SET.TEMPERATURA;break;
		}
		RecBravo.passos[_f_Passo_Rec_Sel].tempo 				= EDT_SET.TEMPO;
		RecBravo.passos[_f_Passo_Rec_Sel].percent_microondas 	= EDT_SET.MICROONDAS;
		RecBravo.passos[_f_Passo_Rec_Sel].percent_teto_lastro 	= EDT_SET.RI;
		RecBravo.passos[_f_Passo_Rec_Sel].vel_motor 			= EDT_SET.VELOCIDADE_MOTOR;
	}

	snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", Grupo_Sel.path);
	uptItemList((char*)_tmp_char, Rec_Sel, _f_Rec_ID_Sel);

	_save_rec_sel();

	_load_passo_receita();
}
void _save_erro(void){
	if(_f_SalvaErro!=ERROS.total && !_c_SalvaErro && !_f_USB_Connet){	//NAO ATUALIZA FORA DO TEMPO OU SE ESTIVER EM PROCESSO NO USB
		memset((char*)&Contadores[10],0x00,sizeof(Contadores[10]));
		Contadores[10].ano 	= DATA_HORA.ano;
		Contadores[10].mes 	= DATA_HORA.mes;
		Contadores[10].dia 	= DATA_HORA.dia;
		Contadores[10].hora = DATA_HORA.hora;
		Contadores[10].min 	= DATA_HORA.min;
		Contadores[10].cont = 0;


		if(ERROS.Erro){
			_f_SalvaErro |= 0x01;

			if(ERROS.CAM && ERROS.IR){
				if(!(_f_SalvaErro&0x02) || !(_f_SalvaErro&0x04)){
					snprintf((char*)Contadores[10].text,sizeof(Contadores[10].text),"%s %s",TXT_ERRO[ID_IDIOMA],TXT_SENSORES_TEMPERATURA[ID_IDIOMA]);
					addRegistro("CNT/FALHAS.DAT", (char*)&Contadores[10], sizeof(Contadores[10]));
					_f_SalvaErro |= 0x02 | 0x04;
				}
			}
			if(ERROS.INV==1){
				if(!(_f_SalvaErro&0x20)){
					snprintf((char*)Contadores[10].text,sizeof(Contadores[10].text),"%s %s",TXT_ERRO[ID_IDIOMA],TXT_INVERSOR[ID_IDIOMA]);
					addRegistro("CNT/FALHAS.DAT", (char*)&Contadores[10], sizeof(Contadores[10]));
					_f_SalvaErro |= 0x20;
				}
			}
			if(ERROS.Painel==1){
				if(!(_f_SalvaErro&0x08)){
					snprintf((char*)Contadores[10].text,sizeof(Contadores[10].text),"%s %s",TXT_ERRO[ID_IDIOMA],TXT_PAINEL[ID_IDIOMA]);
					addRegistro("CNT/FALHAS.DAT", (char*)&Contadores[10], sizeof(Contadores[10]));
					_f_SalvaErro |= 0x08;
				}
			}
			if(ERROS.CAN){
				if(!(_f_SalvaErro&0x80)){
					snprintf((char*)Contadores[10].text,sizeof(Contadores[10].text),"%s %s",TXT_ERRO[ID_IDIOMA],TXT_REDE_CAN[ID_IDIOMA]);
					addRegistro("CNT/FALHAS.DAT", (char*)&Contadores[10], sizeof(Contadores[10]));
					_f_SalvaErro |= 0x80;
				}
			}
			if(ERROS.MAG){
				if(!(_f_SalvaErro&0x40)){
					snprintf((char*)Contadores[10].text,sizeof(Contadores[10].text),"%s %s",TXT_ERRO[ID_IDIOMA],TXT_CLMG[ID_IDIOMA]);
					addRegistro("CNT/FALHAS.DAT", (char*)&Contadores[10], sizeof(Contadores[10]));
					_f_SalvaErro |= 0x40;
				}
			}
		}
		else{
			if(ERROS.CAM){
				if(!(_f_SalvaErro&0x02)){
					snprintf((char*)Contadores[10].text,sizeof(Contadores[10].text),"%s %s",TXT_ALERTA[ID_IDIOMA],TXT_SENSOR_CAMARA[ID_IDIOMA]);
					addRegistro("CNT/FALHAS.DAT", (char*)&Contadores[10], sizeof(Contadores[10]));
					_f_SalvaErro |= 0x02;
				}
			}else{_f_SalvaErro &= ~0x02;}
			if(ERROS.IR){
				if(!(_f_SalvaErro&0x04)){
					snprintf((char*)Contadores[10].text,sizeof(Contadores[10].text),"%s %s",TXT_ALERTA[ID_IDIOMA],TXT_SENSOR_INFERIOR[ID_IDIOMA]);
					addRegistro("CNT/FALHAS.DAT", (char*)&Contadores[10], sizeof(Contadores[10]));
					_f_SalvaErro |= 0x04;
				}
			}else{_f_SalvaErro &= ~0x04;}
			if(ERROS.Painel==2){
				if(!(_f_SalvaErro&0x10)){
					snprintf((char*)Contadores[10].text,sizeof(Contadores[10].text),"%s %s",TXT_ALERTA[ID_IDIOMA],TXT_PAINEL[ID_IDIOMA]);
					addRegistro("CNT/FALHAS.DAT", (char*)&Contadores[10], sizeof(Contadores[10]));
					_f_SalvaErro |= 0x10;
				}
			}else{_f_SalvaErro &= ~0x10;}
			if(ERROS.MAG){
				if(!(_f_SalvaErro&0x40)){
					snprintf((char*)Contadores[10].text,sizeof(Contadores[10].text),"%s %s",TXT_ALERTA[ID_IDIOMA],TXT_CLMG[ID_IDIOMA]);
					addRegistro("CNT/FALHAS.DAT", (char*)&Contadores[10], sizeof(Contadores[10]));
					_f_SalvaErro |= 0x40;
				}
			}else{_f_SalvaErro &= ~0x40;}
		}

		_c_SalvaErro = 5;			//VERIFICA A CADA 30SEGUNDOS SE HA ERRO PARA SALVAR
	}
}
void _save_grp_sel(void){
	uptItemList("RECUSER/ROSTER.DAT", Grupo_Sel, _f_Grupo_ID_Sel);
	update_IOT_Version();
}

void reordenar_lista(void){
	if		(_f_STATUS == ST_CONF_ORDENAR_OP_GRUPO){
		if(_f_Tmp_ID_Sel){
			_f_Tmp_ID_Sel = readTotalId("RECUSER/ROSTER.DAT");
			if(_f_Tmp_ID_Sel>=1){	_f_Tmp_ID_Sel -= 1;}
		}

		reposItemList("RECUSER/ROSTER.DAT", _f_Grupo_ID_Sel, _f_Tmp_ID_Sel);
		_f_Grupo_ID_Sel = _f_Tmp_ID_Sel;

		STATUS=ST_CONF_MENU_EDT_GRUPO;
	}
	else if	(_f_STATUS == ST_CONF_ORDENAR_OP_RECEITA){

		snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", Grupo_Sel.path);

		if(_f_Tmp_ID_Sel){		//SE FOR 1 PEGA A ULTIMA POSICAO PARA MOVER PARA O FIM DA LISTA
			_f_Tmp_ID_Sel = readTotalId((char*)_tmp_char);
			if(_f_Tmp_ID_Sel>=1){	_f_Tmp_ID_Sel -= 1;}
		}

		reposItemList((char*)_tmp_char, _f_Rec_ID_Sel, _f_Tmp_ID_Sel);
		_f_Rec_ID_Sel = _f_Tmp_ID_Sel;

		STATUS=ST_CONF_MENU_EDT_RECEITA;
	}
	else if (_f_STATUS == ST_CONF_ORDENAR_POS_RECEITA){
		snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", Grupo_Sel.path);

		reposItemList((char*)_tmp_char, _f_Tmp_ID_Sel, _f_Rec_ID_Sel);
		//_f_Rec_ID_Sel = _f_Tmp_ID_Sel;

		STATUS=ST_CONF_MENU_EDT_RECEITA;
	}
	else if (_f_STATUS == ST_CONF_ORDENAR_POS_GRUPO){

		reposItemList("RECUSER/ROSTER.DAT", _f_Tmp_ID_Sel, _f_Grupo_ID_Sel);
//		_f_Grupo_ID_Sel = _f_Tmp_ID_Sel;

		STATUS=ST_CONF_MENU_EDT_GRUPO;
	}
	//!TALVES SEJA INTERESSANTE COLOCAR UMA TELA DE AGUARDE
}

void msg_erro(void){
	if(!ERROS.Erro){

		snprintf((char*)_tmp_char,100,"%s-",TXT_ALERTA[ID_IDIOMA]);

		if		(ERROS.CAM){snprintf((char*)_tmp_char,100,"%s %s",_tmp_char,TXT_FALHA_SENSOR_CAMARA[ID_IDIOMA]);}
		else if	(ERROS.IR){snprintf((char*)_tmp_char,100,"%s %s",_tmp_char,TXT_FALHA_SENSOR_INFERIOR[ID_IDIOMA]);}
		else if	(ERROS.Painel==2){snprintf((char*)_tmp_char,100,"%s %s",_tmp_char,TXT_TEMP_ALTA_PAINEL[ID_IDIOMA]);}
		else if	(ERROS.MAG){snprintf((char*)_tmp_char,100,"%s %s",_tmp_char,"CLMG");}

	}
	else{
		snprintf((char*)_tmp_char,100,"%s-",TXT_ERRO[ID_IDIOMA]);

		if		(ERROS.CAN){snprintf((char*)_tmp_char,100,"%s %s",_tmp_char,TXT_FALHA_REDE_CAN[ID_IDIOMA]);}
		else if	(ERROS.CAM && ERROS.IR){snprintf((char*)_tmp_char,100,"%s %s",_tmp_char,TXT_FALHA_SENSORES_TEMPERATURA[ID_IDIOMA]);}
		else if	(ERROS.Painel==1){snprintf((char*)_tmp_char,100,"%s %s,",_tmp_char,TXT_TEMP_ALTA_PAINEL[ID_IDIOMA]);}
		else if	(ERROS.INV){snprintf((char*)_tmp_char,100,"%s %s",_tmp_char,TXT_FALHA_COM_INVERSOR[ID_IDIOMA]);}
		//else if	(ERROS.MAG){snprintf((char*)_tmp_char,100,"%s %s",_tmp_char,TXT_CLMG_BAIXA_CORRENTE_MAG[ID_IDIOMA]);}

		if(_f_STATUS!=ST_GRUPOS){
			snprintf((char*)_tmp_char,100,"%s %s",_tmp_char,TXT_DESLIGUE_LIGUE[ID_IDIOMA]);
		}
		else{
			snprintf((char*)_tmp_char,100,"%s %s",_tmp_char,TXT_DESLIGUE_LIGUE_FORNO[ID_IDIOMA]);
		}
	}


}

void set_icone(void){
	char file[50];

	if(Grupo_Sel.TEMPERATURA_PRE==0){Grupo_Sel.TEMPERATURA_PRE=0;/*Config.TEMPERATURA_PRE*/;}//ADOTA A TEMPERATURA DO GRP1 POR PADRAO

	if(_f_STATUS==ST_CONF_SEL_IMG_GRUPO || _f_STATUS==ST_CONF_EDT_NOME_GRUPO  || _f_STATUS==ST_CONF_ADD_NOME_GRUPO){

		if(!_f_Ico_ID_Sel){
			if		(Grupo_Sel.titulo[0]>=0x41 && Grupo_Sel.titulo[0]<=0x5A){	//LETRA DE A-Z
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "\xE1\x84%c",0x80+(Grupo_Sel.titulo[0]-0x41));
			}
			else if	(Grupo_Sel.titulo[0]>=0x31 && Grupo_Sel.titulo[0]<=0x39){	//NUMERO DE 1-9
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "\xE1\x84%c",0x9A+(Grupo_Sel.titulo[0]-0x31));
			}
			else if (Grupo_Sel.titulo[0]>=0x21 && Grupo_Sel.titulo[0]<=0x2F){		//SIMBOLOS
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "\xE1\x84%c",0xA4+(Grupo_Sel.titulo[0]-0x21));
			}
			else if (Grupo_Sel.titulo[0]==0x40){//@
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "\xE1\x84\xB9");
			}
			else if (Grupo_Sel.titulo[0]==0xC2 && Grupo_Sel.titulo[1]==0xB0){//°
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "\xE1\x84\xBA");
			}
			else if (Grupo_Sel.titulo[0]==0xC3 && Grupo_Sel.titulo[1]==0x87){//Ç
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "\xE1\x84\xBB");
			}
			else if (Grupo_Sel.titulo[0]==0x3A){//:
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "\xE1\x84\xB3");
			}
			else if (Grupo_Sel.titulo[0]==0x3E){//>
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "\xE1\x84\xB7");
			}
			else if (Grupo_Sel.titulo[0]==0x3C){//<
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "\xE1\x84\xB5");
			}
			else if	(Grupo_Sel.titulo[0]==0x30){//NUMERO 0
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "%s",ICO_ZERO);
			}

			Grupo_Sel.FLAGS |= 0x02;	//SETA QUE ESTA USANDO A PRIMEIRA LETRA DO TITULO
		}
		else{
			snprintf(file, sizeof(file), "SYSTEM/CFG/ICO_GRP.DAT");
			readItemList(file, (s_Lista*)&Lista, _f_Ico_ID_Sel);
			memcpy((void*)&Grupo_Sel.ico, (void*)&Lista.ico, sizeof(Grupo_Sel.ico));

			Grupo_Sel.FLAGS &= ~0x02;	//SETA QUE ESTA USANDO ICONE
		}

		if(_f_STATUS!=ST_CONF_EDT_NOME_GRUPO && _f_STATUS!=ST_CONF_ADD_NOME_GRUPO){
			uptItemList("RECUSER/ROSTER.DAT", Grupo_Sel, _f_Grupo_ID_Sel);	//ATUALIZA O GRUPO NA LISTA
		}

		if(_f_save_passo_MT){
			STATUS = ST_CONF_ADD_NOME_RECEITA;
		}
		else{
			if(LAST_STATUS == ST_CONF_ADD_NOME_GRUPO){
				STATUS = ST_CONF_ORDENAR_OP_GRUPO;
			}
			else if(_f_STATUS==ST_CONF_EDT_NOME_GRUPO || _f_STATUS==ST_CONF_ADD_NOME_GRUPO){

			}
			else{
				STATUS = ST_CONF_MENU_EDT_GRUPO;
			}
		}
	}
	else{

		if(!_f_Ico_ID_Sel){
			if		(Rec_Sel.titulo[0]>=0x41 && Rec_Sel.titulo[0]<=0x5A){	//LETRA DE A-Z
				snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "\xE1\x84%c",0x80+(Rec_Sel.titulo[0]-0x41));
			}
			else if	(Rec_Sel.titulo[0]>=0x31 && Rec_Sel.titulo[0]<=0x39){	//NUMERO DE 1-9
				snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "\xE1\x84%c",0x9A+(Rec_Sel.titulo[0]-0x31));
			}
			else if (Rec_Sel.titulo[0]>=0x21 && Rec_Sel.titulo[0]<=0x2F){		//SIMBOLOS
				snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "\xE1\x84%c",0xA4+(Grupo_Sel.titulo[0]-0x21));
			}
			else if (Rec_Sel.titulo[0]==0x40){//@
				snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "\xE1\x84\xB9");
			}
			else if (Rec_Sel.titulo[0]==0xC2 && Rec_Sel.titulo[1]==0xB0){//°
				snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "\xE1\x84\xBA");
			}
			else if (Rec_Sel.titulo[0]==0xC3 && Rec_Sel.titulo[1]==0x87){//Ç
				snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "\xE1\x84\xBB");
			}
			else if (Rec_Sel.titulo[0]==0x3A){//:
				snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "\xE1\x84\xB3");
			}
			else if (Rec_Sel.titulo[0]==0x3E){//>
				snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "\xE1\x84\xB7");
			}
			else if (Rec_Sel.titulo[0]==0x3C){//<
				snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "\xE1\x84\xB5");
			}
			else if	(Rec_Sel.titulo[0]==0x30){//NUMERO 0
				snprintf((char*)Rec_Sel.ico, sizeof(Grupo_Sel.ico), "%s",ICO_ZERO);
			}

			Rec_Sel.FLAGS |= 0x02;	//SETA QUE ESTA USANDO A PRIMEIRA LETRA DO TITULO
		}
		else{
			snprintf(file, sizeof(file), "SYSTEM/CFG/ICO_REC.DAT");
			readItemList(file, (s_Lista*)&Lista, _f_Ico_ID_Sel);
			memcpy((void*)&Rec_Sel.ico, (void*)&Lista.ico, sizeof(Rec_Sel.ico));

			Rec_Sel.FLAGS &= ~0x02;	//SETA QUE ESTA USANDO ICONE
		}

		if(_f_STATUS!=ST_CONF_EDT_NOME_RECEITA && _f_STATUS!=ST_CONF_ADD_NOME_RECEITA){
			snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT",Grupo_Sel.path);
			uptItemList((char*)_tmp_char, Rec_Sel, _f_Rec_ID_Sel);	//ATUALIZA A RECEITA NA LISTA

			memcpy((void*)&RecBravo.cabecalho.ico, (void*)&Rec_Sel.ico, sizeof(RecBravo.cabecalho.ico));

			_save_rec_sel();										//ATUALIZA O ICONE NO ARQUIVO DA RECEITA
		}


		if(_f_save_passo_MT){
			STATUS = ST_CONF_PASSOS_RECEITA;
		}
		else{
			if(LAST_STATUS == ST_CONF_ADD_NOME_RECEITA){
				STATUS = ST_CONF_ORDENAR_OP_RECEITA;
			}
			else if(_f_STATUS==ST_CONF_EDT_NOME_RECEITA || _f_STATUS==ST_CONF_ADD_NOME_RECEITA){

			}
			else{
				STATUS = ST_CONF_MENU_EDT_RECEITA;
			}
		}
	}

}
void set_pre(void){
	if(SET_PRE.TEMPERATURA_PRE==Config.TEMPERATURA_PRE){		Grupo_Sel.TEMPERATURA_PRE = 0;}
	else if(SET_PRE.TEMPERATURA_PRE==Config.TEMPERATURA_PRE_2){ Grupo_Sel.TEMPERATURA_PRE = 1;}
}
void set_favorito(void){

	snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", Grupo_Sel.path);
	uptItemList((char*)_tmp_char, Rec_Sel, _f_Rec_ID_Sel);

	RecBravo.cabecalho.favorito = Rec_Sel.favorito;
	saveRec((char*)Rec_Sel.path, (s_RecBravo*)&RecBravo);

}
void set_delete_item(void){
	//SETA A FLAG DE DELETE DO ITEM MARCADO
	snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", Grupo_Sel.path);
	readItemList((char*)_tmp_char, (s_Lista*)&Rec_Sel, _f_Rec_ID_Sel);

	Rec_Sel.delete = !Rec_Sel.delete;
	uptItemList((char*)_tmp_char, Rec_Sel, _f_Rec_ID_Sel);
}
void del_receita(void){
	snprintf((char*)_tmp_char, sizeof(_tmp_char), "%s/ROSTER.DAT", Grupo_Sel.path);

	deleteFile((char*)Rec_Sel.path);
	deleteItemList((char*)_tmp_char, _f_Rec_ID_Sel);
}

void _verifica_rec_total_delete(void){
	uint16_t i=0;
	uint16_t t=0;

	_f_Max_Progress = 0;
	_f_Last_Progress = 0xFFFF;
	_f_Progress = 0;

	if(_f_STATUS==ST_CONF_DELETANDO_GRUPO ||			//APAGA TODAS AS RECEITAS INCLUSIVE O GRUPO
			_f_STATUS==ST_CONF_DELETANDO_REC_GRUPO){		//APAGA TODAS AS RECEITAS DO GRUPO

		snprintf((char*)_tmp_char,sizeof(_tmp_char),"%s/ROSTER.DAT",Grupo_Sel.path);
		_f_Max_Progress = readTotalId((char*)_tmp_char);
	}
	else if (_f_STATUS==ST_CONF_DELETANDO_REC_SEL_GRUPO){//APAGA AS RECEITAS SELECIONADAS DO GRUPO

		snprintf((char*)_tmp_char,sizeof(_tmp_char),"%s/ROSTER.DAT",Grupo_Sel.path);
		t = readTotalId((char*)_tmp_char);

		for(i=0; i<t; i++){
			readItemList((char*)_tmp_char, (s_Lista*)&Lista, i);
			if(Lista.delete==1){
				_f_Max_Progress+=1;
			}
		}
	}
	else if (_f_STATUS==ST_CONF_DELETANDO_RECEITA){		//APAGA APENAS A RECEITA SELECIONADA
		_f_Max_Progress = 1;
	}

}

void _delete_rec_grupo(void){
	uint16_t i=0;
	uint16_t t=0;

	if(_f_STATUS==ST_CONF_DELETANDO_GRUPO ||				//APAGA TODAS AS RECEITAS INCLUSIVE O GRUPO
			_f_STATUS==ST_CONF_DELETANDO_REC_GRUPO){		//APAGA TODAS AS RECEITAS DO GRUPO

		t = _f_Max_Progress - _f_Progress;
		snprintf((char*)_tmp_char,sizeof(_tmp_char),"%s/ROSTER.DAT",Grupo_Sel.path);


		if(readTotalId((char*)_tmp_char) && (_f_Progress!=_f_Max_Progress)){			//DELETA RECEITAS
			readItemList((char*)_tmp_char, (s_Lista*)&Lista, 0);
			deleteItemList((char*)_tmp_char, 0);
			deleteFile((char*)Rec_Sel.path);
			_f_Progress++;
		}
		else{								//DELETA GRUPO
			if(_f_STATUS==ST_CONF_DELETANDO_GRUPO){
				deleteItemList("RECUSER/ROSTER.DAT", _f_Grupo_ID_Sel);
				deleteFile((char*)_tmp_char);		//LISTA DO GRUPO
				deleteFolder((char*)Grupo_Sel.path);//PASTA DO GRUPO

				STATUS = ST_CONF_ADD_SEL_GRUPO;
				_f_PAGE = 0;
			}
			else{
				STATUS = ST_CONF_MENU_EDT_GRUPO;
			}
		}

	}
	else if (_f_STATUS==ST_CONF_DELETANDO_REC_SEL_GRUPO){//APAGA AS RECEITAS SELECIONADAS DO GRUPO
		snprintf((char*)_tmp_char,sizeof(_tmp_char),"%s/ROSTER.DAT",Grupo_Sel.path);
		t = readTotalId((char*)_tmp_char);
		if(_f_Progress!=_f_Max_Progress){
			for(i=0; i<t; i++){
				readItemList((char*)_tmp_char, (s_Lista*)&Lista, i);

				if(Lista.delete==1){
					deleteItemList((char*)_tmp_char, i);
					deleteFile((char*)Rec_Sel.path);
					_f_Progress++;
					break;
				}
			}
		}
		else {
			STATUS = ST_CONF_MENU_EDT_GRUPO;
		}
	}
	else if (_f_STATUS==ST_CONF_DELETANDO_RECEITA){		//APAGA APENAS A RECEITA SELECIONADA
		if(_f_Progress!=_f_Max_Progress){
			snprintf((char*)_tmp_char,sizeof(_tmp_char),"%s/ROSTER.DAT",Grupo_Sel.path);
			deleteItemList((char*)_tmp_char, _f_Rec_ID_Sel);
			deleteFile((char*)Rec_Sel.path);

			_f_Progress++;
		}
		else {
			STATUS = ST_CONF_ADD_SEL_RECEITA;//ST_CONF_MENU_EDT_RECEITA;
		}
	}

	UpdateScr = 1;
}


uint16_t poww(int i, int j){
	uint16_t result=1;
	while(j>0){
		result *=i;
		j--;
	}
	return result;
}
uint16_t str2int(char *pBuf){
    uint8_t len = 0;
    uint8_t i = 0;
    uint16_t res = 0;
    len = strlen(pBuf);
    for(;len>i;i++){
        res += ((uint16_t)(pBuf[i] - '0'))*poww(10, len-1-i);
        //printf("\nl:%d i:%d r:%d %d\n",len,i,res,poww(10, i));
    }
    return res;
}
void readCSV2(char *pFile, uint16_t pLine, uint16_t pField ){
	FIL fsd;
	uint32_t cnt=0;
    char buf[512];
    uint8_t  pos=0;
    uint8_t  new_line=0;
    uint8_t  new_field=0;
    uint16_t field=0;
    uint16_t line=0;

    memset(&buf,0x00,sizeof(buf));
    memset(&_csv_Value,0x00,sizeof(_csv_Value));
    if(!f_open(&fsd, pFile, FA_OPEN_EXISTING | FA_READ)){
    	while(1){
    		if(!f_read(&fsd, &buf[pos], sizeof(buf[pos]), &cnt)){
    			if(new_line){field=0; pos=0; new_line=0; line++;}
    			if(buf[pos]=='\n'){new_line=1; new_field=1; buf[pos-1]='\0';}
    			if(buf[pos]==';'){new_field=1;}
    			if(buf[pos]=='\0'){break;}

    			if(new_field){
    				field++;
    				buf[pos]=0x00;
    				if(line==pLine && field==(pField+1)){
    					memcpy(&_csv_Value,&buf,pos);
    					break;
    				}
    				pos=0;
    				//printf("\n /%s/ %d %d",buf,line,field);
    				new_field = 0;
    			}
    			else{pos++;}
    		}
    	}
    	f_sync(&fsd);
    	f_close(&fsd);
    }
}

void read_pwm_csv(void){

//	PWM_POWER.CANAL = 0;
//	PWM_POWER.DUTY = 100;
//	PWM_POWER.PERIODO = 8;

	readCSV2("CNT/TENSAO.CSV",1,Config.TENSAO_ENTRADA);
	PWM_POWER.CANAL = 0;
	PWM_POWER.DUTY = str2int(_csv_Value);

	readCSV2("CNT/TENSAO.CSV",1,4);
	PWM_POWER.PERIODO = str2int(_csv_Value);
}


//============================================================
// ROTINAS DE USB
//============================================================
char detectUSB(void){

#ifdef USE_USB
	if(!_f_USB_Connet && Host_UsbConnect()){//USB ACABOU DE SER CONECTADO
		switch(_f_STATUS){	//APENAS TELAS QUE TEM PROCESSO DEPENDENTE DE USB
			case ST_USB_UPDATE_FIRMWARE_INSERIR:{ STATUS=ST_USB_UPDATE_FIRMWARE; _f_USB_Connet = 1; }break;
			case ST_USB_IMPORTAR_INSERIR:{ STATUS=ST_USB_IMPORTAR_MENU; }break;
			case ST_USB_EXPORTAR_INSERIR:{ STATUS=ST_USB_EXPORTAR_MENU; }break;
			case ST_USB_IMPORTAR_TENSAO_INSERIR:{ STATUS=ST_USB_IMPORTAR_TENSAO; }break;
			case ST_USB_EXPORTAR_TENSAO_INSERIR:{ STATUS=ST_USB_EXPORTAR_TENSAO; }break;
			case ST_USB_IMPORTAR_TENSAO:
			case ST_USB_EXPORTAR_TENSAO:
			case ST_USB_IMPORTAR_TUDO:
			case ST_USB_EXPORTAR_TUDO:
			case ST_USB_IMPORTAR_RECEITAS:
			case ST_USB_EXPORTAR_RECEITAS:
			case ST_USB_IMPORTAR_GRUPOS:
			case ST_USB_EXPORTAR_GRUPOS:
			case ST_USB_IMPORTAR_CONF_USER:
			case ST_USB_EXPORTAR_CONF_USER:{ _f_USB_Connet = 1; }break;
			case ST_PRE_AQUECIMENTO:
			case ST_SEL_PRE_AQUECIMENTO:
			case ST_MENU_PRINCIPAL:{ _f_USB_Connet = 1; }break;
			case ST_CONF_EMBTECH:{ if(_f_USB_Test==1){_f_USB_Connet = 1;}}break;
		}
	}

	if(!Host_UsbConnect()){_f_USB_Connet = 0;}//USB DESCONECTADO
#endif

	return _f_USB_Connet;
}

char Update_Firmware(void){

//	uint32_t info_total_bytes=0;


	/*
		1 - Arquivo INFO não encontrado
		2 - Projetos incompativeis
		3 - Espaço insuficiente no cartão SD
	  	4 - Firmware atual com mesma versão do novo firmware
		5 - Arquivo de lista de arquivos não encontrado no pendrive
		6 - Arquivo Não Encontrado no pendrive durante a verificação do pendrive
		7 -
		8 - Total divergente de arquivos copiados
		9 - Erro na leitura do arquivo de origem
		10- Erro na escrita do arquivo de destino
		11- Arquivo gerado diferente do arquivo de origem(é deletado o destino)

	 */



	CTRL_UPDATE = 0x01;	//VERIFICA ARQUIVO DE INFO

	set_free_space_sd(sd_free_space_bytes());
	_f_tick_error = fupdate_check_info((uint16_t*)&_f_Max_Progress, (uint32_t*)&_f_info_total_bytes);
	if(_f_tick_error){return 1;}

	UpdateScr=1;
	_f_WatchDogFeed |= 0x04;
	while(UpdateScr){vTaskDelay(10);}


	CTRL_UPDATE = 0x02;	//INICIA VERIFICACAO

	_f_Progress = 0;
	_f_tick_error = UpdateCheck_Files_Upgrade((uint8_t*)&UpdateScr, 0, (uint16_t*)&_f_Progress);
	if(_f_tick_error || _f_Progress!=_f_Max_Progress){ return 1;}


	CTRL_UPDATE = 0x04;	//INICIA ATUALIZACAO

	_f_Progress = 0;
	_f_tick_error = UpdateCheck_Files_Upgrade((uint8_t*)&UpdateScr, 1, (uint16_t*)&_f_Progress);
	if(_f_tick_error || _f_Progress!=_f_Max_Progress){ return 1;}


	if(fcheck_boot()){ return 1;}

	Eeprom.Flag_Update_Firmware=1;
	Eeprom.CONT_UPDATE_FIRMWARE++;
	EEPROM_Write(0,ADDRESS_EPROM,(char*)&Eeprom,MODE_8_BIT,sizeof(Eeprom));

//	POWER_CONFIG.bits.STATUS = OFF;
	_f_Embnet_off = 1;
	_c_TimeOFFPower = 20;
	EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Turn_Off_Slave,0x00000000);
	while(_c_TimeOFFPower){vTaskDelay(10);}

	NVIC_SystemReset();

	return 0;
}


char Import_Receitas(void){
	char grupo_list_usb[50];
	char grupo_list_sd[50];
	char rec_path[50];
	uint8_t __GRP_Index=0;
	uint32_t __REC_Index=0;
	uint16_t next_id=0;
	uint8_t tick_erro = 0;
	uint8_t tick_tentativa = 10;
	uint8_t type_struct = 0;

	_f_Progress_USB = 0;

	CTRL_UPDATE = 0x02;
	_f_Max_Progress=0;

	//VERIFICA RECEITAS PARA VERSAO ANTIGA
	__GRP_Index = readTotalId("1:BRAVO/RECUSER/LIST.DAT");
	for(;__GRP_Index;__GRP_Index--){
		if		(fget_size_with_struct_list("1:BRAVO/RECUSER/LIST.DAT",sizeof(s_Lista_EXPORT_0x0001)) == 0){type_struct = 1;}
		else if	(fget_size_with_struct_list("1:BRAVO/RECUSER/LIST.DAT",sizeof(s_Lista_NACIONAL_0x0001)) == 0){type_struct = 2;}

		switch(type_struct){
			case 1:{
				readItemList_other("1:BRAVO/RECUSER/LIST.DAT", (void*)&Lista_EXP_0x0001, sizeof(s_Lista_EXPORT_0x0001), __GRP_Index-1);
				snprintf(grupo_list_usb, sizeof(grupo_list_usb),"1:BRAVO/%s/LIST.DAT", Lista_EXP_0x0001.path);
			}break;
			case 2:{
				readItemList_other("1:BRAVO/RECUSER/LIST.DAT", (void*)&Lista_NAC_0x0001, sizeof(s_Lista_NACIONAL_0x0001), __GRP_Index-1);
				snprintf(grupo_list_usb, sizeof(grupo_list_usb),"1:BRAVO/%s/LIST.DAT",Lista_NAC_0x0001.path);
			}break;
		}


		__REC_Index = readTotalId(grupo_list_usb);
		if(!_f_USB_Connet){return 1;}
		for(;__REC_Index;__REC_Index--){
			switch(type_struct){
				case 1:	readItemList_other(grupo_list_usb, (void*)&Lista_EXP_0x0001, sizeof(s_Lista_EXPORT_0x0001), __REC_Index-1); break;
				case 2:	readItemList_other(grupo_list_usb, (void*)&Lista_NAC_0x0001, sizeof(s_Lista_NACIONAL_0x0001), __REC_Index-1); break;
			}
			_f_Max_Progress++;
			if(!_f_USB_Connet){return 1;}
			UpdateScr=1;
			while(UpdateScr){vTaskDelay(1); _f_WatchDogFeed |= 0x04;}
		}
	}
	//VERIFICA PARA VERSAO NOVA
	__GRP_Index = readTotalId("1:BRAVO/RECUSER/ROSTER.DAT");
	for(;__GRP_Index;__GRP_Index--){
		if(Grupo_Sel.version==version_Rec && Grupo_Sel.type==type_Bravo){ //CONFERE SE É A NOVA ESTRUTURA
			readItemList("1:BRAVO/RECUSER/ROSTER.DAT", (s_Lista*)&Grupo_Sel, __GRP_Index-1);

			snprintf(grupo_list_usb, sizeof(grupo_list_usb),"1:BRAVO/%s/ROSTER.DAT",Grupo_Sel.path);

			__REC_Index = readTotalId(grupo_list_usb);
			if(!_f_USB_Connet){return 1;}
			for(;__REC_Index;__REC_Index--){
				readItemList(grupo_list_usb, (s_Lista*)&Rec_Sel, __REC_Index-1);
				_f_Max_Progress++;
				if(!_f_USB_Connet){return 1;}
				UpdateScr=1;
				while(UpdateScr){vTaskDelay(1); _f_WatchDogFeed |= 0x04;}
			}
		}
		else{//COMPATIBILIZA RECEITAS DE VERSAO ANTIGA OU DE MODELO DE EQUIPAMENTO DIFERENTE
			UpdateScr=1;
			while(UpdateScr){vTaskDelay(1); _f_WatchDogFeed |= 0x04;}
		}
	}


	CTRL_UPDATE = 0x04;
	_f_Progress=0;
	//COMPATIBILIZA RECEITAS ANTIGAS COM VERSAO NOVA
	__GRP_Index = readTotalId("1:BRAVO/RECUSER/LIST.DAT");
	for(;__GRP_Index;__GRP_Index--){
		if		(fget_size_with_struct_list("1:BRAVO/RECUSER/LIST.DAT",sizeof(s_Lista_EXPORT_0x0001)) == 0){type_struct = 1;}
		else if	(fget_size_with_struct_list("1:BRAVO/RECUSER/LIST.DAT",sizeof(s_Lista_NACIONAL_0x0001)) == 0){type_struct = 2;}
		switch(type_struct){
			case 1:{
				readItemList_other("1:BRAVO/RECUSER/LIST.DAT", (void*)&Lista_EXP_0x0001, sizeof(s_Lista_EXPORT_0x0001), __GRP_Index-1);
				snprintf(grupo_list_usb, sizeof(grupo_list_usb),"1:BRAVO/%s/LIST.DAT",Lista_EXP_0x0001.path);
			}break;
			case 2:{
				readItemList_other("1:BRAVO/RECUSER/LIST.DAT", (void*)&Lista_NAC_0x0001, sizeof(s_Lista_NACIONAL_0x0001), __GRP_Index-1);
				snprintf(grupo_list_usb, sizeof(grupo_list_usb),"1:BRAVO/%s/LIST.DAT",Lista_NAC_0x0001.path);
			}break;
		}
		next_id = readNextId("RECUSER/ROSTER.DAT");
		snprintf(grupo_list_sd, sizeof(grupo_list_sd),"RECUSER/GRP%d",next_id);

		switch(type_struct){
			case 1:{	//EXPORT
				snprintf((char*)Grupo_Sel.titulo, sizeof(Grupo_Sel.titulo), "%s",Lista_EXP_0x0001.titulo);
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "%s",Lista_EXP_0x0001.ico);
				Grupo_Sel.FLAGS = Lista_EXP_0x0001.FLAGS;
				Grupo_Sel.TEMPERATURA_PRE = Lista_EXP_0x0001.TEMPERATURA_PRE;
				Grupo_Sel.ativo = Lista_EXP_0x0001.ativo;
				Grupo_Sel.delete = Lista_EXP_0x0001.delete;
				Grupo_Sel.favorito = Lista_EXP_0x0001.favorito;
				Grupo_Sel.type = type_Bravo;
				Grupo_Sel.version = version_Rec;
			}break;
			case 2:{	//NACIONAL
				snprintf((char*)Grupo_Sel.titulo, sizeof(Grupo_Sel.titulo), "%s",Lista_NAC_0x0001.titulo);
				snprintf((char*)Grupo_Sel.ico, sizeof(Grupo_Sel.ico), "%s",Lista_NAC_0x0001.ico);
				Grupo_Sel.FLAGS = Lista_NAC_0x0001.FLAGS;
				Grupo_Sel.ativo = Lista_NAC_0x0001.ativo;
				Grupo_Sel.delete = Lista_NAC_0x0001.delete;
				Grupo_Sel.favorito = Lista_NAC_0x0001.favorito;
				Grupo_Sel.type = type_Bravo;
				Grupo_Sel.version = version_Rec;
			}break;
		}

		createFolder(grupo_list_sd);

		snprintf((char*)Grupo_Sel.path, sizeof(Grupo_Sel.path), "%s",grupo_list_sd);
		addItemList("RECUSER/ROSTER.DAT", Grupo_Sel);
		snprintf((char*)Grupo_Sel.path, sizeof(Grupo_Sel.path), "%s/ROSTER.DAT",grupo_list_sd);


		if(!_f_USB_Connet){return 1;}
		__REC_Index = readTotalId(grupo_list_usb);
		for(;__REC_Index;__REC_Index--){
			switch(type_struct){
				case 1:{
					readItemList_other(grupo_list_usb, (void*)&Lista_EXP_0x0001, sizeof(s_Lista_EXPORT_0x0001), __REC_Index-1);
					snprintf(rec_path, sizeof(rec_path), "1:BRAVO/%s",Lista_EXP_0x0001.path);
				}break;
				case 2:{
					readItemList_other(grupo_list_usb, (void*)&Lista_NAC_0x0001, sizeof(s_Lista_NACIONAL_0x0001), __REC_Index-1);
					snprintf(rec_path, sizeof(rec_path), "1:BRAVO/%s",Lista_NAC_0x0001.path);
				}break;
			}
			readItemList(grupo_list_usb, (s_Lista*)&Rec_Sel, __REC_Index-1);

			switch(type_struct){
				case 1:{	//EXPORT
					snprintf((char*)Rec_Sel.titulo, sizeof(Rec_Sel.titulo), "%s",Lista_EXP_0x0001.titulo);
					snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "%s",Lista_EXP_0x0001.ico);
					Rec_Sel.FLAGS = Lista_EXP_0x0001.FLAGS;
					Rec_Sel.TEMPERATURA_PRE = Lista_EXP_0x0001.TEMPERATURA_PRE;
					Rec_Sel.ativo = Lista_EXP_0x0001.ativo;
					Rec_Sel.delete = Lista_EXP_0x0001.delete;
					Rec_Sel.favorito = Lista_EXP_0x0001.favorito;
					Rec_Sel.type = type_Bravo;
					Rec_Sel.version = version_Rec;
				}break;
				case 2:{	//NACIONAL
					snprintf((char*)Rec_Sel.titulo, sizeof(Rec_Sel.titulo), "%s",Lista_NAC_0x0001.titulo);
					snprintf((char*)Rec_Sel.ico, sizeof(Rec_Sel.ico), "%s",Lista_NAC_0x0001.ico);
					Rec_Sel.FLAGS = Lista_NAC_0x0001.FLAGS;
					Rec_Sel.ativo = Lista_NAC_0x0001.ativo;
					Rec_Sel.delete = Lista_NAC_0x0001.delete;
					Rec_Sel.favorito = Lista_NAC_0x0001.favorito;
					Rec_Sel.type = type_Bravo;
					Rec_Sel.version = version_Rec;
					Rec_Sel.TEMPERATURA_PRE = 2;
				}break;
			}

			next_id = readNextId((char*)Grupo_Sel.path);
			snprintf((char*)Rec_Sel.path, sizeof(Rec_Sel.path), "%s/REC%d.DAT",grupo_list_sd,next_id);

			//ATUALIZA CABECALHO DA RECEITA
			readFile((char*)rec_path, (char*)&RecBravo.cabecalho, sizeof(RecBravo.cabecalho), 64, 1);
			RecBravo.cabecalho.version = version_Rec;
			RecBravo.cabecalho.type = type_Bravo;
			writeFile((char*)rec_path, (char*)&RecBravo.cabecalho, sizeof(RecBravo.cabecalho), 64, 1);

			tick_tentativa=10;
			do{
				_f_WatchDogFeed |= 0x04;
				vTaskDelay(1);
				tick_erro = copy_file(rec_path, (char*)Rec_Sel.path);
				tick_tentativa--;
			}while(tick_erro && tick_tentativa);
			if(!tick_erro){
				addItemList((char*)Grupo_Sel.path, Rec_Sel);
			}
			else{
				return 1;
			}

			if(!_f_USB_Connet){return 1;}
			_f_Progress++;
			UpdateScr=1;
			while(UpdateScr){vTaskDelay(1); _f_WatchDogFeed |= 0x04;}
		}
	}

	//IMPORTA RECEITAS DA VERSAO NOVA
	__GRP_Index = readTotalId("1:BRAVO/RECUSER/ROSTER.DAT");
	for(;__GRP_Index;__GRP_Index--){
		readItemList("1:BRAVO/RECUSER/ROSTER.DAT", (s_Lista*)&Grupo_Sel, __GRP_Index-1);

		if(Grupo_Sel.version==version_Rec && Grupo_Sel.type==type_Bravo){

			next_id = readNextId("RECUSER/ROSTER.DAT");
			snprintf(grupo_list_sd, sizeof(grupo_list_sd),"RECUSER/GRP%d",next_id);
			snprintf(grupo_list_usb, sizeof(grupo_list_usb),"1:BRAVO/%s/ROSTER.DAT",Grupo_Sel.path);

			createFolder(grupo_list_sd);

			snprintf((char*)Grupo_Sel.path, sizeof(Grupo_Sel.path), "%s",grupo_list_sd);
			addItemList("RECUSER/ROSTER.DAT", Grupo_Sel);
			snprintf((char*)Grupo_Sel.path, sizeof(Grupo_Sel.path), "%s/ROSTER.DAT",grupo_list_sd);


			if(!_f_USB_Connet){return 1;}
			__REC_Index = readTotalId(grupo_list_usb);
			for(;__REC_Index;__REC_Index--){
				readItemList(grupo_list_usb, (s_Lista*)&Rec_Sel, __REC_Index-1);
				next_id = readNextId((char*)Grupo_Sel.path);
				snprintf(rec_path, sizeof(rec_path), "1:BRAVO/%s",Rec_Sel.path);
				snprintf((char*)Rec_Sel.path, sizeof(Rec_Sel.path), "%s/REC%d.DAT",grupo_list_sd,next_id);

				tick_tentativa=10;
				do{
					_f_WatchDogFeed |= 0x04;
					vTaskDelay(1);
					tick_erro = copy_file(rec_path, (char*)Rec_Sel.path);
					tick_tentativa--;
				}while(tick_erro && tick_tentativa);
				if(!tick_erro){
					addItemList((char*)Grupo_Sel.path, Rec_Sel);
				}
				else{
					return 1;
				}

				if(!_f_USB_Connet){return 1;}
				_f_Progress++;
				UpdateScr=1;
				while(UpdateScr){vTaskDelay(1); _f_WatchDogFeed |= 0x04;}
			}
		}
		else{//COMPATIBILIZA RECEITAS DE VERSAO ANTIGA OU DE MODELO DE EQUIPAMENTO DIFERENTE
			UpdateScr=1;
			while(UpdateScr){vTaskDelay(1); _f_WatchDogFeed |= 0x04;}
		}
	}



	return 0;
}
char Import_Config_User(void){
	s_Config tmp_Config;


	if(!readFile("1:BRAVO/CFG/CONFG.DAT", (char*)&tmp_Config, sizeof(s_Config), 0, 1)){
		Config.Escala_Temperatura = tmp_Config.Escala_Temperatura;
		Config.IDIOMA = tmp_Config.IDIOMA;
		ID_IDIOMA = Config.IDIOMA;
		Config.TEMPO_PRE = tmp_Config.TEMPO_PRE;
		Config.VELOCIDADE_PRE = tmp_Config.VELOCIDADE_PRE;
		Config.TEMPERATURA_PRE = tmp_Config.TEMPERATURA_PRE;
		Config.TEMPERATURA_PRE_2 = tmp_Config.TEMPERATURA_PRE_2;
		Config.TEMPERATURA_PRE_LASTRO = tmp_Config.TEMPERATURA_PRE_LASTRO;
		memcpy((void*)&Config.SENHA_USER, (void*)&tmp_Config.SENHA_USER, sizeof(Config.SENHA_USER));
		memcpy((void*)&SenhaUser,(void*)&Config.SENHA_USER,sizeof(SenhaUser));
		_c_save_eeprom = 1;
	}
	else if(!readFile("1:BRAVO/CFG/CONFG.DAT", (char*)&Config_NAC, sizeof(s_Config_NACIONAL), 0, 1)){
		Config.Escala_Temperatura = Config_NAC.Escala_Temperatura;
		Config.IDIOMA = Config_NAC.IDIOMA;
		ID_IDIOMA = Config_NAC.IDIOMA;
		Config.TEMPO_PRE = Config_NAC.TEMPO_PRE;
		Config.VELOCIDADE_PRE = Config_NAC.VELOCIDADE_PRE;
		Config.TEMPERATURA_PRE = Config_NAC.TEMPERATURA_PRE;
		Config.TEMPERATURA_PRE_LASTRO = Config_NAC.TEMPERATURA_PRE_LASTRO;
		memcpy((void*)&Config.SENHA_USER, (void*)&Config_NAC.SENHA_USER, sizeof(Config.SENHA_USER));
		memcpy((void*)&SenhaUser,(void*)&Config.SENHA_USER,sizeof(SenhaUser));

		_c_save_eeprom = 1;
	}
	else{
		return 1;
	}

	return 0;
}
char Export_Config(void){
	s_Config tmp_Config;
	memcpy(&tmp_Config, (void*)&Config, sizeof(s_Config));
	createFolder("1:BRAVO");
	createFolder("1:BRAVO/CFG");
	if(writeFile("1:BRAVO/CFG/CONFG.DAT", (char*)&tmp_Config, sizeof(s_Config), 0, 1)){
		return 1;
	}

	return 0;
}
char Export_Receitas(void){
	char grupo_list_usb[50];
	char grupo_list_sd[50];
	char rec_path[50];
	uint8_t __GRP_Index=0;
	uint32_t __REC_Index=0;
	uint16_t next_id=0;
	uint8_t tick_erro = 0;
	uint8_t tick_tentativa = 10;


	createFolder("1:BRAVO");
	createFolder("1:BRAVO/RECUSER");

	CTRL_UPDATE = 0x02;

	_f_Max_Progress=0;
	__GRP_Index = readTotalId("RECUSER/ROSTER.DAT");
	for(;__GRP_Index;__GRP_Index--){
		readItemList("RECUSER/ROSTER.DAT", (s_Lista*)&Grupo_Sel, __GRP_Index-1);
		snprintf(grupo_list_sd, sizeof(grupo_list_sd),"%s/ROSTER.DAT",Grupo_Sel.path);

		if(!_f_USB_Connet){return 1;}
		__REC_Index = readTotalId(grupo_list_sd);
		for(;__REC_Index;__REC_Index--){
			readItemList(grupo_list_sd, (s_Lista*)&Rec_Sel, __REC_Index-1);
			_f_Max_Progress++;
			if(!_f_USB_Connet){return 1;}
			UpdateScr=1;
			while(UpdateScr){vTaskDelay(1); _f_WatchDogFeed |= 0x04;}
		}

		_f_WatchDogFeed |= 0x04;
		vTaskDelay(1);
	}


	CTRL_UPDATE = 0x04;
	_f_Progress=0;

	__GRP_Index = readTotalId("RECUSER/ROSTER.DAT");
	for(;__GRP_Index;__GRP_Index--){
		readItemList("RECUSER/ROSTER.DAT", (s_Lista*)&Grupo_Sel, __GRP_Index-1);
		next_id = readNextId("1:BRAVO/RECUSER/ROSTER.DAT");
		snprintf(grupo_list_usb, sizeof(grupo_list_usb),"1:BRAVO/RECUSER/GRP%d",next_id);
		snprintf(grupo_list_sd, sizeof(grupo_list_sd),"%s/ROSTER.DAT",Grupo_Sel.path);

		createFolder(grupo_list_usb);

		snprintf(grupo_list_usb, sizeof(grupo_list_usb),"RECUSER/GRP%d",next_id);
		snprintf((char*)Grupo_Sel.path, sizeof(Grupo_Sel.path), "%s",grupo_list_usb);
		addItemList("1:BRAVO/RECUSER/ROSTER.DAT", Grupo_Sel);
		snprintf((char*)Grupo_Sel.path, sizeof(Grupo_Sel.path), "1:BRAVO/%s/ROSTER.DAT",grupo_list_usb);


		if(!_f_USB_Connet){return 1;}
		__REC_Index = readTotalId(grupo_list_sd);
		for(;__REC_Index;__REC_Index--){
			readItemList(grupo_list_sd, (s_Lista*)&Rec_Sel, __REC_Index-1);
			next_id = readNextId((char*)Grupo_Sel.path);
			snprintf(rec_path, sizeof(rec_path), "1:BRAVO/%s/REC%d.DAT",grupo_list_usb,next_id);

			tick_tentativa=10;
			do{
				_f_WatchDogFeed |= 0x04;
				vTaskDelay(1);
				tick_erro = copy_file((char*)Rec_Sel.path, rec_path);
				tick_tentativa--;
			}while(tick_erro && tick_tentativa);

			snprintf((char*)Rec_Sel.path, sizeof(Rec_Sel.path), "%s/REC%d.DAT",grupo_list_usb,next_id);
			if(!tick_erro){
				addItemList((char*)Grupo_Sel.path, Rec_Sel);
			}
			else{
				return 1;
			}

			_f_Progress++;
			UpdateScr=1;
			while(UpdateScr){vTaskDelay(1);_f_WatchDogFeed |= 0x04;}
		}
	}

	return 0;
}
char Delete_Receitas_SD(void){
	char path[50];
	uint8_t __GRP_Index=0;
	uint32_t __REC_Index=0;

	//if(!_f_USB_Connet){return 1;}

	__GRP_Index = readTotalId("RECUSER/ROSTER.DAT");
	for(;__GRP_Index;__GRP_Index--){
		readItemList("RECUSER/ROSTER.DAT", (s_Lista*)&Grupo_Sel, __GRP_Index-1);

		snprintf(path, sizeof(path),"%s/ROSTER.DAT",Grupo_Sel.path);

		__REC_Index = readTotalId(path);
		for(;__REC_Index;__REC_Index--){
			readItemList(path, (s_Lista*)&Rec_Sel, __REC_Index-1);
			deleteFile((char*)Rec_Sel.path);

			UpdateScr=1;
			while(UpdateScr){vTaskDelay(1); _f_WatchDogFeed |= 0x04;}
		}
		deleteFile(path);
		deleteFolder((char*)Grupo_Sel.path);
	}

	deleteFile("RECUSER/ROSTER.DAT");

	return 0;
}
char Delete_Receitas_USB(void){
	char path[50];
	uint8_t __GRP_Index=0;
	uint32_t __REC_Index=0;

	if(!_f_USB_Connet){return 1;}

	__GRP_Index = readTotalId("1:BRAVO/RECUSER/ROSTER.DAT");
	for(;__GRP_Index;__GRP_Index--){
		readItemList("1:BRAVO/RECUSER/ROSTER.DAT", (s_Lista*)&Grupo_Sel, __GRP_Index-1);

		snprintf(path, sizeof(path),"1:BRAVO/%s/ROSTER.DAT",Grupo_Sel.path);
		if(!_f_USB_Connet){return 1;}

		__REC_Index = readTotalId(path);
		for(;__REC_Index;__REC_Index--){
			readItemList(path, (s_Lista*)&Rec_Sel, __REC_Index-1);

			snprintf((char*)Rec_Sel.path, sizeof(Rec_Sel.path),"1:BRAVO/%s",Rec_Sel.path);
			deleteFile((char*)Rec_Sel.path);
			_f_WatchDogFeed |= 0x04;
			vTaskDelay(1);
		}
		snprintf((char*)Grupo_Sel.path, sizeof(Grupo_Sel.path),"1:BRAVO/%s",Grupo_Sel.path);

		deleteFile(path);
		deleteFolder((char*)Grupo_Sel.path);
	}

	deleteFile("1:BRAVO/RECUSER/ROSTER.DAT");
	deleteFolder("1:BRAVO/RECUSER");
	deleteFolder("1:BRAVO");

	return 0;
}
char Import_Params_Tensao(void){
	char ret = 4;

//	if(checkFile("1:BRAVO/TENSAO.CSV")){
//		deleteFile("SYSTEM/CFG/TENSAO.CSV");
//		//ret = copy_file("1:BRAVO/TENSAO.CSV", "SYSTEM/CFG/TENSAO.CSV");
//	}
	f_rename("CNT/TENSAO.CSV", "CNT/TENSAO_OLD.CSV");
	ret = copy_file("1:BRAVO/TENSAO.CSV", "CNT/TENSAO.CSV");
	if(ret){
		f_rename("CNT/TENSAO_OLD.CSV", "CNT/TENSAO.CSV");
	}
	return ret;
}
char Export_Params_Tensao(void){
	char ret = 4;
//	if(checkFile("SYSTEM/CFG/TENSAO.CSV")){
//		deleteFile("1:BRAVO/TENSAO.CSV");
//		ret = copy_file("SYSTEM/CFG/TENSAO.CSV", "1:BRAVO/TENSAO.CSV");
//	}
	ret = copy_file("CNT/TENSAO.CSV", "1:BRAVO/TENSAO.CSV");
	return ret;
}

char Calib_Touch_USB(void){
	char buf[20];
	char buf2[20];

	if(!readFile("1:BRAVO/CALIB.DAT", (char*)&buf, sizeof(buf), 0, 1)){
		snprintf(buf2,sizeof(buf2),"bRAvOcOPARockET");
		if(memcmp(buf, buf2, sizeof(buf))){
			Config.Calib = 0;
			_c_save_eeprom = 2;
		}
		else{
			return 1;
		}
	}
	else{
		return 1;
	}
	deleteFile("1:BRAVO/CALIB.DAT");

	return 0;
}
char Teste_USB(void){
	char buf1[20];
	char buf2[20];
	uint16_t wait=0;

	if(!checkFile("1:TESTE_USB.DAT")){deleteFile("1:TESTE_USB.DAT");}

	_f_USB_Test = 2;//GRAVAR
	UpdateScr = 1; wait=1000;
	while(UpdateScr || wait){vTaskDelay(1); wait--;}

	snprintf(buf1,sizeof(buf1),"bRAvOcOPARockET");
	if(!writeFile("1:TESTE_USB.DAT", (char*)&buf1, sizeof(buf1), 0, 1)){

		_f_USB_Test = 3;//LER
		UpdateScr = 1; wait=1000;
			while(UpdateScr || wait){vTaskDelay(1); wait--;}

		if(!readFile("1:TESTE_USB.DAT", (char*)&buf2, sizeof(buf2), 0, 1)){

			if(!memcmp(&buf2,&buf1,sizeof(buf1))){
				_f_USB_Test = 4;//DELETAR
				UpdateScr = 1; wait=1000;
					while(UpdateScr || wait){vTaskDelay(1); wait--;}

				deleteFile("1:TESTE_USB.DAT");
				if(checkFile("1:TESTE_USB.DAT")){
					_f_USB_Test = 5;//OK
					UpdateScr = 1; wait=1000;
						while(UpdateScr || wait){vTaskDelay(1); wait--;}
				}
				else{
					_f_USB_Test = 6;//ERRO
					UpdateScr = 1;
					while(UpdateScr){vTaskDelay(1);}
					return 1;
				}
			}
			else{
				_f_USB_Test = 6;//ERRO
				UpdateScr = 1;
				while(UpdateScr){vTaskDelay(1);}
				return 1;
			}

		}
		else{
			_f_USB_Test = 6;//ERRO
			UpdateScr = 1;
			while(UpdateScr){vTaskDelay(1);}
			return 1;
		}
	}
	else{
		_f_USB_Test = 6;//ERRO
		UpdateScr = 1;
		while(UpdateScr){vTaskDelay(1);}
		return 1;
	}

	return 0;
}


//============================================================
// UPDATE COMPONENTES
//============================================================
void _update_TecladoNum(void){
	char __text[15];
	uint8_t __pos=0;
	uint16_t __tmp1=0;
	uint16_t __tmp2=0;
	uint8_t Pos_senha=0;
	uint8_t _t_dia=0;
	uint8_t _t_mes=0;
	uint16_t _t_ano=0;

	memset(&__text,0x00,sizeof(__text));

	if(_s_TecladoCaracter==0x00){		//BOTAO BACKSPACE
		if(_c_TecladoPos>0){
			_s_TecladoDigitado[_c_TecladoPos]=' ';
			_c_TecladoPos--;
			_s_TecladoDigitado[_c_TecladoPos]=' ';
			//memset((void*)&_s_TecladoDigitado[_c_TecladoPos], ' ', sizeof(_s_TecladoDigitado[_c_TecladoPos]));

		}
	}

	else if(_s_TecladoCaracter==0x01){	//BOTAO OK

		if(_f_STATUS==ST_CONFIGURACOES){
			if(_c_TecladoPos<6){//COMPLETA OS CARACTERES FALTANTES
				for (Pos_senha=_c_TecladoPos;Pos_senha<6;Pos_senha++){//_c_TecladoMax
					_s_TecladoDigitado[Pos_senha] = ' ';
				}
			}

			if(memcmp((void*)_s_TecladoDigitado, (void*)SenhaUser, 6) == 0){
				if(_f_save_passo_MT){
					STATUS = ST_CONF_ADD_SEL_GRUPO;
					_f_PAGE = 0;
				}
				else{
					_f_PAGE = 0;
					ScrLoad=1;
				}
			}
			else if (memcmp((void*) _s_TecladoDigitado, (void*)SenhaFabricante, sizeof(SenhaFabricante)) == 0){
				_f_PAGE = 0;
				STATUS=ST_CONFIGURACOES_FABRICANTE;
			}
			else if(memcmp((void*)_s_TecladoDigitado, (void*)SenhaEmbtech, sizeof(SenhaEmbtech)) == 0){
				_f_PAGE = 0;
				STATUS=ST_CONF_EMBTECH;
			}
			else if(memcmp((void*)_s_TecladoDigitado, (void*)SenhaPontos, sizeof(SenhaPontos)) == 0){
				Config._f_Pontos_Toque = !Config._f_Pontos_Toque;
				_c_save_eeprom = 3;
				ScrLoad = 1;
			}
			else if(memcmp((void*)_s_TecladoDigitado, (void*)SenhaTensaoExport, sizeof(SenhaTensaoExport)) == 0){
				STATUS=ST_USB_EXPORTAR_TENSAO_INSERIR;
				ScrLoad = 1;
			}
			else if(memcmp((void*)_s_TecladoDigitado, (void*)SenhaTensaoImport, sizeof(SenhaTensaoImport)) == 0){
				STATUS=ST_USB_IMPORTAR_TENSAO_INSERIR;
				ScrLoad = 1;
			}
			else if(memcmp((void*)_s_TecladoDigitado, (void*)SenhaBypassPre, sizeof(SenhaBypassPre)) == 0){
				SET_PRE.PREAQUECEU = 1;
				_f_PAGE = 0;
				SET_PRE.TEMPERATURA_PRE=Config.TEMPERATURA_PRE;
				SET_PRE.BYPASS = 1;
				STATUS=ST_HOME;
				ScrLoad = 1;
			}
			else if(memcmp((void*)_s_TecladoDigitado, (void*)SenhaBypassPre2, sizeof(SenhaBypassPre2)) == 0){
				SET_PRE.PREAQUECEU = 1;
				_f_PAGE = 0;
				SET_PRE.TEMPERATURA_PRE=Config.TEMPERATURA_PRE_2;
				SET_PRE.BYPASS = 1;
				STATUS=ST_HOME;
				ScrLoad = 1;
			}


			memset((void*)&_s_TecladoDigitado,0x00,sizeof(_s_TecladoDigitado));
			_c_TecladoPos = 0;
		}

		else if(_f_STATUS==ST_CONF_RESTAURAR_FABRICA){
			for (Pos_senha=0;Pos_senha<_c_TecladoPos;Pos_senha++){//_c_TecladoMax
				if (_s_TecladoDigitado[Pos_senha]!=SenhaUser[Pos_senha]){break;}
				else if (Pos_senha==(_c_TecladoPos-1)){//else if (Pos_senha==5) {
					STATUS=ST_CONF_RESTAURAR_FABRICA_PROCESS;
				}
			}
			memset((void*)&_s_TecladoDigitado,0x00,sizeof(_s_TecladoDigitado));
			_c_TecladoPos = 0;
		}
		else if(LAST_STATUS==ST_MODO_MANUAL_AJ || LAST_STATUS==ST_CONF_EDT_PASSO_RECEITA || _f_STATUS == ST_CONF_PRE_AQUEC){

			lock_touch = 1;
			if(_c_TecladoPos!=0){
				_c_TecladoPos--;
				for(__pos=0; __pos<=_c_TecladoPos; __pos++){
					_s_TecladoDigitado[__pos] = _s_TecladoDigitado[__pos]-48;
				}

				if(_f_PP_teclado==PP_TEMPERATURA || _f_PP_teclado==PP_TEMP_TETO || _f_PP_teclado==PP_TEMP_LASTRO){
					switch(_c_TecladoPos){
						case 0:{
							__tmp1 = (uint16_t)_s_TecladoDigitado[0];
						}break;
						case 1:{
							__tmp1 = (uint16_t)concatInt(_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
						}break;
						case 2:{
							__tmp2 = (uint16_t)concatInt(_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
							__tmp1 = (uint16_t)concatInt(__tmp2,_s_TecladoDigitado[2]);
						}break;
					}
					if(Config.Escala_Temperatura==1){//CONVERTE PARA °C PARA TRATAR E SALVAR
						if(__tmp1<=32 && __tmp1!=0){
							__tmp1 = 10;
						}
						else if(__tmp1==0){}
						else{
							__tmp1 = ((__tmp1-32)*FATOR_TEMP_F);
						}

//						switch(_f_PP_teclado){
//						case PP_TEMPERATURA:{
//							_f_Temperatura_Pre = 2;
////							if(LAST_STATUS==ST_CONF_EDT_PASSO_RECEITA){Rec_Sel.TEMPERATURA_PRE = 2;}
//							if((__tmp1)>Config.TEMPERATURA_MAX){EDT_SET.TEMPERATURA = Config.TEMPERATURA_MAX;}
//							else if (__tmp2==0){_f_Temperatura_Pre = Grupo_Sel.TEMPERATURA_PRE; if(LAST_STATUS==ST_CONF_EDT_PASSO_RECEITA){Rec_Sel.TEMPERATURA_PRE = Grupo_Sel.TEMPERATURA_PRE;}}
//							else if ((__tmp1)<TEMPERATURA_MIN){EDT_SET.TEMPERATURA = TEMPERATURA_MIN;}
//							else{EDT_SET.TEMPERATURA = (int32_t)__tmp1;}
//						}break;
//						case PP_TEMP_TETO:{
//							if((__tmp1)>Config.TEMPERATURA_MAX){Config.TEMPERATURA_PRE = Config.TEMPERATURA_MAX;}
//							else if ((__tmp1)<TEMPERATURA_MIN){Config.TEMPERATURA_PRE = TEMPERATURA_MIN;}
//							else{Config.TEMPERATURA_PRE 		= (((int32_t)__tmp1));}
//							_c_save_eeprom = 3;
//						}break;
//						case PP_TEMP_LASTRO:{
//							if((__tmp1)>Config.TEMPERATURA_MAX){Config.TEMPERATURA_PRE_LASTRO = Config.TEMPERATURA_MAX;	}
//							else if ((__tmp1)<TEMPERATURA_MIN){Config.TEMPERATURA_PRE_LASTRO = TEMPERATURA_MIN;}
//							else{Config.TEMPERATURA_PRE_LASTRO 		= (((int32_t)__tmp1));	}
//							_c_save_eeprom = 3;
//						}break;
//						}
					}
					else{
						__tmp1 = __tmp1 * FATOR_TEMP;

//						switch(_f_PP_teclado){
//						case PP_TEMPERATURA:{
//							_f_Temperatura_Pre = 2;
////							if(LAST_STATUS==ST_CONF_EDT_PASSO_RECEITA){Rec_Sel.TEMPERATURA_PRE = 2;}
//							if((__tmp1*FATOR_TEMP)>Config.TEMPERATURA_MAX){EDT_SET.TEMPERATURA = Config.TEMPERATURA_MAX;}
//							else if (__tmp1==0){_f_Temperatura_Pre = Grupo_Sel.TEMPERATURA_PRE; if(LAST_STATUS==ST_CONF_EDT_PASSO_RECEITA){Rec_Sel.TEMPERATURA_PRE = Grupo_Sel.TEMPERATURA_PRE;}}
//							else if ((__tmp1*FATOR_TEMP)<TEMPERATURA_MIN){EDT_SET.TEMPERATURA = TEMPERATURA_MIN;}
//							else{EDT_SET.TEMPERATURA = (int32_t)__tmp1*FATOR_TEMP;}
//						}break;
//						case PP_TEMP_TETO:{
//							if((__tmp1*FATOR_TEMP)>Config.TEMPERATURA_MAX){Config.TEMPERATURA_PRE = Config.TEMPERATURA_MAX;}
//							else if ((__tmp1*FATOR_TEMP)<TEMPERATURA_MIN){Config.TEMPERATURA_PRE = TEMPERATURA_MIN;}
//							else{Config.TEMPERATURA_PRE 		= (((int32_t)__tmp1) * FATOR_TEMP);}
//							_c_save_eeprom = 3;
//						}break;
//						case PP_TEMP_LASTRO:{
//							if((__tmp1*FATOR_TEMP)>Config.TEMPERATURA_MAX){Config.TEMPERATURA_PRE_LASTRO = Config.TEMPERATURA_MAX;	}
//							else if ((__tmp1*FATOR_TEMP)<TEMPERATURA_MIN){Config.TEMPERATURA_PRE_LASTRO = TEMPERATURA_MIN;}
//							else{Config.TEMPERATURA_PRE_LASTRO 		= (((int32_t)__tmp1) * FATOR_TEMP);	}
//							_c_save_eeprom = 3;
//						}break;
//						}
					}

					switch(_f_PP_teclado){
					case PP_TEMPERATURA:{
						_f_Temperatura_Pre = 2;
						if((__tmp1)>Config.TEMPERATURA_MAX){EDT_SET.TEMPERATURA = Config.TEMPERATURA_MAX;}
						else if (__tmp1==0){_f_Temperatura_Pre = Grupo_Sel.TEMPERATURA_PRE;}
						else if ((__tmp1)<TEMPERATURA_MIN){EDT_SET.TEMPERATURA = TEMPERATURA_MIN;}
						else{EDT_SET.TEMPERATURA = (int32_t)__tmp1;}
					}break;
					case PP_TEMP_TETO:{
						if((__tmp1)>Config.TEMPERATURA_MAX){Config.TEMPERATURA_PRE = Config.TEMPERATURA_MAX;}
						else if ((__tmp1)<TEMPERATURA_MIN){Config.TEMPERATURA_PRE = TEMPERATURA_MIN;}
						else{Config.TEMPERATURA_PRE 		= (((int32_t)__tmp1));}
						_c_save_eeprom = 3;
					}break;
					case PP_TEMP_LASTRO:{
						if((__tmp1)>Config.TEMPERATURA_MAX){Config.TEMPERATURA_PRE_LASTRO = Config.TEMPERATURA_MAX;	}
						else if ((__tmp1)<TEMPERATURA_MIN){Config.TEMPERATURA_PRE_LASTRO = TEMPERATURA_MIN;}
						else{Config.TEMPERATURA_PRE_LASTRO 		= (((int32_t)__tmp1));	}
						_c_save_eeprom = 3;
					}break;
					}

				}
				else if(_f_PP_teclado==PP_TEMPO){

					if(_s_TecladoDigitado[0] == ' '){_s_TecladoDigitado[0]=0;}
					if(_s_TecladoDigitado[1] == ' '){_s_TecladoDigitado[1]=0;}
					if(_s_TecladoDigitado[2] == ' '){_s_TecladoDigitado[2]=0;}
					if(_s_TecladoDigitado[3] == ' '){_s_TecladoDigitado[3]=0;}

					memset(&__tmp2,0x00,sizeof(__tmp2));
					memset(&__tmp1,0x00,sizeof(__tmp1));

					__tmp2 = (uint16_t)concatInt(_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
					__tmp1 = (uint16_t)concatInt(_s_TecladoDigitado[2],_s_TecladoDigitado[3]);

					if(_f_STATUS==ST_CONF_PRE_AQUEC){
						Config.TEMPO_PRE = (uint32_t)(__tmp2*60)+__tmp1;
						if(Config.TEMPO_PRE>(30*60)){Config.TEMPO_PRE=30*60;}
						_c_save_eeprom = 3;
					}
					else{
						EDT_SET.TEMPO = (uint32_t)(__tmp2*60)+__tmp1;
						if(EDT_SET.TEMPO>MAX_TEMPO_SETPOINT){EDT_SET.TEMPO = MAX_TEMPO_SETPOINT;}
					}

				}
			}

			_f_PP_teclado = 0;
			if(_f_STATUS!=ST_CONF_PRE_AQUEC){
				STATUS=LAST_STATUS;
			}
		}
		else if (LAST_STATUS==ST_CONF_PRE_AQUEC){
			lock_touch = 1;
			if(_c_TecladoPos!=0){
				_c_TecladoPos--;
				for(__pos=0; __pos<=_c_TecladoPos; __pos++){
					_s_TecladoDigitado[__pos] = _s_TecladoDigitado[__pos]-48;
				}


				if(_f_PP_teclado==PP_TEMP_PRE_1 || _f_PP_teclado==PP_TEMP_PRE_2){
					switch(_c_TecladoPos){
					case 0:{
						__tmp1 = (uint16_t)_s_TecladoDigitado[0];
					}break;
					case 1:{
						__tmp1 = (uint16_t)concatInt(_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
					}break;
					case 2:{
						__tmp2 = (uint16_t)concatInt(_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
						__tmp1 = (uint16_t)concatInt(__tmp2,_s_TecladoDigitado[2]);
					}break;
					}
					if(Config.Escala_Temperatura==1){//CONVERTE PARA °C PARA TRATAR E SALVAR
						__tmp1 = ((__tmp1-32)*FATOR_TEMP_F);
					}
					else{
						__tmp1 *= FATOR_TEMP;
					}

					switch(_f_PP_teclado){
					case PP_TEMP_PRE_1:{
						__tmp2=0;
						if(SET_PRE.TEMPERATURA_PRE==Config.TEMPERATURA_PRE){__tmp2=1;}
						if((__tmp1)>Config.TEMPERATURA_MAX){Config.TEMPERATURA_PRE = Config.TEMPERATURA_MAX;}
						else if ((__tmp1)<TEMPERATURA_MIN){Config.TEMPERATURA_PRE = TEMPERATURA_MIN;}
						else{Config.TEMPERATURA_PRE = (int32_t)__tmp1;}
						if(__tmp2==1){SET_PRE.TEMPERATURA_PRE=Config.TEMPERATURA_PRE; SET_POINT.TEMPERATURA=SET_PRE.TEMPERATURA_PRE;}
					}break;
					case PP_TEMP_PRE_2:{
						__tmp2=0;
						if(SET_PRE.TEMPERATURA_PRE==Config.TEMPERATURA_PRE_2){__tmp2=1;}
						if((__tmp1)>Config.TEMPERATURA_MAX){Config.TEMPERATURA_PRE_2 = Config.TEMPERATURA_MAX;}
						else if ((__tmp1)<TEMPERATURA_MIN){Config.TEMPERATURA_PRE_2 = TEMPERATURA_MIN;}
						else{Config.TEMPERATURA_PRE_2 		= (((int32_t)__tmp1));}
						if(__tmp2==1){SET_PRE.TEMPERATURA_PRE=Config.TEMPERATURA_PRE_2; SET_POINT.TEMPERATURA=SET_PRE.TEMPERATURA_PRE;}
					}break;
					}
				}
				else if(_f_PP_teclado==PP_TEMPO_PRE){

					if(_s_TecladoDigitado[0] == ' '){_s_TecladoDigitado[0]=0;}
					if(_s_TecladoDigitado[1] == ' '){_s_TecladoDigitado[1]=0;}
					if(_s_TecladoDigitado[2] == ' '){_s_TecladoDigitado[2]=0;}
					if(_s_TecladoDigitado[3] == ' '){_s_TecladoDigitado[3]=0;}

					memset(&__tmp2,0x00,sizeof(__tmp2));
					memset(&__tmp1,0x00,sizeof(__tmp1));

					__tmp2 = (uint16_t)concatInt(_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
					__tmp1 = (uint16_t)concatInt(_s_TecladoDigitado[2],_s_TecladoDigitado[3]);

					Config.TEMPO_PRE = (uint32_t)(__tmp2*60)+__tmp1;
					if(Config.TEMPO_PRE>(30*60)){Config.TEMPO_PRE=30*60;}
				}
			}

			_c_save_eeprom = 3;
			_f_PP_teclado = 0;
			if(_f_STATUS!=ST_CONF_PRE_AQUEC){
				STATUS=LAST_STATUS;
			}
		}
		else if (_f_STATUS == ST_CONF_EDT_AQ_DOU_RECEITA){
			//IF PARA GARANTIR QUE VAI SALVAR APENAS QUANDO TIVER SELECIONADO
			if(_f_PP_teclado==PP_AQUECER || _f_PP_teclado==PP_DOURAR || _f_PP_teclado==PP_AQUECER_DOURAR){
				for(__pos=0; __pos<=_c_TecladoPos; __pos++){
					_s_TecladoDigitado[__pos] = _s_TecladoDigitado[__pos]-48;
				}

				if(_c_TecladoPos!=0){
					_c_TecladoPos--;
					switch(_c_TecladoPos){
						case 0:{
							__tmp1 = (uint16_t)_s_TecladoDigitado[0];
						}break;
						case 1:{
							__tmp1 = (uint16_t)concatInt(_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
						}break;
					}
					switch(_f_PP_teclado){
						case PP_AQUECER:{RecBravo.cabecalho.aquecer 		= (uint8_t)__tmp1&0x00FF; if(RecBravo.cabecalho.aquecer>59){RecBravo.cabecalho.aquecer = 59;}}break;
						case PP_DOURAR:{RecBravo.cabecalho.dourar 			= (uint8_t)__tmp1&0x00FF; if(RecBravo.cabecalho.dourar>59){RecBravo.cabecalho.dourar = 59;}}break;
						case PP_AQUECER_DOURAR:{RecBravo.cabecalho.dourar_aquecer	= (uint8_t)__tmp1&0x00FF; if(RecBravo.cabecalho.dourar_aquecer>59){RecBravo.cabecalho.dourar_aquecer = 59;}}break;
					}

					_save_rec_sel();
				}
			}
			_f_PP_teclado = 0;
			UpdateScr=1;
		}
//		else if (_f_STATUS == ST_CONF_PRE_AQUEC){
//			//IF PARA GARANTIR QUE VAI SALVAR APENAS QUANDO TIVER SELECIONADO
//			if(_f_PP_teclado==PP_TEMP_TETO || _f_PP_teclado==PP_TEMP_LASTRO || _f_PP_teclado==PP_TEMPO){
//				for(__pos=0; __pos<=_c_TecladoPos; __pos++){
//					_s_TecladoDigitado[__pos] = _s_TecladoDigitado[__pos]-48;
//				}
//
//				_c_TecladoPos--;
//				switch(_c_TecladoPos){
//				case 0:{
//					__tmp1 = (uint16_t)_s_TecladoDigitado[0];
//				}break;
//				case 1:{
//					__tmp1 = (uint16_t)concatInt(_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
//				}break;
//				case 2:{
//					__tmp1 = (uint16_t)concatInt(_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
//					__tmp1 = (uint16_t)concatInt(__tmp1,_s_TecladoDigitado[2]);
//				}break;
//				}
//				switch(_f_PP_teclado){
//				case PP_TEMP_TETO:{
//					if((__tmp1*FATOR_TEMP)>Config.TEMPERATURA_MAX){
//						Config.TEMPERATURA_PRE = Config.TEMPERATURA_MAX;
//					}
//					else{
//						Config.TEMPERATURA_PRE 		= (((int32_t)__tmp1) * FATOR_TEMP);
//					}
//				}break;
//
//				case PP_TEMP_LASTRO:{
//					if((__tmp1*FATOR_TEMP)>Config.TEMPERATURA_MAX){
//						Config.TEMPERATURA_PRE_LASTRO = Config.TEMPERATURA_MAX;
//					}
//					else{
//						Config.TEMPERATURA_PRE_LASTRO 		= (((int32_t)__tmp1) * FATOR_TEMP);
//					}
//				}break;
//
//				case PP_TEMPO:{
//
//					if(_s_TecladoDigitado[0] == ' '){_s_TecladoDigitado[0]=0;}
//					if(_s_TecladoDigitado[1] == ' '){_s_TecladoDigitado[1]=0;}
//					if(_s_TecladoDigitado[2] == ' '){_s_TecladoDigitado[2]=0;}
//					if(_s_TecladoDigitado[3] == ' '){_s_TecladoDigitado[3]=0;}
//
//					memset(&__tmp2,0x00,sizeof(__tmp2));
//					memset(&__tmp1,0x00,sizeof(__tmp1));
//
//					__tmp2 = (uint16_t)concatInt(_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
//					__tmp1 = (uint16_t)concatInt(_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
//
//					Config.TEMPO_PRE = (uint32_t)(__tmp2*60)+__tmp1;
//
//					if(Config.TEMPO_PRE>5999){
//						Config.TEMPO_PRE=5999;
//					}
//
//				}break;
//				}
//
//				_c_save_eeprom=3;
//
//			}
//
//			_f_PP_teclado = 0;
//			UpdateScr;
//		}

		else if (_f_STATUS == ST_NOVA_SENHA_USER){
			if(_c_TecladoPos!=0){
				memcpy((void*)SenhaTemp, (void*)_s_TecladoDigitado, sizeof(SenhaTemp));
				if(LAST_STATUS == ST_ALTERAR_SENHA){
					STATUS = ST_CONFIRMA_SENHA_USER;
				}
				else if(LAST_STATUS == ST_CONFIGURACOES){
					STATUS = ST_CONFIRMA_SENHA_USER_USER;
				}
			}
		}
		else if (_f_STATUS == ST_CONFIRMA_SENHA_USER){
			if(memcmp((void*)SenhaTemp, (void*)_s_TecladoDigitado, sizeof(SenhaTemp)) == 0){
				memcpy((void*)SenhaUser, (void*)_s_TecladoDigitado, sizeof(SenhaUser));
				memcpy((void*)Config.SENHA_USER, (void*)SenhaUser, sizeof(Config.SENHA_USER));
				_c_save_eeprom=3;

				_f_PAGE=0;
				STATUS = ST_ALTERAR_SENHA;

			}
			else{
				STATUS = ST_CONFIRMA_SENHA_USER;
			}


		}
		else if (_f_STATUS == ST_CONFIRMA_SENHA_USER_USER){
			if(memcmp((void*)SenhaTemp, (void*)_s_TecladoDigitado, sizeof(SenhaTemp)) == 0){
				memcpy((void*)SenhaUser, (void*)_s_TecladoDigitado, sizeof(SenhaUser));
				memcpy((void*)Config.SENHA_USER, (void*)SenhaUser, sizeof(Config.SENHA_USER));
				_c_save_eeprom=3;

				_f_PAGE=1;
				STATUS = ST_CONFIGURACOES;

			}
			else{
				STATUS = ST_CONFIRMA_SENHA_USER_USER;
			}


		}
		else if (_f_STATUS == ST_NOVA_SENHA_FAB){
			if(_c_TecladoPos!=0){
				memcpy((void*)SenhaTemp, (void*)_s_TecladoDigitado, sizeof(SenhaTemp));
				STATUS = ST_CONFIRMA_SENHA_FAB;
			}
		}
		else if (_f_STATUS == ST_CONFIRMA_SENHA_FAB){
			if(memcmp((void*)SenhaTemp, (void*)_s_TecladoDigitado, sizeof(SenhaTemp)) == 0){
				memcpy((void*)SenhaFabricante, (void*)_s_TecladoDigitado, sizeof(SenhaFabricante));
				memcpy((void*)Config.SENHA_FAB, (void*)SenhaFabricante, sizeof(Config.SENHA_FAB));
				_c_save_eeprom=3;
				STATUS = ST_ALTERAR_SENHA;
			}
			else{
				STATUS = ST_NOVA_SENHA_FAB;
			}
		}
		else if(_f_STATUS==ST_CONF_DATA_HORA){
			if(_c_TecladoPos!=0){
				switch(_f_PP_teclado){

					case PP_AJUSTE_DATA:{
						switch(Config.Formato_data){
						case 0:{
							snprintf(__text,sizeof(__text),"%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
							DATA_HORA.dia = atoi(__text);
						}break;
						case 1:{
							snprintf(__text,sizeof(__text),"%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
							DATA_HORA.mes = atoi(__text);
						}break;
						case 2:{
							snprintf(__text,sizeof(__text),"%c%c%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
							DATA_HORA.ano = atoi(__text);
						}break;
						}

						switch(Config.Formato_data){
						case 0:{
							snprintf(__text,sizeof(__text),"%c%c",_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
							DATA_HORA.mes = atoi(__text);
						}break;
						case 1:{
							snprintf(__text,sizeof(__text),"%c%c",_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
							DATA_HORA.dia = atoi(__text);
						}break;
						case 2:{
							snprintf(__text,sizeof(__text),"%c%c",_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
							DATA_HORA.mes = atoi(__text);
						}break;
						}

						switch(Config.Formato_data){
						case 0:{
							snprintf(__text,sizeof(__text),"%c%c%c%c",_s_TecladoDigitado[4],_s_TecladoDigitado[5],_s_TecladoDigitado[6],_s_TecladoDigitado[7]);
							DATA_HORA.ano = atoi(__text);
						}break;
						case 1:{
							snprintf(__text,sizeof(__text),"%c%c%c%c",_s_TecladoDigitado[4],_s_TecladoDigitado[5],_s_TecladoDigitado[6],_s_TecladoDigitado[7]);
							DATA_HORA.ano = atoi(__text);
						}break;
						case 2:{
							snprintf(__text,sizeof(__text),"%c%c",_s_TecladoDigitado[6],_s_TecladoDigitado[7]);
							DATA_HORA.dia = atoi(__text);
						}break;
						}

						if(valida_data(DATA_HORA.dia, DATA_HORA.mes, DATA_HORA.ano)){
							get_rtctime();
						}
						else{
							set_rtctime();
						}

					}break;

					case PP_AJUSTE_HORAS:{
						snprintf(__text,sizeof(__text),"%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
						DATA_HORA.hora = atoi(__text);

						snprintf(__text,sizeof(__text),"%c%c",_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
						DATA_HORA.min = atoi(__text);

						if(Config.Formato_hora&0x01 && !(Config.Formato_hora&0x10) && DATA_HORA.hora<12){DATA_HORA.hora+=12;}
						else if(Config.Formato_hora&0x01 && Config.Formato_hora&0x10 && DATA_HORA.hora==12){DATA_HORA.hora=0;}

						if(valida_hora(DATA_HORA.hora, DATA_HORA.min, 0)){
							get_rtctime();
						}
						else{
							set_rtctime();
						}

					}break;

				}
			}
			//STATUS = ST_CONF_DATA_HORA;
			_f_PP_teclado = 0;
			_f_last_PP_teclado = 0;
			UpdateScr=1;

		}
		else if(_f_STATUS==ST_DADOS_DO_FORNO){
			if(_c_TecladoPos!=0){
				switch(_f_PP_teclado){
					case PP_NUM_SERIE:{
						memcpy((void*)&Config.NUM_SERIE,(void*)&_s_TecladoDigitado,sizeof(Config.NUM_SERIE));
						_c_save_eeprom = 3;
					}break;

					case PP_DATA_FAB:{
						//memcpy((void*)&Config.DATA_FABRICACAO,(void*)&_s_TecladoDigitado,sizeof(Config.NUM_SERIE));
						snprintf(__text,sizeof(__text),"%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
						_t_dia = atoi(__text);

						snprintf(__text,sizeof(__text),"%c%c",_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
						_t_mes = atoi(__text);

						snprintf(__text,sizeof(__text),"%c%c%c%c",_s_TecladoDigitado[4],_s_TecladoDigitado[5],_s_TecladoDigitado[6],_s_TecladoDigitado[7]);
						_t_ano = atoi(__text);

						if(valida_data(_t_dia, _t_mes, _t_ano)){
							memcpy((void*)&_s_TecladoDigitado,(void*)&Config.DATA_FABRICACAO,sizeof(_s_TecladoDigitado));
						}
						else{
							memcpy((void*)&Config.DATA_FABRICACAO,(void*)&_s_TecladoDigitado,sizeof(Config.DATA_FABRICACAO));
						}

					}break;
				}
			}
			//STATUS = ST_CONF_DATA_HORA;
			_f_PP_teclado = 0;
			UpdateScr=1;

		}
		else if(_f_STATUS==ST_CONF_INTERNET){
			if(_c_TecladoPos!=0){
				switch(_f_PP_teclado){
					case PP_IP:{		snprintf((char*)Config.IP, 		sizeof(Config.IP),		"%s",_s_TecladoDigitado);}break;
					case PP_MASCARA:{	snprintf((char*)Config.MASCARA, sizeof(Config.MASCARA),	"%s",_s_TecladoDigitado);}break;
					case PP_GATEWAY:{	snprintf((char*)Config.GATWAY, 	sizeof(Config.GATWAY),	"%s",_s_TecladoDigitado);}break;
					case PP_DNS:{		snprintf((char*)Config.DNS, 	sizeof(Config.DNS),		"%s",_s_TecladoDigitado);}break;
				}

				_c_save_eeprom = 3;
			}

			_f_PP_teclado = 0;
			UpdateScr=1;
		}



	}//FIM OK

	else if(_c_TecladoPos <_c_TecladoMax){
		//memset((void*)&_s_TecladoDigitado[_c_TecladoPos],0x00,sizeof(_s_TecladoDigitado[_c_TecladoPos]));
		_s_TecladoDigitado[_c_TecladoPos] = _s_TecladoCaracter;
		_c_TecladoPos++;
	}



	if(_f_STATUS == ST_CONF_EDT_AQ_DOU_RECEITA){
		switch(_f_PP_teclado){
			case PP_AQUECER:{
				hMem2=GUI_MEMDEV_Create(128,90,260,42);	GUI_MEMDEV_Select(hMem2);
				Draw_Background();
				snprintf(__text,sizeof(__text),"%c%cs",_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
				_DrawButtonTransparente(128,90,260,42,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,CENTER);
				_DrawButtonTransparente(128,90,260,42,"__  ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,BOTTOM);
				GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
			}break;
			case PP_DOURAR:{
				hMem2=GUI_MEMDEV_Create(128,206,260,42);	GUI_MEMDEV_Select(hMem2);
				Draw_Background();
				snprintf(__text,sizeof(__text),"%c%cs",_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
				_DrawButtonTransparente(128,206,260,42,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,CENTER);
				_DrawButtonTransparente(128,206,260,42,"__  ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,BOTTOM);
				GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
			}break;
			case PP_AQUECER_DOURAR:{
				hMem2=GUI_MEMDEV_Create(128,322,260,42);	GUI_MEMDEV_Select(hMem2);
				Draw_Background();
				snprintf(__text,sizeof(__text),"%c%cs",_s_TecladoDigitado[0],_s_TecladoDigitado[1]);
				_DrawButtonTransparente(128,322,260,42,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,CENTER);
				_DrawButtonTransparente(128,322,260,42,"__  ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,BOTTOM);
				GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
			}break;
		}
	}

	else if(_f_STATUS == ST_CONF_DATA_HORA){
		switch(_f_PP_teclado){

			case PP_AJUSTE_DATA:{

				hMem2=GUI_MEMDEV_Create(147,56,230,55);	GUI_MEMDEV_Select(hMem2);
				Draw_Background();
				switch(Config.Formato_data){
				case 0:
				case 1:{
					snprintf(__text,sizeof(__text),"%c%c/%c%c/%c%c%c%c", _s_TecladoDigitado[0],_s_TecladoDigitado[1], _s_TecladoDigitado[2],_s_TecladoDigitado[3],_s_TecladoDigitado[4],_s_TecladoDigitado[5], _s_TecladoDigitado[6],_s_TecladoDigitado[7]);
					_DrawButtonTransparente(152,56,220,55,"__  __  ____ ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_48,CENTER,BOTTOM);
				}break;
				case 2:{
					snprintf(__text,sizeof(__text),"%c%c%c%c/%c%c/%c%c", _s_TecladoDigitado[0],_s_TecladoDigitado[1], _s_TecladoDigitado[2],_s_TecladoDigitado[3],_s_TecladoDigitado[4],_s_TecladoDigitado[5], _s_TecladoDigitado[6],_s_TecladoDigitado[7]);
					_DrawButtonTransparente(152,56,220,55,"____  __  __ ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_48,CENTER,BOTTOM);
				}break;
				}
				_DrawButtonTransparente(147,56,230,55,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_48,CENTER,CENTER);//152,56,220,55
				GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
			}break;


			case PP_AJUSTE_HORAS:{
				hMem2=GUI_MEMDEV_Create(152,167,220,55);	GUI_MEMDEV_Select(hMem2);
				Draw_Background();
				if(Config.Formato_hora&0x01){
					snprintf(__text,sizeof(__text),"  %c%c:%c%c%s",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],_s_TecladoDigitado[3],(Config.Formato_hora&0x10)?"am":"pm");
					_DrawButtonTransparente(152,167,220,55,"__ __       ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_55,CENTER,BOTTOM);
				}
				else{
					snprintf(__text,sizeof(__text),"     %c%c:%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
					_DrawButtonTransparente(152,167,220,55,"__ __ ",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_55,CENTER,BOTTOM);
				}

				_DrawButtonTransparente(152,167,220,55,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_55,LEFT,CENTER);
				//_DrawButtonTransparente(152,167,220,55,"__   __",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_55,CENTER,TOP);
				GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
			}break;
		}
	}

	else if(_f_STATUS == ST_DADOS_DO_FORNO){

		UpdateScr = 1;
//		switch(_f_PP_teclado){
//
//		case PP_NUM_SERIE:{
//
//			hMem2=GUI_MEMDEV_Create(202,87,174,78);	GUI_MEMDEV_Select(hMem2);
//			Draw_Background();
//
//			snprintf(__text,sizeof(__text),"%c%c.%c%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],_s_TecladoDigitado[3],_s_TecladoDigitado[4]);
//			_DrawButtonTransparente(202,87,174,36,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_24,CENTER,CENTER);
//			_DrawButtonTransparente(202,100,174,36,"__  ___",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_24,CENTER,TOP);
//			GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
//
//		}break;
//
//
//		case PP_DATA_FAB:{
//
//			hMem2=GUI_MEMDEV_Create(202,173,174,78);	GUI_MEMDEV_Select(hMem2);
//			Draw_Background();
//
//			snprintf(__text,sizeof(__text),"%c%c/%c%c/%c%c%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],_s_TecladoDigitado[3],_s_TecladoDigitado[4],_s_TecladoDigitado[5],_s_TecladoDigitado[6],_s_TecladoDigitado[7]);
//			_DrawButtonTransparente(202,173,174,72,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_24,CENTER,BOTTOM);
//			_DrawButtonTransparente(202,230,174,36,"__ __ ____",_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_24,CENTER,TOP);
//			GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
//		}break;
//
//		}
	}

	else if(_f_STATUS == ST_CONF_PRE_AQUEC){
		switch(_f_PP_teclado){
		case PP_TEMP_TETO:{
			hMem2=GUI_MEMDEV_Create(15,50,160,99);	GUI_MEMDEV_Select(hMem2);
			snprintf(__text,sizeof(__text),"%c%c%c%s",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],UN_TEMP[Config.Escala_Temperatura]);
			_DrawButton(15,50,160,99,__text,_COR_CINZA_CLARO, _COR_PRETO,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);
			_DrawButtonTransparente(15,50,160,99," ___",_COR_PRETO,&SIF_FONT_EXO2_REGULAR_65,LEFT,BOTTOM);
			GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
		}break;
		case PP_TEMP_LASTRO:{
			hMem2=GUI_MEMDEV_Create(15,168,160,99);	GUI_MEMDEV_Select(hMem2);
			snprintf(__text,sizeof(__text),"%c%c%c%s",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],UN_TEMP[Config.Escala_Temperatura]);
			_DrawButton(15,168,160,99,__text,_COR_CINZA_CLARO, _COR_PRETO,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);
			_DrawButtonTransparente(15,168,160,99," ___",_COR_PRETO,&SIF_FONT_EXO2_REGULAR_65,LEFT,BOTTOM);
			GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
		}break;

		case PP_TEMPO:{
//			if(_s_TecladoDigitado[0] == ' '){_s_TecladoDigitado[0]=0;}
//			if(_s_TecladoDigitado[1] == ' '){_s_TecladoDigitado[1]=0;}
//			if(_s_TecladoDigitado[2] == ' '){_s_TecladoDigitado[2]=0;}
//			if(_s_TecladoDigitado[3] == ' '){_s_TecladoDigitado[3]=0;}

			memset(&__text,0x00,sizeof(__text));

			hMem2=GUI_MEMDEV_Create(15,282,160,99);	GUI_MEMDEV_Select(hMem2);
			snprintf(__text,sizeof(__text),"%c%c:%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
			_DrawButton(15,282,160,99,(char*)__text,_COR_CINZA_CLARO, _COR_PRETO,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);
			GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
			_DrawButtonTransparente(15,282,160,99,"__ __",_COR_PRETO,&SIF_FONT_EXO2_REGULAR_65,CENTER,BOTTOM);
		}break;
		}
	}
	else if(_f_STATUS==ST_CONF_RESTAURAR_FABRICA){
		memset(&__text,0x00,sizeof(__text));
		memset(&__tmp2,0x00,sizeof(__tmp2));
		memset(&__tmp1,0x00,sizeof(__tmp1));
		memset(&__text,'*',_c_TecladoPos);

		//senha
		hMem2=GUI_MEMDEV_Create(150,276,245,50);
		GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButtonTransparente(150,276,245,50,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_70,CENTER,CENTER);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}
	else if(_f_STATUS==ST_CONFIGURACOES || _f_PP_teclado == PP_CONFIG){
		memset(&__text,0x00,sizeof(__text));
		memset(&__tmp2,0x00,sizeof(__tmp2));
		memset(&__tmp1,0x00,sizeof(__tmp1));
		memset(&__text,'*',_c_TecladoPos);

		//senha
		hMem2=GUI_MEMDEV_Create(35,265,340,130);
		GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		//_DrawButton(112,277,340,140,__text,_COR_CINZA_ESCURO,_COR_BRANCO,_COR_CINZA_ESCURO,&GUI_Font32B_1,CENTER,CENTER,NORMAL,8);
		_DrawButtonTransparente(35,265,340,140,__text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_70,CENTER,CENTER);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}
	else if(LAST_STATUS==ST_MODO_MANUAL_AJ || LAST_STATUS==ST_CONF_EDT_PASSO_RECEITA){
		memset(&__text,0x00,sizeof(__text));
		if(_f_PP_teclado==PP_TEMPERATURA){
			snprintf(__text,sizeof(__text),"%c%c%c%s",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],UN_TEMP[Config.Escala_Temperatura]);
			_DrawButton(84,282,160,99,__text,_COR_CINZA_CLARO,GUI_BLACK,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);

		}
		if(_f_PP_teclado==PP_TEMPO){
			snprintf(__text,sizeof(__text),"%c%c:%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
			_DrawButton(556,282,160,99,__text,_COR_CINZA_CLARO,GUI_BLACK,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);
		}
	}
	else if(LAST_STATUS==ST_CONF_PRE_AQUEC){
		if(_f_PP_teclado==PP_TEMP_PRE_1){
			snprintf(__text,sizeof(__text),"%c%c%c%s",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],UN_TEMP[Config.Escala_Temperatura]);
			_DrawButton(135,44,160,99,__text,_COR_CINZA_CLARO,GUI_BLACK,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);
		}
		else if(_f_PP_teclado==PP_TEMP_PRE_2){
			snprintf(__text,sizeof(__text),"%c%c%c%s",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],UN_TEMP[Config.Escala_Temperatura]);
			_DrawButton(135,221,160,99,__text,_COR_CINZA_CLARO,GUI_BLACK,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);
		}
		else if(_f_PP_teclado==PP_TEMPO_PRE){
			snprintf(__text,sizeof(__text),"%c%c:%c%c",_s_TecladoDigitado[0],_s_TecladoDigitado[1],_s_TecladoDigitado[2],_s_TecladoDigitado[3]);
			_DrawButton(503,44,160,99,__text,_COR_CINZA_CLARO,GUI_BLACK,_COR_CINZA_CLARO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER,NORMAL,18);
		}
	}
}
void _update_TecladoAlpha(void){
	uint8_t _char_especial = 0;
	char text[100];

	//TRATAMENTO DINAMICO DAS TECLAS PARA CARACTER
	if(_f_PP_teclado==TEC_ALPHA){
		switch(_s_TecladoCaracter){
			case 0: _s_TecladoCaracter = 'Q'; break;
			case 1: _s_TecladoCaracter = 'W'; break;
			case 2: _s_TecladoCaracter = 'E'; break;
			case 3: _s_TecladoCaracter = 'R'; break;
			case 4: _s_TecladoCaracter = 'T'; break;
			case 5: _s_TecladoCaracter = 'Y'; break;
			case 6: _s_TecladoCaracter = 'U'; break;
			case 7: _s_TecladoCaracter = 'I'; break;
			case 8: _s_TecladoCaracter = 'O'; break;
			case 9: _s_TecladoCaracter = 'P'; break;
			case 10: _s_TecladoCaracter = 'A'; break;
			case 11: _s_TecladoCaracter = 'S'; break;
			case 12: _s_TecladoCaracter = 'D'; break;
			case 13: _s_TecladoCaracter = 'F'; break;
			case 14: _s_TecladoCaracter = 'G'; break;
			case 15: _s_TecladoCaracter = 'H'; break;
			case 16: _s_TecladoCaracter = 'J'; break;
			case 17: _s_TecladoCaracter = 'K'; break;
			case 18: _s_TecladoCaracter = 'L'; break;
			case 19: _s_TecladoCaracter = 0xC7; break;
			case 20: _s_TecladoCaracter = 'Z'; break;
			case 21: _s_TecladoCaracter = 'X'; break;
			case 22: _s_TecladoCaracter = 'C'; break;
			case 23: _s_TecladoCaracter = 'V'; break;
			case 24: _s_TecladoCaracter = 'B'; break;
			case 25: _s_TecladoCaracter = 'N'; break;
			case 26: _s_TecladoCaracter = 'M'; break;
			case 27: _s_TecladoCaracter = 0x00; break;
			case 29: _s_TecladoCaracter = ' '; break;
			case 30: _s_TecladoCaracter = 0x01; break;
		}
	}
	else{
		switch(_s_TecladoCaracter){
			case 0: _s_TecladoCaracter = '1'; break;
			case 1: _s_TecladoCaracter = '2'; break;
			case 2: _s_TecladoCaracter = '3'; break;
			case 3: _s_TecladoCaracter = '4'; break;
			case 4: _s_TecladoCaracter = '5'; break;
			case 5: _s_TecladoCaracter = '6'; break;
			case 6: _s_TecladoCaracter = '7'; break;
			case 7: _s_TecladoCaracter = '8'; break;
			case 8: _s_TecladoCaracter = '9'; break;
			case 9: _s_TecladoCaracter = '0'; break;
			case 10: _s_TecladoCaracter = '@'; break;
			case 11: _s_TecladoCaracter = '#'; break;
			case 12: _s_TecladoCaracter = '/'; break;
			case 13: _s_TecladoCaracter = '!'; break;
			case 14: _s_TecladoCaracter = '%'; break;
			case 15: _s_TecladoCaracter = '&'; break;
			case 16: _s_TecladoCaracter = '('; break;
			case 17: _s_TecladoCaracter = ')'; break;
			case 18: _s_TecladoCaracter = '<'; break;
			case 19: _s_TecladoCaracter = '>'; break;
			case 20: _s_TecladoCaracter = ','; break;
			case 21: _s_TecladoCaracter = '.'; break;
			case 22: _s_TecladoCaracter = ':'; break;
			case 23: _s_TecladoCaracter = '-'; break;
			case 24: _s_TecladoCaracter = '+'; break;
			case 25: _s_TecladoCaracter = '*'; break;
			case 26: _s_TecladoCaracter = 0xB0; break;
			case 27: _s_TecladoCaracter = 0x00; break;
			case 29: _s_TecladoCaracter = ' '; break;
			case 30: _s_TecladoCaracter = 0x01; break;
		}
	}

	//TRATAMENTO DO TECLADO CONFORME AS TECLAS DIGITADAS
	if(_s_TecladoCaracter==0x00){
		_f_Clear_Text = 0;
		if(_c_TecladoPos>0){
			//CARACTERES NA FAIXA ESPECIAL DA FONTE
			if(_s_TecladoDigitado[_c_TecladoPos-1]=='\x87' && ((_c_TecladoPos-1)>=1)){
				if(_s_TecladoDigitado[_c_TecladoPos-2]=='\xC3'){
					_char_especial = 1;
				}
			}
			if(_s_TecladoDigitado[_c_TecladoPos-1]=='\xB0' && ((_c_TecladoPos-1)>=1)){
				if(_s_TecladoDigitado[_c_TecladoPos-2]=='\xC2'){
					_char_especial = 1;
				}
			}

			if(_char_especial){
				_c_TecladoPos-=2;
				_s_TecladoDigitado[_c_TecladoPos] = '\0';
				_s_TecladoDigitado[_c_TecladoPos+1] = '\0';
			}
			if(!_char_especial){
				_c_TecladoPos--; _s_TecladoDigitado[_c_TecladoPos]='\0';
			}
		}
	}
	else if(_s_TecladoCaracter==0x01){
		if		(_f_STATUS == ST_CONF_ADD_NOME_GRUPO){

			_f_Grupo_ID_Sel = readNextId("RECUSER/ROSTER.DAT");								//VERIFICA O PROXIMO ID DISPONIVEL

			memcpy((void*)&Grupo_Sel.titulo,(void*)&_s_TecladoDigitado,sizeof(Grupo_Sel.titulo));
			snprintf((char*)Grupo_Sel.path, sizeof(Grupo_Sel.path), "RECUSER/GRP%d",_f_Grupo_ID_Sel);

			if(_check_name_grupo()==0){	//VERIFICA SE JA EXISTE ESSE NOME
				_f_Ico_ID_Sel = 0;
				set_icone();
				set_pre();
				addItemList("RECUSER/ROSTER.DAT", Grupo_Sel);			//ADICIONA O ITEM NA LISTA
				createFolder((char*)Grupo_Sel.path);				//CRIA A PASTA DO GRUPO
				STATUS = ST_CONF_SEL_IMG_GRUPO;
			}
			else{
				STATUS = ST_CONF_NOME_EXISTENTE;
			}
		}
		else if	(_f_STATUS == ST_CONF_ADD_NOME_RECEITA){

			snprintf(text, sizeof(text), "%s/ROSTER.DAT",Grupo_Sel.path);
			_f_Rec_ID_Sel = readNextId(text);												//VERIFICA O PROXIMO ID DISPONIVEL

			memcpy((void*)&Rec_Sel.titulo, (void*)&_s_TecladoDigitado,sizeof(Rec_Sel.titulo));
			snprintf((char*)Rec_Sel.path, sizeof(Rec_Sel.path), "%s/REC%d.DAT",Grupo_Sel.path,_f_Rec_ID_Sel);

			memset((void*)&RecBravo,0x00,sizeof(RecBravo));
			memcpy((void*)&RecBravo.cabecalho.titulo,(void*)&_s_TecladoDigitado,sizeof(RecBravo.cabecalho.titulo));
			memcpy((void*)&RecBravo.cabecalho.categoria,(void*)&Grupo_Sel.titulo,sizeof(RecBravo.cabecalho.categoria));

			if(_check_name_receitas()==0){	//VERIFICA SE JA EXISTE ESSE NOME
				_f_Ico_ID_Sel = 0;
				set_icone();
				addItemList(text, Rec_Sel);			//ADICIONA O ITEM NA LISTA
				//saveRec((char*)Rec_Sel.path, (s_RecBravo*)&RecBravo);	//CRIA A RECEITA NOVA
				_save_rec_sel();

				STATUS = ST_CONF_SEL_IMG_RECEITA;
			}
			else{
				STATUS = ST_CONF_NOME_EXISTENTE;
			}
		}
		else if (_f_STATUS == ST_CONF_EDT_NOME_GRUPO){

			memcpy((void*)&Grupo_Sel.titulo,(void*)&_s_TecladoDigitado,sizeof(Grupo_Sel.titulo));

			if(_check_name_grupo()==0){	//VERIFICA SE JA EXISTE ESSE NOME
				uptItemList("RECUSER/ROSTER.DAT", Grupo_Sel, _f_Grupo_ID_Sel);	//ATUALIZA O GRUPO NA LISTA

				if(Grupo_Sel.FLAGS&0x02){
					set_icone();
				}

				STATUS = ST_CONF_MENU_EDT_GRUPO;
			}
			else{
				STATUS = ST_CONF_NOME_EXISTENTE;
			}
		}
		else if (_f_STATUS == ST_CONF_EDT_NOME_RECEITA){

			snprintf(text, sizeof(text), "%s/ROSTER.DAT",Grupo_Sel.path);
			memcpy((void*)&Rec_Sel.titulo,(void*)&_s_TecladoDigitado,sizeof(Rec_Sel.titulo));
			memcpy((void*)&RecBravo.cabecalho.titulo,(void*)&_s_TecladoDigitado,sizeof(RecBravo.cabecalho.titulo));
			memcpy((void*)&RecBravo.cabecalho.categoria,(void*)&Grupo_Sel.titulo,sizeof(RecBravo.cabecalho.categoria));

			if(_check_name_receitas()==0){	//VERIFICA SE JA EXISTE ESSE NOME
				//saveRec((char*)Rec_Sel.path, (s_RecBravo*)&RecBravo);			//ATUALIZA O NOME DA RECEITA NO ARQUIVO
				uptItemList(text, Rec_Sel, _f_Rec_ID_Sel);						//ATUALIZA A RECEITA NA LISTA
				_save_rec_sel();

				//REALIZA A LEITURA DOS PARAMETROS PARA CONFIRMAR QUE SALVOU
				//memset((void*)&RecBravo,0x00,sizeof(RecBravo));
				//loadRec((char*)Rec_Sel.path, (s_RecBravo*)&RecBravo);
				if(Rec_Sel.FLAGS&0x02){
					set_icone();
				}

				STATUS = ST_CONF_MENU_EDT_RECEITA;
			}
			else{
				STATUS = ST_CONF_NOME_EXISTENTE;
			}
		}
		else if (_f_STATUS == ST_CONF_ADD_INFO_PASSO){
			if(_s_TecladoDigitado[0]!=0x00){
				memcpy((void*)&RecBravo.passos[_f_Passo_Rec_Sel].instrucao,(void*)&_s_TecladoDigitado,sizeof(RecBravo.passos[_f_Passo_Rec_Sel].instrucao));
			}
			else{
				memset((void*)&RecBravo.passos[_f_Passo_Rec_Sel].instrucao,0x00,sizeof(RecBravo.passos[_f_Passo_Rec_Sel].instrucao));
			}

			//saveRec((char*)Rec_Sel.path, (s_RecBravo*)&RecBravo);			//ATUALIZA O NOME DA RECEITA NO ARQUIVO
			uptItemList(text, Rec_Sel, _f_Rec_ID_Sel);						//ATUALIZA A RECEITA NA LISTA
			_save_rec_sel();

			//REALIZA A LEITURA DOS PARAMETROS PARA CONFIRMAR QUE SALVOU
			//memset((void*)&RecBravo,0x00,sizeof(RecBravo));
			//loadRec((char*)Rec_Sel.path, (s_RecBravo*)&RecBravo);

			STATUS = ST_CONF_EDT_PARAM_PASSO_RECEITA;
		}

	}
	else if(_c_TecladoPos <_c_TecladoMax){
		if(_f_Clear_Text){
			memset((void*)&_s_TecladoDigitado, 0x00, sizeof(_s_TecladoDigitado));
			_c_TecladoPos=0;
			_f_Clear_Text = 0;
		}
		if(_s_TecladoCaracter==0xC7){
			snprintf((char*)_s_TecladoDigitado, sizeof(_s_TecladoDigitado), "%s%c%c",_s_TecladoDigitado,'\xC3','\x87');
			_c_TecladoPos+=2;
		}
		else if(_s_TecladoCaracter==0xB0){
			snprintf((char*)_s_TecladoDigitado, sizeof(_s_TecladoDigitado), "%s%c%c",_s_TecladoDigitado,'\xC2','\xB0');
			_c_TecladoPos+=2;
		}
		else{
			_s_TecladoDigitado[_c_TecladoPos] = _s_TecladoCaracter;
			_c_TecladoPos++;
		}
	}

	//TEXTO DIGITADO
	hMem2=GUI_MEMDEV_Create(45,31,571,38);
	GUI_MEMDEV_Select(hMem2);
	Draw_Background();
	_DrawButtonTransparente(45,31,571,38,(char*)_s_TecladoDigitado,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_36,LEFT,CENTER);
	GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
}
void _update_Slider(void){
	char text[10];
	//GUI_MEMDEV_Handle hMem2, hMem2Last, hMem2Now;

	if(_f_PP_teclado==PP_TURBINA){

		EDT_SET.VELOCIDADE_MOTOR = _f_op_Touch;

		//VALOR PARAMETRO
		hMem2 = GUI_MEMDEV_Create(31,66,50,30);
		GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		snprintf(text,sizeof(text),"%d",EDT_SET.VELOCIDADE_MOTOR);
		_DrawButtonTransparente(31,66,40,30,text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

		hMem2=GUI_MEMDEV_Create(72+((LAST_EDT_SET.VELOCIDADE_MOTOR/10)*59),47,68,68);	GUI_MEMDEV_Select(hMem2);

		Draw_Background();
		_DrawButton(89,78,623,6,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER,NORMAL,2);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

		hMem2=GUI_MEMDEV_Create(72+((EDT_SET.VELOCIDADE_MOTOR/10)*59),47,68,68);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButton(89,78,622,6,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER,NORMAL,2);
		_DrawButtonTransparente(84+((EDT_SET.VELOCIDADE_MOTOR/10)*59),47,68,68,ICO_CIRC_INTERNO,_COR_BRANCO,&SIF_FONT_ICON_68x68,CENTER,CENTER);
		_DrawButtonTransparente(72+((EDT_SET.VELOCIDADE_MOTOR/10)*59),47,68,68,ICO_CIRC_EXTERNO,_COR_BRANCO,&SIF_FONT_ICON_68x68,CENTER,CENTER);

		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

	}
	else if(_f_PP_teclado==PP_MAGNETRON){


		EDT_SET.MICROONDAS = _f_op_Touch;
		hMem2=GUI_MEMDEV_Create(31,145,50,30);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		snprintf(text,sizeof(text),"%d",EDT_SET.MICROONDAS);
		_DrawButtonTransparente(31,145,40,30,text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);


		hMem2=GUI_MEMDEV_Create(72+((LAST_EDT_SET.MICROONDAS/10)*59),126,68,68);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButton(89,157,623,6,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER,NORMAL,2);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

		hMem2=GUI_MEMDEV_Create(72+((EDT_SET.MICROONDAS/10)*59),126,68,68);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButton(89,157,623,6,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER,NORMAL,2);
		_DrawButtonTransparente(84+((EDT_SET.MICROONDAS/10)*59), 126,68,68,ICO_CIRC_INTERNO,_COR_BRANCO,&SIF_FONT_ICON_68x68,CENTER,CENTER);
		_DrawButtonTransparente(72+((EDT_SET.MICROONDAS/10)*59), 126,68,68,ICO_CIRC_EXTERNO,_COR_BRANCO,&SIF_FONT_ICON_68x68,CENTER,CENTER);

		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}
	else if(_f_PP_teclado==PP_TEMP_LASTRO){

		EDT_SET.RI = _f_op_Touch;
		hMem2=GUI_MEMDEV_Create(31,223,50,30);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		snprintf(text,sizeof(text),"%d",EDT_SET.RI);
		_DrawButtonTransparente(31,223,40,30,text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER);

		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

		hMem2=GUI_MEMDEV_Create(72+((LAST_EDT_SET.RI/10)*59),204,68,68);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButton(89,235,623,6,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER,NORMAL,2);

		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

		hMem2=GUI_MEMDEV_Create(72+((EDT_SET.RI/10)*59),204,68,68);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButton(89,235,623,6,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER,NORMAL,2);

		_DrawButtonTransparente(84+((EDT_SET.RI/10)*59),204,68,68,ICO_CIRC_INTERNO,_COR_BRANCO,&SIF_FONT_ICON_68x68,CENTER,CENTER);
		_DrawButtonTransparente(72+((EDT_SET.RI/10)*59),204,68,68,ICO_CIRC_EXTERNO,_COR_BRANCO,&SIF_FONT_ICON_68x68,CENTER,CENTER);

		//GUI_SetColor(_COR_BRANCO);
		//GUI_FillCircle(84+((SET_POINT.RI/10)*59)+27, 191+22, 22);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);
	}
	else if(_f_PP_teclado==PP_VEL_MOTOR){

		hMem2=GUI_MEMDEV_Create(72+((Config.VELOCIDADE_PRE/10)*59),162,68,68);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButton(71,193,659,6,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER,NORMAL,2);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

		Config.VELOCIDADE_PRE = _f_op_Touch;

		hMem2=GUI_MEMDEV_Create(72+((Config.VELOCIDADE_PRE/10)*59),162,68,68);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButton(71,193,659,6,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER,NORMAL,2);
		_DrawButtonTransparente(84+((Config.VELOCIDADE_PRE/10)*59),162,68,68,ICO_CIRC_INTERNO,_COR_BRANCO,&SIF_FONT_ICON_68x68,CENTER,CENTER);
		_DrawButtonTransparente(72+((Config.VELOCIDADE_PRE/10)*59),162,68,68,ICO_CIRC_EXTERNO,_COR_BRANCO,&SIF_FONT_ICON_68x68,CENTER,CENTER);

		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);


		hMem2 = GUI_MEMDEV_Create(320,249,160,99);
		GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButtonTransparenteContorno(320,249,160,99, 9, 5, _COR_BRANCO);
		snprintf(text,sizeof(text),"%d %%",Config.VELOCIDADE_PRE);
		_DrawButtonTransparente(320,249,160,99,text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

		_c_save_eeprom = 3;

	}
	else if(_f_PP_teclado==PP_TESTAR_CARGAS){

		hMem2=GUI_MEMDEV_Create(72+((SET_POINT.VELOCIDADE_MOTOR/10)*59),162,68,68);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButton(71,193,659,6,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER,NORMAL,2);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

		SET_POINT.VELOCIDADE_MOTOR = _f_op_Touch;

		hMem2=GUI_MEMDEV_Create(72+((SET_POINT.VELOCIDADE_MOTOR/10)*59),162,68,68);	GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButton(71,193,659,6,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_30,CENTER,CENTER,NORMAL,2);
		_DrawButtonTransparente(84+((SET_POINT.VELOCIDADE_MOTOR/10)*59),162,68,68,ICO_CIRC_INTERNO,_COR_BRANCO,&SIF_FONT_ICON_68x68,CENTER,CENTER);
		_DrawButtonTransparente(72+((SET_POINT.VELOCIDADE_MOTOR/10)*59),162,68,68,ICO_CIRC_EXTERNO,_COR_BRANCO,&SIF_FONT_ICON_68x68,CENTER,CENTER);

		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);


		hMem2 = GUI_MEMDEV_Create(320,249,160,99);
		GUI_MEMDEV_Select(hMem2);
		Draw_Background();
		_DrawButtonTransparenteContorno(320,249,160,99, 9, 5, _COR_BRANCO);
		snprintf(text,sizeof(text),"%d %%",SET_POINT.VELOCIDADE_MOTOR);
		_DrawButtonTransparente(320,249,160,99,text,_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_65,CENTER,CENTER);
		GUI_MEMDEV_Select(0); GUI_MEMDEV_CopyToLCD(hMem2); GUI_MEMDEV_Delete(hMem2);

		_c_save_eeprom = 3;


	}

}

void _update_Botoes_Barra(void){
	uint32_t cor = GUI_RED;

	if(_imprimeBarra == 1){
		if(ERROS.total != _erro_anterior){

			if(_f_STATUS == ST_MENU_PRINCIPAL){
				hMem2=GUI_MEMDEV_Create(15,399,771,67);	GUI_MEMDEV_Select(hMem2);
			}
			else{
				hMem2=GUI_MEMDEV_Create(404,402,200,68);	GUI_MEMDEV_Select(hMem2);
				_DrawButton(404,399,240,67,"",_COR_CINZA_CLARO,_COR_CINZA_CLARO,_COR_CINZA_CLARO,&GUI_Font32_1,CENTER,CENTER,NORMAL,0);
			}


			if(ERROS.total==0x00){
				if(_f_STATUS == ST_MENU_PRINCIPAL){

					Draw_Background();
				}

				if(_bt_ExibeHome != 1){
					_DrawButtonTransparente(404,402,185,68,ICO_GRUPOS,GUI_BLACK,&SIF_FONT_ICON_68x68,CENTER,CENTER);
					botao[_last_grupos].disable = 0;
				}
				else{
					botao[_last_grupos].disable = 1;
				}


			}
			else{

				if(_f_STATUS == ST_MENU_PRINCIPAL){
					_DrawButton(15,399,771,67,"",_COR_CINZA_CLARO,_COR_CINZA_CLARO,_COR_CINZA_CLARO,&GUI_Font20B_1,CENTER,CENTER,NORMAL,14);
				}

				botao[_last_grupos].disable = 0;

				//_DrawButton(404,402,240,68,"",_COR_CINZA_CLARO,_COR_CINZA_CLARO,_COR_CINZA_CLARO,&GUI_Font32_1,CENTER,CENTER,NORMAL,40);

				if(!ERROS.Erro){cor = GUI_YELLOW;}
				msg_erro();

				_DrawButtonTransparente(404,402,185,60,ICO_GRUPOS,_COR_PRETO,&SIF_FONT_ICON_68x68,CENTER,CENTER);

				_DrawButtonTransparente(495,398,68,68,ICO_ALERTA_FUNDO,_COR_PRETO,&SIF_FONT_ICON_68x68,CENTER,CENTER);
				_DrawButtonTransparente(500,398,68,68,ICO_ALERTA_FRENTE,cor,&SIF_FONT_ICON_68x68,CENTER,CENTER);
			}

			GUI_MEMDEV_Select(0);	GUI_MEMDEV_CopyToLCD(hMem2);	GUI_MEMDEV_Delete(hMem2);
			_erro_anterior = ERROS.total;
		}
	}

}


void _zera_Contador(void){

	if(_f_STATUS == ST_CONT_FALHAS){
		checkFile("CNT/FALHAS.DAT");
		deleteFile("CNT/FALHAS.DAT");
	}
	else if(_f_STATUS == ST_CONT_LIMPEZAS){
		checkFile("CNT/LIMPEZAS.DAT");
		deleteFile("CNT/LIMPEZAS.DAT");
	}
	else if(_f_STATUS == ST_CONT_RECEITAS){
		Config.CONTADORES[0] = 0;
		_c_save_eeprom = 3;
	}
	else if(_f_STATUS == ST_CONT_FILTROS){
		Config.CONTADORES[1] = 0;
		_c_save_eeprom = 3;
	}
}

void _print_Tela_Em_Desenvolvimento(void){

	if(ScrLoad){
		Draw_Background();

		_DrawButtonTransparente(51,100,750,300,TXT_DESENVOLVIMENTO[ID_IDIOMA],_COR_BRANCO,&SIF_FONT_EXO2_REGULAR_42,CENTER,CENTER);
	}

	ScrLoad=0; UpdateScr=0;
}


void WatchDog_Init(void){
#ifdef WATCHDOG

	//Configurações do Watchdog
//	WWDT_SetTimerConstant(500000);  //(4 x 500.000)/500.000 = 4 seg
//	WWDT_SetMode(WWDT_RESET_MODE,ENABLE);
//	WWDT_Enable(ENABLE);
//	WWDT_FeedStdSeq();
//	if (WWDT_GetStatus(WWDT_TIMEOUT_FLAG)) {_f_WatchDog=1;}
//	WWDT_ClrTimeOutFlag();

	Chip_WWDT_Init(LPC_WWDT);
	Chip_WWDT_SetTimeOut(LPC_WWDT, WDT_OSC*4);

	Chip_WWDT_SetOption(LPC_WWDT, WWDT_WDMOD_WDRESET);

	NVIC_ClearPendingIRQ(WDT_IRQn);
	NVIC_EnableIRQ(WDT_IRQn);

	Chip_WWDT_Start(LPC_WWDT);

	if(Chip_WWDT_GetStatus(LPC_WWDT)&WWDT_WDMOD_WDRESET){_f_WatchDog=1;}
	Chip_WWDT_ClearStatusFlag(LPC_WWDT, WWDT_WDMOD_WDTOF | WWDT_WDMOD_WDINT);

	_f_WatchDogFeed=0x00;
#endif
}
//----------------------------------------------------------------------------
//	BOARD INIT
//----------------------------------------------------------------------------
volatile s32_t tcpipdone = 0;
void BoardInit(void){

	Chip_IOCON_PinMuxSet(LPC_IOCON, LED_MOD[0].PORT, LED_MOD[0].PIN, LED_MOD[0].FUNC);
	Chip_IOCON_PinMuxSet(LPC_IOCON, LED_MOD[1].PORT, LED_MOD[1].PIN, LED_MOD[1].FUNC);

	LPC_GPIO2->DIR |= (1<<26);
	LPC_GPIO2->SET |= (1<<26);

	Chip_RTC_Init(LPC_RTC);
	Chip_RTC_CntIncrIntConfig (LPC_RTC, RTC_AMR_CIIR_IMSEC, ENABLE);
	Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE);
	NVIC_EnableIRQ(RTC_IRQn);
	Chip_RTC_Enable(LPC_RTC, ENABLE);


	Chip_EEPROM_Init(LPC_EEPROM);
	EEPROM_Read(0,ADDRESS_RESET,(char*)&_f_Reset_IHM,MODE_8_BIT,sizeof(_f_Reset_IHM));
	if(_f_Reset_IHM == 0x0B){
		_f_Reset_IHM = 0x0A;
		EEPROM_Write(0,ADDRESS_RESET,(char*)&_f_Reset_IHM,MODE_8_BIT,sizeof(_f_Reset_IHM));
		ResetISR();
	}
	else{
		_f_Reset_IHM = 0x0B;
		EEPROM_Write(0,ADDRESS_RESET,(char*)&_f_Reset_IHM,MODE_8_BIT,sizeof(_f_Reset_IHM));
	}
	EEPROM_Read(0,ADDRESS_CONFIG,(char*)&Config,MODE_8_BIT,sizeof(s_Config));
	EEPROM_Read(0,ADDRESS_IOT_CONFIG,(char*)&IOT_Config,MODE_8_BIT,sizeof(IOT_Config));

	//IOT_Config.RESET = 0;

	//FORMATA A EEPROM
	//Config.Format=0;
	if(Config.Format!=2){
		//VALORES PADRAO
		memset((void*)&Config, 0x00, sizeof(Config));
		Config.Calib = 0;
		Config.Format = 2;
		Config.Version = 0;

		memset((void*)&Eeprom, 0x00, sizeof(Eeprom));
		EEPROM_Write(0,ADDRESS_EPROM,(char*)&Eeprom,MODE_8_BIT,sizeof(Eeprom));

		_c_save_eeprom = 4;
	}
	//ATUALIZA A VERSAO DA ESTRUTURA DA CONFIG PARA UM VALOR PADRAO CONHECIDO
	if(Config.Version<1){
		memcpy((void*)Config.SENHA_USER, (void*)SenhaUser, sizeof(Config.SENHA_USER));
		memcpy((void*)Config.SENHA_FAB, (void*)SenhaFabricante, sizeof(Config.SENHA_FAB));
		memset((void*)Config.DATA_FABRICACAO, '0', sizeof(Config.DATA_FABRICACAO));
		memset((void*)Config.NUM_SERIE, '0', sizeof(Config.NUM_SERIE));

		memset((void*)Config.IP, '0', sizeof(Config.IP));
		memset((void*)Config.GATWAY, '0', sizeof(Config.GATWAY));
		memset((void*)Config.MASCARA, '0', sizeof(Config.MASCARA));
		memset((void*)Config.DNS, '0', sizeof(Config.DNS));

		memset((void*)Config.DATA_FABRICACAO, '0', sizeof(Config.DATA_FABRICACAO));
		memset((void*)Config.NUM_SERIE, '0', sizeof(Config.NUM_SERIE));

		Config.TEMPERATURA_PRE = 275*FATOR_TEMP;
		Config.TEMPERATURA_PRE_LASTRO = 275*FATOR_TEMP;
		Config.TEMPO_PRE = 5*60;
		Config.VELOCIDADE_PRE = 50;

		Config.ACESSOS = 0xFF;			//LIBERA ACESSOS

		Config.IDIOMA = 0; 				//IDIOMA PORTUGUES

		Config.VOLUME_BUZZER = 100;

		Config.Version = 1;
		_c_save_eeprom = 3;
	}
	if(Config.Version<2){
		Config.TEMPERATURA_PRE_2 = 250*FATOR_TEMP;
		Config.TENSAO_ENTRADA = 0;
		Config.SENSOR_CORRENTE = 0;

		Config.Version = 2;//!! A HORA QUE TERMINAR O PACKAGE MUDAR PARA 2
		_c_save_eeprom = 3;
	}
	if(Config.Version<3){ //CALIBRACAO DO JAPONES
		Config.Calib = 0;

		Config.Version = 3;//!! A HORA QUE TERMINAR O PACKAGE MUDAR PARA 2
		_c_save_eeprom = 3;
	}
	if(Config.Version<4){
		Config._f_FDA = 0;

		Config.Version = 4;
		_c_save_eeprom = 3;
	}
	if(Config.Version<5){
		Config.TEMPERATURA_PRE = (350*FATOR_TEMP_F)-32;
		Config.TEMPERATURA_PRE_2 = (500*FATOR_TEMP_F)-32;

		Config.Version = 5;
		_c_save_eeprom = 3;
	}
	if(Config.Version<6){
		Config._f_Pontos_Toque = 0;
		Config.Version = 6;
		_c_save_eeprom = 3;
	}
	if(Config.Version<7){
		Config._f_FDA = 0;
		Config.Version = 7;
		_c_save_eeprom = 3;
	}
	if(Config.Version<8){
		Config.SENSOR_CORRENTE = 1;
		Config.Version = 8;
		_c_save_eeprom = 3;
	}
	if(Config.Version<9){
		Config.VOLUME_BUZZER = 0;
		Config.Version = 9;
		_c_save_eeprom = 3;
	}
//	Config.Version = 9;
	if(Config.Version<10){

		memset(&IOT_Config,0x00,sizeof(IOT_Config));

		Config.Version = 10;
		_c_save_eeprom = 3;
		_c_save_IOT_config = 1;
	}

	memset(&IOT_MANAGER,0x00,sizeof(IOT_MANAGER));

	memcpy((void*)Config.SENHA_FAB, (void*)SenhaFabricante, sizeof(Config.SENHA_FAB));

//	if(Config.Version<1){
//		memcpy((void*)Config.SENHA_USER, (void*)SenhaUser, sizeof(Config.SENHA_USER));
//		memcpy((void*)Config.SENHA_FAB, (void*)SenhaFabricante, sizeof(Config.SENHA_FAB));
//		Config.Version = 1;
//		_c_save_eeprom = 3;
//	}
//	if(Config.Version<2){//MUDOU A SENHA DO FABRICANTE
//		memcpy((void*)Config.SENHA_FAB, (void*)SenhaFabricante, sizeof(Config.SENHA_FAB));
//		Config.Version = 2;
//		_c_save_eeprom = 3;
//	}
//	if(Config.Version<3){
//
//		memset((void*)Config.DATA_FABRICACAO, '0', sizeof(Config.DATA_FABRICACAO));
//		memset((void*)Config.NUM_SERIE, '0', sizeof(Config.NUM_SERIE));
//
//		Config.Version = 3;
//		_c_save_eeprom = 3;
//	}
//	if(Config.Version<4){
//
//		Config.TEMPERATURA_PRE = 275*FATOR_TEMP;
//		Config.TEMPERATURA_PRE_LASTRO = 275*FATOR_TEMP;
//		Config.TEMPO_PRE = 5*60;
//		Config.VELOCIDADE_PRE = 100;
//
//
//		Config.Version = 4;
//		_c_save_eeprom = 3;
//	}
//	if(Config.Version<5){
//		Config.ACESSOS = 0xFF;
//
//		Config.Version = 5;
//		_c_save_eeprom = 3;
//	}



	__low_level_init();

	EEPROM_Read(0,ADDRESS_EPROM,(char*)&Eeprom,MODE_8_BIT,sizeof(Eeprom));

//	Buz_Init();
//	Buz_Set(0);

	Board_I2C_Init(I2C0);

	/* Initialize I2C */
	Chip_I2C_Init(I2C0);
	Chip_I2C_SetClockRate(I2C0, 400000);

	Chip_I2C_SetMasterEventHandler(I2C0, Chip_I2C_EventHandlerPolling);

	CPT_FT5216_Init();
//	Delay_us(500);
//	ebt_i2c_init();
//	ebt_i2c_start();
//	ebt_i2c_write(0x70); //Slave Address (Write)
//	ebt_i2c_write(0xA4); //ID_G_Mode
//	ebt_i2c_write(0x01); //Disable interrupt status to host
//	ebt_i2c_stop();


	MCP9803_Write_register(0x90,0x01,0x06); //configuração mcp9803


	CPT_FT5216_Read_DEVICE_MODE();

//	Host_Init();

	WatchDog_Init();

//	Chip_IOCON_PinMux(LPC_IOCON, 4, 24, IOCON_MODE_INACT, IOCON_FUNC0);
//	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 4, 24);
//	Chip_GPIO_WritePortBit(LPC_GPIO, 4, 24, 0);


	Init_SDMMC();
//	USB_Init(FlashDisk_MS_Interface.Config.PortNumber, USB_MODE_Host);

	VariaveisInit();

//	_DBG("UNIFICADO\n\r");

}

void msDelay(uint32_t ms)
{
	vTaskDelay((configTICK_RATE_HZ * ms) / 1000);
}

//----------------------------------------------------------------------------
//	TASKS
//----------------------------------------------------------------------------
void MainTask(void);
void MainTask(void) {

//	debug_frmwrk_init();
	//_DBG("FreeRTOS EMWIN FatFS  Example runing\n\r");

	SystemCoreClockUpdate();
	Board_Init();

	BoardInit();

	vPortDefineHeapRegions(xHeapRegions);


	xTaskCreate(task_IOT, 		(signed char *) "t_IOT", configMINIMAL_STACK_SIZE*30, NULL, 1, NULL );
	//xTaskCreate(task_MainGUI, 	(signed char *) "t_MainGUI", configMINIMAL_STACK_SIZE*16, NULL, 2, NULL );
	xTaskCreate(task_Ms, 		(signed char *) "t_Ms", configMINIMAL_STACK_SIZE, NULL, 11, NULL );
	xTaskCreate(task_Led01, 	(signed char *) "t_Led01", configMINIMAL_STACK_SIZE, NULL, 10, NULL );
#ifdef USE_USB
	xTaskCreate(task_USB, 		(signed char *) "t_USB", configMINIMAL_STACK_SIZE*20, NULL, 4, NULL );
#endif
	xTaskCreate(task_CTRLSys, 	(signed char *) "t_CTRLSys", configMINIMAL_STACK_SIZE*10, NULL, 5, NULL );
//	xTaskCreate(task_IOT, 		(signed char *) "t_IOT", configMINIMAL_STACK_SIZE*20, NULL, 1, NULL );


	vTaskStartScheduler();

//	return 1;
	for(;;);
//	for(;;){
//		if(IOT_MANAGER.task.running == 0 && IOT_MANAGER.task.run == 0 && IOT_MANAGER.task.start == 0){
//			xTaskCreate(task_IOT, 		(signed char *) "t_IOT", configMINIMAL_STACK_SIZE*20, NULL, 1, NULL );
//			IOT_MANAGER.task.start = 1;
//		}
//		else if (IOT_MANAGER.task.start == 1 && (IOT_MANAGER.task.running == 0 || IOT_MANAGER.task.run == 0)){
//			IOT_MANAGER.task.start = 0;
//		}
//	}
}

int main(void)
{
	MainTask();
}


GUI_PID_STATE touch;
volatile uint8_t CREATE_FILE_USB=0;

static void task_MainGUI( void *pvParameters ){
	char text[200];

	_v_revisao = atoi(SVN_REV)+1;

	GUI_Init();
	WM_SetDesktopColor(_COR_PRETO);

	GUI_UC_SetEncodeUTF8();
	GUI_UC_SetEncodeUTF8();
	GUI_UC_SetEncodeUTF8();

	GUI_Clear();
	GUI_SetBkColor(_COR_PRETO);

	Chip_GPIO_SetPinState(LPC_GPIO, LCD[19].PORT, LCD[19].PIN, 1);
	Chip_GPIO_SetPinState(LPC_GPIO, LCD[20].PORT, LCD[20].PIN, 1);

	fs_sd = malloc(sizeof (FATFS));
	if(!f_mount(fs_sd,"0:", 1)){
		_f_SD_Connet = 1;
	}

	DEBUGOUT("size Config: %ld \r\n",sizeof(Config));
	DEBUGOUT("size IOT_Config: %ld \r\n",sizeof(IOT_Config));

	vTaskDelay(1000);

	_f_IMG_Back=1;


//	CarregaArquivoRAM((char*)_tmp_ram_B0008,"SYSTEM/TUX.png");
//	size_Tux_png = size_last_file;

	//ATUALIZA A PASTA SYSTEM APOS A ATUALIZACAO USB
	if(Eeprom.Flag_Update_Firmware==1){
		snprintf(text,sizeof(text),"0:SYSTEM_%04d%02d%02d",DATA_HORA.ano,DATA_HORA.mes,DATA_HORA.dia);
		f_rename("0:SYSTEM", text);
		f_rename("0:UPDATE", "0:SYSTEM");
		Eeprom.Flag_Update_Firmware=0;
		EEPROM_Write(0,ADDRESS_EPROM,(char*)&Eeprom,MODE_8_BIT,sizeof(Eeprom));
	}


	//============================================================
	// CALIBRACAO TOUCH
	//============================================================
//	Config.Calib = 0;
	Config.Calib = 1;
	if(!Config.Calib){
		_DrawButton(0,0,800,480,"",_COR_BRANCO,_COR_BRANCO,_COR_BRANCO,&GUI_Font20B_1,CENTER,CENTER,NORMAL,0);
		_DrawButtonTransparente(0,220,800,20,"TOUCH INSIDE CORNER RIGHT BOTTOM",GUI_RED,&GUI_Font24_1,CENTER,CENTER);
		GUI_SetColor(GUI_RED);
		GUI_FillCircle(800, 480, 5);
		while(!Touch.Pressed){CPT_FT5216_ReadTouch(); _f_WatchDogFeed |= 0x01; vTaskDelay(10);}
		Config.CALIB_SCREEN_X = Touch.X1;
		Config.CALIB_SCREEN_Y = Touch.Y1;
		Config.Calib = 1;
		_c_save_eeprom = 3;
	}
	CPT_FT5216_SetPointCalib(800,480);
	CPT_FT5216_SetCalib(Config.CALIB_SCREEN_X,Config.CALIB_SCREEN_Y);


	SET_PRE.PREAQUECEU = 0;
	STATUS = ST_SEL_PRE_AQUECIMENTO;//ST_MENU_PRINCIPAL;

	_f_STATUS_BUZZER = BUZ_DESLIGADO;
	_f_PAGE = 0;
	_f_LastSTATUS = 0xFF;
	_c_TimeMenuInicial= 90;
	_f_USB_file_calib = 0;

	_imprimeBarra = 0;
	CREATE_FILE_USB = 0;

//	verifica_contadores();
//	read_pwm_csv();

	//_DBG("MainGUI running...\n\r");
	while (1) {

		_f_WatchDogFeed |= 0x01;

		CPT_FT5216_ReadTouch();
		touch.x = Touch.X1;
		touch.y = Touch.Y1;
		touch.Pressed = Touch.Pressed;

		if(!_c_TimeTempPainel){
			_v_TempPainel=((MCP9803_Read_register(0x90,0x00)/2))*FATOR_TEMP;
			if		(_v_TempPainel>(85*FATOR_TEMP)){Config.TimePainel[2]++;}
			else if	(_v_TempPainel>(75*FATOR_TEMP)){Config.TimePainel[1]++;}
			else if (_v_TempPainel>(65*FATOR_TEMP)){Config.TimePainel[0]++;}

			if(_v_TempPainel>Config.MaxTempPainel){Config.MaxTempPainel = _v_TempPainel;}
			_c_TimeTempPainel = 5;
		}

		if (Touch.Pressed==0 ) { lock_touch=0; _f_beep=0; lock_beep=0;  }//_f_STATUS_BUZZER = BUZ_DESLIGADO;

		if(_c_save_eeprom==1){
			if(memcmp((void*)&Last_Config, (void*)&Config,sizeof(s_Config))){
				EEPROM_Write(0,ADDRESS_CONFIG,(char*)&Config,MODE_8_BIT,sizeof(s_Config));
				memcpy((void*)&Last_Config, (void*)&Config, sizeof(s_Config));
				_c_save_eeprom=0;
			}
		}
		if(_c_save_IOT_config==1){
			EEPROM_Write(0,ADDRESS_IOT_CONFIG,(char*)&IOT_Config,MODE_8_BIT,sizeof(IOT_Config));
			_c_save_IOT_config = 0;
		}

		if(_c_save_Serial){
			EEPROM_Write(0,ADDRESS_EPROM,(char*)&Eeprom,MODE_8_BIT,sizeof(Eeprom));
			_c_save_Serial = 0;
		}

		GUI_Exec();


//		//PONTOS VERMELHOS NA TELA
		if(Config._f_Pontos_Toque){
			GUI_SetColor(GUI_RED);
			GUI_FillCircle(Touch.X1, Touch.Y1, 2);

			//PONTOS VERMELHOS NA TELA
			GUI_SetColor(GUI_YELLOW);
			GUI_FillCircle(Touch.X2, Touch.Y2, 2);

			//PONTOS VERMELHOS NA TELA
			GUI_SetColor(GUI_GREEN);
			GUI_FillCircle(Touch.X3, Touch.Y3, 2);

			//PONTOS VERMELHOS NA TELA
			GUI_SetColor(GUI_ORANGE);
			GUI_FillCircle(Touch.X4, Touch.Y4, 2);

			//PONTOS VERMELHOS NA TELA
			GUI_SetColor(GUI_BLUE);
			GUI_FillCircle(Touch.X5, Touch.Y5, 2);
		}

		//CONFERE SE HOUVE TOQUE EM UMA AREA PREDEFINIDA
		if(!lock_touch && !UpdateBotao){
			_check_botao_press();
		}

		//VERIFICA SE O PENDRIVE FOI CONECTADO E TEM ALGUMA TELA QUE NECESSITA DE PROCESSO NO USB
		detectUSB();

		//ATUALIZA A F_STATUS APENAS AQUI PARA NÃO TER SOBREPOSICAO DE TELA
		_f_STATUS = STATUS;

		//CASO NO MEIO DO PRINT ALTERE A TELA SÓ SERÁ ATUALIZADA NO PROXIMO CICLO
		if(_f_LastSTATUS!=_f_STATUS){LAST_STATUS=_f_LastSTATUS; _f_LastSTATUS=_f_STATUS; ScrLoad=1;}

#ifdef SIMULA_OPERACAO
		if(ScrLoad){_f_simula_desenho_porta=1;}
#endif



		if(ScrLoad){
			if(_f_STATUS!=ST_EXE_FINALIZACAO && _f_STATUS!=ST_EXE_LANCHES && _f_STATUS!=ST_MODO_MANUAL_OPER){
				CTRL_OPERACAO=0x00;
			}
			_erro_anterior = 0;
			_c_Ciclo_Aquec=0;
			_c_Ciclo_Mag=0;

			_f_Update_Itm = 0;
			_f_Rec_Carregada = 0;	//RESETA PARA QUANDO FOR EXECUTAR UMA NOVA RECEITA
			_f_Ordenar = 0;
			_f_Info_Passo = 0;

			_bt_ExibeHome = 0;
			_imprimeBarra = 0;

			LAST_CTRL_UPDATE = 0;

			Last_Erro_Total = 0;

			_f_Last_Progress=0;

			bot = 0;
			_clean_botao();
			memset((void*)&LAST_SET_POINT,0xFF,sizeof(LAST_SET_POINT));
			memset((void*)&LAST_EDT_SET,0xFF,sizeof(LAST_SET_POINT));

			_bt_VOLTAR=0;

			if(_f_STATUS==ST_EXE_LANCHES && _f_Botoes_Fim_Exe==4){//BOTAO REPETIR
				LAST_STATUS = ST_RECEITAS;
			}
			if(_f_STATUS!=ST_EXE_LANCHES){
				_f_Botoes_Fim_Exe = 0;
			}

			if(STATUS != ST_CONFIGURACOES && STATUS != ST_CONFIGURACOES_FABRICANTE){
				_f_MAX_PAGE = 0;
			}

			if(_f_STATUS==ST_HOME || _f_STATUS==ST_GRUPOS || _f_STATUS==ST_TESTAR_PASSO){
				CTRL_OPERACAO &= ~0x40;
			}
		}



		switch(_f_STATUS){
			//THIS CODE WAS REMOVED
			default:{
				if(ScrLoad || UpdateScr){_print_Tela_Em_Desenvolvimento();}
			}break;
		}



		if(memcmp((void*)&LAST_SET_POINT, (void*)&SET_POINT,sizeof(LAST_SET_POINT))){
			memcpy((void*)&LAST_SET_POINT, (void*)&SET_POINT, sizeof(LAST_SET_POINT));
		}
		if(memcmp((void*)&LAST_EDT_SET, (void*)&EDT_SET,sizeof(LAST_EDT_SET))){
			memcpy((void*)&LAST_EDT_SET, (void*)&EDT_SET, sizeof(LAST_EDT_SET));
		}

#ifdef SIMULA_OPERACAO
		if(_f_simula_desenho_porta){
			_DrawButton(700, 0, 100, 100, "Porta", GUI_WHITE, GUI_BLACK, GUI_WHITE, &SIF_FONT_EXO2_REGULAR_24, CENTER, CENTER, NORMAL, 0);
			_f_simula_desenho_porta=0;
		}
		if((touch.x>700) && (touch.x<(700+100)) && (touch.y>0) && (touch.y<(0+100))){
			if(_f_simula_botao_porta==0){
				if(POWER_DIG_IN.bits.PORTA==7){POWER_DIG_IN.bits.PORTA=0; STATUS_POWER.S_PORTA=0;}else{POWER_DIG_IN.bits.PORTA=7; STATUS_POWER.S_PORTA=1;}
			}
			_f_simula_botao_porta=2;
		}
#endif


		_save_erro();		//VERIFICA SE HA ERROS E SALVA NO REGISTRO DE CONTADORES 'CNT/FALHSAS.DAT'

		load_iot_parameters();
		save_iot_grupos_receitas();


		snprintf(text,sizeof(text),"%d %d %d %d %d %d %d %d %d",IOT_MANAGER._c_time_get_status, IOT_TIME_ERROR, IOT_SENDED, IOT_ERROR, IOT_MANAGER.IOT_FRAME, IOT_LINE_PASS,IOT_Config.server.VERSAO_SERVER, IOT_Config.server.VERSAO_LOCAL,_c_ETH_Tempo_medio_resposta);
		MensagemDebug(0,0,400,30,text);


#ifdef VERSAO_BETA
		//VERSAO DATA E HORA DA COMPILACAO
#ifdef EXIBE_BANNER
		snprintf(text,sizeof(text),"R%d %s %s",_v_revisao,__DATE__,__TIME__);
		MensagemDebug(650,470,300,30,text);
#endif
#endif

		cont_GUI++;
		vTaskDelay(10);
	}
}

static void task_Led01( void *pvParameters ){
	uint8_t led_state=0;

	//_DBG("Led01 running...\n\r");
	while(1){
		_f_WatchDogFeed |= 0x08;
		GPIO_Set(26,led_state);
		led_state = !led_state;
		cont_LED01++;
		vTaskDelay(300);
	}
}

static void task_CTRLSys( void *pvParameters ){
	uint8_t __c_100ms=0;
	uint8_t __f_veloc_motor=0;
	uint8_t __c_time_beep=0;
	uint8_t __f_fim_rec=0;
	uint8_t __f_porta_fechou=0;
	uint16_t __c_time_porta_aberta=300;


	//_DBG("CTRLSys running...\n\r");


	EmbNET_SetDevice(IDMod,Net,Type,(uint16_t)CLASS_ID,(uint16_t)(Eeprom.SN_LOTE),(uint16_t)(Eeprom.SN_SERIE));
	EmbNET_StartUp();

	lock_beep=0;
	_f_beep=0;

	while(1){
		_f_WatchDogFeed |= 0x02;
		if(_f_USB_Process){_f_WatchDogFeed|=0x04;}//if(STATUS == ST_USB_UPDATE_FIRMWARE){_f_WatchDogFeed|=0x04;}
		cont_CTRLSys++;

#ifdef WATCHDOG
		if(_f_WatchDogFeed&0x01 && _f_WatchDogFeed&0x02 && _f_WatchDogFeed&0x04 && _f_WatchDogFeed&0x08){
			Chip_WWDT_Feed(LPC_WWDT);
			_f_WatchDogFeed = 0x00;
		}
#endif

		if(lock_beep && !_f_beep){_f_beep=1; _c_time_beep_int=2;}
		if(Config.VOLUME_BUZZER==0){
			if(_c_time_beep_int){ Buz_Set(0); }else{	Buz_Set(1);}
		}
		//==============================================================================
		//	CONTADORES DE TEMPO
		//==============================================================================
		__c_100ms++;
		if(__c_100ms>10){
			_c_Ciclo_Mag++;
			if(_c_Ciclo_Mag>MAX_CICLO_MAG){_c_Ciclo_Mag=0;}

			_c_Ciclo_Aquec++;
			if(_c_Ciclo_Aquec>MAX_CICLO_AQ){_c_Ciclo_Aquec=0;}

			__c_100ms=0;

			__c_time_beep++;
			if(_c_time_beep_int>0){_c_time_beep_int--;}

			if(STATUS!=ST_HIGIENIZACAO){
				if(CTRL_OPERACAO&0x01 || CTRL_OPERACAO&0x02){if(__c_time_porta_aberta>0){__c_time_porta_aberta--;}} //ATRASO DE 30SEG APOS ABRIR A PORTA PARA ACIONAR BUZZER
				else{__c_time_porta_aberta=300;}
			}

			EmbNET_TickTime100ms();
		}
		if(_c_timeOutBuzzerExt==30){__c_time_porta_aberta=300;}//TIMEOUT BUZZER DE TELA

		if((STATUS!=ST_HIGIENIZACAO && STATUS!=ST_PRE_AQUEC_GRP) || (STATUS==ST_PRE_AQUEC_GRP && SET_PRE.AQUECE)){
			//		if		(CTRL_OPERACAO&0x01){		_f_STATUS_BUZZER=BUZ_PULSANTE;  }	//PORTA ABERTA
			if		(CTRL_OPERACAO&0x01){
				if(!__c_time_porta_aberta){_f_STATUS_BUZZER=BUZ_PULSANTE; }else{_f_STATUS_BUZZER=BUZ_DESLIGADO;} if(CTRL_OPERACAO&0x04){__f_fim_rec=1;}
				if(_f_STATUS==ST_TESTAR_PASSO || _f_STATUS==ST_EXE_LANCHES || _f_STATUS==ST_MODO_MANUAL_OPER){
					if(!tmpMaisExe){CTRL_OPERACAO|=0x02;} else {STATUS=ST_HOME;}
				}
			}	//PORTA ABERTA
			else if (CTRL_OPERACAO&0x08){}
			else if	(CTRL_OPERACAO&0x04){		if(!__f_fim_rec){_f_STATUS_BUZZER=BUZ_PULSANTE; }else{_f_STATUS_BUZZER=BUZ_DESLIGADO;}}	//FIM DE RECEITA
			else if	(!(CTRL_OPERACAO&0x01)){	/*_f_STATUS_BUZZER=BUZ_DESLIGADO;*/	__f_fim_rec=0; if(CTRL_OPERACAO&0x02){_f_STATUS_BUZZER=BUZ_PULSANTE; }}	//SEM BUZZER


			//==============================================================================
			//	CONTROLE DA PORTA
			//==============================================================================
			//if(POWER_DIG_IN.bits.PORTA!=7)
			if(!STATUS_POWER.S_PORTA){CTRL_OPERACAO |= 0x01; __f_porta_fechou=2; if(_f_STATUS!=ST_HIGIENIZACAO)_c_AtrasoPlay=0;} else {CTRL_OPERACAO &= ~0x01; if(__f_porta_fechou==2){__f_porta_fechou=1;}}

			if(__f_porta_fechou==1){	//FECHOU A PORTA
				//CTRL_OPERACAO &= ~0x02;	//DA O PLAY

#ifdef PLAY_FDA
				//-------------------------- PARA O TESTE DO FDA
				if(STATUS==ST_EXE_FINALIZACAO || STATUS==ST_EXE_LANCHES){
					STATUS = ST_EXE_LANCHES;
					_f_Botoes_Fim_Exe = 4;
					CTRL_OPERACAO = 0x00;
				}
#endif
				//------------------------- FIM - PARA O TESTE DO FDA

				if(!(CTRL_OPERACAO&0x02)){
					_f_STATUS_BUZZER=BUZ_DESLIGADO;
					__f_porta_fechou=0;
				}
			}
		}
		else if((STATUS==ST_PRE_AQUEC_GRP)){
			//==============================================================================
			//	CONTROLE DA PORTA
			//==============================================================================
			//if(POWER_DIG_IN.bits.PORTA!=7)
			if(!STATUS_POWER.S_PORTA){CTRL_OPERACAO |= 0x01; __f_porta_fechou=2; if(_f_STATUS!=ST_HIGIENIZACAO)_c_AtrasoPlay=0;} else {CTRL_OPERACAO &= ~0x01; if(__f_porta_fechou==2){__f_porta_fechou=1;}}
		}





		//===============================================================================
		//	CONTROLE OPERACAO
		//===============================================================================
		if(!CTRL_OPERACAO || Config.Demonstracao==1){
//			POWER_DIG_OUT.bits.GERAL = ON;

			//============================================================
			// DETERMINA SE HAVERA PRE-AQUECIMENTO
			//============================================================
			if(STATUS==ST_MODO_MANUAL_OPER || STATUS==ST_TESTAR_CARGAS){ _f_PreAquecimento = OFF;}
			else {_f_PreAquecimento = ON;}

			switch(STATUS){
				case ST_DESLIGADO:
				case ST_SEL_PRE_AQUECIMENTO:
				case ST_MENU_PRINCIPAL:{
					POWER_DIG_OUT.bits.RELE_MAG = OFF;
					POWER_DIG_OUT.bits.AQ_CAM = OFF;
					POWER_DIG_OUT.bits.AQ_RI = OFF;

					_f_Motor = OFF;
					__f_veloc_motor = 0;

				}break;
				case ST_TESTAR_PASSO:
				case ST_EXE_LANCHES:
				case ST_MODO_MANUAL_OPER:{
					_f_PreAquecimento = OFF;

					_f_Motor = ON;

					if(_f_Rec_Carregada){//SO INICIA QUANDO A RECEITA FOR CARREGADA

						__f_veloc_motor = SET_POINT.VELOCIDADE_MOTOR;
						_ctrl_tempo_sonda();
						_ctrl_mag();
						_ctrl_aquecimento();
						carrega_proximo_passo();
					}

				}break;
				case ST_HIGIENIZACAO:{

					if(_f_PAGE==0 && ((POWER.TEMP_1>SET_POINT.TEMPERATURA && !(_f_FALHA_SENSORES&0x01)) || (POWER.TEMP_2>SET_POINT.TEMPERATURA && !(_f_FALHA_SENSORES&0x02))) ){
						_f_Motor = ON;
						__f_veloc_motor = 50;
					}
					else{
						_f_Motor = OFF;
					}
					POWER_DIG_OUT.buffer = 0x0000;
				}break;

				default:{
					_c_TEMPO_CORRENTE = 0;
					if(CTRL_OPERACAO&0x01){
						CTRL_OPERACAO &= ~(0x04);	//REMOVE FIM DE RECEITA
					}

					_f_Motor = ON;
					__f_veloc_motor = 1;
					POWER_DIG_OUT.bits.RELE_MAG = OFF;
					if(!_f_PreAquecimento){
						POWER_DIG_OUT.bits.AQ_CAM = OFF;
						POWER_DIG_OUT.bits.AQ_RI = OFF;
					}
					else{
						if(STATUS!=ST_MODO_MANUAL_AJ && LAST_STATUS!=ST_MODO_MANUAL_AJ && STATUS!=ST_CONF_EDT_PASSO_RECEITA && LAST_STATUS!=ST_CONF_EDT_PASSO_RECEITA && STATUS!=PP_TECLADO_NUM
								&& STATUS!=ST_CONF_EDT_PARAM_PASSO_RECEITA){
//							SET_POINT.TEMPERATURA = SET_PRE.TEMPERATURA_PRE;
						}

						if((_f_STATUS==ST_PRE_AQUEC_GRP && SET_PRE.AQUECE) || _f_STATUS!=ST_PRE_AQUEC_GRP){
							_ctrl_aquecimento();
						}

						if((POWER.TEMP_1<SET_POINT.TEMPERATURA && !(_f_FALHA_SENSORES&0x01)) || (POWER.TEMP_2<SET_POINT.TEMPERATURA && !(_f_FALHA_SENSORES&0x02))){__f_veloc_motor = Config.VELOCIDADE_PRE;}
						else if(((POWER.TEMP_1 >= SET_POINT.TEMPERATURA) || _f_FALHA_SENSORES&0x01) && ((POWER.TEMP_2 >= SET_POINT.TEMPERATURA) || _f_FALHA_SENSORES&0x02)){

							__f_veloc_motor = 1;
							if(!_f_TimePreAque && STATUS==ST_PRE_AQUECIMENTO){
								_c_TimePreAque = Config.TEMPO_PRE;
								_f_TimePreAque = 1;
							}
							if(STATUS==ST_PRE_AQUECIMENTO && _f_TimePreAque && !_c_TimePreAque){
								_f_TimePreAque=1;
								if(!_c_TimePreAque && !ScrLoad){
									STATUS=ST_HOME;
									_f_PAGE = 0;
									_f_TimePreAque=0;
									SET_PRE.PREAQUECEU = 1;
								}
							}
							if(STATUS==ST_PRE_AQUEC_GRP && SET_PRE.AQUECE){
								STATUS=ST_HOME;
								_f_STATUS_BUZZER=BUZ_PULSANTE;
								_c_timeOutBuzzerExt = 30;
							}
						}
					}
				}break;
			}//FIM SWITCH
		}
		else{
			if(CTRL_OPERACAO&0x01){
				CTRL_OPERACAO &= ~(0x04);	//REMOVE FIM DE RECEITA
			}
			switch(_f_STATUS){
			case ST_HIGIENIZACAO:{
				if(_f_PAGE==0 && ((POWER.TEMP_1>SET_POINT.TEMPERATURA && !(_f_FALHA_SENSORES&0x01)) || (POWER.TEMP_2>SET_POINT.TEMPERATURA && !(_f_FALHA_SENSORES&0x02))) ){
				//if(_f_PAGE==0 && POWER.TEMP_1>SET_POINT.TEMPERATURA){
					_f_Motor = ON;
					__f_veloc_motor = 50;
				}
				else{
					_f_Motor = OFF;
				}
				POWER_DIG_OUT.buffer = 0x0000;
			}break;
			case ST_PRE_AQUEC_GRP:{
				if(SET_PRE.AQUECE==0){//ESFRIA
					if((POWER.TEMP_1>SET_POINT.TEMPERATURA && !(_f_FALHA_SENSORES&0x01)) || (POWER.TEMP_2>SET_POINT.TEMPERATURA && !(_f_FALHA_SENSORES&0x02))){
						_f_Motor = ON;
						__f_veloc_motor = 50;
					}
					else{
						if(_f_STATUS!=ST_HOME){
							STATUS=ST_HOME;
							_f_STATUS_BUZZER=BUZ_PULSANTE;
							_c_timeOutBuzzerExt = 30;
						}
						_f_Motor = OFF;
					}
					POWER_DIG_OUT.buffer = 0x0000;
				}
				else{
					_f_Motor = OFF;
					POWER_DIG_OUT.buffer = 0x0000;
				}
			}break;
			default:
				CTRL_OPERACAO &= ~(0x04);	//REMOVE FIM DE RECEITA

				_f_Motor = OFF;
				POWER_DIG_OUT.buffer = 0x0000;
			}
		}


		//===============================================================================
		//	VENTILADOR
		//===============================================================================
		POWER_DIG_OUT.bits.VENTILADOR = ON;


		//===============================================================================
		//	TESTE DE CARGAS SOBREPOE O RESTANTE
		//===============================================================================
		if((STATUS==ST_TESTAR_CARGAS || STATUS==ST_TESTE_INVERSOR) && !ScrLoad){
			_f_PreAquecimento = OFF;

			if(_f_teste_carga && TESTE_POWER.buffer && !(_f_teste_carga_ON)){
				if(_f_teste_carga==TESTE_POWER.buffer){_f_teste_carga = 0; TESTE_POWER.buffer=0x00; UpdateScr=1;}
				_c_time_teste_carga = 0;
			}

			if(_f_teste_carga){
				POWER_DIG_OUT.buffer = 0x0000;
				TESTE_POWER.buffer = _f_teste_carga;
				_c_time_teste_carga=0;
				_f_teste_carga=0;
				_f_teste_carga_ON=1;
				UpdateScr=1;
			}

			POWER_DIG_OUT.buffer = 0x0000;
			_f_STATUS_BUZZER=BUZ_DESLIGADO;
			_f_Motor = OFF;
			__f_veloc_motor = 1;


			if(TESTE_POWER.BUZZER){_f_STATUS_BUZZER = BUZ_PULSANTE; }
			//if(TESTE_POWER.MOTOR){POWER_DIG_OUT.bits.GERAL=ON;}//_f_Motor=ON; __f_veloc_motor=20;}
			if(TESTE_POWER.MICROONDAS){
				if(!(CTRL_OPERACAO&0x01)){SET_POINT.MICROONDAS=100; _ctrl_mag();}else{POWER_DIG_OUT.buffer = 0x0000;}
				POWER_DIG_OUT.bits.GERAL=ON;
			}
			if(TESTE_POWER.VENTILADOR){POWER_DIG_OUT.bits.VENTILADOR = ON;}
			if(TESTE_POWER.RES_LASTRO){POWER_DIG_OUT.bits.GERAL=ON; POWER_DIG_OUT.bits.AQ_RI = ON; _f_Motor=ON; __f_veloc_motor=1;}
			if(TESTE_POWER.RES_TETO){POWER_DIG_OUT.bits.GERAL=ON; POWER_DIG_OUT.bits.AQ_CAM = ON; _f_Motor=ON; __f_veloc_motor=1;}
			if(TESTE_POWER.CONTATOR){POWER_DIG_OUT.bits.GERAL=OFF;}else{POWER_DIG_OUT.bits.GERAL=ON;}


			if(_c_time_teste_carga>MAX_TIME_TESTE_CARGA){
				TESTE_POWER.buffer = 0x00;
				_c_time_teste_carga = 0;
				UpdateScr=1;
			}

			if(!_f_teste_Vinculo){
				VINCULOS.bits.FUNC = 0x00; VINCULOS.bits.VETOR = 0; VINCULOS.bits.VALOR = 0;
				VINCULOS.bits.DEST = VINC_OUT7; VINCULOS.bits.ORIG = VINC_INV;
				EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Vinculos,VINCULOS.buffer);
				vTaskDelay(1);
				EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Out,(uint32_t)POWER_DIG_OUT.buffer);
				vTaskDelay(1);
				_f_teste_Vinculo = 1;
			}

			if(STATUS==ST_TESTE_INVERSOR){
				_f_Motor = ON;
				__f_veloc_motor = SET_POINT.VELOCIDADE_MOTOR;
			}
		}
		else if(_f_teste_Vinculo==1){
			_f_MOD_connect|=MASK_MOD_POWER;
			_f_MOD_config|=MASK_MOD_POWER;
			if(_f_teste_Vinculo==1){
				VINCULOS.bits.FUNC = 0x04|0x02; VINCULOS.bits.VETOR = 0; VINCULOS.bits.VALOR = 0;
				VINCULOS.bits.DEST = VINC_OUT7; VINCULOS.bits.ORIG = VINC_INV;
				EmbNET_SendFrame_Set(IDMod_Power,Net,Attrib_Set_Vinculos,VINCULOS.buffer);
				vTaskDelay(1);
				_f_teste_Vinculo=0;
			}
		}

		//==============================================================================
		//	CONTROLE DO BUZZER
		//==============================================================================
		switch(_f_STATUS_BUZZER){
			case BUZ_LIGADO:{
//				POWER_DIG_OUT.bits.BUZZER=ON;
				if		(__c_time_beep<8){POWER_DIG_OUT.bits.BUZZER=ON;}
				else if	(__c_time_beep<12){POWER_DIG_OUT.bits.BUZZER=OFF;}
				else {__c_time_beep=0;}
			}break;
			case BUZ_PULSANTE:{
				if		(__c_time_beep<5){POWER_DIG_OUT.bits.BUZZER=ON;}
				else if	(__c_time_beep<8){POWER_DIG_OUT.bits.BUZZER=OFF;}
				else {__c_time_beep=0;}
			}break;
			case BUZ_DESLIGADO:{__c_time_beep=0; POWER_DIG_OUT.bits.BUZZER=OFF;}break;
			default:{__c_time_beep=0; POWER_DIG_OUT.bits.BUZZER=OFF;}break;
		}

		if(Config.VOLUME_BUZZER){
			Buz_Set(!POWER_DIG_OUT.bits.BUZZER);
			POWER_DIG_OUT.bits.BUZZER=OFF;
		}


		//===============================================================================
		//	CONTROLE DO MOTOR
		//===============================================================================
		if(!_f_Motor){	//MOTOR DESLIGADO
			POWER_MODBUS.bits.LIGA_MOTOR=OFF;
			POWER_MODBUS.bits.VELOCIDADE=0;
		}
		else{			//MOTOR LIGADO
			POWER_MODBUS.bits.LIGA_MOTOR=ON; POWER_MODBUS.bits.VELOCIDADE=(uint32_t)__f_veloc_motor; POWER_MODBUS.bits.SENTIDO_MOTOR=0;//SEM REVERSAO
			//			if 			(_c_Motor_Reversao < 150) 	{POWER_MODBUS.bits.LIGA_MOTOR=ON; POWER_MODBUS.bits.VELOCIDADE=(uint32_t)__f_veloc_motor; POWER_MODBUS.bits.SENTIDO_MOTOR=0;/*_f_Motor_Sentido;*/}
			//			else if 	(_c_Motor_Reversao < 153)	{POWER_MODBUS.bits.LIGA_MOTOR=OFF; POWER_MODBUS.bits.VELOCIDADE=0;  _f_Motor_Sentido=!_f_Motor_Sentido; _c_Motor_Reversao=0;}
		}


		if(LAST_CTRL_OPERACAO != CTRL_OPERACAO){
			UpdateScr = 1;
			LAST_CTRL_OPERACAO = CTRL_OPERACAO;
		}

		//===============================================================================
		//	ERROS/ALERTAS
		//===============================================================================
		if((POWER.TEMP_1>(500*FATOR_TEMP)) || (POWER.TEMP_1<(-500*FATOR_TEMP))){	if(!_c_time_erro_CAM){ERROS.CAM = 1;} _f_FALHA_SENSORES|=0x01;} else {ERROS.CAM = 0; if(!_c_time_erro_init)_c_time_erro_CAM=TIME_MAX_ERRO_SENSOR_TEMP; _f_FALHA_SENSORES&=~0x01;}
		if((POWER.TEMP_2>(500*FATOR_TEMP)) || (POWER.TEMP_2<(-500*FATOR_TEMP))){	if(!_c_time_erro_IR){ERROS.IR = 1;}   _f_FALHA_SENSORES|=0x02;} else {ERROS.IR = 0;  if(!_c_time_erro_init)_c_time_erro_IR=TIME_MAX_ERRO_SENSOR_TEMP;  _f_FALHA_SENSORES&=~0x02;}

		//ERRO/ALERTA PAINEL
		if 		((POWER.TEMP_BOARD>1440) || _v_TempPainel>1440){//ERRO PAINEL
			if(!_c_time_erro_Painel){ERROS.Painel = 1;}//DEPOIS DO TEMPO GERA O ERRO
		}
		else{
			_c_time_erro_Painel=TIME_MAX_ERRO_PAINEL;
			if	(((POWER.TEMP_BOARD>1170) && (POWER.TEMP_BOARD<1440)) || ((_v_TempPainel>1170) && (_v_TempPainel<1440)) ){ERROS.Painel=2;}//ALERTA PAINEL
		}

		//ERRO INVERSOR
		if(POWER_ERROS.bits.INVERSOR){ERROS.INV = 1;}else{ERROS.INV = 0;}
		if(POWER_ERROS.bits.MAG && Config.SENSOR_CORRENTE){	  ERROS.MAG = 1;}else{ERROS.MAG = 0;}

		//ERROS SENSORES
		if(!ERROS.Erro){
			if		(CTRL_OPERACAO&0x10){ERROS.CAN=1;	ERROS.Erro = 1;  if(_c_count_erro_CAN>5){_f_Embnet_off = 1;}}
			else if (ERROS.CAM && ERROS.IR){			ERROS.Erro = 1;  CTRL_OPERACAO|= 0x08;}
			else if (ERROS.INV){						ERROS.Erro = 1;}
			else if (ERROS.Painel==1){					ERROS.Erro = 1;}
			//else if (ERROS.MAG){						ERROS.Erro = 1;}
			else {										ERROS.Erro = 0;}
		}
		else{
			if ((!ERROS.CAM || !ERROS.IR)
					&& (!ERROS.INV && !ERROS.Painel && !ERROS.CAN && !ERROS.MAG)){ERROS.Erro = 0; CTRL_OPERACAO &= ~0x08;}
		}

		//FALHA NOS SENSORES DE TEMP ATUA DIRETAMENTE NA RESISTENCIA
		if((POWER.TEMP_1>(500*FATOR_TEMP)) || (POWER.TEMP_1<(-500*FATOR_TEMP))){POWER_DIG_OUT.bits.AQ_CAM=0;}
		if((POWER.TEMP_2>(500*FATOR_TEMP)) || (POWER.TEMP_2<(-500*FATOR_TEMP))){POWER_DIG_OUT.bits.AQ_RI=0;}

		if(Last_Erro_Total!=ERROS.total){
			UpdateScr = 1;

			if(_f_buzzer_erro_alerta != ERROS.total){
				if(_f_STATUS==ST_GRUPOS || _f_STATUS==ST_PRE_AQUECIMENTO || _f_STATUS==ST_TESTAR_PASSO || _f_STATUS==ST_EXE_LANCHES || _f_STATUS==ST_MODO_MANUAL_OPER || _f_STATUS==ST_MENU_PRINCIPAL){
					if(ERROS.Erro){
						_f_STATUS_BUZZER = BUZ_LIGADO;
						_c_timeOutBuzzerExt = 30;
					}
					else{
						if(ERROS.total>_f_buzzer_erro_alerta){
							_f_STATUS_BUZZER = BUZ_PULSANTE;
							_c_timeOutBuzzerExt = 30;
						}
					}
				}
				_f_buzzer_erro_alerta = ERROS.total;
			}
			Last_Erro_Total=ERROS.total;
		}

		if(ERROS.Erro){
			_f_Motor = OFF;
			CTRL_OPERACAO |= 0x02;
			POWER_MODBUS.bits.LIGA_MOTOR=OFF;
			POWER_MODBUS.bits.VELOCIDADE=0;
//			POWER_DIG_OUT.buffer = 0x0000;
			POWER_DIG_OUT.bits.AQ_CAM = 0;
			POWER_DIG_OUT.bits.AQ_RI = 0;
			POWER_DIG_OUT.bits.RELE_MAG = 0;
			POWER_DIG_OUT.bits.GERAL = 0;
		}

		if(POWER_MODBUS.bits.VELOCIDADE==0){
			_f_Motor = OFF;
			POWER_MODBUS.bits.LIGA_MOTOR=OFF;
			POWER_DIG_OUT.bits.AQ_CAM = OFF;
			POWER_DIG_OUT.bits.AQ_RI = OFF;
		}

		//===============================================================================
		//	CONTROLA SAIDA PARA CHAVEAR TENSAO DO EQUIPAMENTO
		//===============================================================================
		if(Config.TENSAO_ENTRADA==3){	POWER_DIG_OUT.bits.TENSAO = 0;}
		else{							POWER_DIG_OUT.bits.TENSAO = 1;}

		//===============================================================================
		//	EMBNET
		//===============================================================================
		if(_c_conexao!=0x07){
			POWER_DIG_OUT.buffer = 0x0000;
		}
		Tratativas_Embnet_CTRLSys();

		vTaskDelay(10);
	}
}

static void task_USB( void *pvParameters ){

	CREATE_FILE_USB = 1;
	USB_Init(FlashDisk_MS_Interface.Config.PortNumber, USB_MODE_Host);
	fs_usb = malloc(sizeof (FATFS));

	while(1){

		_f_WatchDogFeed |= 0x04;

		if(_f_USB_file_calib && (_f_STATUS==ST_MENU_PRINCIPAL || _f_STATUS==ST_PRE_AQUECIMENTO || _f_STATUS==ST_SEL_PRE_AQUECIMENTO)){_f_USB_Connet=0;}
		else if(_f_USB_Connet){//PROCESSO NECESSARIO DO USB

			if(!(f_mount(fs_usb, "1:", 1))){

				switch(_f_STATUS){

					case ST_USB_EXPORTAR_TENSAO:{
						_f_USB_Process=1;
						_f_tick_error = 0;
						_f_tick_error = Export_Params_Tensao();
						if(_f_tick_error){
							STATUS = ST_USB_EXPORTAR_TENSAO_FALHA;
						}
						else{
							vTaskDelay(1000);
							STATUS= ST_CONFIGURACOES;
						}
					}break;
					case ST_USB_IMPORTAR_TENSAO:{
						_f_USB_Process=1;
						_f_tick_error = 0;
						_f_tick_error = Import_Params_Tensao();
						if(_f_tick_error){
							STATUS = ST_USB_IMPORTAR_TENSAO_FALHA;
						}
						else{
							read_pwm_csv();
							vTaskDelay(1000);
							STATUS= ST_CONFIGURACOES;
						}
					}break;

					case ST_CONF_EMBTECH:{
						_f_USB_Process=1;
						Teste_USB();

					}break;

					case ST_PRE_AQUECIMENTO:
					case ST_SEL_PRE_AQUECIMENTO:
					case ST_MENU_PRINCIPAL:{
							_f_USB_Process=1;
							_f_USB_file_calib = 1;
							if(!Calib_Touch_USB()){
								_c_TimeOFFPower = 5;
								while(_c_TimeOFFPower){vTaskDelay(10);}
								NVIC_SystemReset();
							}
					}break;

					case ST_USB_UPDATE_FIRMWARE:{
						_f_USB_Process=1;
						if(Update_Firmware()){
							STATUS = ST_USB_UPDATE_FIRMWARE_ERRO;
						}
					}break;

					case ST_USB_IMPORTAR_TUDO:{
						_f_USB_Process=1;
						Delete_Receitas_SD();
						if(Import_Receitas()){
							STATUS= ST_USB_IMPORTAR_FALHA;
						}
						else{
							if(Import_Config_User()){
								STATUS= ST_USB_IMPORTAR_FALHA;
							}
							else{
								STATUS= ST_CONFIGURACOES;
							}
						}
					}break;

					case ST_USB_EXPORTAR_TUDO:{
						_f_USB_Process=1;
						Delete_Receitas_USB();
						if(Export_Receitas()){
							STATUS= ST_USB_EXPORTAR_FALHA;
						}
						else{
							if(Export_Config()){
								STATUS= ST_USB_EXPORTAR_FALHA;
							}
							else{
								STATUS= ST_CONFIGURACOES;
							}
						}
					}break;

					case ST_USB_EXPORTAR_RECEITAS:
					case ST_USB_EXPORTAR_GRUPOS:{
						_f_USB_Process=1;
						Delete_Receitas_USB();
						if(Export_Receitas()){
							STATUS= ST_USB_EXPORTAR_FALHA;
						}
						else{
							STATUS= ST_CONFIGURACOES;
						}
					}break;

					case ST_USB_IMPORTAR_RECEITAS:
					case ST_USB_IMPORTAR_GRUPOS:{
						_f_USB_Process=1;
						Delete_Receitas_SD();
						if(Import_Receitas()){
							STATUS= ST_USB_IMPORTAR_FALHA;
						}
						else{
							STATUS= ST_CONFIGURACOES;
						}
					}break;
					case ST_USB_IMPORTAR_CONF_USER:{
						_f_USB_Process=1;
						if(Import_Config_User()){
							STATUS= ST_USB_IMPORTAR_FALHA;
						}
						else{
							STATUS= ST_CONFIGURACOES;
						}
					}break;
					case ST_USB_EXPORTAR_CONF_USER:{
						_f_USB_Process=1;
						if(Export_Config()){
							STATUS= ST_USB_EXPORTAR_FALHA;
						}
						else{
							STATUS= ST_CONFIGURACOES;
						}
					}break;
				}

				f_mount(0, "1:", 1);

				_f_USB_Connet = 0;
				_f_USB_Process = 0;
			}
		}
		else if(_f_USB_Process){
			STATUS = ST_USB_REMOVIDO;
			_f_USB_Process = 0;
		}

		switch(_f_STATUS){
			case ST_CONF_RESTAURAR_FABRICA_PROCESS:{
				_f_USB_Process=1;
				_load_padrao_fabrica();
				_f_USB_Process=0;
			}break;
		}

		_c_timeOutStatus++;
		cont_USB++;
		vTaskDelay(10);
	}
}

volatile uint8_t init_phy = 0;
static void task_IOT(void *pvParameters) {

//	ip_addr_t ipaddr, netmask, gw, dns;
	volatile s32_t tcpipdone = 0;
	uint32_t physts;
	static int prt_ip = 0;

	int _f_NET_STATUS = 0;

	memset(&IOT_MANAGER.upload, 0x00,sizeof(IOT_MANAGER.upload));
	memset(&IOT_MANAGER.download, 0x00,sizeof(IOT_MANAGER.download));
	memset(&IOT_MANAGER.flags, 0x00,sizeof(IOT_MANAGER.flags));
	memset(&IOT_MANAGER.load, 0x00,sizeof(IOT_MANAGER.load));
	IOT_MANAGER._c_wait_send = 0;
	IOT_MANAGER.IOT_FRAME = IOT_IDDLE;
	IOT_MANAGER._c_time_out_response = 0;
	_f_NET_STATUS = 0;
	_c_ETH_TIMER = 0;



	IOT_MANAGER.task.run = 1;
	IOT_MANAGER.task.running = 1;


//	_f_task_exec |= TASK_ETH;

//	enetInit();

//	enetPhyReset();
	DEBUGOUT("device-token: %s\n",IOT_Config.server.TOKEN);
	DEBUGOUT("pincode: %s\n",IOT_Config.server.PINCODE);
	DEBUGOUT("numero serial: %s\n",Config.NUM_SERIE);

	/* Wait until the TCP/IP thread is finished before
	   continuing or wierd things may happen */
	DEBUGSTR("Waiting for TCPIP thread to initialize...\r\n");
	if(init_phy == 0){
		tcpip_init(tcpip_init_done_signal, (void *) &tcpipdone);
		while (!tcpipdone) {
			msDelay(1);
		}
		init_phy = 1;
	}

	Config._f_DHCP = 1;
	/* Static IP assignment */
//#if LWIP_DHCP
	if(Config._f_DHCP == 1){
		IP4_ADDR(&gw, 0, 0, 0, 0);
		IP4_ADDR(&ipaddr, 0, 0, 0, 0);
		IP4_ADDR(&netmask, 0, 0, 0, 0);
	}
//#else
	else {
		IP4_ADDR(&gw, 192, 168, 222, 1);
		IP4_ADDR(&ipaddr, 192, 168, 222, 201);
		IP4_ADDR(&netmask, 255, 255, 255, 0);
//		IP4_ADDR(&dns, DNS[0], DNS[1], DNS[2], DNS[3]);
//		dns_setserver(0, &dns);
	}
//#endif

	/* Add netif interface for lpc17xx_8x */
	memset(&lpc_netif, 0, sizeof(lpc_netif));
	if (!netif_add(&lpc_netif, &ipaddr, &netmask, &gw, NULL, lpc_enetif_init,
				   tcpip_input)) {
		DEBUGSTR("Net interface failed to initialize\r\n");
		while(1);
	}
	netif_set_default(&lpc_netif);
	netif_set_up(&lpc_netif);

	/* Enable MAC interrupts only after LWIP is ready */
	NVIC_SetPriority(ETHERNET_IRQn, config_ETHERNET_INTERRUPT_PRIORITY);
	NVIC_EnableIRQ(ETHERNET_IRQn);

//#if LWIP_DHCP
	if(Config._f_DHCP == 1){
		dhcp_start(&lpc_netif);
	}
//#endif


	IOT_ERROR = SIOT_CABLE_DISCONECTED;
	IOT_MANAGER.flags.cable_disconnected = 1;

//	while(1){
//	while (IOT_MANAGER.task.run) {
	while(1){
//		if(IOT_CHANGE_IP_CONFIG == 1){
//			DEBUGOUT("Changing IP\n");
//			Config._f_DHCP = !Config._f_DHCP;
//			netif_set_down(&lpc_netif);
//			dhcp_stop(&lpc_netif);
//
//			if(Config._f_DHCP == 1){
//				IP4_ADDR(&gw, 0, 0, 0, 0);
//				IP4_ADDR(&ipaddr, 0, 0, 0, 0);
//				IP4_ADDR(&netmask, 0, 0, 0, 0);
//				dhcp_start(&lpc_netif);
//			}
//			else {
//				IP4_ADDR(&gw, 192, 168, 222, 1);
//				IP4_ADDR(&ipaddr, 192, 168, 222, 181);
//				IP4_ADDR(&netmask, 255, 255, 255, 0);
//				//IP4_ADDR(&dns, DNS[0], DNS[1], DNS[2], DNS[3]);
//				//dns_setserver(0, &dns);
//			}
//			IOT_TIMER_CHANGE_IP = 20;
//			IOT_CHANGE_IP_CONFIG = 0;
//			DEBUGOUT("Changed!\n");
//		}

		/* Call the PHY status update state machine once in a while
		   to keep the link status up-to-date */
		physts = lpcPHYStsPoll();

		/* Only check for connection state when the PHY status has changed */
		if (physts & PHY_LINK_CHANGED) {
			if (physts & PHY_LINK_CONNECTED) {
				//Board_LED_Set(0, true);
				prt_ip = 0;
//				dhcp_renew(&lpc_netif);

				/* Set interface speed and duplex */
				if (physts & PHY_LINK_SPEED100) {
					Chip_ENET_Set100Mbps(LPC_ETHERNET);
					NETIF_INIT_SNMP(&lpc_netif, snmp_ifType_ethernet_csmacd, 100000000);
				}
				else {
					Chip_ENET_Set10Mbps(LPC_ETHERNET);
					NETIF_INIT_SNMP(&lpc_netif, snmp_ifType_ethernet_csmacd, 10000000);
				}
				if (physts & PHY_LINK_FULLDUPLX) {
					Chip_ENET_SetFullDuplex(LPC_ETHERNET);
				}
				else {
					Chip_ENET_SetHalfDuplex(LPC_ETHERNET);
				}

				tcpip_callback_with_block((tcpip_callback_fn) netif_set_link_up,
										  (void *) &lpc_netif, 1);

				DEBUGOUT("Cabo Conectado\n");
				IOT_MANAGER.flags.cable_disconnected = 0;
				IOT_ERROR = SIOT_WAIT_IP;
			}
			else {
				//Board_LED_Set(0, false);
				tcpip_callback_with_block((tcpip_callback_fn) netif_set_link_down,
										  (void *) &lpc_netif, 1);
				DEBUGOUT("Cabo Desconectado\n");
				IOT_MANAGER.flags.cable_disconnected = 1;
				IOT_ERROR = SIOT_CABLE_DISCONECTED;
			}
			DEBUGOUT("Link connect status: %d\r\n", ((physts & PHY_LINK_CONNECTED) != 0));
			/* Delay for link detection (250mS) */
			vTaskDelay(configTICK_RATE_HZ / 4);
		}

		/* Print IP address info */
		if (!prt_ip) {
			if (lpc_netif.ip_addr.addr) {
				static char tmp_buff[16];
				DEBUGOUT("IP_ADDR    : %s\r\n", ipaddr_ntoa_r((const ip_addr_t *) &lpc_netif.ip_addr, tmp_buff, 16));
				DEBUGOUT("NET_MASK   : %s\r\n", ipaddr_ntoa_r((const ip_addr_t *) &lpc_netif.netmask, tmp_buff, 16));
				DEBUGOUT("GATEWAY_IP : %s\r\n", ipaddr_ntoa_r((const ip_addr_t *) &lpc_netif.gw, tmp_buff, 16));
				//ST_CONF_INTERNET
				snprintf(IP_used,sizeof(IP_used),"%s",ipaddr_ntoa_r((const ip_addr_t *) &lpc_netif.ip_addr, tmp_buff, 16));
				snprintf(GATWAY_used,sizeof(GATWAY_used),"%s",ipaddr_ntoa_r((const ip_addr_t *) &lpc_netif.gw, tmp_buff, 16));
				snprintf(MASK_used,sizeof(MASK_used),"%s",ipaddr_ntoa_r((const ip_addr_t *) &lpc_netif.netmask, tmp_buff, 16));
				prt_ip = 1;
				_f_NET_STATUS = 1;
				IOT_ERROR = SIOT_CONECTION_FAILED;
			}
		}

		if(IOT_TIME_ERROR > (5*60) && IOT_WAIT_CLOSE_ALL_CONNECTIONS==0){
			IOT_WAIT_CLOSE_ALL_CONNECTIONS = 5*60;
		}


		if(IOT_MANAGER.flags.cable_disconnected == 0 && IOT_WAIT_CLOSE_ALL_CONNECTIONS == 0){
			//DEBUGOUT("\r\n%d %d %d",_c_ETH_TIMER,IOT_MANAGER.flags.send_frame,IOT_MANAGER._c_wait_send);

			switch(_f_NET_STATUS){
			case 0: break;
			case 1:{
//				TAGO_TIME_OUT = 5;
//				TAGO_SD_W_SEND = 1; //log do que foi enviado
//				if (TAGO_SEND_BACKUP){TAGO_SENDED_BACKUP=1; TAGO_SEND_BACKUP=0;}
//				if (tcp_setup((const char*)server)==ERR_OK) _f_NET_STATUS = 2;

				make_frame_IOT();

				if(IOT_MANAGER.flags.send_frame != 0 && IOT_MANAGER._c_wait_send == 0 && IOT_MANAGER.IOT_FRAME != IOT_IDDLE && IOT_MANAGER._c_time_out_response == 0){
					if (tcp_setup((const char*)server_url,server_port)==ERR_OK){
						_f_NET_STATUS = 2;
						_c_ETH_TIMER=20;	//tempo minimo entre um pacote e outro
						_c_ETH_Tempo_resposta = 0;

						IOT_MANAGER.flags.send_frame = 0;
						IOT_MANAGER.IOT_FRAME_SENDED = IOT_MANAGER.IOT_FRAME;
						IOT_MANAGER._c_time_out_response = 30;
					}
					else{
						IOT_ERROR = SIOT_ERRO_DNS;
						vTaskDelay(configTICK_RATE_HZ);
					}
				}
				else{
					DEBUGOUT("\r\nwait unlock iot:%d st:%d s:%d t:%d\r\n",IOT_MANAGER.IOT_FRAME,IOT_MANAGER._c_time_get_status,IOT_MANAGER._c_wait_send,IOT_MANAGER._c_time_out_response);
					vTaskDelay(configTICK_RATE_HZ);
				}

			}break;
			default:{
				if(_c_ETH_TIMER==0 && IOT_MANAGER._c_time_out_response == 0){
					_f_NET_STATUS=1;
					DEBUGOUT("\r\nResend \r\n");
					if(IOT_MANAGER.flags.send_success == 0){
						switch(IOT_MANAGER.IOT_FRAME_SENDED){
						case IOT_UPLOAD_GRUPO:{
							IOT_MANAGER.upload._c_index_grupo -= IOT_MANAGER.upload._c_aux_grupo;
						}break;

						case IOT_UPLOAD_RECEITA:{
							if(IOT_MANAGER.flags.next_grupo == 1){
								IOT_MANAGER.upload._c_index_grupo --;
							}
							IOT_MANAGER.upload._c_index_receitas -= IOT_MANAGER.upload._c_aux_receita;
							IOT_MANAGER.upload._c_index_receita -= IOT_MANAGER.upload._c_aux_receita;
						}break;
						}
					}
					IOT_MANAGER.buf_load = 0;
				}
				vTaskDelay(configTICK_RATE_HZ);
			}
			}
		}
		else{//CABO DESCONECTADO SEMPRE SALVA BACKUP
//			if(_f_SEND_TAGO){TAGO_SD_W_BACKUP=1; _f_SEND_TAGO=0;}
			vTaskDelay(configTICK_RATE_HZ);
		}

		IOT_MANAGER._c_timer_task = 5;
	}
	DEBUGSTR("Closing IOT Task\r\n");
	//IOT_MANAGER.task.running = 0;
//	dhcp_reboot(&lpc_netif);
//	netif_set_link_down(&lpc_netif);
//	netif_set_down(&lpc_netif);
	dhcp_stop(&lpc_netif);
	netif_remove(&lpc_netif);
	vTaskDelete(xtask_IOT);
}

static void task_Ms( void *pvParameters ){
//	xTaskCreate(task_IOT, 		(signed char *) "t_IOT", configMINIMAL_STACK_SIZE*20, NULL, 1, NULL );
	while(1){
		timerCntms++;
		_c_ETH_Tempo_resposta++;

//		if(IOT_MANAGER.task.running == 0 && IOT_MANAGER.task.run == 0 && IOT_MANAGER.task.start == 0){
//			xTaskCreate(task_IOT, 		(signed char *) "t_IOT", configMINIMAL_STACK_SIZE*20, NULL, 1, &xtask_IOT );
//			IOT_MANAGER.task.start = 1;
//			IOT_MANAGER._c_timer_task = 5;
//		}
//		else if (IOT_MANAGER._c_timer_task == 0){
//			IOT_MANAGER.task.start = 0;
//			if(IOT_MANAGER.task.run == 1){
//				vTaskDelete(xtask_IOT);
//			}
//			IOT_MANAGER.task.running = 0;
//			IOT_MANAGER.task.run = 0;
//			IOT_MANAGER._c_timer_task = 5;
//		}

		vTaskDelay(1);
	}
}

