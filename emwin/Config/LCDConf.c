/*********************************************************************
*                SEGGER Microcontroller GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2014  SEGGER Microcontroller GmbH & Co. KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

** emWin V5.24 - Graphical user interface for embedded applications **
All  Intellectual Property rights  in the Software belongs to  SEGGER.
emWin is protected by  international copyright laws.  Knowledge of the
source code may not be used to write a similar product.  This file may
only be used in accordance with the following terms:

The software has been licensed to  NXP Semiconductors USA, Inc.  whose
registered  office  is  situated  at 411 E. Plumeria Drive, San  Jose,
CA 95134, USA  solely for  the  purposes  of  creating  libraries  for
NXPs M0, M3/M4 and  ARM7/9 processor-based  devices,  sublicensed  and
distributed under the terms and conditions of the NXP End User License
Agreement.
Full source code is available at: www.segger.com

We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : LCDConf.c
Purpose     : Display controller configuration (single layer)
---------------------------END-OF-HEADER------------------------------
*/

#include "GUI.h"
#include "GUIDRV_Lin.h"

#include "chip.h"
#include "board_types.h"

extern PIN_COM_TypeDef LCD [21];


/*********************************************************************
*
*       Layer configuration (to be modified)
*
**********************************************************************
*/
//
// Physical display size
//
#define XSIZE_PHYS 800
#define YSIZE_PHYS 480

//
// Virtual display size
//
#define VXSIZE_PHYS (XSIZE_PHYS)
#define VYSIZE_PHYS (YSIZE_PHYS)

//
// Display framebuffer size
// These values can be reduced if using a smaller display and might have
// to be adjusted if using virtual screens. The values will be checked
// during emWin init.
//
#define FB_XSIZE  800
#define FB_YSIZE  480

//
// Pixel width in bytes
//
#define PIXEL_WIDTH  2

//
// Video RAM address
//
#define VRAM_ADDR_PHYS  (uint32_t)&_aVRAM[0]

//
// Color conversion
//
#define COLOR_CONVERSION GUICC_M565//GUICC_565

//
// Display driver
//
#define DISPLAY_DRIVER GUIDRV_LIN_16

////
//// Buffers / VScreens
////
//#define NUM_BUFFERS  1 // Number of multiple buffers to be used
//#define NUM_VSCREENS 1 // Number of virtual screens to be used

/*********************************************************************
*
*       Configuration checking
*
**********************************************************************
*/
#ifndef   VRAM_ADDR
  #define VRAM_ADDR 0 // TBD by customer: This has to be the frame buffer start address
#endif
#ifndef   XSIZE_PHYS
  #error Physical X size of display is not defined!
#endif
#ifndef   YSIZE_PHYS
  #error Physical Y size of display is not defined!
#endif
#ifndef   COLOR_CONVERSION
  #error Color conversion not defined!
#endif
#ifndef   DISPLAY_DRIVER
  #error No display driver defined!
#endif
#ifndef   NUM_VSCREENS
  #define NUM_VSCREENS 1
#else
  #if (NUM_VSCREENS <= 0)
    #error At least one screeen needs to be defined!
  #endif
#endif
#if (NUM_VSCREENS > 1) && (NUM_BUFFERS > 1)
  #error Virtual screens and multiple buffers are not allowed!
	//U32 static _aVRAM[FB_XSIZE * FB_YSIZE / (4 / PIXEL_WIDTH)]  __attribute__ ((section(".VRAM"))) = { 0 };
#endif

#define MATRIX_ARB  (*(volatile U32*)(0x400FC188))

U32 static _aVRAM[FB_XSIZE * FB_YSIZE / (4 / PIXEL_WIDTH)]  __attribute__ ((section(".VRAM"))) = { 0 };

/*********************************************************************
*
*       _InitLCDC
*
* Function description
*   Inits the LCD controller, backlight and sets the frame buffer.
*/
static void _InitLCDC(void) {
	LCD_CONFIG_T LcdConfig;

	LcdConfig.HBP = 46;
	LcdConfig.HFP = 210;
	LcdConfig.HSW = 42;
	LcdConfig.PPL = 800;
	LcdConfig.VBP = 23;
	LcdConfig.VFP = 22;
	LcdConfig.VSW = 10;
	LcdConfig.LPP = 480;
	LcdConfig.IOE = 0;
	LcdConfig.IPC = 0;
	LcdConfig.IHS = 0;
	LcdConfig.IVS = 1;
	LcdConfig.ACB = 1;
	LcdConfig.BPP = 6;
	LcdConfig.LCD = LCD_TFT;
	LcdConfig.color_format = LCD_COLOR_FORMAT_BGR;
	LcdConfig.Dual = 0;

	Chip_LCD_Init(LPC_LCD, &LcdConfig);
	Chip_Clock_SetLCDClockDiv(1);
	Chip_LCD_SetUPFrameBuffer(LPC_LCD, (uint32_t*)VRAM_ADDR_PHYS);
	Chip_LCD_PowerOn(LPC_LCD);

	Chip_IOCON_PinMuxSet(LPC_IOCON, LCD[16].PORT, LCD[16].PIN, IOCON_FUNC0 | IOCON_MODE_INACT);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD[16].PORT, LCD[16].PIN);
	Chip_GPIO_SetPinState(LPC_GPIO, LCD[16].PORT, LCD[16].PIN, 0);
}

static void _InitLCDPorts(void) {
	int i=0;

	for(i=0; i<21; i++){
		Chip_IOCON_PinMuxSet(LPC_IOCON, LCD[i].PORT, LCD[i].PIN, LCD[i].FUNC);
	}

	//SET BACKLIGHT TO OFF
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD[19].PORT, LCD[19].PIN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD[20].PORT, LCD[20].PIN);

	Chip_GPIO_SetPinState(LPC_GPIO, LCD[19].PORT, LCD[19].PIN, 0);
	Chip_GPIO_SetPinState(LPC_GPIO, LCD[20].PORT, LCD[20].PIN, 0);
}

static void _InitController(unsigned LayerIndex) {
	SystemCoreClockUpdate();
	_InitLCDPorts();

	MATRIX_ARB = 0            // Set AHB Matrix priorities [0..3] with 3 being highest priority
			| (1 <<  0)  // PRI_ICODE : I-Code bus priority. Should be lower than PRI_DCODE for proper operation.
			| (2 <<  2)  // PRI_DCODE : D-Code bus priority.
			| (0 <<  4)  // PRI_SYS   : System bus priority.
			| (0 <<  6)  // PRI_GPDMA : General Purpose DMA controller priority.
			| (0 <<  8)  // PRI_ETH   : Ethernet: DMA priority.
			| (3 << 10)  // PRI_LCD   : LCD DMA priority.
			| (0 << 12)  // PRI_USB   : USB DMA priority.
			;

	_InitLCDC();
	//
	// Set display size and video-RAM address
	//
	LCD_SetSizeEx (XSIZE_PHYS, YSIZE_PHYS, LayerIndex);
	LCD_SetVSizeEx(VXSIZE_PHYS, VYSIZE_PHYS, LayerIndex);
	LCD_SetVRAMAddrEx(LayerIndex, (void*)VRAM_ADDR_PHYS);
}

/*********************************************************************
*
*       _SetDisplayOrigin()
*/
static void _SetDisplayOrigin(int x, int y) {
  (void)x;
  //
  // Set start address for display data and enable LCD controller
  //
//  Chip_LCD_SetUPFrameBuffer(LPC_LCD, VRAM_ADDR_PHYS + (y * YSIZE_PHYS * PIXEL_WIDTH));
  LPC_LCD->UPBASE = VRAM_ADDR_PHYS + (y * YSIZE_PHYS * PIXEL_WIDTH);  // Needs to be set, before LCDC is enabled
}



/*********************************************************************
*
*       LCD_X_Config
*
* Purpose:
*   Called during the initialization process in order to set up the
*   display driver configuration.
*/
void LCD_X_Config(void) {

  GUI_DEVICE_CreateAndLink(DISPLAY_DRIVER, COLOR_CONVERSION, 0, 0);
  //
  // Display driver configuration, required for Lin-driver
  //
  LCD_SetPosEx(0, 0, 0);
  if (LCD_GetSwapXYEx(0)) {
    LCD_SetSizeEx  (0, YSIZE_PHYS , XSIZE_PHYS);
    LCD_SetVSizeEx (0, VYSIZE_PHYS, VXSIZE_PHYS);
  } else {
    LCD_SetSizeEx  (0, XSIZE_PHYS , YSIZE_PHYS);
    LCD_SetVSizeEx (0, VXSIZE_PHYS, VYSIZE_PHYS);
  }
  LCD_SetVRAMAddrEx(0, (void*)VRAM_ADDR);
  //
  // Set user palette data (only required if no fixed palette is used)
  //
  #if defined(PALETTE)
    LCD_SetLUTEx(0, PALETTE);
  #endif
}

/*********************************************************************
*
*       LCD_X_DisplayDriver
*
* Purpose:
*   This function is called by the display driver for several purposes.
*   To support the according task the routine needs to be adapted to
*   the display controller. Please note that the commands marked with
*   'optional' are not cogently required and should only be adapted if
*   the display controller supports these features.
*
* Parameter:
*   LayerIndex - Index of layer to be configured
*   Cmd        - Please refer to the details in the switch statement below
*   pData      - Pointer to a LCD_X_DATA structure
*
* Return Value:
*   < -1 - Error
*     -1 - Command not handled
*      0 - Ok
*/
int LCD_X_DisplayDriver(unsigned LayerIndex, unsigned Cmd, void * pData) {
  #ifndef _WINDOWS
  LCD_X_SETORG_INFO * p;
  #endif
  int r;

  (void) LayerIndex;

  switch (Cmd) {
  //
  // Required
  //
  case LCD_X_INITCONTROLLER:
    //
    // Called during the initialization process in order to set up the
    // display controller and put it into operation. If the display
    // controller is not initialized by any external routine this needs
    // to be adapted by the customer...
    //
    // ...
    #ifndef _WINDOWS
      _InitController(0);
    #endif
    return 0;
  case LCD_X_SETORG:
    //
    // Required for setting the display origin which is passed in the 'xPos' and 'yPos' element of p
    //
    #ifndef _WINDOWS
      p = (LCD_X_SETORG_INFO *)pData;
      _SetDisplayOrigin(p->xPos, p->yPos);
    #endif
    return 0;
  default:
    r = -1;
  }
  return r;
}


/*************************** End of file ****************************/
