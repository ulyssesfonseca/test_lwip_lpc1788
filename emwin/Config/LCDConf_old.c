/*********************************************************************
*                SEGGER Microcontroller GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2014  SEGGER Microcontroller GmbH & Co. KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

** emWin V5.24 - Graphical user interface for embedded applications **
All  Intellectual Property rights  in the Software belongs to  SEGGER.
emWin is protected by  international copyright laws.  Knowledge of the
source code may not be used to write a similar product.  This file may
only be used in accordance with the following terms:

The software has been licensed to  NXP Semiconductors USA, Inc.  whose
registered  office  is  situated  at 411 E. Plumeria Drive, San  Jose,
CA 95134, USA  solely for  the  purposes  of  creating  libraries  for
NXPs M0, M3/M4 and  ARM7/9 processor-based  devices,  sublicensed  and
distributed under the terms and conditions of the NXP End User License
Agreement.
Full source code is available at: www.segger.com

We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : LCDConf.c
Purpose     : Display controller configuration (single layer)
---------------------------END-OF-HEADER------------------------------
*/

#include "GUI.h"
#include "GUIDRV_Lin.h"

#include "chip.h"
#include "board_types.h"


extern PIN_COM_TypeDef LCD [21];


/*********************************************************************
*
*       Layer configuration (to be modified)
*
**********************************************************************
*/
//
// Physical display size
//
#define XSIZE_PHYS 800
#define YSIZE_PHYS 480

//
// Virtual display size
//
#define VXSIZE_PHYS (XSIZE_PHYS)
#define VYSIZE_PHYS (YSIZE_PHYS)

//
// Display framebuffer size
// These values can be reduced if using a smaller display and might have
// to be adjusted if using virtual screens. The values will be checked
// during emWin init.
//
#define FB_XSIZE  800
#define FB_YSIZE  480

//
// Pixel width in bytes
//
#define PIXEL_WIDTH  2

//
// Video RAM address
//
#define VRAM_ADDR_PHYS  (uint32_t)&_aVRAM[0]

//
// Color conversion
//
#define COLOR_CONVERSION GUICC_M565

//
// Display driver
//
#define DISPLAY_DRIVER GUIDRV_LIN_16

//
// Buffers / VScreens
//
//#define NUM_BUFFERS  1 // Number of multiple buffers to be used
//#define NUM_VSCREENS 1 // Number of virtual screens to be used

/*********************************************************************
*
*       Configuration checking
*
**********************************************************************
*/
#ifndef   VRAM_ADDR
  #define VRAM_ADDR 0 // TBD by customer: This has to be the frame buffer start address
#endif
#ifndef   XSIZE_PHYS
  #error Physical X size of display is not defined!
#endif
#ifndef   YSIZE_PHYS
  #error Physical Y size of display is not defined!
#endif
#ifndef   COLOR_CONVERSION
  #error Color conversion not defined!
#endif
#ifndef   DISPLAY_DRIVER
  #error No display driver defined!
#endif
#ifndef   NUM_VSCREENS
  #define NUM_VSCREENS 1
#else
  #if (NUM_VSCREENS <= 0)
    #error At least one screeen needs to be defined!
  #endif
#endif
#if (NUM_VSCREENS > 1) && (NUM_BUFFERS > 1)
  #error Virtual screens and multiple buffers are not allowed!
	//U32 static _aVRAM[FB_XSIZE * FB_YSIZE / (4 / PIXEL_WIDTH)]  __attribute__ ((section(".VRAM"))) = { 0 };
#endif

#define MATRIX_ARB  (*(volatile U32*)(0x400FC188))

U32 static _aVRAM[FB_XSIZE * FB_YSIZE / (4 / PIXEL_WIDTH)]  __attribute__ ((section(".VRAM"))) = { 0 };

static U32 _FindClockDivisor(U32 TargetClock) {
  U32 Divider;
  U32 r;

  Divider = 1;
  while (((SystemCoreClock / Divider) > TargetClock) && (Divider <= 0x3F)) {
    Divider++;
  }
  if (Divider <= 1) {
    r = (1 << 26);  // Skip divider logic if clock divider is 1
  } else {
    //
    // Use found divider
    //
    Divider -= 2;
    r = 0
        | (((Divider >> 0) & 0x1F)
        | (((Divider >> 5) & 0x1F) << 27))
        ;
  }
  return r;
}

/*********************************************************************
*
*       _InitLCDC
*
* Function description
*   Inits the LCD controller, backlight and sets the frame buffer.
*/
static void _InitLCDC(void) {
	uint32_t i;
	LCD_CONFIG_T LcdConfig;

	LcdConfig.HBP = 46;
	LcdConfig.HFP = 210;
	LcdConfig.HSW = 42;
	LcdConfig.PPL = 800;
	LcdConfig.VBP = 23;
	LcdConfig.VFP = 22;
	LcdConfig.VSW = 10;
	LcdConfig.LPP = 480;
	LcdConfig.IOE = 0;
	LcdConfig.IPC = 0;
	LcdConfig.IHS = 0;
	LcdConfig.IVS = 1;
	LcdConfig.ACB = 1;
	LcdConfig.BPP = 6;
	LcdConfig.LCD = LCD_TFT;
	LcdConfig.color_format = LCD_COLOR_FORMAT_BGR;
	LcdConfig.Dual = 0;

//	Chip_LCD_Init(LPC_LCD, &LcdConfig);
//	Chip_Clock_SetLCDClockDiv(1);
//	Chip_LCD_SetUPFrameBuffer(LPC_LCD, VRAM_ADDR_PHYS);
//	Chip_LCD_PowerOn(LPC_LCD);

	LPC_SYSCTL->PCONP |=  (1UL << 0);  // Power the LCDC
	LPC_LCD->CTRL &= ~(1UL << 0);  // Disable the LCDC
	LPC_LCD->TIMH  = 0             // Configure horizontal axis
			| ((((U32)LcdConfig.PPL / 16) - 1) <<  2)
			|  (((U32)LcdConfig.HSW - 1)       <<  8)
			|  (((U32)LcdConfig.HFP - 1)       << 16)
			|  (((U32)LcdConfig.HBP - 1)       << 24)
			;
	LPC_LCD->TIMV  = 0             // Configure vertical axis
			| (((U32)LcdConfig.LPP - 1) <<  0)
			| (((U32)LcdConfig.VSW - 1) << 10)
			| (((U32)LcdConfig.VFP)     << 16)
			| (((U32)LcdConfig.VBP)     << 24)
			;
	LPC_LCD->POL   = 0             // Configure clock and signal polarity
			| (_FindClockDivisor(33000000) <<  0)
			| (((U32)LcdConfig.ACB - 1)         <<  6)
			| (((U32)LcdConfig.IVS)             << 11)
			| (((U32)LcdConfig.IHS)             << 12)
			| (((U32)LcdConfig.IPC)             << 13)
			| (((U32)LcdConfig.PPL - 1)         << 16)
			;
	LPC_LCD->CTRL  = 0             // Configure operating mode and panel parameters
			| ((U32)LcdConfig.BPP << 1)
			| ((U32)1            << 8)
			| ((U32)LcdConfig.LCD << 5)
			;
	for (i = 0; i < GUI_COUNTOF(LPC_LCD->PAL); i++) {
		LPC_LCD->PAL[i] = 0;  // Clear the color palette with black
	}
	LPC_SYSCTL->LCD_CFG = 0x0;  // No panel clock prescaler
	//
	// Enable LCDC
	//
	LPC_LCD->UPBASE  = VRAM_ADDR_PHYS;
	LPC_LCD->CTRL   |= (1 <<  0);                 // Enable LCD signals
	LPC_LCD->CTRL   |= (1 << 11);                 // Enable LCD power

	LPC_IOCON->p[2][0] = 0;
	LPC_GPIO2->DIR |=  1UL << 0;
	LPC_GPIO2->CLR |=  1UL << 0;

}

static void _InitLCDPorts(void) {
	int i=0;

//	for(i=0; i<21; i++){
//		Chip_IOCON_PinMuxSet(LPC_IOCON, LCD[i].PORT, LCD[i].PIN, LCD[i].FUNC);
//	}
//
//	//SET BACKLIGHT TO OFF
//	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD[19].PORT, LCD[19].PIN);
//	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LCD[20].PORT, LCD[20].PIN);
//
//	Chip_GPIO_SetPinState(LPC_GPIO, LCD[19].PORT, LCD[19].PIN, 0);
//	Chip_GPIO_SetPinState(LPC_GPIO, LCD[20].PORT, LCD[20].PIN, 0);

	//R
	LPC_IOCON->p[2][12]  = 5;  // LCD_VD_3		R0
	LPC_IOCON->p[2][6]  = 7;  // LCD_VD_4		R1
	LPC_IOCON->p[2][7]  = 7;  // LCD_VD_5 	R2
	LPC_IOCON->p[2][8]  = 7;  // LCD_VD_6		R3
	LPC_IOCON->p[2][9]  = 7;  // LCD_VD_7		R4

	//G
	LPC_IOCON->p[1][20]  = 7;  // LCD_VD_10	G0
	LPC_IOCON->p[1][21]  = 7;  // LCD_VD_11	G1
	LPC_IOCON->p[1][22]  = 7;  // LCD_VD_12	G2
	LPC_IOCON->p[1][23]  = 7;  // LCD_VD_13	G3
	LPC_IOCON->p[1][24]  = 7;  // LCD_VD_14	G4
	LPC_IOCON->p[1][25]  = 7;  // LCD_VD_15	G5

	//B
	LPC_IOCON->p[2][13]  = 7;  // LCD_VD_19	B0
	LPC_IOCON->p[1][26]  = 7;  // LCD_VD_20	B1
	LPC_IOCON->p[1][27]  = 7;  // LCD_VD_21	B2
	LPC_IOCON->p[1][28]  = 7;  // LCD_VD_22	B3
	LPC_IOCON->p[1][29]  = 7;  // LCD_VD_23	B4


	LPC_IOCON->p[2][0]  = 7;  // LCD_PWR
	//LPC_IOCON->p[2][1 ]  = 7;  // LCD_LE - DISP
	LPC_IOCON->p[2][2]  = 7;  // LCD_DCLK
	//LPC_IOCON->p[2][3 ]  = 7;  // LCD_FP
	LPC_IOCON->p[2][4]  = 7;  // LCD_ENAB_M
	//LPC_IOCON->p[2][5 ]  = 7;  // LCD_LP


	LPC_IOCON->p[2][27]  = 0;
	LPC_GPIO2->DIR   |= (1 << 27);
	LPC_GPIO2->SET   |= (1 << 27);  // Initially set backlight to off


	LPC_IOCON->p[1][18]  = 0;
	LPC_GPIO1->DIR   |= (1 << 18);
	LPC_GPIO1->SET   |= (1 << 18);  // Initially set backlight to off

}

static void _InitController(unsigned LayerIndex) {
	SystemCoreClockUpdate();
	_InitLCDPorts();

	MATRIX_ARB = 0            // Set AHB Matrix priorities [0..3] with 3 being highest priority
			| (1 <<  0)  // PRI_ICODE : I-Code bus priority. Should be lower than PRI_DCODE for proper operation.
			| (2 <<  2)  // PRI_DCODE : D-Code bus priority.
			| (0 <<  4)  // PRI_SYS   : System bus priority.
			| (0 <<  6)  // PRI_GPDMA : General Purpose DMA controller priority.
			| (0 <<  8)  // PRI_ETH   : Ethernet: DMA priority.
			| (3 << 10)  // PRI_LCD   : LCD DMA priority.
			| (0 << 12)  // PRI_USB   : USB DMA priority.
			;

	_InitLCDC();
	//
	// Set display size and video-RAM address
	//
	LCD_SetSizeEx (XSIZE_PHYS, YSIZE_PHYS, LayerIndex);
	LCD_SetVSizeEx(VXSIZE_PHYS, VYSIZE_PHYS, LayerIndex);
	LCD_SetVRAMAddrEx(LayerIndex, (void*)VRAM_ADDR_PHYS);
}

/*********************************************************************
*
*       _SetDisplayOrigin()
*/
static void _SetDisplayOrigin(int x, int y) {
  (void)x;
  //
  // Set start address for display data and enable LCD controller
  //
//  Chip_LCD_SetUPFrameBuffer(LPC_LCD, VRAM_ADDR_PHYS + (y * YSIZE_PHYS * PIXEL_WIDTH));
  LPC_LCD->UPBASE = VRAM_ADDR_PHYS + (y * YSIZE_PHYS * PIXEL_WIDTH);  // Needs to be set, before LCDC is enabled
}



/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       LCD_X_Config
*
* Purpose:
*   Called during the initialization process in order to set up the
*   display driver configuration.
*   
*/
void LCD_X_Config(void) {
  //
  // At first initialize use of multiple buffers on demand
  //
  #if (NUM_BUFFERS > 1)
    GUI_MULTIBUF_Config(NUM_BUFFERS);
  #endif
  //
  // Set display driver and color conversion for 1st layer
  //
  GUI_DEVICE_CreateAndLink(DISPLAY_DRIVER, COLOR_CONVERSION, 0, 0);
  //
  // Display driver configuration, required for Lin-driver
  //
  LCD_SetPosEx(0, 0, 0);//NAO TINHA
  if (LCD_GetSwapXYEx(0)) {	//if (LCD_GetSwapXY()) {
	  LCD_SetSizeEx  (0, YSIZE_PHYS , XSIZE_PHYS);
	  LCD_SetVSizeEx (0, VYSIZE_PHYS, VXSIZE_PHYS);
  } else {
	  LCD_SetSizeEx  (0, XSIZE_PHYS , YSIZE_PHYS);
	  LCD_SetVSizeEx (0, VXSIZE_PHYS, VYSIZE_PHYS);
  }
  LCD_SetVRAMAddrEx(0, (void *)VRAM_ADDR);
  //
  // Set user palette data (only required if no fixed palette is used)
  //
  #if defined(PALETTE)
    LCD_SetLUTEx(0, PALETTE);
  #endif
}

/*********************************************************************
*
*       LCD_X_DisplayDriver
*
* Purpose:
*   This function is called by the display driver for several purposes.
*   To support the according task the routine needs to be adapted to
*   the display controller. Please note that the commands marked with
*   'optional' are not cogently required and should only be adapted if 
*   the display controller supports these features.
*
* Parameter:
*   LayerIndex - Index of layer to be configured
*   Cmd        - Please refer to the details in the switch statement below
*   pData      - Pointer to a LCD_X_DATA structure
*
* Return Value:
*   < -1 - Error
*     -1 - Command not handled
*      0 - Ok
*/
int LCD_X_DisplayDriver(unsigned LayerIndex, unsigned Cmd, void * pData) {
	int r;
	LCD_X_SETORG_INFO * p;

	(void) LayerIndex;

	switch (Cmd) {
		case LCD_X_INITCONTROLLER: {
			//
			// Called during the initialization process in order to set up the
			// display controller and put it into operation. If the display
			// controller is not initialized by any external routine this needs
			// to be adapted by the customer...
			//
			// ...
			_InitController(0);
			return 0;
		}
//		case LCD_X_SETVRAMADDR: {
//			//
//			// Required for setting the address of the video RAM for drivers
//			// with memory mapped video RAM which is passed in the 'pVRAM' element of p
//			//
//			LCD_X_SETVRAMADDR_INFO * p;
//			p = (LCD_X_SETVRAMADDR_INFO *)pData;
//			//...
//			return 0;
//		}
		case LCD_X_SETORG: {
			//
			// Required for setting the display origin which is passed in the 'xPos' and 'yPos' element of p
			//
			p = (LCD_X_SETORG_INFO *)pData;
			_SetDisplayOrigin(p->xPos, p->yPos);
			return 0;
		}
//		case LCD_X_SHOWBUFFER: {
//			//
//			// Required if multiple buffers are used. The 'Index' element of p contains the buffer index.
//			//
//			LCD_X_SHOWBUFFER_INFO * p;
//			p = (LCD_X_SHOWBUFFER_INFO *)pData;
//			//...
//			return 0;
//		}
//		case LCD_X_SETLUTENTRY: {
//			//
//			// Required for setting a lookup table entry which is passed in the 'Pos' and 'Color' element of p
//			//
//			LCD_X_SETLUTENTRY_INFO * p;
//			p = (LCD_X_SETLUTENTRY_INFO *)pData;
//			//...
//			return 0;
//		}
//		case LCD_X_ON: {
//			//
//			// Required if the display controller should support switching on and off
//			//
//			return 0;
//		}
//		case LCD_X_OFF: {
//			//
//			// Required if the display controller should support switching on and off
//			//
//			// ...
//			return 0;
//		}
		default:
			r = -1;
	}
	return r;
}

/*************************** End of file ****************************/
