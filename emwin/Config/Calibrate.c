/*-----------------------------------------------------------------------------
 Embtech Tecnologia Embarcada S/A
 ------------------------------------------------------------------------------
 Nome:  					Calibrate.c
 Descri��o: 			Fun��es para calibra��o e teste do display 7POL TFT Resistivo
									utilizando o m�dulo EA LPC1788
 Autor: 					Marcos Vinicius Gon�alves
 Vers�o Inicial: 	1.0.0
 Data: 						24/09/2015
 
 ATEN��O: Ajustar a p�gina (EEP_PAGE) e offset (EEP_OFFSET) para grava��o dos 
 dados de calibra��o na EEPROM interna do LPC1788 em um endere�o que n�o esteja 
 sendo utilizado pela aplica��o final.
 *----------------------------------------------------------------------------*/

#include "calibrate.h"
//#include "lpc177x_8x_eeprom.h"
#include "stdio.h"

#define LEFT          0
#define RIGHT         1
#define CENTER        2

#define NORMAL        0
#define REV           1

#define EEP_PAGE   		1
#define EEP_OFFSET 		0

//Pontos de Refer�ncia para calibra��o:

//Valores do AD padr�o, amostrados do display. Estes valores s�o substitu�dos em caso de nova calibra��o.
int X_AD[5]={2024,2599,2575,1444,1420};
int Y_AD[5]={2143,1641,2706,2615,1644}; 


//5 pontos fixos de calibra��o. Alterar estes vetores em caso de deslocamento dos pontos de calibra��o.
int X[5]={240,120,360,360,120};
int Y[5]={400,200,200,600,600};
int X_ch;
int Y_ch;

// Touch screen results
int _TouchX;
int _TouchY;
int _Touch;
int _TouchZ;	
int _TouchZ1;
int _TouchZ2;

int DifX,DifY;

const char TXT_CALIBRACAO[3][90]={"ROTINA DE CALIBRA��O DO DISPLAY.\n POR FAVOR, CLIQUE NA TELA PARA INICIAR.","DISPLAY CALIBRATION ROUTINE.\n PLEASE TOUCH THE SCREEN TO START.","RUTINA DE CALIBRACI�N DE LA PANTALLA.\n POR FAVOR TOQUE EN LA PANTALLA PARA EMPEZAR."};
const char TXT_CLIQUE[3][36]={"CLIQUE NO CENTRO\n DO C�RCULO.","CLICK ON THE CIRCLE\n CENTER.","TOQUE EN EL CENTRO\n DEL C�RCULO."};
const char TXT_CALIBRACAO_EFETUADA[3][84]={"CALIBRA��O CONCLU�DA! CLIQUE NA TELA PARA\n INICIAR A VERIFICA��O.","CALIBRATION COMPLETED! CLICK ON THE SCREEN\n TO START CHECKING ROUTINE.","CALIBRACI�N COMPLETADO! TOQUE EN LA PANTALLA\n PARA INICIAR LA VERIFICACI�N."};
const char TXT_VERIFICACAO_ERRO[3][81]={"ERRO NA VERIFICA��O!\n POR FAVOR, REALIZE A CALIBRA��O NOVAMENTE.","ERROR CHECKING!\n PLEASE REPEAT CALIBRATION ROUTINE.","FALLO EN LA VERIFICACI�N!\n POR FAVOR REALIZAR LA CALIBRACI�N NUEVAMENTE."};
const char TXT_VERIFICACAO_OK[3][112]={"VERIFICA��O EFETUADA COM SUCESSO!\n POR FAVOR, TOQUE NA TELA PARA TESTAR A CALIBRA��O.","CHECKING SUCCESSFUL!\n PLEASE TOUCH THE SCREEN TO TEST CALIBRATION.","VERIFICACI�N HECHO CON �XITO!\n POR FAVOR, TOQUE EN LA PANTALLA PARA EMPEZAR LA PRUEBA DE CALIBRACI�N."};
const char TXT_ERRO_DIFERENCA_TOUCH[3][60]={"DIVERG�NCIA ENCONTRADA NO TOQUE: %dpx EM X; %dpx EM Y.","DIVERGENCE FOUND IN TOUCH: %dpx IN X; %dpx IN Y.","DIVERGENCIA ENCONTRADO EN TOQUE: %dpx EN X; %dpx EN Y."};

typedef struct {
	unsigned char IsCalibrate;
	double AlfaX;
	double AlfaY;
  double BetaX;
	double BetaY;
	double DeltaX;
	double DeltaY;
} SCalib;

SCalib TS_Calib; 

/******************************************************************************
	_print_calibration
	
	Esta fun��o � respons�vel por criar e atualizar as telas usadas no processo
	de calibra��o do display.
	
	@param
		Step - Passo do processo de calibra��o
		Language - Idioma. 
							 0-> Portugu�s
							 1-> Ingl�s
							 2-> Espanhol
		pNewFont	- Ponteiro para a fonte que ser� utilizada nos textos impressos 
								durante a calibra��o e verifica��o.
******************************************************************************/
void _print_calibration(unsigned char Step, unsigned char Language, const GUI_FONT GUI_UNI_PTR * pNewFont) {				
	
	short int PosX=0, PosY=0;
	unsigned char Align;
	unsigned char Print_Circle;
	char text[100];
	
	GUI_MEMDEV_Handle hMem;

	hMem = GUI_MEMDEV_Create(0,0,480,800);
	GUI_MEMDEV_Select(hMem);	
	
	GUI_SetColor(GUI_BLUE); 
	
	TS_DrawButton(0,0,480,800," ",GUI_WHITE, GUI_WHITE, GUI_WHITE,pNewFont,CENTER,NORMAL);	
	
	Align=LEFT;
	Print_Circle=0;
	
	switch (Step){	
		case 0: 			
			TS_DrawRectText(0,380,480,40,(char *)TXT_CALIBRACAO[Language],0x9E4720,pNewFont,CENTER);
		break;
		case 1: 
			PosX=X[0]; PosY=Y[0]; Print_Circle=1;
		break;
		case 2: 
			PosX=X[1]; PosY=Y[1]; Print_Circle=1;
		break;
		case 3: 
			PosX=X[2]; PosY=Y[2]; Align=RIGHT; Print_Circle=1;
		break;
		case 4: 
			PosX=X[3]; PosY=Y[3]; Align=RIGHT; Print_Circle=1;
		break;
		case 5: 
			PosX=X[4]; PosY=Y[4]; Print_Circle=1;
		break;
		case 6:		
			TS_Calc_Calibration();			
			TS_DrawRectText(0,380,480,40,(char *)TXT_CALIBRACAO_EFETUADA[Language],0x9E4720,pNewFont,CENTER);
		break;
		case 7:
			PosX=X_ch=X[1]-2*Step;//10
			PosY=Y_ch=Y[1]-2*Step;//10
			Print_Circle=1;
		break;
		case 8:			
			PosX=X_ch=X[2]-5*Step;//10
			PosY=Y_ch=Y[2]-5*Step;//10
			Align=RIGHT; Print_Circle=1;
		break;		
		case 9:			
			PosX=X_ch=X[3]+5*Step;
			PosY=Y_ch=Y[3]+5*Step;
			Align=RIGHT; Print_Circle=1;
		break;	
		case 10:			
			PosX=X_ch=X[4]+7*Step;
			PosY=Y_ch=Y[4]+7*Step;
			Print_Circle=1;
		break;	
		case 11:					
			TS_Calib.IsCalibrate=1;
			//EEPROM_Write(EEP_OFFSET,EEP_PAGE,(char *)&TS_Calib,MODE_8_BIT,sizeof(SCalib));
			TS_DrawRectText(0,380,480,40,(char *)TXT_VERIFICACAO_OK[Language],0x9E4720,pNewFont,CENTER);
		break;		
		default: 
			TS_DrawRectText(0,380,480,40,(char *)TXT_VERIFICACAO_ERRO[Language],GUI_RED,pNewFont,CENTER);
			sprintf(text,(char *)TXT_ERRO_DIFERENCA_TOUCH[Language],DifX, DifY);
			TS_DrawRectText(0,460,480,40,text,0x9E4720,pNewFont,CENTER);
		break;
	}
	
	if (Print_Circle){
		GUI_SetColor(0x9E4720); GUI_FillCircle(PosX, PosY,10); GUI_SetColor(GUI_WHITE); GUI_FillCircle(PosX, PosY,2);
		if (Align==RIGHT){PosX-=230;} else {PosX+=30;}
		PosY-=20;
		TS_DrawRectText(PosX,PosY,200,40,(char *)TXT_CLIQUE[Language],0x9E4720,pNewFont,Align);		
	} 
		
	GUI_MEMDEV_Select(0);
	GUI_MEMDEV_CopyToLCD(hMem);
	GUI_MEMDEV_Delete(hMem);		

//	sprintf(text,"IsCalibrate:%d // Step:%d", 
//							TS_Calib.IsCalibrate, Step);
//	TS_DrawButton(0,0,800,20,text,GUI_WHITE,0x9E4720,GUI_WHITE,pNewFont,LEFT,NORMAL);	
}

/******************************************************************************
	TS_Init_Touch
	
	Esta fun��o � respons�vel por carregar as vari�veis da mem�ria EEPROM interna
	do LPC1788 e verificar se a rotina de calibra��o j� foi executada neste 
	m�dulo.
******************************************************************************/
unsigned char TS_Init_Touch(void){
	
	//EEPROM_Read(EEP_OFFSET,EEP_PAGE,(char *)&TS_Calib,MODE_8_BIT,sizeof(SCalib));
	
	if (TS_Calib.IsCalibrate!=1){
		TS_Calc_Calibration();
	}	
	
	return TS_Calib.IsCalibrate;
}

/******************************************************************************
	TS_Calc_Calibration
	
	Esta fun��o � respons�vel por calcular os coeficientes de calibra��o que 
	ser�o utilizados no c�lculo das coordenadas do ponto de toque.
******************************************************************************/
void TS_Calc_Calibration(void){
		double a, b, c, d, e, Delta, DeltaX1, DeltaX2, DeltaX3, DeltaY1, DeltaY2, DeltaY3;	
		double X1, X2, X3, Y1, Y2, Y3;
	
		a=pow(X_AD[0],2) + pow(X_AD[1],2) + pow(X_AD[2],2) + pow(X_AD[3],2) + pow(X_AD[4],2);
		b=pow(Y_AD[0],2) + pow(Y_AD[1],2) + pow(Y_AD[2],2) + pow(Y_AD[3],2) + pow(Y_AD[4],2);
		c=X_AD[0]*Y_AD[0] + X_AD[1]*Y_AD[1] + X_AD[2]*Y_AD[2] + X_AD[3]*Y_AD[3] + X_AD[4]*Y_AD[4];
		d=X_AD[0]+X_AD[1]+X_AD[2]+X_AD[3]+X_AD[4];
		e=Y_AD[0]+Y_AD[1]+Y_AD[2]+Y_AD[3]+Y_AD[4];
		
		X1=X_AD[0]*X[0]+X_AD[1]*X[1]+X_AD[2]*X[2]+X_AD[3]*X[3]+X_AD[4]*X[4];
		X2=Y_AD[0]*X[0]+Y_AD[1]*X[1]+Y_AD[2]*X[2]+Y_AD[3]*X[3]+Y_AD[4]*X[4];
		X3=X[0]+X[1]+X[2]+X[3]+X[4];

		Y1=X_AD[0]*Y[0]+X_AD[1]*Y[1]+X_AD[2]*Y[2]+X_AD[3]*Y[3]+X_AD[4]*Y[4];
		Y2=Y_AD[0]*Y[0]+Y_AD[1]*Y[1]+Y_AD[2]*Y[2]+Y_AD[3]*Y[3]+Y_AD[4]*Y[4];
		Y3=Y[0]+Y[1]+Y[2]+Y[3]+Y[4];						
		
		Delta = 5*(a*b-pow(c,2))+2*c*d*e-a*pow(e,2)-b*pow(d,2);
		DeltaX1 = 5*(X1*b-X2*c)+e*(X2*d-X1*e)+X3*(c*e-b*d);
		DeltaX2 = 5*(X2*a-X1*c)+d*(X1*e-X2*d)+X3*(c*d-a*e);
		DeltaX3 = X3*(a*b-pow(c,2))+X1*(c*e-b*d)+X2*(c*d-a*e);

		DeltaY1 = 5*(Y1*b-Y2*c)+e*(Y2*d-Y1*e)+Y3*(c*e-b*d);
		DeltaY2 = 5*(Y2*a-Y1*c)+d*(Y1*e-Y2*d)+Y3*(c*d-a*e);
		DeltaY3 = Y3*(a*b-pow(c,2))+Y1*(c*e-b*d)+Y2*(c*d-a*e);
		
		TS_Calib.AlfaX=DeltaX1/Delta;
		TS_Calib.BetaX=DeltaX2/Delta;
		TS_Calib.DeltaX=DeltaX3/Delta;
		
		TS_Calib.AlfaY=DeltaY1/Delta;
		TS_Calib.BetaY=DeltaY2/Delta;
		TS_Calib.DeltaY=DeltaY3/Delta;					
}

/******************************************************************************
	TS_Calc_New_Point_XY
	
	Esta fun��o � respons�vel por calcular os valores dos pontos X e Y de acordo 
	com os valores lidos no AD e os coeficientes determinados na rotina de 
	calibra��o do display.
	
	@param
		AD_X - Valor de X lido do AD
		AD_Y - Valor de Y lido do AD
		NewX - Ponteiro do valor do ponto X que ser� calculado pela fun��o
		NewY - Ponteiro do valor do ponto Y que ser� calculado pela fun��o.
******************************************************************************/
void TS_Calc_New_Point_XY(int AD_X, int AD_Y, int *NewX, int *NewY){
	
	double CalcX;
	double CalcY;
	
	CalcX = TS_Calib.AlfaX * AD_X + TS_Calib.BetaX * AD_Y + TS_Calib.DeltaX;
	CalcY = TS_Calib.AlfaY * AD_X + TS_Calib.BetaY * AD_Y + TS_Calib.DeltaY;
	
	*NewX = CalcX;
	*NewY = CalcY;
}

/******************************************************************************
	TS_DrawRectText
	
	Esta fun��o � respons�vel por escrever um texto utilizando um fundo 
	transparente de acordo com os par�metros fornecidos.
	
	@param
		x - Coordenada X do ret�ngulo onde ser� inserido o texto
		y - Coordenada Y do ret�ngulo onde ser� inserido o texto
		w - Largura do ret�ngulo
		h - Altura do ret�ngulo
		text - Texto que ser� exibido
		color_font - cor da fonte que ser� usada
		pNewFont - Ponteiro indicando a fonte que ser� utilizada no texto
		Align - Alinhamento do texto. RIGHT, LEFT e CENTER
******************************************************************************/
void TS_DrawRectText(int x, int y, int w, int h, char * text, U32 color_font, const GUI_FONT GUI_UNI_PTR * pNewFont,U8 Align) {
	
  GUI_RECT Area; 
	
	Area.x0 = x+5;
	Area.x1 = x + w;
	Area.y0 = y+5;
	Area.y1 = y + h;
	
	GUI_SetFont(pNewFont);	

	GUI_SetColor(color_font);
	GUI_SetTextMode(GUI_TM_TRANS);
	
	if (Align==CENTER){
		GUI_DispStringInRect(text,&Area,GUI_TA_VCENTER |GUI_TA_HCENTER);	
	} else if (Align==LEFT){
		GUI_DispStringInRect(text,&Area,GUI_TA_VCENTER |GUI_TA_LEFT);	
	} else if (Align==RIGHT){
		GUI_DispStringInRect(text,&Area,GUI_TA_VCENTER |GUI_TA_RIGHT);	
	}	
}

/******************************************************************************
TS_DrawButton	

	Esta fun��o � respons�vel por criar uma �rea ret�ngular na tela e escrever um 
	texto de acordo com os par�metros fornecidos.
	
	@param
		x - Coordenada X do ret�ngulo 
		y - Coordenada Y do ret�ngulo 
		w - Largura do ret�ngulo
		h - Altura do ret�ngulo
		text - Texto que ser� exibido
		color - Cor do fundo do ret�ngulo
		color_font - cor da fonte que ser� usada
		color_cont - cor da borda do ret�ngulo
		pNewFont - Ponteiro indicando a fonte que ser� utilizada no texto
		Align - Alinhamento do texto. RIGHT, LEFT e CENTER
		Mode - Modo do esquema de cores. NORMAL ou REV(Inverte cores da fonte e
					 do fundo do bot�o.
******************************************************************************/
void TS_DrawButton(int x, int y, int w, int h, char * text,U32 color, U32 color_font, U32 color_cont, const GUI_FONT GUI_UNI_PTR * pNewFont, U8 Align, U8 Mode) {
	
  GUI_RECT Area; 
	
	if (Mode==NORMAL) {GUI_SetColor(color);} else {GUI_SetColor(color_font);}

	GUI_FillRect(x,y,x+w,y+h);
	
	GUI_SetColor(color_cont);
	GUI_DrawRect(x,y,x+w,y+h);
	
	Area.x0 = x+5;
	Area.x1 = x + w;
	Area.y0 = y+5;
	Area.y1 = y + h;
	
	GUI_SetFont(pNewFont);
		

	if (Mode==NORMAL) {GUI_SetColor(color_font);} else {GUI_SetColor(color);}
	GUI_SetTextMode(GUI_TM_TRANS);
	
	if (Align==CENTER){
		GUI_DispStringInRect(text,&Area,GUI_TA_VCENTER |GUI_TA_HCENTER);	
	} else if (Align==LEFT){
		GUI_DispStringInRect(text,&Area,GUI_TA_VCENTER |GUI_TA_LEFT);	
	} else if (Align==RIGHT){
		GUI_DispStringInRect(text,&Area,GUI_TA_VCENTER |GUI_TA_RIGHT);	
	}
}

