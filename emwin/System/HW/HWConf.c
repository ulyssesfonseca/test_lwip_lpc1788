/*********************************************************************
 *                SEGGER MICROCONTROLLER GmbH & Co. KG                *
 *        Solutions for real time microcontroller applications        *
 **********************************************************************
 *                                                                    *
 *        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
 *                                                                    *
 *        Internet: www.segger.com    Support:  support@segger.com    *
 *                                                                    *
 **********************************************************************

----------------------------------------------------------------------
File    : HWConf.c for NXP LPC1788 CPU and Embedded Artists LPC1788
          eval board.
Purpose : Initializes and handles the hardware for emWin without using
          any RTOS.
          Feel free to modify this file acc. to your target system.
--------  END-OF-HEADER  ---------------------------------------------
 */

#include "chip.h"
#include "HWConf.h"
#include "board_types.h"

extern PIN_COM_TypeDef LED_MOD[2];

#define SDRAM_SIZE       (32 * 1024 * 1024)

/* Pin muxing configuration */
STATIC const PINMUX_GRP_T pinmuxing[] = {
		/* CAN RD1 and TD1 */
		{0x0, 0,  (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x0, 1,  (IOCON_FUNC1 | IOCON_MODE_INACT)},
		/* UART 0 debug port (via USB bridge) */
		{0x0, 2,  (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x0, 3,  (IOCON_FUNC1 | IOCON_MODE_INACT)},
		/* I2S */
		{0x0, 4,  (IOCON_FUNC1 | IOCON_MODE_INACT)},	/* I2S RX clock */
		{0x0, 5,  (IOCON_FUNC1 | IOCON_MODE_INACT)},	/* I2S RX WS */
		{0x0, 6,  (IOCON_FUNC1 | IOCON_MODE_INACT)},	/* I2S RX SDA */
		{0x0, 7,  (IOCON_FUNC1 | IOCON_MODE_INACT | IOCON_DIGMODE_EN)},	/* I2S TX clock */
		{0x0, 8,  (IOCON_FUNC1 | IOCON_MODE_INACT | IOCON_DIGMODE_EN)},	/* I2S TX WS */
		{0x0, 9,  (IOCON_FUNC1 | IOCON_MODE_INACT | IOCON_DIGMODE_EN)},	/* I2S TX SDA */

		{0x0, 13, (IOCON_FUNC1 | IOCON_MODE_INACT)},	/* USB LED */
		{0x0, 14, (IOCON_FUNC3 | IOCON_MODE_INACT)},	/* USB Softconnect */
		/* SSP 0 */
		{0x0, 15, (IOCON_FUNC1 | IOCON_MODE_INACT)},	/* SSP CLK */
		{0x0, 16, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x0, 17, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x0, 18, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		/* ADC */
		{0x0, 25, (IOCON_FUNC1 | IOCON_MODE_INACT | IOCON_ADMODE_EN)},
		/* DAC */
		{0x0, 26, (IOCON_FUNC2 | IOCON_DAC_EN | IOCON_HYS_EN | IOCON_MODE_PULLUP)},
		/* USB */
		{0x0, 29, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x0, 30, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x0, 31, (IOCON_FUNC1 | IOCON_MODE_INACT)},

		/* ENET */
		{0x1, 0, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x1, 1, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x1, 4, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x1, 8, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x1, 9, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x1, 10, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x1, 14, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x1, 15, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x1, 16, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x1, 17, (IOCON_FUNC1 | IOCON_MODE_INACT)},
		{0x1, 27, (IOCON_FUNC4 | IOCON_MODE_INACT | IOCON_DIGMODE_EN)}, /* CLKOUT */

		/* JOYSTICK */
		{2, 26, (IOCON_FUNC0 | IOCON_MODE_INACT)}, 	/* JOYSTICK_UP */
		{2, 23, (IOCON_FUNC0 | IOCON_MODE_INACT)}, 	/* JOYSTICK_DOWN */
		{2, 25, (IOCON_FUNC0 | IOCON_MODE_INACT)}, 	/* JOYSTICK_LEFT */
		{2, 27, (IOCON_FUNC0 | IOCON_MODE_INACT)}, 	/* JOYSTICK_RIGHT */
		{2, 22, (IOCON_FUNC0 | IOCON_MODE_INACT)}, 	/* JOYSTICK_PRESS */

		/*   For the EA LPC1788 VBUS is not connected.  Leave it as GPIO. */
		/* {0x1, 30, (IOCON_FUNC2 | IOCON_MODE_INACT)}, */ /* USB_VBUS */

		/* FIXME NOT COMPLETE */

		/* LEDs */
		{0x2, 26, (IOCON_FUNC0 | IOCON_MODE_INACT)},
		{0x2, 27, (IOCON_FUNC0 | IOCON_MODE_INACT)},
};

/* EMC clock delay */
#define CLK0_DELAY 7

/* Keil SDRAM timing and chip Config */
STATIC const IP_EMC_DYN_CONFIG_T IS42S32800D_config = {
		EMC_NANOSECOND(64000000 / 4096),
		0x01,				/* Command Delayed */
		3,					/* tRP */
		7,					/* tRAS */
		EMC_NANOSECOND(70),	/* tSREX */
		EMC_CLOCK(0x01),	/* tAPR */
		EMC_CLOCK(0x05),	/* tDAL */
		EMC_NANOSECOND(12),	/* tWR */
		EMC_NANOSECOND(60),	/* tRC */
		EMC_NANOSECOND(60),	/* tRFC */
		EMC_NANOSECOND(70),	/* tXSR */
		EMC_NANOSECOND(12),	/* tRRD */
		EMC_CLOCK(0x02),	/* tMRD */
		{
				{
						EMC_ADDRESS_DYCS0,	/* EA Board uses DYCS0 for SDRAM */
						2,	/* RAS */

						EMC_DYN_MODE_WBMODE_PROGRAMMED |
						EMC_DYN_MODE_OPMODE_STANDARD |
						EMC_DYN_MODE_CAS_2 |
						EMC_DYN_MODE_BURST_TYPE_SEQUENTIAL |
						EMC_DYN_MODE_BURST_LEN_4,

						EMC_DYN_CONFIG_DATA_BUS_32 |
						EMC_DYN_CONFIG_LPSDRAM |
						EMC_DYN_CONFIG_8Mx16_4BANKS_12ROWS_9COLS |
						EMC_DYN_CONFIG_MD_SDRAM
				},
				{0, 0, 0, 0},
				{0, 0, 0, 0},
				{0, 0, 0, 0}
		}
};

/* NorFlash timing and chip Config */
STATIC const IP_EMC_STATIC_CONFIG_T SST39VF320_config = {
		0,
		EMC_STATIC_CONFIG_MEM_WIDTH_16 |
		EMC_STATIC_CONFIG_CS_POL_ACTIVE_LOW |
		EMC_STATIC_CONFIG_BLS_HIGH /* |
							      EMC_CONFIG_BUFFER_ENABLE*/,

								  EMC_NANOSECOND(0),
								  EMC_NANOSECOND(35),
								  EMC_NANOSECOND(70),
								  EMC_NANOSECOND(70),
								  EMC_NANOSECOND(40),
								  EMC_CLOCK(4)
};

/* NandFlash timing and chip Config */
STATIC const IP_EMC_STATIC_CONFIG_T K9F1G_config = {
		1,
		EMC_STATIC_CONFIG_MEM_WIDTH_8 |
		EMC_STATIC_CONFIG_CS_POL_ACTIVE_LOW |
		EMC_STATIC_CONFIG_BLS_HIGH /* |
							      EMC_CONFIG_BUFFER_ENABLE*/,

								  EMC_NANOSECOND(0),
								  EMC_NANOSECOND(35),
								  EMC_NANOSECOND(70),
								  EMC_NANOSECOND(70),
								  EMC_NANOSECOND(40),
								  EMC_CLOCK(4)
};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void _DelayMs(U32 ms) {
	uint32_t cycle = (SystemCoreClock/10000)*ms;
	while (cycle) cycle--;
}
static int _TestSDRAM(void) {
	volatile U32 * pWriteLong;
	volatile U16 * pWriteShort;
	U32            Data;
	U32            i;
	U32            j;

	pWriteLong  = (U32 *)EMC_ADDRESS_DYCS0;
	pWriteShort = (U16 *)EMC_ADDRESS_DYCS0;
	//
	// Fill 16 bit wise
	//
	for (i = 0; i < (SDRAM_SIZE / 0x40000); i++) {
		for (j = 0; j < 0x100; j++) {
			*pWriteShort++ = (i + j);
			*pWriteShort++ = (i + j) + 1;
		}
	}
	//
	// Verifying
	//
	pWriteLong = (U32*)EMC_ADDRESS_DYCS0;
	for (i = 0; i < (SDRAM_SIZE / 0x40000); i++) {
		for (j = 0; j < 0x100; j++) {
			Data = *pWriteLong++;
			if (Data != (((((i + j) + 1) & 0xFFFF) << 16) | ((i + j) & 0xFFFF))) {
				return 1;  // Error
			}
		}
	}
	return 0;  // O.K.
}
static void _FindDelay(int DelayType) {
	U32 Delay;
	U32 Min;
	U32 Max;
	U32 v;

	//
	// Init start values
	//
	Delay = 0x00;
	Min   = 0xFF;
	Max   = 0xFF;
	//
	// Test for DLY min./max. values
	//
	while (Delay < 32) {
		//
		// Setup new DLY value to test
		//
		if (DelayType == 0) {
			v                 = LPC_SYSCTL->EMCDLYCTL & ~0x001Ful;
			LPC_SYSCTL->EMCDLYCTL = v | Delay;
		} else {
			v                 = LPC_SYSCTL->EMCDLYCTL & ~0x1F00ul;
			LPC_SYSCTL->EMCDLYCTL = v | (Delay << 8);
		}
		//
		// Test configured DLY value and find out min./max. values that will work
		//
		if (_TestSDRAM() == 0) {
			//
			// Test passed, remember min. DLY value if not done yet
			//
			if (Min == 0xFF) {
				Min = Delay;
			}
		} else {
			//
			// Test failed, if a min. value has been found before, remember the current value for max.
			//
			if (Min != 0xFF) {
				Max = Delay;
			}
		}
		Delay++;
	}
	//
	// Calc DLY value
	//
	if        (Max != 0xFF) {  // If we found a min. and max. value we use the average of the min. and max. values to get an optimal DQSIN delay
		Delay = (Min + Max) / 2;
	} else if (Min != 0xFF) {  // If we found only a min. value we use the average of the min. value and the longest DLY value to get an optimal DQSIN delay
		Delay = (Min + 0x1F) / 2;
	} else {                   // No working max. and/or min. values found
		while (1);  // Fatal error
	}
	//
	// Setup DLY value to work with
	//
	if (DelayType == 0) {
		v                 = LPC_SYSCTL->EMCDLYCTL & ~0x001Ful;
		LPC_SYSCTL->EMCDLYCTL = v | Delay;
	} else {
		v                 = LPC_SYSCTL->EMCDLYCTL & ~0x1F00ul;
		LPC_SYSCTL->EMCDLYCTL = v | (Delay << 8);
	}
}
static U32 _CalibrateOsc(void) {
	U32 Cnt;
	U32 v;
	U32 i;

	//
	// Init start values
	//
	Cnt = 0;
	//
	// Calibrate osc.
	//
	for (i = 0; i < 10; i++) {
		LPC_SYSCTL->EMCCAL = (1 << 14);     // Start calibration
		v = LPC_SYSCTL->EMCCAL;
		while ((v & (1 << 15)) == 0) {  // Wait for calibration done
			v = LPC_SYSCTL->EMCCAL;
		}
		Cnt += (v & 0xFF);
	}
	return (Cnt / 10);
}
static void _AdjustEMCTiming(U32 Delay) {
	U32 FBClkDly;
	U32 FBDelay;
	U32 CmdDly;
	U32 v;

	FBDelay           = _CalibrateOsc();
	v                 = LPC_SYSCTL->EMCDLYCTL;
	CmdDly            = ((v &  0x001Ful) * Delay / FBDelay) & 0x1F;
	FBClkDly          = ((v &  0x1F00ul) * Delay / FBDelay) & 0x1F00;
	LPC_SYSCTL->EMCDLYCTL =  (v & ~0x1F1Ful) | FBClkDly | CmdDly;
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

///* Setup system clocking */
//void Board_SetupClocking(void){
//	/* Enable PBOOST for CPU clock over 100MHz */
//	Chip_SYSCTL_EnableBoost();
//
//	Chip_SetupXtalClocking();
//
//	/* SPIFI clocking will be derived from Main PLL with a divider of 2 (60MHz) */
//	Chip_Clock_SetSPIFIClockDiv(2);
//	Chip_Clock_SetSPIFIClockSource(SYSCTL_SPIFICLKSRC_MAINPLL);
//
//}
//
///* Sets up system pin muxing */
//void Board_SetupMuxing(void){
//	int i, j;
//
//	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_EMC);
//	Chip_SYSCTL_PeriphReset(SYSCTL_RESET_IOCON);
//
//	/* Setup data, address, and EMC control pins with high slew rate */
//	for (i = 3; i <= 4; i++) {
//		for (j = 0; j <= 31; j++) {
//			Chip_IOCON_PinMuxSet(LPC_IOCON, (uint8_t) i, (uint8_t) j, (IOCON_FUNC1 | IOCON_FASTSLEW_EN));
//		}
//	}
//	for (i = 14; i <= 31; i++) {
//		Chip_IOCON_PinMuxSet(LPC_IOCON, 2, (uint8_t) i, (IOCON_FUNC1 | IOCON_FASTSLEW_EN));
//	}
//
//	/* Setup system level pin muxing */
//	//	Chip_IOCON_SetPinMuxing(LPC_IOCON, pinmuxing, sizeof(pinmuxing) / sizeof(PINMUX_GRP_T));
//}
//
///* Setup external memories */
//void Board_SetupExtMemory(void){
//	/* Setup EMC Delays */
//	/* Move all clock delays together */
//	LPC_SYSCTL->EMCDLYCTL = (CLK0_DELAY) | (CLK0_DELAY << 8) | (CLK0_DELAY << 16 | (CLK0_DELAY << 24));
//
//	/* Setup EMC Clock Divider for divide by 2 */
//	/* Setup EMC clock for a divider of 2 from CPU clock. Enable EMC clock for
//	   external memory setup of DRAM. */
//	Chip_Clock_SetEMCClockDiv(SYSCTL_EMC_DIV2);
//	Chip_SYSCTL_PeriphReset(SYSCTL_RESET_EMC);
//
//	/* Init EMC Controller -Enable-LE mode- clock ratio 1:1 */
//	Chip_EMC_Init(1, 0, 0);
//
//	/* Init EMC Dynamic Controller */
//	Chip_EMC_Dynamic_Init((IP_EMC_DYN_CONFIG_T *) &IS42S32800D_config);
//
//	//	/* Init EMC Static Controller CS0 */
//	//	Chip_EMC_Static_Init((IP_EMC_STATIC_CONFIG_T *) &SST39VF320_config);
//	//
//	//
//	//	/* Init EMC Static Controller CS1 */
//	//	Chip_EMC_Static_Init((IP_EMC_STATIC_CONFIG_T *) &K9F1G_config);
//
//	/* EMC Shift Control */
//	LPC_SYSCTL->SCS |= 1;
//}
//
///* Set up and initialize hardware prior to call to main */
//void Board_SystemInit(void){
//	volatile U32 CmdDly;
//	volatile U32 Dummy;
//	volatile U32 i;
//
//	Chip_SystemInit();
//	SystemCoreClockUpdate();
//
//	Board_SetupMuxing();
//	Board_SetupClocking();
////	Board_SetupExtMemory();
//
//
//	LPC_SYSCTL->PCONP     |= (1 << 11);   // Turn on EMC peripheral clock
//	LPC_SYSCTL->EMCDLYCTL  = 0x00001010;
//	LPC_EMC->CONTROL   = 1;           // EMC enable
//	LPC_EMC->CONFIG    = 0;
//
//	//	Board_SetupMuxing();
//	//	Board_SetupClocking();
//	//	Board_SetupExtMemory();
//
//	//
//	// Port init
//	//
//	LPC_IOCON->p[3][0]  = 1;  // D0
//	LPC_IOCON->p[3][1]  = 1;  // D1
//	LPC_IOCON->p[3][2]  = 1;  // D2
//	LPC_IOCON->p[3][3]  = 1;  // D3
//	LPC_IOCON->p[3][4]  = 1;  // D4
//	LPC_IOCON->p[3][5]  = 1;  // D5
//	LPC_IOCON->p[3][6]  = 1;  // D6
//	LPC_IOCON->p[3][7]  = 1;  // D7
//	LPC_IOCON->p[3][8]  = 1;  // D8
//	LPC_IOCON->p[3][9]  = 1;  // D9
//	LPC_IOCON->p[3][10] = 1;  // D10
//	LPC_IOCON->p[3][11] = 1;  // D11
//	LPC_IOCON->p[3][12] = 1;  // D12
//	LPC_IOCON->p[3][13] = 1;  // D13
//	LPC_IOCON->p[3][14] = 1;  // D14
//	LPC_IOCON->p[3][15] = 1;  // D15
//	LPC_IOCON->p[3][16] = 1;  // D16
//	LPC_IOCON->p[3][17] = 1;  // D17
//	LPC_IOCON->p[3][18] = 1;  // D18
//	LPC_IOCON->p[3][19] = 1;  // D19
//	LPC_IOCON->p[3][20] = 1;  // D20
//	LPC_IOCON->p[3][21] = 1;  // D21
//	LPC_IOCON->p[3][22] = 1;  // D22
//	LPC_IOCON->p[3][23] = 1;  // D23
//	LPC_IOCON->p[3][24] = 1;  // D24
//	LPC_IOCON->p[3][25] = 1;  // D25
//	LPC_IOCON->p[3][26] = 1;  // D26
//	LPC_IOCON->p[3][27] = 1;  // D27
//	LPC_IOCON->p[3][28] = 1;  // D28
//	LPC_IOCON->p[3][29] = 1;  // D29
//	LPC_IOCON->p[3][30] = 1;  // D30
//	LPC_IOCON->p[3][31] = 1;  // D31
//
//	LPC_IOCON->p[4][0]  = 1;  // A0
//	LPC_IOCON->p[4][1]  = 1;  // A1
//	LPC_IOCON->p[4][2]  = 1;  // A2
//	LPC_IOCON->p[4][3]  = 1;  // A3
//	LPC_IOCON->p[4][4]  = 1;  // A4
//	LPC_IOCON->p[4][5]  = 1;  // A5
//	LPC_IOCON->p[4][6]  = 1;  // A6
//	LPC_IOCON->p[4][7]  = 1;  // A7
//	LPC_IOCON->p[4][8]  = 1;  // A8
//	LPC_IOCON->p[4][9]  = 1;  // A9
//	LPC_IOCON->p[4][10] = 1;  // A10
//	LPC_IOCON->p[4][11] = 1;  // A11
//	LPC_IOCON->p[4][12] = 1;  // A12
//	LPC_IOCON->p[4][13] = 1;  // A13
//	LPC_IOCON->p[4][14] = 1;  // A14
//	LPC_IOCON->p[4][15] = 1;  // A15
//	LPC_IOCON->p[4][16] = 1;  // A16
//	LPC_IOCON->p[4][17] = 1;  // A17
//	LPC_IOCON->p[4][18] = 1;  // A18
//	LPC_IOCON->p[4][19] = 1;  // A19
//	LPC_IOCON->p[4][20] = 1;  // A20
//	LPC_IOCON->p[4][21] = 1;  // A21
//	LPC_IOCON->p[4][22] = 1;  // A22
//	LPC_IOCON->p[4][23] = 1;  // A23
//
//	LPC_IOCON->p[4][24] = 1;  // OE
//	LPC_IOCON->p[4][25] = 1;  // WE
//	LPC_IOCON->p[4][26] = 1;  // BLS0
//	LPC_IOCON->p[4][27] = 1;  // BLS1
//	LPC_IOCON->p[4][28] = 1;  // BLS2
//	LPC_IOCON->p[4][29] = 1;  // BLS3
//	LPC_IOCON->p[4][30] = 1;  // CS0
//	LPC_IOCON->p[4][31] = 1;  // CS1
//	LPC_IOCON->p[2][14] = 1;  // CS2
//	LPC_IOCON->p[2][15] = 1;  // CS3
//	LPC_IOCON->p[2][16] = 1;  // CAS
//	LPC_IOCON->p[2][17] = 1;  // RAS
//	LPC_IOCON->p[2][18] = 1;  // CLKOUT0
//	LPC_IOCON->p[2][19] = 1;  // CLKOUT1
//	LPC_IOCON->p[2][20] = 1;  // DYCS0
//	LPC_IOCON->p[2][21] = 1;  // DYCS1
//	LPC_IOCON->p[2][22] = 1;  // DYCS2
//	LPC_IOCON->p[2][23] = 1;  // DYCS3
//	LPC_IOCON->p[2][24] = 1;  // CKEOUT0
//	LPC_IOCON->p[2][25] = 1;  // CKEOUT1
//	LPC_IOCON->p[2][26] = 1;  // CKEOUT2
//	LPC_IOCON->p[2][27] = 1;  // CKEOUT3
//	LPC_IOCON->p[2][28] = 1;  // DQMOUT0
//	LPC_IOCON->p[2][29] = 1;  // DQMOUT1
//	LPC_IOCON->p[2][30] = 1;  // DQMOUT2
//	LPC_IOCON->p[2][31] = 1;  // DQMOUT3
//
//
//	//
//	// Setup EMC config for SDRAM, timings for 60MHz bus
//	//
//	LPC_EMC->DYNAMICCONFIG0    = 0x00004480;  // 256MB, 8Mx32, 4 banks, 12 rows, 9 columns, buffers disabled
//	LPC_EMC->DYNAMICRASCAS0    = 0x00000202;  // 2 RAS, 2 CAS latency */
//	LPC_EMC->DYNAMICREADCONFIG = 0x00000001;  // Command delayed strategy, using EMCCLKDELAY
//	LPC_EMC->DYNAMICRP         = 0x00000001;  // n + 1 clock cycles
//	LPC_EMC->DYNAMICRAS        = 0x00000003;  // n + 1 clock cycles
//	LPC_EMC->DYNAMICSREX       = 0x00000005;  // n + 1 clock cycles
//	LPC_EMC->DYNAMICAPR        = 0x00000002;  // n + 1 clock cycles
//	LPC_EMC->DYNAMICDAL        = 0x00000003;  // n     clock cycles
//	LPC_EMC->DYNAMICWR         = 0x00000001;  // n + 1 clock cycles
//	LPC_EMC->DYNAMICRC         = 0x00000004;  // n + 1 clock cycles
//	LPC_EMC->DYNAMICRFC        = 0x00000004;  // n + 1 clock cycles
//	LPC_EMC->DYNAMICXSR        = 0x00000005;  // n + 1 clock cycles
//	LPC_EMC->DYNAMICRRD        = 0x00000001;  // n + 1 clock cycles
//	LPC_EMC->DYNAMICMRD        = 0x00000001;  // n + 1 clock cycles
//	_DelayMs(100);
//	LPC_EMC->DYNAMICCONTROL    = 0x00000183;  // Issue NOP command
//	_DelayMs(200);
//	LPC_EMC->DYNAMICCONTROL    = 0x00000103;  // Issue PALL command
//	LPC_EMC->DYNAMICREFRESH    = 0x00000002;  // n * 16 clock cycles
//	for (i = 0; i < 0x80; i++);               // Wait 128 AHB clock cycles
//	LPC_EMC->DYNAMICREFRESH    = 0x0000003A;  // n * 16 clock cycles
//	//
//	// Init SDRAM
//	//
//	LPC_EMC->DYNAMICCONTROL = 0x00000083;                             // Issue MODE command
//	Dummy = *((volatile U32*)(EMC_ADDRESS_DYCS0 | (0x22 << (2+2+9))));  // 4 burst, 2 CAS latency
//	LPC_EMC->DYNAMICCONTROL = 0x00000000;                             // Issue NORMAL command
//	LPC_EMC->DYNAMICCONFIG0 = 0x00084480;                             // 256MB, 8Mx32, 4 banks, 12 rows, 9 columns, buffers (re-)enabled
//	//
//	// Auto calibrate timings
//	//
//	CmdDly = _CalibrateOsc();
//	//
//	// Find best delay values
//	//
//	_FindDelay(0);  // EMCDLY
//	_FindDelay(1);  // FBCLKDLY
//	_AdjustEMCTiming(CmdDly);
//
////	Chip_IOCON_PinMuxSet(LPC_IOCON, LED_MOD[0].PORT, LED_MOD[0].PIN, LED_MOD[0].FUNC);
////	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_MOD[0].PORT, LED_MOD[0].PIN);
////	Chip_GPIO_SetPinState(LPC_GPIO, LED_MOD[0].PORT, LED_MOD[0].PIN, 0);
////
////	while(1){
////		Chip_GPIO_SetPinState(LPC_GPIO, LED_MOD[0].PORT, LED_MOD[0].PIN, 0);
////		_DelayMs(500);
////		Chip_GPIO_SetPinState(LPC_GPIO, LED_MOD[0].PORT, LED_MOD[0].PIN, 1);
////		_DelayMs(500);
////	}
//}
//



/*********************************************************************
 *
 *       __low_level_init()
 *
 *       Initialize memory controller, clock generation and pll
 *
 *       Has to be modified, if another CPU clock frequency should be
 *       used. This function is called during startup and
 *       has to return 1 to perform segment initialization
 */
#ifdef __cplusplus
extern "C" {
#endif
	// Avoid "no ptototype" warning
	int __low_level_init(void);
#ifdef __cplusplus
}
#endif
int __low_level_init(void) {
	//  SystemCoreClockUpdate();   // Ensure, the SystemCoreClock is set
	//  _EMC_Init();               // Init SDRAM, NAND- and NOR-flash
	Board_SystemInit();
	return 1;
}

/*****  End of file  ************************************************/
