
//-------- <<< Use Configuration Wizard in Context Menu >>> -----------------
// <h>Call-Back
// ==============
// <i> Define quais call-back's ser�o utilizadas


//   <q>SET
//   <i>Habilita a call-back SET
//   <i>Default: 0
#define CALLBK_FRAMESET 				0


//   <q>GET
//   <i>Habilita a call-back GET
//   <i>Default: 0
#define CALLBK_FRAMEGET 				1


//   <q>REPLY
//   <i>Habilita a call-back REPLY
//   <i>Default: 0
#define CALLBK_FRAMEREPLY 				1


//   <q>GETERROR
//   <i>Habilita a call-back GETERROR
//   <i>Default: 0
#define CALLBK_FRAMEGETERROR 				0


//   <q>REPLYERROR
//   <i>Habilita a call-back REPLYERROR
//   <i>Default: 0
#define CALLBK_FRAMEREPLYERROR 				0


//   <q>ATTRIBERROR
//   <i>Habilita a call-back ATTRIBERROR
//   <i>Default: 0
#define CALLBK_FRAMEATTRIBERROR 				0


//   <q>TRIGGER
//   <i>Habilita a call-back TRIGGER
//   <i>Default: 0
#define CALLBK_FRAMETRIGGER 				1


//   <q>SETDEVICE
//   <i>Habilita a call-back SETDEVICE
//   <i>Default: 0
#define CALLBK_FRAMESETDEVICE 				1


//   <q>LIVE
//   <i>Habilita a call-back LIVE
//   <i>Default: 0
#define CALLBK_LIVE 				1


//   <q>OFFLINE
//   <i>Habilita a call-back OFFLINE
//   <i>Default: 0
#define CALLBK_OFFLINE 				0

//   <q>ONLINE
//   <i>Habilita a call-back ONLINE
//   <i>Default: 0
#define CALLBK_ONLINE 				0


//   <q>SNIFFER
//   <i>Habilita a call-back GET
//   <i>Default: 0
#define SNIFFER 				0

// </h>

// <h>Driver CAN
// ==============
// 		<o>Driver <0=>LPC11xx <1=>LPC_17xx 	
// 		<i> Define qual driver ser� utilizado para a CAN
//  	<i>Define qual driver de CAN ser� utilizado para interfacear a EmbNet
#define DRIVER_CAN 1
#if (DRIVER_CAN==0)
	#define _CAN_LPC11_
#elif	(DRIVER_CAN==1)
	#define _CAN_LPC1788_
#endif
// </h>

//------------- <<< end of configuration section >>> -----------------------

